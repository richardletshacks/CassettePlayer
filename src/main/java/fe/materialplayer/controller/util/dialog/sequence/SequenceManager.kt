package fe.materialplayer.controller.util.dialog.sequence

import com.jfoenix.controls.JFXDialog
import fe.materialplayer.controller.util.dialog.DialogUtil

class SequenceManager(private val direction: Direction) {
    private var pointer: Int = 0;
    private var dialogs: MutableList<SequencePart> = mutableListOf()
    private var currentDialog: SequencePart? = null
    private var lastPointer: Pointer? = null

    fun start(vararg dialogs: SequencePart) {
        this.dialogs.addAll(dialogs)
        this.dialogs.forEach {
            it.controller.sequenceManager = this
        }

        this.show()
    }

    fun show(pointer: Pointer = Pointer.CURRENT) {
        this.currentDialog?.controller?.closeDialog(this.lastPointer, pointer, this.direction)

        this.pointer += pointer.add
        this.currentDialog = this.dialogs[this.pointer].also {
            DialogUtil.createUnclosableDialog(it.controller, it.fxml, pointer.transition(this.direction))
        }

        this.lastPointer = pointer
    }

    fun previous() {
        this.show(Pointer.PREVIOUS)
    }

    fun next() {
        this.show(Pointer.NEXT)
    }

    enum class Pointer(val add: Int) {
        PREVIOUS(-1) {
            override fun transition(direction: Direction): JFXDialog.DialogTransition = direction.previous
        },
        CURRENT(0) {
            override fun transition(direction: Direction): JFXDialog.DialogTransition = direction.current
        },
        NEXT(1) {
            override fun transition(direction: Direction): JFXDialog.DialogTransition = direction.next
        };

        abstract fun transition(direction: Direction): JFXDialog.DialogTransition
    }

    enum class Direction(val next: JFXDialog.DialogTransition,
                         val current: JFXDialog.DialogTransition,
                         val previous: JFXDialog.DialogTransition) {
        HORIZONTAL(
                JFXDialog.DialogTransition.RIGHT,
                JFXDialog.DialogTransition.CENTER,
                JFXDialog.DialogTransition.LEFT),
        VERTICAL(
                JFXDialog.DialogTransition.TOP,
                JFXDialog.DialogTransition.CENTER,
                JFXDialog.DialogTransition.BOTTOM)

    }
}