package fe.materialplayer.controller.view.playlist.validator;

import com.jfoenix.controls.JFXTextField;
import com.jfoenix.validation.RequiredFieldValidator;
import fe.materialplayer.controller.view.playlist.validator.DuplicatePlaylistValidator;

import java.util.ResourceBundle;

public class PlaylistNameValidatorHelper {

    public PlaylistNameValidatorHelper(JFXTextField tfPlaylistName, ResourceBundle resourceBundle){
        RequiredFieldValidator requiredFieldValidator = new RequiredFieldValidator(resourceBundle.getString("playlistNameRequired"));
        DuplicatePlaylistValidator duplicatePlaylistValidator = new DuplicatePlaylistValidator(resourceBundle.getString("playlistExists"));

        tfPlaylistName.getValidators().addAll(requiredFieldValidator, duplicatePlaylistValidator);
        tfPlaylistName.focusedProperty().addListener((o, oldVal, newVal) -> {
            if (!newVal) {
                tfPlaylistName.validate();
            }
        });
    }
}
