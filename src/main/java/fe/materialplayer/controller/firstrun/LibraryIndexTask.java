package fe.materialplayer.controller.firstrun;

import fe.logger.Logger;
import fe.materialplayer.controller.util.index.IndexTask;
import fe.materialplayer.data.indexable.Song;

import java.io.File;
import java.util.List;
import java.util.ResourceBundle;

public class LibraryIndexTask extends IndexTask<Void> {

    private final File dir;
    private final boolean recursive;

    protected final Logger logger = new Logger("LibraryIndexTask", true);

    public LibraryIndexTask(ResourceBundle resourceBundle, File dir, boolean recursive) {
        super(resourceBundle);

        this.dir = dir;
        this.recursive = recursive;
    }

    @Override
    protected Void call() throws Exception {
        List<File> files = this.scanSubDir(this.dir, this.recursive);
        List<Song> songs = this.parseSongs(files, false);
        this.logger.print(Logger.Type.INFO, "Parsed %d songs", songs.size());

        this.storeSongs(songs);

        return null;
    }

    public File getDir() {
        return dir;
    }

    public boolean isRecursive() {
        return recursive;
    }
}
