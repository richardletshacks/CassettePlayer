package fe.materialplayer.util.threading;

import java.util.ArrayList;
import java.util.List;

public abstract class StoppableRunnable implements Runnable {
    protected ThreadManager threadManager = ThreadManager.getInstance();
    private final List<StoppableRunnable> subThreads = new ArrayList<>();

    protected boolean running = true;

    public boolean isRunning() {
        return running;
    }

    public void updateRunningStatus(boolean running) {
        this.running = running;
        if (!this.running) {
            for (StoppableRunnable sr : this.subThreads) {
                this.threadManager.killThread(sr);
            }
        }
    }

    public Thread registerSubThread(StoppableRunnable sr) {
        Thread t = this.threadManager.createThread(sr);
        this.subThreads.add(sr);

        return t;
    }
}
