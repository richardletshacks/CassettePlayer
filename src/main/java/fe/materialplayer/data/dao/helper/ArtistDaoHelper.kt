package fe.materialplayer.data.dao.helper

import fe.materialplayer.data.dao.base.IdDaoHelper
import fe.materialplayer.data.indexable.Artist
import fe.materialplayer.data.indexable.Song
import fe.materialplayer.music.remover.LibrarySongRemover

class ArtistDaoHelper : IdDaoHelper<Artist>(Artist::class.java, "name", true) {

    override fun addToList(item: Artist) {
        super.addToList(item)

        item.parentArtist?.let { parent ->
            if (item !in parent.memberArtists) {
                parent.memberArtists.add(item)
            }
        }
    }

    override fun removeFromList(item: Artist) {
        super.removeFromList(item)

        item.parentArtist?.let { parent ->
            if (item in parent.memberArtists) {
                parent.memberArtists.remove(item)
            }
        }
    }

    fun containsArtist() = this.items.find { it == UNKNOWN_ARTIST } != null

    fun existsArtist(name: String?): Artist? {
        return this.items.find { it.name.equals(name, ignoreCase = true) }
    }

    fun getRemoveArtists(song: Song): MutableMap<Artist, LibrarySongRemover.UpdateState> {
        val removeArtists = mutableMapOf<Artist, LibrarySongRemover.UpdateState>()
        song.artists.forEach { art ->
            removeArtists[art] =
                if (art.songs.size == 1) LibrarySongRemover.UpdateState.REMOVE else LibrarySongRemover.UpdateState.UPDATE
        }

        return removeArtists
    }

    companion object {
        @JvmField
        val UNKNOWN_ARTIST = Artist("Unknown Artist", null)
    }
}