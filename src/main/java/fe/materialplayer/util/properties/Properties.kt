package fe.materialplayer.util.properties

import com.google.gson.*
import fe.materialplayer.util.SingletonHolder
import javafx.collections.ObservableMap
import java.io.*

class Properties private constructor(dir: File)  {

    private var propertiesFile: File = File(dir, "properties.json")
    private var rootProp: JsonObject
    private val gson: Gson = GsonBuilder().setPrettyPrinting().create()

    init {
        this.rootProp = if (this.propertiesFile.exists()) {
            JsonParser.parseReader(BufferedReader(FileReader(this.propertiesFile))) as JsonObject
        } else {
            JsonObject()
        }
    }

    companion object : SingletonHolder<Properties, File>(::Properties)

    fun getStringProperty(name: String): String? = this.getProperty(name)?.asString
    fun getBooleanProperty(name: String, default: Boolean = false): Boolean = this.getProperty(name)?.asBoolean ?: default

    fun addProperty(name: String, value: JsonPrimitive): Properties {
        this.rootProp.add(name, value)
        return this
    }

    fun getProperty(name: String): JsonPrimitive? = this.rootProp.getAsJsonPrimitive(name)

    fun save() {
        BufferedWriter(FileWriter(this.propertiesFile)).buffered().use {
            it.write(this.gson.toJson(this.rootProp))
        }
    }
}