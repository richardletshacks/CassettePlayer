package fe.materialplayer.controller.util.cell

import fe.materialplayer.util.properties.ColorThemeManager
import fe.materialplayer.util.properties.ColorType
import javafx.beans.binding.Bindings
import javafx.geometry.Pos
import javafx.scene.control.Label
import javafx.scene.control.ListCell
import javafx.scene.layout.VBox
import javafx.scene.paint.Color

open class SimpleLineBoxListCell<T> : ListCell<T>() {
    enum class LineType(val acquireColor: () -> Color) {
        TITLE({ ColorThemeManager.theme.value.getColor(ColorType.TEXT_COLOR) }), SUB({ fe.materialplayer.util.StyleConstants.SUB_TITLE_COLOR });
    }

    fun makeLabelBox(item: T, lines: FormattablePair): VBox {
        val title = Label(lines.key).apply {
            bindLabel(this, LineType.TITLE, item)
        }

        val sub = Label(lines.value).apply {
            bindLabel(this, LineType.SUB, item)
        }

        return VBox(title, sub).apply {
            alignment = Pos.CENTER_LEFT
        }
    }

    open fun bindLabel(label: Label, lineType: LineType, item: T) {
        label.maxWidth = this.listView.width - 150
        label.textFillProperty().bind(Bindings.createObjectBinding({ lineType.acquireColor.invoke() }, ColorThemeManager.theme))
    }
}