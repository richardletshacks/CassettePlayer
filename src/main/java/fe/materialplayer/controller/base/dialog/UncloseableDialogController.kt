package fe.materialplayer.controller.base.dialog

import fe.materialplayer.controller.util.dialog.sequence.SequenceManager
import javafx.beans.property.BooleanProperty
import javafx.beans.property.SimpleBooleanProperty

abstract class UnclosableDialogController : DialogController(false) {
    var sequenceManager: SequenceManager? = null

    protected var done: BooleanProperty = SimpleBooleanProperty()
    private var movedOn: BooleanProperty = SimpleBooleanProperty()

    operator fun next() {
        movedOn.set(true)
        sequenceManager?.next()
    }

    fun previous() {
        movedOn.set(true)
        sequenceManager?.previous()
    }

    fun hasMovedOn(): Boolean {
        return movedOn.get()
    }

    fun isDone(): Boolean {
        return done.get()
    }

    fun reset() {
        done.set(false)
        movedOn.set(false)
    }

}