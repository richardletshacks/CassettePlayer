package fe.materialplayer.music.mpris;

import fe.materialplayer.data.indexable.Song;
import fe.materialplayer.util.Util;
import org.freedesktop.dbus.types.DBusMapType;
import org.freedesktop.dbus.types.Variant;

import java.io.File;
import java.util.Map;

public class MetadataExtractor {
    public static final int SECOND_TO_MICROSECOND = 1000000;

    public static Variant<?> toMetadata(Song song) {
        Map<String, Variant<?>> metadata = new java.util.HashMap<>(Map.of(
                "mpris:trackid", new Variant<>(song.getId()),
                "mpris:length", new Variant<>((long) (song.getLength() * SECOND_TO_MICROSECOND)),
                "xesam:artist", new Variant<>(song.getArtistName()),
                "xesam:album", new Variant<>(song.getAlbumName()),
                "xesam:title", new Variant<>(song.getTitle())
        ));

        File cover = song.getCoverFile(Song.CoverType.ORIGINAL);
        if (cover != null) {
            metadata.put("mpris:artUrl", new Variant<>(Util.fileToUriString(cover)));
        }

        return new Variant<>(metadata, new DBusMapType(String.class, Variant.class));
    }
}
