package fe.materialplayer.music.remover

import fe.logger.Logger
import fe.materialplayer.data.indexable.*
import fe.materialplayer.music.MusicLibrary
import fe.materialplayer.music.MusicPlayer
import java.util.*

class LibrarySongRemover {

    val nmChecker = NMChecker(this)
    val artistChecker = ArtistChecker(this)
    val albumChecker = AlbumChecker(this)
    val genreChecker = GenreChecker(this)

    val checkers = listOf(
            artistChecker, albumChecker, genreChecker, nmChecker
    )

    fun removeNonExistentLibrarySongs() {
        this.removeSongs(LIBRARY.songDao.items.filter { !it.file.exists() })
    }

    fun removeSongs(songs: List<Song>) {
        val removeSongs = LinkedList<Song>()
        songs.forEach { song ->
            LOGGER.print(Logger.Type.INFO, "Song %s does not seem to exist anymore, removing from library..", song)

            println("${song.album.id}  ${song.album.songs} ${song.genre.id} ${song.genre.songs}")
            removeSongs.add(song)

            this.checkSongAll(song)
            this.removeFromPlaylists(song)
            MusicPlayer.getInstance().removeAllOccurrences(songs)
        }

        this.executeAll()
        this.batchDelete(removeSongs)
    }

    private fun checkSongAll(song: Song) {
        this.checkers.forEach {
            it.check(song)
        }
    }

    fun executeAll() {
        this.checkers.forEach {
            it.execute()
        }
    }

    fun batchDelete(removeSongs: List<Song>) {
        LIBRARY.songDao.batchDelete(removeSongs, true)
    }


    private fun removeFromPlaylists(song: Song) {
        LIBRARY.playlistDao.items.forEach {
            it.removeAllSong(song)
        }
    }

    companion object {
        val LOGGER = Logger("LibrarySongRemover")
        val LIBRARY = MusicLibrary.getInstance()
    }

    enum class UpdateState {
        REMOVE, UPDATE
    }
}