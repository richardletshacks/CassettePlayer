package fe.materialplayer.util.artistimage;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import fe.logger.Logger;
import fe.materialplayer.data.indexable.Artist;
import fe.materialplayer.util.Util;
import fe.materialplayer.util.threading.StoppableRunnable;
import fe.materialplayer.util.threading.ThreadManager;
import javafx.scene.image.Image;
import kotlin.jvm.functions.Function1;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Deque;
import java.util.Optional;
import java.util.concurrent.LinkedBlockingDeque;

public class ArtistImageLoader extends StoppableRunnable {


    private final Logger logger = new Logger("ArtistImageLoaderManager");
    private final Deque<Artist> scheduledArtists = new LinkedBlockingDeque<>();

    public void nextArtist(Artist artist) {
        this.scheduledArtists.remove(artist);
        this.logger.print(Logger.Type.INFO, "Next artist will be %s", artist.getName());
        this.scheduledArtists.addFirst(artist);
    }

    public void start() {
        if (!this.running) {
            this.running = true;
            ThreadManager.getInstance().createThread(this).start();
        }
    }

    private ArtistImageLoader() {
        this.running = false;
    }

    private static ArtistImageLoader instance;

    public static ArtistImageLoader getInstance() {
        if (instance == null) {
            instance = new ArtistImageLoader();
        }

        return instance;
    }

    @Override
    public void run() {
        while (this.running) {
            if (this.scheduledArtists.peek() != null) {
                Artist artist = this.scheduledArtists.poll();

                try {
                    Optional<String> url = this.findImage(artist);
                    if (url.isPresent()) {
                        artist.setArtistImage(artist.downloadImage(url.get()));
                    } else {
                        artist.setArtistImage(Artist.DEFAULT_ARTIST_IMAGE);
                    }

                    artist.setLoadingImage(false);
                } catch (IOException | RateLimitedException e) {
                    e.printStackTrace();
                }
            }

            try {
                Thread.sleep(250);
            } catch (InterruptedException ignored) {
            }
        }
    }

    private static final String DEEZER_API_URL = "http://api.deezer.com/search/artist?q=%s";
    private static final String DEEZER_UNKNOWN_ARTIST_IMAGE = "images/artist//250x250-000000-80-0-0.jpg";

    public Optional<String> findImage(Artist artist) throws RateLimitedException, IOException {
        String query = URLEncoder.encode(artist.getName(), StandardCharsets.UTF_8);
        HttpURLConnection con = (HttpURLConnection) new URL(String.format(DEEZER_API_URL, query)).openConnection();
        if (con.getResponseCode() == 200) {
            JsonObject obj = (JsonObject) Util.useInputStream(con.getInputStream(), (Function1<InputStream, Object>) i -> JsonParser.parseReader(new InputStreamReader(i)));
            if (obj.get("data") != null) {
                for (JsonElement el : obj.getAsJsonArray("data")) {
                    JsonObject artistObj = (JsonObject) el;
                    String img = artistObj.getAsJsonPrimitive("picture_medium").getAsString();
                    if (img.endsWith(DEEZER_UNKNOWN_ARTIST_IMAGE)) {
                        this.logger.print(Logger.Type.INFO, "Artist %s does not seem to have a proper image", artist.getName());
                        return Optional.empty();
                    }

                    this.logger.print(Logger.Type.INFO, "Found %s for artist %s", img, artist.getName());
                    return Optional.of(img);
                }
            } else {
                throw new RateLimitedException();
            }
        }

        return Optional.empty();
    }
}
