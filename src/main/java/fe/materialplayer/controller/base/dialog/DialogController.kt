package fe.materialplayer.controller.base.dialog

import com.jfoenix.controls.JFXDialog
import fe.materialplayer.controller.base.BaseController
import fe.materialplayer.controller.util.dialog.sequence.SequenceManager

abstract class DialogController(val scaled: Boolean) : BaseController() {
    var dialog: JFXDialog? = null

    open fun closeDialog() {
        this.dialog?.close()
    }

    fun closeDialog(oldPointer: SequenceManager.Pointer?, newPointer: SequenceManager.Pointer, direction: SequenceManager.Direction) {
        this.dialog?.animateClose(
                (oldPointer == SequenceManager.Pointer.NEXT || oldPointer == SequenceManager.Pointer.CURRENT) && newPointer == SequenceManager.Pointer.NEXT
                        || (oldPointer == SequenceManager.Pointer.PREVIOUS && newPointer == SequenceManager.Pointer.PREVIOUS))
    }
}