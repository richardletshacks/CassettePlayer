package fe.materialplayer.controller.firstrun

import fe.materialplayer.controller.base.dialog.UnclosableDialogController
import fe.materialplayer.controller.css.annotation.property.IntCSSProperties
import fe.materialplayer.controller.css.annotation.property.IntCSSProperty
import fe.materialplayer.controller.css.annotation.theme.ThemeBackgroundColor
import fe.materialplayer.controller.css.annotation.theme.ThemeTextColor
import fe.materialplayer.controller.sidebar.language.LanguageBaseController
import fe.materialplayer.util.StyleConstants
import javafx.event.EventHandler
import javafx.fxml.FXML
import javafx.scene.layout.GridPane
import javafx.scene.layout.HBox
import javafx.scene.layout.VBox
import java.net.URL
import java.util.*

class FirstRunSelectLang : UnclosableDialogController(), LanguageBaseController {
    @FXML
    @ThemeBackgroundColor(cssProperty = StyleConstants.CSS_BACKGROUND_COLOR_PROPERTY)
    @IntCSSProperties(
            IntCSSProperty(property = StyleConstants.CSS_BACKGROUND_RADIUS, 7),
            IntCSSProperty(property = StyleConstants.CSS_BORDER_RADIUS, 7))
    private lateinit var gpRoot: GridPane

    @FXML
    @ThemeTextColor
    private lateinit var vbLabels: VBox

    @FXML
    private lateinit var hbLanguages: HBox

    override fun initialize(location: URL, resources: ResourceBundle) {
        super.initialize(location, resources)

        this.addLanguages(this.hbLanguages).forEach {
            it.onMouseClicked = EventHandler { this.next() }
        }
    }
}