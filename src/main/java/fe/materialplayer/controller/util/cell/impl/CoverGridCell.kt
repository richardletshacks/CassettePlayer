package fe.materialplayer.controller.util.cell.impl

import fe.materialplayer.data.indexable.Song
import fe.materialplayer.data.indexable.Song.CoverType
import fe.materialplayer.data.indexable.base.BackgroundColorable
import fe.materialplayer.util.image.ImageUtil
import javafx.application.Platform
import javafx.beans.binding.Bindings
import javafx.geometry.HPos
import javafx.scene.Node
import javafx.scene.control.Label
import javafx.scene.image.ImageView
import javafx.scene.layout.ColumnConstraints
import javafx.scene.layout.GridPane
import javafx.scene.layout.Priority
import javafx.scene.layout.VBox

open class CoverGridCell(protected val formatType: Song.FormatStyle) : ColorableCell<Song>() {

    fun makeLabel(item: Song): Label {
        return Label(item.format(this.formatType)).apply {
            bindLabel(this, LineType.TITLE, item)
        }
    }


    fun makeAlbumCover(item: Song, ivSize: Int, coverType: CoverType): ImageView {
        val iv = ImageUtil.makeIv(ivSize)
        Platform.runLater { iv.image = item.loadSongCover(coverType) }

        return iv
    }

    fun makeBasicGrid(nodeLeft: Node, leftMin: Double, nodeRight: Node): GridPane {
        val gp = GridPane()
        gp.hgap = 7.0

        val ccIv = ColumnConstraints()
        val ccLabel = ColumnConstraints()
        ccLabel.hgrow = Priority.ALWAYS

        gp.columnConstraints.addAll(ccIv, ccLabel)
        gp.add(nodeLeft, 0, 0)
        gp.add(nodeRight, 1, 0)

        return gp
    }
}