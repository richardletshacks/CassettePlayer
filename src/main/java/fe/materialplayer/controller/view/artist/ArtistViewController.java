package fe.materialplayer.controller.view.artist;

import com.jfoenix.controls.JFXListView;
import fe.materialplayer.controller.content.artist.ArtistContentController;
import fe.materialplayer.controller.css.annotation.color.LightPrimaryColored;
import fe.materialplayer.controller.css.annotation.color.PrimaryColored;
import fe.materialplayer.controller.css.annotation.theme.ThemeTextColor;
import fe.materialplayer.controller.css.annotation.theme.ThemeBackgroundColor;
import fe.materialplayer.controller.misc.CrashViewerController;
import fe.materialplayer.controller.base.IndexController;
import fe.materialplayer.controller.base.OptionsBindableController;
import fe.materialplayer.controller.util.ItemFetcher;
import fe.materialplayer.controller.util.popup.PopupState;
import fe.materialplayer.controller.util.popup.impl.ArtistablePopup;
import fe.materialplayer.controller.view.PlayOptionBoxController;
import fe.materialplayer.data.indexable.Album;
import fe.materialplayer.data.indexable.Artist;
import fe.materialplayer.music.MusicLibrary;
import fe.materialplayer.util.SnackbarHelper;
import fe.materialplayer.util.resource.FxmlUtil;
import javafx.beans.Observable;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.GridPane;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.concurrent.ExecutionException;

public class ArtistViewController extends IndexController<Artist> implements OptionsBindableController {

    @FXML
    private GridPane gpRoot;

    @FXML
    @ThemeBackgroundColor
    @PrimaryColored
    @LightPrimaryColored
    @ThemeTextColor
    private JFXListView<Artist> lvArtists;

    private final PopupState<Artist> popupState = new PopupState<>();
    private ArtistablePopup<Artist> popup;


    public ArtistViewController() {
        super(Artist.DEFAULT_SORT_TYPE, MusicLibrary.Companion.getInstance().getArtistDao());

        this.itemFetcher = new ItemFetcher<>(daoHelper.getItems(), this.sortMode.getSortComparator(), false);
    }

    @Override
    public void initialize(@NotNull URL location, @NotNull ResourceBundle resources) {
        super.initialize(location, resources);
        this.initListViewPadding(this.lvArtists);

        this.popup = new ArtistablePopup<>(this.popupState, resources);

        FxmlUtil.loadNonBlockingFxml("view/PlayOptionBox", new PlayOptionBoxController<Artist>() {
            @NotNull
            @Override
            public List<Artist> provideSongs() {
                return getSearchFilteredItemsOrAll();
            }
        }, node -> gpRoot.add((Node) node, 0, 0));

        this.itemFetcher.setListView(this.lvArtists);
        this.itemFetcher.fetchItems(FETCH_SIZE, ItemFetcher.FetchType.INITIAL);

        this.lvArtists.setCellFactory(arg0 -> new ArtistListCell(resources));
    }

    @FXML
    public void lvArtistsClicked(MouseEvent event) {
        Artist artist = this.lvArtists.getSelectionModel().getSelectedItem();
        if (artist != null) {
            if (event.getButton() == MouseButton.PRIMARY && event.getClickCount() == 2) {
                ArtistContentController.show(artist, null);
            } else if (event.getButton() == MouseButton.SECONDARY) {
                this.popupState.setCurrentState(artist);
                this.popup.buildAndShow(this.lvArtists, event, null);
            }
        }
    }

    @FXML
    public void lvArtistsScrolled(ScrollEvent event) {
        this.itemFetcher.fetchItems(FETCH_SIZE, ItemFetcher.FetchType.SCROLL);
    }

    @Override
    public void handleOptionsClick(MouseEvent event) {
        try {
            this.showPopup(Artist.SORT_TYPES, this.sortMode, (Node) event.getSource());
        } catch (IOException e) {
            e.printStackTrace();
            new CrashViewerController("SHOW_OPTIONS_POPUP",
                    String.format("Error while showing options popup for sort mode %s", this.sortMode), e).show();
        }
    }

    @Override
    public void handleSearchTextChanged(Observable observable, String oldValue, String newValue) {
        this.execSearch(newValue, this.lvArtists);
    }
}


