package fe.materialplayer.controller.css.annotation.theme

import fe.materialplayer.util.StyleConstants

@Target(AnnotationTarget.FIELD)
@Retention(AnnotationRetention.RUNTIME)
@MustBeDocumented
annotation class ThemeSecondaryTextColor(val cssProperty: String = StyleConstants.CSS_SECONDARY_TEXT_FILL_VAR)