package fe.materialplayer.controller.content.resize.button

import com.jfoenix.controls.JFXButton
import javafx.event.EventHandler
import javafx.scene.image.Image
import javafx.scene.input.MouseEvent

class CtrlResizeButton(button: JFXButton, event: EventHandler<in MouseEvent>, graphic: Image, text: String, ctrlText: String, ctrlLength: Int, maxSize: Int)
    : ResizeButton(button, event, graphic, text, maxSize) {
    init {
        button.onMouseEntered = EventHandler { evt: MouseEvent ->
            val showCtrl = evt.isControlDown
            decreaseAnimation.stop()
            increaseAnimation = makeTimeline(maxSize + (if (showCtrl) ctrlLength else 0).toDouble())
            increaseAnimation!!.onFinished = EventHandler {
                this.button.text = this.text + if (showCtrl) " $ctrlText" else ""
            }
            increaseAnimation!!.play()
        }
    }
}

