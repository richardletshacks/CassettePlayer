package fe.materialplayer.controller.css.annotation.text

import fe.materialplayer.util.StyleConstants

@Target(AnnotationTarget.FIELD)
@Retention(AnnotationRetention.RUNTIME)
@MustBeDocumented
annotation class PrimaryExtractedTextFill(val cssProperty: String = StyleConstants.CSS_TEXT_FILL_VAR)