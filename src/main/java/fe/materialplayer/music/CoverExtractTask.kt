package fe.materialplayer.music

import fe.materialplayer.data.indexable.Album
import fe.materialplayer.data.indexable.Song.CoverType
import fe.materialplayer.util.color.BackgroundColorExtractor.extract
import fe.materialplayer.util.hexString
import fe.materialplayer.util.image.ImageResizer
import fe.materialplayer.util.isTextWhite
import fe.mp3taglib.TagAlbumArt
import java.io.ByteArrayInputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import javax.imageio.ImageIO

class CoverExtractTask(val album: Album, val dir: File, private val tagAlbumArt: TagAlbumArt) {

    @Throws(IOException::class)
    fun extract(): Boolean {
        val originalMimeType = SongFactory.extractMimeType(tagAlbumArt.mimeType)
        return if (originalMimeType.isNotEmpty()) {
            val originalCover = SongFactory.createCoverFile(dir, CoverType.ORIGINAL.toString(), originalMimeType)
            FileOutputStream(originalCover).use { fos -> fos.write(tagAlbumArt.bytes) }

            val bufferedImage = ByteArrayInputStream(tagAlbumArt.bytes).use {
                ImageIO.read(it)
            }

            val color = extract(bufferedImage)

            album.apply {
                this.setBackgroundColor(color.hexString)
                this.isWhiteText = color.isTextWhite()
                this.coverDir = dir.absolutePath
                this.coverMimeType = originalMimeType
            }

            ImageResizer.resize(bufferedImage, RESIZE_SIZES, dir, originalMimeType)
            true
        } else false
    }

    companion object {
        private val RESIZE_SIZES = mapOf(
            60 to CoverType.S60X60,
            160 to CoverType.S160X160
        )
    }
}
