package fe.materialplayer.util.os

import fe.materialplayer.Main
import java.io.File

object OSHelper {

    const val folderName = Main.APP_NAME

    @JvmStatic
    fun getOS(): OS {
        System.getProperty("os.name").toLowerCase().let { name ->
            return if (name.contains("mac") || name.contains("darwin")) {
                OS.MAC_OS
            } else if (name.contains("win")) {
                OS.WINDOWS
            } else if (name.contains("nux")) {
                OS.LINUX
            } else {
                OS.UNKNOWN
            }
        }
    }

    enum class OS {
        LINUX {
            override fun getDir(): File {
                val xdg = System.getenv("XDG_CONFIG_HOME")
                return if (xdg != null && xdg.isNotEmpty()) {
                    File(xdg, folderName)
                } else {
                    File(System.getenv("HOME") + "/.config", folderName)
                }
            }
        },
        MAC_OS {
            override fun getDir() = File("~/Library/Application Support", folderName)
        },
        WINDOWS {
            override fun getDir() = File(System.getenv("APPDATA"), folderName)

        },
        UNKNOWN {
            override fun getDir() = File(folderName)
        };

        abstract fun getDir(): File
    }
}