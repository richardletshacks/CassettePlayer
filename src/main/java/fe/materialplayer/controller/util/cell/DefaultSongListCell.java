package fe.materialplayer.controller.util.cell;

import fe.materialplayer.controller.util.cell.impl.CoverGridCell;
import fe.materialplayer.data.indexable.Song;
import fe.materialplayer.music.MusicPlayer;
import fe.materialplayer.util.SnackbarHelper;
import fe.materialplayer.util.Util;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.VBox;

import java.util.Collections;
import java.util.ResourceBundle;


public class DefaultSongListCell extends CoverGridCell {

    private static final MusicPlayer PLAYER = MusicPlayer.getInstance();
    private final ResourceBundle resources;

    public DefaultSongListCell(Song.FormatStyle formatType, ResourceBundle resources) {
        super(formatType);

        this.resources = resources;
    }

    @Override
    protected void updateItem(Song item, boolean empty) {
        super.updateItem(item, empty);

        if (empty || item == null) {
            this.setText(null);
            this.setGraphic(null);
        } else {
            VBox vbox = this.makeLabelBox(item, item.getBoxLines(this.getFormatType()));
            ImageView iv = this.makeAlbumCover(item, 35, Song.CoverType.S60X60);

            this.makeListeners(item);

            this.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
            this.setGraphic(this.makeBasicGrid(iv, 35, vbox));

            this.setOnMouseClicked(evt -> {
                if (evt.getButton() == MouseButton.MIDDLE) {
                    PLAYER.addNextSongs(Collections.singletonList(item), false);
                    SnackbarHelper.createMinWidthTextBar(resources.getString("addedNextSong"), 300);
                }
            });
        }
    }


}
