package fe.materialplayer.util

import javafx.scene.paint.Color
import java.util.*
import kotlin.math.roundToInt

val Color.hexString: String
    get() {
        return "#%02x%02x%02x%02x".format(
                (this.red * 255.0).roundToInt(),
                (this.green * 255.0).roundToInt(),
                (this.blue * 255.0).roundToInt(),
                (this.opacity * 255.0).roundToInt())
    }

val Color.luma: Double
    get() {
        return 0.2126 * this.red + 0.7152 * this.green + 0.0722 * this.blue
    }

val Color.textColor: Color
    get() {
        return if (this.isTextWhite()) Color.WHITE else Color.BLACK
    }

val Color.rgbString: String
    get() {
        return "rgb(${this.red}, ${this.green}, ${this.blue})"
    }

fun Color.lumaDiff(other: Color) = this.luma - other.luma

fun Color.isTextWhite() = 0.5 > luma

fun String.encodeBase64() = this.toByteArray().encodeBase64()

fun ByteArray.encodeBase64() = Base64.getEncoder().encodeToString(this).replace("/", "-")

fun String.decodeBase64() = Base64.getDecoder().decode(this.replace("-", "/"))

/**
 * Just pasted over [partition] but added the option to map to a specific type
 */
inline fun <T, X> Iterable<T>.partitionMap(map: (T) -> X, predicate: (T) -> Boolean): Pair<List<X>, List<X>> {
    val first = ArrayList<X>()
    val second = ArrayList<X>()
    for (element in this) {
        if (predicate(element)) {
            first.add(map(element))
        } else {
            second.add(map(element))
        }
    }

    return Pair(first, second)
}