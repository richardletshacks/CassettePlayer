package fe.materialplayer.controller.base.dialog

import com.jfoenix.controls.JFXButton
import fe.materialplayer.controller.css.annotation.property.IntCSSProperties
import fe.materialplayer.controller.css.annotation.property.IntCSSProperty
import fe.materialplayer.util.StyleConstants
import fe.materialplayer.util.properties.ColorThemeManager
import fe.materialplayer.util.image.ImageUtil
import javafx.fxml.FXML
import java.net.URL
import java.util.*

abstract class ClosableDialogController(val closeBtn: Boolean, scaled: Boolean) : DialogController(scaled) {
    @FXML
    @IntCSSProperties(IntCSSProperty(property = StyleConstants.CSS_BACKGROUND_RADIUS, 35))
    protected lateinit var btClose: JFXButton

    override fun initialize(location: URL, resources: ResourceBundle) {
        super.initialize(location, resources)

        if (closeBtn) {
            this.btClose.graphic = ImageUtil.makeThemeBoundIv("clear", 17)
            this.btClose.ripplerFillProperty().bind(ColorThemeManager.accentColor)
        }
    }

    @FXML
    fun btCloseClicked() {
        this.closeDialog()
    }
}