package fe.materialplayer;

import com.github.zafarkhaja.semver.ParseException;
import com.github.zafarkhaja.semver.Version;
import com.google.gson.JsonPrimitive;
import com.jfoenix.controls.JFXDrawersStack;
import fe.logger.LogFormatter;
import fe.logger.Logger;
import fe.materialplayer.font.FontManager;
import fe.materialplayer.music.MusicLibrary;
import fe.materialplayer.music.MusicPlayer;
import fe.materialplayer.scene.SceneManager;
import fe.materialplayer.tray.TrayPlayer;
import fe.materialplayer.util.BuildPropertyParser;
import fe.materialplayer.util.os.OSHelper;
import fe.materialplayer.util.StyleConstants;
import fe.materialplayer.util.Util;
import fe.materialplayer.util.image.ImageUtil;
import fe.materialplayer.util.wallpaper.WallpaperChanger;
import fe.materialplayer.util.os.LinuxDesktop;
import fe.materialplayer.util.properties.Properties;
import fe.materialplayer.util.threading.ThreadManager;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.image.ImageView;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.StackPane;

import javafx.stage.Stage;
import kotlin.Pair;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.Objects;

public class Main extends Application {
    private static final Version EMPTY_VERSION = Version.forIntegers(0, 0, 0);

    private final SceneManager sceneManager = SceneManager.INSTANCE;
    private final ThreadManager threadManager = ThreadManager.getInstance();
    private MusicPlayer musicPlayer;
    private final Logger logger = new Logger("Main");

    private Properties properties;

    private Version version = EMPTY_VERSION;
    private LocalDateTime builtAt = LocalDateTime.MIN;

    private boolean debug;
    private boolean debugDir;

    private boolean donatorBuild;

    public static final String APP_NAME = "CassettePlayer";
    private static final String DEV_BUILD_INDICATOR = "dev_build";

    private static Main instance;
    private String fullVersion;
    private Stage stage;

    private final OSHelper.OS currentOS = OSHelper.getOS();
    private final LinuxDesktop linuxDesktop = LinuxDesktop.Companion.detect();

    private File dir = this.currentOS.getDir();
    private StackPane spFileDragRoot;
    private WallpaperChanger wallpaperCreator;

    @Override
    public void start(Stage stage) {
        instance = this;

        for (String param : this.getParameters().getRaw()) {
            if (param.equalsIgnoreCase("--debug")) {
                this.debug = true;
            } else if(param.equalsIgnoreCase("--debug-dir")){
                this.debugDir = true;
            }
        }

        if (this.debug && this.debugDir) {
            dir = new File("debug", APP_NAME);
            new File("debug", "music_dir").mkdirs();
        }

        dir.mkdirs();

        this.properties = Properties.Companion.createInstance(dir);
        this.stage = stage;


        try {
            Map<String, String> buildProperties = BuildPropertyParser.parse("/version.properties");
            String version = buildProperties.get("semver");
            this.fullVersion = buildProperties.get("fullVersion");
            String builtAt = buildProperties.get("builtAt");
            this.donatorBuild = Boolean.parseBoolean(buildProperties.get("donator"));

            if (version != null) {
                try {
                    this.version = Version.valueOf(version);
                } catch (ParseException ignored) {
                }
            }

            if (builtAt != null) {
                this.builtAt = LocalDateTime.parse(builtAt.replace("\\", ""));
            }

            boolean devBuild = (version != null && version.equalsIgnoreCase(DEV_BUILD_INDICATOR))
                    || this.version.equals(EMPTY_VERSION);
            if (devBuild) {
                this.debug = true;
            }

            this.musicPlayer = MusicPlayer.getInstance();


            Logger.setGlobalDisabled(!this.debug);

            this.logger.print(Logger.Type.INFO, "Launching %s (%s) built @ %s (donator: %s)", version, fullVersion, this.builtAt, this.donatorBuild);
            this.logger.print(Logger.Type.INFO, "Java: %s, JavaFX: %s, OS: %s, Desktop: %s",
                    System.getProperty("java.version"),
                    System.getProperty("javafx.version"),
                    this.currentOS, this.linuxDesktop);


            FontManager.Companion.createInstance(dir).loadFonts();
            MusicLibrary.Companion.createInstance(dir).setup();
            this.wallpaperCreator = WallpaperChanger.Companion.createInstance(dir);

            JsonPrimitive volume;
            if ((volume = this.properties.getProperty("volume")) != null) {
                this.musicPlayer.setVolume(volume.getAsDouble());
            }

            JsonPrimitive tray = this.properties.getProperty("trayIcon");
            boolean showTray = tray == null || tray.getAsBoolean();

            if (showTray && TrayPlayer.INSTANCE.show()) {
                Platform.setImplicitExit(false);
                stage.iconifiedProperty().addListener((observable, oldValue, newValue) -> {
                    if (newValue && !oldValue && TrayPlayer.INSTANCE.isPresent()) {
                        this.stage.hide();
                    }
                });
            }


            JFXDrawersStack drawerStack = this.sceneManager.setupStack(stage);
            this.spFileDragRoot = new StackPane(drawerStack);
            ImageView ivFileDragIcon = ImageUtil.makeThemeBoundIv("library_add", 200);

            Scene scene = new Scene(spFileDragRoot, 1000, 650);

            scene.setOnDragOver(evt -> {
                if (evt.getDragboard().hasFiles() && Util.isMp3File(evt.getDragboard().getFiles())) {
                    drawerStack.setEffect(StyleConstants.BOX_BLUR);
                    if (!spFileDragRoot.getChildren().contains(ivFileDragIcon)) {
                        spFileDragRoot.getChildren().add(ivFileDragIcon);
                    }
                    evt.acceptTransferModes(TransferMode.ANY);
                }

                evt.consume();
            });

            scene.setOnDragDropped(evt -> {
                String musicDirStr = this.properties.getStringProperty("musicDir");
                if (musicDirStr != null) {
                    File musicDir = new File(musicDirStr);
                    for (File f : evt.getDragboard().getFiles()) {
                        try {
                            Files.copy(f.toPath(), new File(musicDir, f.getName()).toPath(), StandardCopyOption.REPLACE_EXISTING);
                        } catch (IOException e) {
                            this.logger.print(Logger.Type.ERROR, e);
                        }
                    }

                    evt.consume();
                }
            });

            scene.setOnDragExited(evt -> {
                drawerStack.setEffect(null);
                spFileDragRoot.getChildren().remove(ivFileDragIcon);
            });

            stage.getIcons().add(ImageUtil.loadIcon("logo/ic_launcher_round"));
            stage.setOnCloseRequest(event -> Platform.exit());
            stage.setMinHeight(650);
            stage.setMinWidth(1000);
            stage.setTitle(devBuild ?
                    String.format("%s - %s built @ %s", APP_NAME, this.fullVersion, this.builtAt) :
                    APP_NAME);

            stage.setScene(scene);
            stage.show();
        } catch (Exception e) {
            e.printStackTrace();
            this.logger.print(Logger.Type.ERROR, "%s", e);
        }
    }

    public StackPane getSpFileDragRoot() {
        return spFileDragRoot;
    }

    public boolean isDonatorBuild() {
        return donatorBuild;
    }

    public boolean isDebug() {
        return debug;
    }

    public Stage getStage() {
        return stage;
    }

    public Pair<Double, Double> getStageSize() {
        return new Pair<>(this.stage.getWidth(), this.stage.getHeight());
    }

    public static Main getInstance() {
        return instance;
    }

    public OSHelper.OS getCurrentOS() {
        return currentOS;
    }

    public LinuxDesktop getLinuxDesktop() {
        return linuxDesktop;
    }

    public File getDir() {
        return dir;
    }

    public Version getVersion() {
        return version;
    }

    public String getFullVersion() {
        return fullVersion;
    }

    public LocalDateTime getBuiltAt() {
        return builtAt;
    }

    public void show() {
        this.stage.setIconified(false);
        this.stage.show();
    }

    @Override
    public void stop() {
        this.threadManager.killThreads();
        MusicLibrary.Companion.getInstance().closeConnection();

        this.properties.addProperty("volume", new JsonPrimitive(this.musicPlayer.getVolume())).save();

        if (MusicPlayer.MPRIS_SUPPORT && this.musicPlayer.getMprisPlayer() != null) {
            this.musicPlayer.getMprisPlayer().stop();
        } else {
            this.musicPlayer.getHotKeyProvider().reset();
            this.musicPlayer.getHotKeyProvider().stop();
        }

        this.wallpaperCreator.resetWallpaper();

        File logDir = new File(this.dir, "logs");
        if (!logDir.exists()) {
            logDir.mkdir();
        }

        if (this.debug) {
            try (BufferedWriter bw = new BufferedWriter(new FileWriter(new File(logDir, String.format("log-%d.txt", System.currentTimeMillis()))))) {
                bw.write(LogFormatter.formatAll());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        System.exit(0);
    }

    public static void main(String[] args) {
        launch(args);
    }
}

