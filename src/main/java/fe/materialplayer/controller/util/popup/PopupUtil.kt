package fe.materialplayer.controller.util.popup

import com.jfoenix.controls.JFXButton
import fe.materialplayer.controller.content.ContentController
import fe.materialplayer.controller.css.bindStyle
import fe.materialplayer.util.StyleConstants
import fe.materialplayer.util.properties.ColorThemeManager.theme
import fe.materialplayer.util.properties.ColorType
import fe.materialplayer.util.image.ImageUtil
import javafx.event.EventHandler
import javafx.geometry.Pos
import javafx.scene.input.MouseEvent


object PopupUtil {

    @JvmStatic
    fun createPopupButton(text: String?, graphic: String?, handler: EventHandler<in MouseEvent?>?): JFXButton {
        val btn = JFXButton(text).apply {
            this.bindStyle()
                    .bindToThemeColor(StyleConstants.CSS_BACKGROUND_COLOR_PROPERTY, ColorType.BACKGROUND_COLOR)
                    .bindToThemeColor(StyleConstants.CSS_TEXT_FILL_PROPERTY, ColorType.TEXT_COLOR)
                    .setIntCSSProperty(StyleConstants.CSS_BACKGROUND_RADIUS, 0)
                    .build()
        }
        graphic?.let {
            btn.graphic = ImageUtil.makeIv(ContentController.OVERVIEW_ICON_SIZE,
                    { ImageUtil.loadIcon(theme.get().findIcon(graphic)) },
                    theme)
        }

        btn.prefWidth = Int.MAX_VALUE.toDouble()
        btn.prefHeight = 40.0
        btn.alignment = Pos.CENTER_LEFT

        handler?.let {
            btn.addEventHandler(MouseEvent.MOUSE_CLICKED, it)
        }

        return btn
    }

    @JvmStatic
    fun createPopupButton(text: String?, handler: EventHandler<in MouseEvent?>?): JFXButton {
        return createPopupButton(text, null, handler)
    }

    @JvmStatic
    fun createPopupButton(text: String?, graphic: String?): JFXButton {
        return createPopupButton(text, graphic, null)
    }
}