package fe.materialplayer.controller.sidebar

import com.google.gson.JsonPrimitive
import com.jfoenix.controls.JFXToggleButton
import com.jfoenix.controls.materialbutton.JFXContainedMaterialButton
import fe.materialplayer.Main
import fe.materialplayer.controller.content.ContentController
import fe.materialplayer.controller.css.StyleBinder
import fe.materialplayer.controller.css.annotation.color.PrimaryColored
import fe.materialplayer.controller.css.annotation.text.PrimaryExtractedTextFill
import fe.materialplayer.controller.css.annotation.theme.ThemeTextColor
import fe.materialplayer.controller.misc.ColorPickerController
import fe.materialplayer.controller.sidebar.language.ChangeLanguageController
import fe.materialplayer.controller.util.dialog.DialogUtil
import fe.materialplayer.tray.TrayPlayer
import fe.materialplayer.util.StyleConstants
import fe.materialplayer.util.properties.ColorThemeManager.lightPrimaryColor
import fe.materialplayer.util.properties.ColorThemeManager.primaryColor
import fe.materialplayer.util.properties.ColorType
import fe.materialplayer.util.wallpaper.WallpaperChanger
import fe.materialplayer.util.resource.FxmlUtil
import javafx.event.ActionEvent
import javafx.fxml.FXML
import javafx.scene.control.Label
import javafx.scene.layout.VBox
import javafx.scene.text.Font
import java.net.URL
import java.util.*

class SettingsController : ContentController() {
    @FXML
    @ThemeTextColor
    private lateinit var vbSettings: VBox

    @FXML
    @ThemeTextColor
    private lateinit var lbSettings: Label

    @FXML
    private lateinit var tgbTray: JFXToggleButton

    @FXML
    private lateinit var tgbChangeWallpaper: JFXToggleButton

    @FXML
    @PrimaryColored(cssProperty = StyleConstants.CSS_BACKGROUND_COLOR_PROPERTY)
    @PrimaryExtractedTextFill(cssProperty = StyleConstants.CSS_TEXT_FILL_PROPERTY)
    private lateinit var btChangeLanguage: JFXContainedMaterialButton

    @FXML
    private lateinit var lbColor: Label

    override fun initialize(location: URL, resources: ResourceBundle) {
        super.initialize(location, resources)

        tgbTray.isSelected = properties.getBooleanProperty("trayIcon", true)
        tgbChangeWallpaper.isSelected = properties.getBooleanProperty("changeWallpaper", false)

        StyleBinder.bindPaintToObjectColor(primaryColor, tgbTray.toggleColorProperty(), tgbChangeWallpaper.toggleColorProperty())
        StyleBinder.bindPaintToObjectColor(lightPrimaryColor, tgbTray.toggleLineColorProperty(), tgbChangeWallpaper.toggleLineColorProperty())
        StyleBinder.bindPaintToThemeColor(ColorType.TEXT_COLOR, tgbTray.textFillProperty(), tgbChangeWallpaper.textFillProperty())

        if (!Main.getInstance().isDonatorBuild) {
            with(resources.getString("donatorRestricted")) {
                lbColor.text = this
                tgbChangeWallpaper.text += " ($this)"
            }

            tgbChangeWallpaper.isDisable = true
        }

        //javafx is retarded, this is defined in the fxml aswell but since we have lbColor as variable here, the font/size from the fxml file is ignored for some reason????
        lbColor.font = Font("HK Grotesk SemiBold", 16.0)

        //TODO: maybe dont use vbox here? seems to lag a lot
        vbSettings.children.add(FxmlUtil.loadFxml("ColorPicker", ColorPickerController()).load<VBox>())
    }

    @FXML
    fun tgbTrayClicked(event: ActionEvent) {
        properties.addProperty("trayIcon", JsonPrimitive(tgbTray.isSelected)).save()
        if (tgbTray.isSelected) {
            TrayPlayer.show()
        } else {
            TrayPlayer.remove()
        }
    }

    @FXML
    fun tgbChangeWallpaperClicked(event: ActionEvent) {
        properties.addProperty("changeWallpaper", JsonPrimitive(tgbChangeWallpaper.isSelected)).save()
        if (!tgbChangeWallpaper.isSelected) {
            WallpaperChanger.getInstance().resetWallpaper()
        }
    }

    @FXML
    fun btChangeLanguageClicked(event: ActionEvent) {
        DialogUtil.loadDialog("main/sidebar/ChangeLanguage", null, ChangeLanguageController())
    }
}