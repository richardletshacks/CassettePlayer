package fe.materialplayer.controller.util.cell

class FormattablePair(var key: String, var value: String) {
    private var keyFormatted = false
    private var valueFormatted = false

    fun formatKey(vararg args: Any?): String {
        if (!keyFormatted) {
            key = String.format(key, *args)
            keyFormatted = true
        }

        return key
    }

    fun formatValue(vararg args: Any?): String {
        if (!valueFormatted) {
            value = String.format(value, *args)
            valueFormatted = true
        }

        return value
    }

    fun getOneliner(): String {
        return "$key $value"
    }
}