package fe.materialplayer.controller.view.playlist.controller;

import com.jfoenix.controls.JFXTextField;
import com.jfoenix.controls.materialbutton.JFXContainedMaterialButton;
import com.jfoenix.controls.materialbutton.JFXOutlinedMaterialButton;
import com.jfoenix.controls.materialbutton.JFXTextMaterialButton;
import fe.materialplayer.controller.content.ContentController;
import fe.materialplayer.controller.css.annotation.color.AccentColored;
import fe.materialplayer.controller.css.annotation.color.LightPrimaryColored;
import fe.materialplayer.controller.css.annotation.color.PrimaryColored;
import fe.materialplayer.controller.css.annotation.text.AccentExtractedTextFill;
import fe.materialplayer.controller.css.annotation.text.PrimaryExtractedTextFill;
import fe.materialplayer.controller.css.annotation.theme.ThemeSecondaryTextColor;
import fe.materialplayer.controller.css.annotation.theme.ThemeTextColor;
import fe.materialplayer.controller.misc.CrashViewerController;
import fe.materialplayer.data.indexable.Playlist;
import fe.materialplayer.util.M3UUtil;
import fe.materialplayer.util.SnackbarHelper;
import fe.materialplayer.util.StyleConstants;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.stage.FileChooser;
import org.jetbrains.annotations.NotNull;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class ExportPlaylistController extends ContentController {

    @FXML
    @ThemeTextColor(cssProperty = StyleConstants.CSS_TEXT_FILL_PROPERTY)
    private Label lbExport;

    @FXML
    @ThemeTextColor(cssProperty = StyleConstants.CSS_TEXT_FILL_PROPERTY)
    @LightPrimaryColored
    private JFXTextMaterialButton btCancel;

    @FXML
//    @AccentColored(cssProperty = StyleConstants.CSS_BACKGROUND_COLOR_PROPERTY)
//    @AccentExtractedTextFill(cssProperty = StyleConstants.CSS_TEXT_FILL_PROPERTY)
    private JFXOutlinedMaterialButton btSelect;

    @FXML
    @PrimaryColored(cssProperty = StyleConstants.CSS_BACKGROUND_COLOR_PROPERTY)
    @PrimaryExtractedTextFill(cssProperty = StyleConstants.CSS_TEXT_FILL_PROPERTY)
    private JFXContainedMaterialButton btCreate;

    @FXML
    @PrimaryColored
    @ThemeTextColor
    @ThemeSecondaryTextColor
    private JFXTextField tfFile;

    private final Playlist playlist;
    private ResourceBundle resources;

    public ExportPlaylistController(Playlist playlist) {
        super(false, false);
        this.playlist = playlist;
    }


    @Override
    public void initialize(@NotNull URL location, @NotNull ResourceBundle resources) {
        super.initialize(location, resources);

        this.resources = resources;
    }

    private static final String FILE_EXT = ".m3u8";

    @FXML
    public void btSelectClicked(ActionEvent event) {
        FileChooser fc = new FileChooser();
        fc.setTitle(this.resources.getString("selectFileToSaveTo"));
        fc.getExtensionFilters().add(new FileChooser.ExtensionFilter(String.format("Playlist file (*%s)", FILE_EXT), String.format("*%s", FILE_EXT)));

        File f = fc.showSaveDialog(this.btCancel.getScene().getWindow());
        if (f != null) {
            String file = f.getAbsolutePath();
            if (!file.endsWith(FILE_EXT)) {
                file += FILE_EXT;
            }

            this.tfFile.setText(file);
        }
    }

    @FXML
    public void btCancelClicked(ActionEvent event) {
        this.closeDialog();
    }

    @FXML
    public void btCreateClicked(ActionEvent event) {
        File file = new File(this.tfFile.getText());

        try (BufferedWriter bw = new BufferedWriter(new FileWriter(file))) {
            bw.write(M3UUtil.exportPlaylist(this.playlist));

            SnackbarHelper.createMinWidthTextBar(this.resources.getString("playlistHasBeenExported"), 300);
        } catch (IOException e) {
            e.printStackTrace();
            new CrashViewerController("PLAYLIST_EXPORT_FAILED",
                    String.format("Failed to export playlist %s to %s", this.playlist.getName(), file), e).show();
        }

        this.closeDialog();
    }

}
