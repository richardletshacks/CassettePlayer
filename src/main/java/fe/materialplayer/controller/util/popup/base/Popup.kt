package fe.materialplayer.controller.util.popup.base

import com.jfoenix.controls.JFXPopup
import fe.materialplayer.controller.util.popup.PopupState
import fe.materialplayer.data.indexable.base.Indexable
import javafx.event.EventHandler
import javafx.scene.Node
import javafx.scene.input.MouseEvent
import javafx.scene.layout.StackPane
import javafx.scene.layout.VBox
import javafx.stage.WindowEvent


abstract class Popup<T : Indexable>(open val state: PopupState<T>) {

    private val width: Double = 200.0
    protected var popup: JFXPopup? = null
    private val vBox: VBox = VBox()

    open fun addItem(vararg nodes: Node) {
        for (node in nodes) {
            this.addItem(node)
        }
    }

    open fun addItem(node: Node): Popup<T>? {
        return this.addItem(node, vBox.children.size)
    }

    fun containsNode(node: Node): Boolean {
        return this.vBox.children.contains(node)
    }

    fun removeItems(nodes: List<Node>) {
        nodes.forEach {
            this.vBox.children.remove(it)
        }
    }

    open fun addItem(node: Node, position: Int): Popup<T> {
        this.vBox.children.add(position, node)
        node.addEventHandler(MouseEvent.MOUSE_CLICKED) { this.popup?.hide() }

        return this
    }

    fun nodeSize() = vBox.children.size

    open fun build(): JFXPopup {
        this.popup?.let {
            return it
        }

        val sp = StackPane(this.vBox)
        sp.prefWidth = this.width
        sp.prefHeight = 40.0
        this.popup = JFXPopup(sp).apply {
            this.showingProperty().addListener { _, _, newValue ->
                if (newValue) {
                    showPopup()
                } else {
                    hidePopup()
                }
            }
        }

        return this.popup!!
    }

    fun buildAndShow(node: Node, event: MouseEvent, onClose: Function0<Unit>? = null) {
        this.build().also { popup ->
            onClose?.let {
                popup.showingProperty().addListener { _, _, nv ->
                    if (!nv) {
                        onClose.invoke()
                    }
                }
            }

            popup.show(
                node,
                JFXPopup.PopupVPosition.TOP,
                JFXPopup.PopupHPosition.LEFT, event.x, event.y
            )
        }
    }

    open fun hidePopup() {}
    open fun showPopup() {}
}