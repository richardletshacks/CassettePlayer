package fe.materialplayer.svg

import javafx.beans.property.ObjectProperty
import javafx.scene.layout.Background
import javafx.scene.layout.Region
import javafx.scene.layout.StackPane
import javafx.scene.paint.Color
import javafx.scene.shape.SVGPath


data class SVGIcon(val paths: List<String>)

object SVGDrawer {

    @JvmField
    val ADD_ICON = SVGIcon(listOf("M0 0h24v24H0z", "M19 13h-6v6h-2v-6H5v-2h6V5h2v6h6v2z"))
    val SHUFFLE_ICON = SVGIcon(
        listOf(
            "M0 0h24v24H0z",
            "M10.59 9.17L5.41 4 4 5.41l5.17 5.17 1.42-1.41zM14.5 4l2.04 2.04L4 18.59 5.41 20 17.96 7.46 20 9.5V4h-5.5zm.33 9.41l-1.41 1.41 3.13 3.13L14.5 20H20v-5.5l-2.04 2.04-3.13-3.13z"
        )
    )
    val PLAY_ICON = SVGIcon(listOf("M0 0h24v24H0z", "M8 5v14l11-7z"))
    val PLAYLIST_PLUS = SVGIcon(
        listOf(
            "M0 0h24v24H0z",
            "m 16.691902,14.78 9.8e-5,-4 h -2.000098 v 4 h -4 v 2 h 4 v 4 h 2 v -4 h 4 v -2 zM 4.6983049,10.779662 12.692,10.78 v 2 l -7.994,-3.41e-4 z m 0,-4.000001 H 16.692 v 2 H 4.6983049 Z m 0,8.000339 H 8.692 v 2 l -3.9936922,-2.82e-4 z"
        )
    )
    val PLAYLIST_PLAY = SVGIcon(listOf("M0 0h24v24H0V0z", "M4 10h12v2H4zm0-4h12v2H4zm0 8h8v2H4zm10 0v6l5-3z"))
    val QUEUE_ADD = SVGIcon(
        listOf(
            "M0 0h24v24H0z",
            "M15 6H3v2h12V6zm0 4H3v2h12v-2zM3 16h8v-2H3v2zM17 6v8.18c-.31-.11-.65-.18-1-.18-1.66 0-3 1.34-3 3s1.34 3 3 3 3-1.34 3-3V8h3V6h-5z"
        )
    )
    val QUEUE_ADD_NEXT = SVGIcon(
        listOf(
            "M0 0h24v24H0z",
            "M21,3H3C1.89,3,1,3.89,1,5v12c0,1.1,0.89,2,2,2h5v2h8v-2h2v-2H3V5h18v8h2V5C23,3.89,22.1,3,21,3z M13,10V7h-2v3H8v2h3v3 h2v-3h3v-2H13z M24,18l-4.5,4.5L18,21l3-3l-3-3l1.5-1.5L24,18z"
        )
    )

    @JvmField
    val SEARCH_ICON = SVGIcon(
        listOf(
            "M0 0h24v24H0z",
            "M15.5 14h-.79l-.28-.27C15.41 12.59 16 11.11 16 9.5 16 5.91 13.09 3 9.5 3S3 5.91 3 9.5 5.91 16 9.5 16c1.61 0 3.09-.59 4.23-1.57l.27.28v.79l5 4.99L20.49 19l-4.99-5zm-6 0C7.01 14 5 11.99 5 9.5S7.01 5 9.5 5 14 7.01 14 9.5 11.99 14 9.5 14z"
        )
    )

    @JvmStatic
    fun draw(svgIcon: SVGIcon, size: Double, fills: Map<Int, ObjectProperty<Color>>): StackPane {
        val sp = StackPane()
        svgIcon.paths.forEachIndexed { i, it ->
            val svgPath = SVGPath().apply {
                content = it
            }

            svgPath.resize(size, size)
            resize(svgPath, size, size)
            fills[i]?.let {
                svgPath.fillProperty().bind(it)
            }

            sp.children.add(svgPath)
        }

        return sp.apply {
            setMaxSize(size, size)
        }
    }

    private fun resize(svg: SVGPath, width: Double, height: Double) {
        val originalWidth = svg.prefWidth(-1.0)
        val originalHeight = svg.prefHeight(originalWidth)
        svg.scaleX = width / originalWidth
        svg.scaleY = height / originalHeight
    }
}