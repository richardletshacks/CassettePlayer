package fe.materialplayer.data.indexable.base;

import fe.materialplayer.util.StyleConstants;
import javafx.scene.paint.Color;

public interface BackgroundColorable {
    String getHexBackgroundColor();

    Color getBackgroundColor();

    boolean isWhiteText();

    static String toBackgroundStyle(BackgroundColorable colorable) {
        return StyleConstants.toBackgroundStyle(colorable.getHexBackgroundColor());
    }

    static String getStringTextColor(BackgroundColorable colorable) {
        return colorable.isWhiteText() ? "white" : "black";
    }

    static Color getTextColor(BackgroundColorable colorable) {
        if(colorable == null){
            return Color.BLACK;
        }

        return colorable.isWhiteText() ? Color.WHITE : Color.BLACK;
    }
}
