package fe.materialplayer.data.dao.helper

import com.j256.ormlite.stmt.DeleteBuilder
import com.j256.ormlite.stmt.UpdateBuilder
import fe.materialplayer.data.dao.base.NMDaoHelper
import fe.materialplayer.data.dao.base.SetupDao
import fe.materialplayer.data.dao.base.SetupDao.Companion.library
import fe.materialplayer.data.indexable.Playlist
import fe.materialplayer.data.indexable.nm.PlaylistSong
import fe.materialplayer.data.indexable.Song
import fe.materialplayer.data.indexable.base.table.Table
import java.sql.SQLException

class PlaylistSongDaoHelper : NMDaoHelper<PlaylistSong, Playlist, Song>(PlaylistSong::class.java,
        Pair(PlaylistSong.PLAYLIST_ID_COLUMN_NAME, PlaylistSong.INDEX_COLUMN_NAME),
        PlaylistSong.PLAYLIST_ID_COLUMN_NAME, { it.id }), SetupDao {

    //TODO: invoke overridden delete() on NMDaoHelper
    @Throws(SQLException::class)
    fun deletePlaylistItem(playlist: Playlist, index: Int) {
        val deleteBuilder: DeleteBuilder<PlaylistSong, Int> = this.deleteBuilder()
        deleteBuilder.where().eq(PlaylistSong.INDEX_COLUMN_NAME, index).and().eq(PlaylistSong.PLAYLIST_ID_COLUMN_NAME, playlist.id)
        deleteBuilder.delete()
    }

    @Throws(SQLException::class)
    fun movePlaylistItemDown(playlist: Playlist, index: Int) {
        val updateBuilder: UpdateBuilder<PlaylistSong, Int> = this.updateBuilder()
        updateBuilder.updateColumnValue(PlaylistSong.INDEX_COLUMN_NAME, index - 1)
        updateBuilder.where().eq(PlaylistSong.INDEX_COLUMN_NAME, index).and().eq(PlaylistSong.PLAYLIST_ID_COLUMN_NAME, playlist.id)
        updateBuilder.update()
    }

    @Throws(SQLException::class)
    fun swapPlaylistItems(playlist: Playlist, index1: Int, index2: Int) {
        val updateBuilder: UpdateBuilder<PlaylistSong, Int> = this.updateBuilder()
        for (items in arrayOf(intArrayOf(-index1, index1), intArrayOf(index1, index2), intArrayOf(index2, -index1))) {
            updateBuilder.updateColumnValue(PlaylistSong.INDEX_COLUMN_NAME, items[0])
            updateBuilder.where().eq(PlaylistSong.INDEX_COLUMN_NAME, items[1]).and()
                    .eq(PlaylistSong.PLAYLIST_ID_COLUMN_NAME, playlist.id)
            updateBuilder.update()
        }
    }


    override fun setup(queryResult: MutableList<out Table>) {
        val songsToAdd = HashMap<Playlist, MutableList<PlaylistSong>>()

        queryResult.forEach { table ->
            val ps = table as PlaylistSong

            val s: Song? = library.songDao.findItemById(ps.songId)
            val p: Playlist? = library.playlistDao.findItemById(ps.playlistId)
            ps.song = s
            ps.playlist = p

            p?.let { songsToAdd.computeIfAbsent(it) { ArrayList() }.add(ps) }
        }

        for ((key, value) in songsToAdd) {
            key.loadSongs(value)
        }
    }

    override fun findRelationFirst(first: Playlist): List<PlaylistSong> {
        TODO("Not yet implemented")
    }

    override fun findRelationSecond(second: Song): List<PlaylistSong> {
        TODO("Not yet implemented")
    }
}