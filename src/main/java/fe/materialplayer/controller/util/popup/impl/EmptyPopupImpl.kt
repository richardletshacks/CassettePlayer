package fe.materialplayer.controller.util.popup.impl

import fe.materialplayer.controller.util.popup.PopupState
import fe.materialplayer.controller.util.popup.base.Popup
import fe.materialplayer.data.indexable.base.Indexable

class EmptyPopupImpl<T : Indexable>(state: PopupState<T>) : Popup<T>(state)