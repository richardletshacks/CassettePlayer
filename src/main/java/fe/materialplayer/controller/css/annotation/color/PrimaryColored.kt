package fe.materialplayer.controller.css.annotation.color

import fe.materialplayer.util.StyleConstants

@Target(AnnotationTarget.FIELD)
@Retention(AnnotationRetention.RUNTIME)
@MustBeDocumented
annotation class PrimaryColored(val cssProperty: String = StyleConstants.CSS_PRIMARY_COLOR_VAR)