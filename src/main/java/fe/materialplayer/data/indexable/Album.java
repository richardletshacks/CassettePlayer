package fe.materialplayer.data.indexable;


import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;
import fe.materialplayer.controller.util.cell.FormattablePair;
import fe.materialplayer.data.dao.helper.AlbumDaoHelper;
import fe.materialplayer.data.indexable.base.Artistable;
import fe.materialplayer.data.indexable.base.BackgroundColorable;
import fe.materialplayer.data.indexable.base.IFormattedLines;
import fe.materialplayer.data.sort.SortType;
import fe.materialplayer.util.resource.ResourceBundleHelper;
import fe.materialplayer.util.Util;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Collection;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;


@DatabaseTable(tableName = "album")
public class Album extends Artistable implements BackgroundColorable, IFormattedLines {

    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField
    private String name;

    @DatabaseField
    private String year;

    private final ObservableList<Artist> artists = FXCollections.observableArrayList();

    @ForeignCollectionField(foreignFieldName = "album")
    private ForeignCollection<Song> songs;

    @DatabaseField
    private String coverDir;

    @DatabaseField
    private String coverMimeType;

    @DatabaseField
    private String backgroundColor;

    @DatabaseField
    private boolean whiteText;

    private List<Song> nonHiddenSongs;

    private static final String DEFAULT_ALBUM_ART_DOMINANT_COLOR = "#6a6a6a";

    public Album() {
    }

    public Album(String name, String year) {
        this.name = name;
        this.year = year;

        this.backgroundColor = DEFAULT_ALBUM_ART_DOMINANT_COLOR;
        this.whiteText = true;
    }

    public void addArtist(List<Artist> artists) {
        this.artists.addAll(artists);
    }

    public void addArtist(Artist artist) {
        this.artists.add(artist);
    }

    public Album setId(int id) {
        this.id = id;
        return this;
    }

    public static final SortType<Album> DEFAULT_SORT_TYPE = new SortType<>("Album", Comparator.comparing(Album::getName, String.CASE_INSENSITIVE_ORDER));
    public static final List<SortType<Album>> SORT_TYPES = List.of(
            new SortType<>("Artist", Comparator.comparing(Album::getArtistName, String.CASE_INSENSITIVE_ORDER)),
            DEFAULT_SORT_TYPE,
            new SortType<>("Year", Comparator.comparing(Album::getYear, String.CASE_INSENSITIVE_ORDER)));

    @Override
    protected String[] compareArray() {
        return new String[]{this.name, this.getArtistName(), this.year};
    }

    @Override
    public Collection<Song> toSongList() {
        return this.getSongs();
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof Album) {
            return ((Album) o).id == this.id;
        }

        return false;
    }

    @Override
    public String toString() {
        return this.name;
    }

    public String getCoverDir() {
        return coverDir;
    }

    public void setCoverDir(String coverDir) {
        this.coverDir = coverDir;
    }

    private static final Image DEFAULT_ALBUM_ART = Util.useInputStream(Album.class.getResourceAsStream("/icon/default_album_art.png"),
            in -> new Image(in, Song.CoverType.ORIGINAL_300X300.getSize(), Song.CoverType.ORIGINAL_300X300.getSize(), false, false));

    public ForeignCollection<Song> getActualSongs() {
        return songs;
    }

    public Image getCover(Song.CoverType type) {
        if (this.coverDir != null) {
            File f = this.getCoverFile(type);
            if (f.exists()) {
                try (FileInputStream fis = new FileInputStream(f)) {
                    Image img;
                    if (type.getSize() == -1) {
                        img = new Image(fis);
                    } else {
                        img = new Image(fis, type.getSize(), type.getSize(), false, false);
                    }

                    return img;
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return DEFAULT_ALBUM_ART;
    }

    public File getCoverFile(Song.CoverType type) {
        return new File(this.coverDir, String.format("%s.%s", type, this.coverMimeType));
    }

    public void setYear(String year) {
        this.year = year;
    }

    @Override
    public List<Artist> getArtists() {
        return this.artists;
    }

    @Override
    public String getArtistName() {
        return Artist.createNameString(this.artists);
    }

    public Color getTextColor() {
        return this.isWhiteText() ? Color.WHITE : Color.BLACK;
    }

    @Override
    public boolean isWhiteText() {
        return whiteText;
    }

    public void setWhiteText(boolean whiteText) {
        this.whiteText = whiteText;
    }

    public List<Song> getSongs() {
        if (this.nonHiddenSongs == null) {
            this.nonHiddenSongs = new LinkedList<>();
            for (Song s : this.songs) {
                if (!s.isHidden()) {
                    this.nonHiddenSongs.add(s);
                }
            }
        }

        return nonHiddenSongs;
    }

    public void resetLazySongList() {
        this.nonHiddenSongs = null;
    }

    public void setCoverMimeType(String coverMimeType) {
        this.coverMimeType = coverMimeType;
    }

    public String getCoverMimeType() {
        return coverMimeType;
    }

    public int getId() {
        return id;
    }

    public void setBackgroundColor(String backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    @Override
    public String getHexBackgroundColor() {
        return backgroundColor;
    }

    private Color backgroundColObj;

    @Override
    public Color getBackgroundColor() {
        if (this.backgroundColObj == null) {
            this.backgroundColObj = Color.valueOf(this.getHexBackgroundColor());
        }

        return this.backgroundColObj;
    }

    public String getYear() {
        if (this.year == null) {
            return "Unknown Year";
        }

        return year;
    }

    public String getName() {
        if (name == null || name.equals(AlbumDaoHelper.UNKNOWN_ALBUM)) {
            return ResourceBundleHelper.getBundle().getString("unknownAlbum");
        }

        return name;
    }

    @Override
    public FormattablePair getBoxLines() {
        return new FormattablePair(this.name, String.format("%s %s %%s", this.getArtistName(), Song.SMALL_BLACK_CIRCLE));
    }
}
