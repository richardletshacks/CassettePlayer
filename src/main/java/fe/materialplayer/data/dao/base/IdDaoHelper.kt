package fe.materialplayer.data.dao.base

import fe.materialplayer.data.indexable.base.table.IdTable
import java.sql.SQLException

open class IdDaoHelper<T : IdTable>(clazz: Class<T>, col: String, useCache: Boolean = false) : DaoHelper<T>(clazz, col, useCache) {

    @Throws(SQLException::class)
    override fun refresh(item: T): T {
        this.removeFromList(item)
        return this.querySpecific(item.id).also {
            this.addToList(it)
        }
    }

    @Throws(SQLException::class)
    fun querySpecific(id: Int): T = this.dao.queryForId(id)

    open fun findItemById(id: Int): T? {
        return this.items.find { it.id == id }
    }
}