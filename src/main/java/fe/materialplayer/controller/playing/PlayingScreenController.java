package fe.materialplayer.controller.playing;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXHamburger;
import com.jfoenix.controls.JFXListView;
import com.jfoenix.controls.JFXPopup;
import fe.materialplayer.Main;
import fe.materialplayer.controller.css.StyleBinder;
import fe.materialplayer.controller.css.annotation.theme.ThemeBackgroundColor;
import fe.materialplayer.controller.css.annotation.theme.ThemeTextColor;
import fe.materialplayer.controller.misc.CrashViewerController;
import fe.materialplayer.controller.util.PluralHelper;
import fe.materialplayer.controller.util.cell.FormattablePair;
import fe.materialplayer.controller.util.dialog.DialogUtil;
import fe.materialplayer.controller.util.popup.PopupState;
import fe.materialplayer.controller.util.popup.PopupUtil;
import fe.materialplayer.controller.util.popup.impl.SongPopup;
import fe.materialplayer.controller.view.playlist.controller.CreatePlaylistController;
import fe.materialplayer.data.indexable.Song;
import fe.materialplayer.data.indexable.base.BackgroundColorable;
import fe.materialplayer.font.FontManager;
import fe.materialplayer.font.PlayerFont;
import fe.materialplayer.music.MusicLibrary;
import fe.materialplayer.music.MusicPlayer;
import fe.materialplayer.util.*;
import fe.materialplayer.util.image.ImageUtil;
import fe.materialplayer.util.image.icon.ColoredIconCache;
import fe.materialplayer.util.properties.ColorThemeManager;
import fe.materialplayer.util.properties.ColorType;
import fe.materialplayer.util.wallpaper.WallpaperChanger;
import javafx.animation.Interpolator;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.util.Duration;
import org.jetbrains.annotations.NotNull;

import java.net.URL;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.ResourceBundle;

public class PlayingScreenController extends PlayStatusDisplayer {

    @FXML
    private StackPane spBase;

    @FXML
    private ImageView ivCover;

    @FXML
    private StackPane spRoot;

    @FXML
    private JFXButton btShuffle;

    @FXML
    private JFXButton btRepeat;

    @FXML
    private JFXButton btShuffleLoop;

    @FXML
    private JFXButton btClose;

    @FXML
    @ThemeBackgroundColor
    private JFXListView<Song> lvQueue;

    @FXML
    private Label lbSizeLength;

    @FXML
    @ThemeTextColor
    private JFXHamburger hbMenu;

    @FXML
    private StackPane spKebabRoot;

    @FXML
    private ImageView ivPlayingIcon;

    @FXML
    @ThemeTextColor(cssProperty = StyleConstants.CSS_TEXT_FILL_PROPERTY)
    private Label lbCurrentSong;

    @FXML
    private Label lbCurrentSongSub;

    @FXML
    private ColumnConstraints ccMusicNote;

    @FXML
    private JFXButton btFavourite;

    private static final int IMAGE_ROUNDING = 9;
    private static final MusicPlayer PLAYER = MusicPlayer.getInstance();
    private static final MusicLibrary LIBRARY = MusicLibrary.Companion.getInstance();

    private static final WallpaperChanger WALLPAPER_CREATOR = WallpaperChanger.Companion.getInstance();

    private static final ColoredIconCache ARROW_DOWN_CACHE = new ColoredIconCache(Map.of(
            Color.BLACK, "arrow_down_black",
            Color.WHITE, "arrow_down_white"
    ));

    private static final ColoredIconCache FAVORITE_FILLED = new ColoredIconCache(Map.of(
            Color.BLACK, "favorite_filled_black",
            Color.WHITE, "favorite_filled_white"
    ));

    private static final ColoredIconCache FAVORITE_EMPTY = new ColoredIconCache(Map.of(
            Color.BLACK, "favorite_empty_black",
            Color.WHITE, "favorite_empty_white"
    ));

    private final IntegerProperty popupIndex = new SimpleIntegerProperty();
    private final PopupState<Song> popupState = new PopupState<>();
    private SongPopup popup;

    private int lastIdx;
    private boolean firstBackgroundChange = true;

    private final Song initialSong;
    private final EventHandler<? super MouseEvent> clickEvent;
    private ResourceBundle resources;

    private final SimpleBooleanProperty currentSongFavorite = new SimpleBooleanProperty();
    private static final PlayerFont HK_GROTESK = FontManager.Companion.getHK_GROTESK_SEMI_BOLD();

    public PlayingScreenController(Song initialSong, EventHandler<? super MouseEvent> clickEvent) {
        super(JFXPopup.PopupVPosition.TOP, 0);

        this.initialSong = initialSong;
        this.clickEvent = clickEvent;
    }

    @Override
    public void initialize(@NotNull URL location, @NotNull ResourceBundle resources) {
        super.initialize(location, resources);
        this.resources = resources;

        new StyleBinder(this.spBase)
                .bindToThemeColor(StyleConstants.CSS_BACKGROUND_COLOR_PROPERTY, ColorType.BACKGROUND_COLOR)
                .setIntCSSProperty(StyleConstants.CSS_BACKGROUND_RADIUS, 5)
                .setIntCSSProperty(StyleConstants.CSS_BORDER_RADIUS, 5).build();

        this.currentSongFavorite.setValue(this.player.getCurrentSong().isFavorite());

        this.ivPlayingIcon.imageProperty().bind(ImageUtil.createIconBinding("music_note"));
        this.setCurrentPlayingText(this.initialSong);

        this.lbCurrentSongSub.setTextFill(StyleConstants.SUB_TITLE_COLOR);

        this.popup = (SongPopup) new SongPopup(this.popupState, resources)
                .addItem(PopupUtil.createPopupButton(resources.getString("removeFromQueue"), "remove",
                        evt -> this.player.removeFromQueue(this.popupIndex.get())
                ), 1);

        StyleBinder.bindPaintToComplexObjectProperty(() -> this.player.getCurrentSong().getBackgroundColor(), this.player.nowPlayingProperty(),
                this.btFavourite.ripplerFillProperty(),
                this.btPrevious.ripplerFillProperty(),
                this.btTogglePlay.ripplerFillProperty(),
                this.btNext.ripplerFillProperty(),
                this.btVolume.ripplerFillProperty());

        StyleBinder.bindPaintToObjectColor(ColorThemeManager.getPrimaryColor(),
                this.btRepeat.ripplerFillProperty(), this.btShuffle.ripplerFillProperty(),
                this.btShuffleLoop.ripplerFillProperty());

        StyleBinder.bindPaintToComplexObjectProperty(
                () -> BackgroundColorable.getTextColor(this.player.getCurrentSong()), this.player.nowPlayingProperty(),
                this.btClose.textFillProperty());

        StyleBinder.bindPaintToComplexObjectProperty(() -> ColorThemeManager.getTheme().get().adjustedColor(this.player.getCurrentSong().getBackgroundColor()),
                this.player.nowPlayingProperty(),
                this.lbSizeLength.textFillProperty());


        PropertyBinder.bindTextProperty(() -> this.player.shuffleProperty().get() ? StyleConstants.LIGHT_GRAY_BACKGROUND : StyleConstants.TRANSPARENT_BACKGROUND, this.player.shuffleProperty());

        this.lbSizeLength.textProperty().bind(Bindings.createStringBinding(() -> {
                    if (this.player.queueChangedProperty().get()) {
                        this.player.queueChangedProperty().set(false);
                    }

                    if (this.player.nowPlayingProperty().get() == null) {
                        return this.makeSizeLengthString(this.player.getQueue().size(), this.player.queueLengthProperty().get());
                    }

                    return this.makeSizeLengthString(
                            this.player.getQueue().size() - this.player.getCurrentIndex() - 1,
                            this.player.queueLengthWithoutFirstProperty().doubleValue());
                },
                this.player.queueLengthProperty(),
                this.player.nowPlayingProperty(),
                this.player.queueChangedProperty()));

        this.btFavourite.setGraphic(ImageUtil.makeIv(18, Bindings.createObjectBinding(
                () -> (this.currentSongFavorite.get() ? FAVORITE_FILLED : FAVORITE_EMPTY).getIcon(ColorThemeManager.getTheme().get().getColor(ColorType.TEXT_COLOR)),
                this.player.nowPlayingProperty(), this.currentSongFavorite, ColorThemeManager.getTheme())
        ));

        this.lbSizeLength.setFont(HK_GROTESK.get(14));

        this.btClose.setGraphic(this.makeIconColoredCacheIv(18, ARROW_DOWN_CACHE));

        this.btRepeat.setGraphic(this.player.loopModeObjectProperty().get().getImageView());
        this.btShuffle.setGraphic(ImageUtil.makeThemeBoundIv("shuffle2", 25));
        this.btShuffleLoop.setGraphic(ImageUtil.makeThemeBoundIv("shuffle_repeat", 25));

        this.lbEnd.setText(this.initialSong.getFormattedLength());

        this.lvQueue.setItems(this.player.getQueue());
        this.lvQueue.setCellFactory(param -> new PlayingQueueListCell());

        this.player.loopModeObjectProperty().addListener((observable, oldValue, newValue) -> {
            this.btRepeat.setStyle(newValue.getBackgroundColor());
            this.btRepeat.setGraphic(newValue.getImageView());
        });

        this.player.shuffleLoopProperty().addListener((observable, oldValue, newValue) -> {
            this.btShuffleLoop.setStyle(newValue ? StyleConstants.LIGHT_GRAY_BACKGROUND : StyleConstants.TRANSPARENT_BACKGROUND);
        });


        ImageUtil.roundImageWrapper(this.ivCover, this.initialSong.loadSongCover(Song.CoverType.ORIGINAL_300X300), IMAGE_ROUNDING);

        this.spRoot.setStyle(BackgroundColorable.toBackgroundStyle(this.initialSong));
        Platform.runLater(() -> this.changeWallpaper(this.initialSong));

        this.spRoot.widthProperty().addListener((observable, oldValue, newValue) -> {
            double size = Math.min(Song.CoverType.ORIGINAL_300X300.getSize(), newValue.doubleValue() / 4);

            this.ivCover.setFitHeight(size);
            this.ivCover.setFitWidth(size);
        });

        ScopeHelper.with(ColorThemeManager.getTheme().get().adjustedColor(this.initialSong.getBackgroundColor()), color -> {
            this.setVolumeColor(color);
            this.slPosition.skinProperty().addListener((observable, oldValue, newValue) -> {
                StyleBinder.setSliderColor(this.slPosition, color);
            });
        });

        this.slPosition.setMax(this.initialSong.getLength());

        this.selectIndex(this.player.getCurrentIndex());

        this.player.nowPlayingProperty().addListener((observable, prevSong, newSong) -> {
            this.currentSongFavorite.setValue(this.player.getCurrentSong().isFavorite());

            this.setCurrentPlayingText(newSong);

            int curIndex = this.player.getCurrentIndex();

            Platform.runLater(() -> ImageUtil.roundImageWrapper(this.ivCover, newSong.loadSongCover(Song.CoverType.ORIGINAL_300X300), IMAGE_ROUNDING));

            // DO _NOT_ USE RUNLATER BEFORE IF CHECK
            // lastIdx might have already changed before the runnable is executed so wrong animation will be used!
            if (curIndex > this.lastIdx) {
                Platform.runLater(() -> {
                    this.changePlayingScreenBackground(prevSong, newSong, SongSwitchDirection.NEXT);
                    this.changeWallpaper(newSong);
                });
            } else {
                Platform.runLater(() -> {
                    this.changePlayingScreenBackground(newSong, prevSong, SongSwitchDirection.PREV);
                    this.changeWallpaper(newSong);
                });
            }

            ScopeHelper.with(ColorThemeManager.getTheme().get().adjustedColor(newSong.getBackgroundColor()), color -> {
                StyleBinder.setSliderColor(this.slPosition, color);
                this.setVolumeColor(color);
            });

            this.selectIndex(Math.min(curIndex + 1, this.player.getQueue().size()));
            this.lastIdx = curIndex;
        });

        this.player.playerStoppedProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue) {
                Platform.runLater(WALLPAPER_CREATOR::resetWallpaper);
                this.clickEvent.handle(null);
            }
        });
    }

    public void setCurrentPlayingText(Song song) {
        FormattablePair pair = song.getBoxLines(Song.FormatStyle.TITLE_ARTIST_ALBUM);
        this.lbCurrentSong.setText(pair.getKey());
        this.lbCurrentSongSub.setText(pair.getValue());
    }

    public void addKeyListener() {
        EventHandler<KeyEvent> eventFilter = event -> {
            if (event.getCode() == KeyCode.SPACE) {
                this.player.togglePlaying();
                event.consume();
            }
        };

        Main.getInstance().getStage().addEventFilter(KeyEvent.KEY_PRESSED, eventFilter);

        EventHandler<MouseEvent> removeFilter = event -> Main.getInstance().getStage().removeEventFilter(KeyEvent.KEY_PRESSED, eventFilter);

        this.ivCover.addEventHandler(MouseEvent.MOUSE_CLICKED, removeFilter);
        this.btClose.addEventHandler(MouseEvent.MOUSE_CLICKED, removeFilter);

        this.ivCover.setOnMouseClicked(this.clickEvent);
        this.btClose.setOnMouseClicked(this.clickEvent);
    }


    private JFXPopup queuePopup;

    @FXML
    public void riMenuClicked(MouseEvent event) {
        if (queuePopup == null) {
            queuePopup = new JFXPopup();
            VBox vBox = new VBox(
                    PopupUtil.createPopupButton(this.resources.getString("saveQueueToPlaylist"), "save", e ->
                            DialogUtil.loadDialog("view/playlist/CreatePlaylist", queuePopup, new CreatePlaylistController(evt -> {
                                try {
                                    evt.getPlaylist().addSongs(PLAYER.getQueue());
                                } catch (Exception ex) {
                                    ex.printStackTrace();
                                    new CrashViewerController("ADD_SONGS_TO_PLAYLIST",
                                            String.format("Error while adding songs %s to playlist %s", evt.getPlaylist(), PLAYER.getQueue()), ex).show();
                                }
                            }))),
                    PopupUtil.createPopupButton(this.resources.getString("clearQueue"), "clear", evt -> {
                        this.player.clearQueue();
                        this.clickEvent.handle(evt);
                    }),
                    PopupUtil.createPopupButton(this.resources.getString("jumpToCurrent"), "restore", evt -> {
                        this.selectIndex(this.player.getCurrentIndex() + 1);
                        queuePopup.hide();
                    })
            );

            StackPane sp = new StackPane(vBox);
            sp.setPrefWidth(200);
            sp.setPrefHeight(40);

            queuePopup.setPopupContent(sp);
        }

        queuePopup.show(this.spKebabRoot,
                JFXPopup.PopupVPosition.TOP,
                JFXPopup.PopupHPosition.RIGHT, event.getX() - 20, event.getY()
        );
    }

    @FXML
    public void btFavouriteClicked(ActionEvent event) {
        Song song = this.player.getCurrentSong();
        if (song != null) {
            song.setFavoritedAt(song.isFavorite() ? null : LocalDateTime.now());
            this.currentSongFavorite.set(song.isFavorite());

            try {
                LIBRARY.getSongDao().update(song);
                if (song.isFavorite()) {
                    LIBRARY.getSongDao().favoriteSong(song);
                } else {
                    LIBRARY.getSongDao().unfavoriteSong(song);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @FXML
    public void btRepeatClicked(ActionEvent event) {
        this.player.switchMode(this.player.loopModeObjectProperty().get().next());
    }

    @FXML
    public void btShuffleClicked(ActionEvent event) {
        this.player.toggleShuffle();
        this.selectIndex(this.player.getCurrentIndex());
    }

    @FXML
    public void btShuffleLoopClicked(ActionEvent event) {
        this.player.loopModeObjectProperty().set(MusicPlayer.LoopMode.NO_LOOP);
        if (this.player.shuffleProperty().get()) {
            this.player.setShuffle(false);
        }

        this.player.toggleShuffleLoop();
    }

    @FXML
    public void lvQueueClicked(MouseEvent event) {
        Song s = this.lvQueue.getSelectionModel().getSelectedItem();
        if (s != null && event.getPickResult().getIntersectedNode().getClass() != PlayingQueueListCell.class) {
            if (event.getButton() == MouseButton.SECONDARY) {
                this.popupIndex.set(this.lvQueue.getSelectionModel().getSelectedIndex());

                this.popupState.setCurrentState(s);
                this.popup.buildAndShow(this.lvQueue, event, () -> {
                    this.lvQueue.getSelectionModel().clearSelection();
                    return null;
                });
            } else if (event.getButton() == MouseButton.PRIMARY) {
                this.player.jumpToIndex(this.lvQueue.getSelectionModel().getSelectedIndex());
            }
        }
    }

    public String makeSizeLengthString(int itemsLeft, double timeLeft) {
        return String.format("%s %s %d %s %s %s",
                this.resources.getString("upNext"),
                Song.SMALL_BLACK_CIRCLE,
                itemsLeft,
                PluralHelper.getString(this.resources, itemsLeft, "song", "songs"),
                Song.SMALL_BLACK_CIRCLE,
                itemsLeft > 0 ? FormatUtil.formatLength(timeLeft) : "0:00");
    }

    public ImageView makeIconColoredCacheIv(int size, ColoredIconCache cache) {
        return ImageUtil.makeIv(size, () -> cache.getIcon(BackgroundColorable.getTextColor(this.player.getCurrentSong())), this.player.nowPlayingProperty());
    }

    public void selectIndex(int index) {
        this.lvQueue.getSelectionModel().clearSelection();
        this.lvQueue.scrollTo(index);
    }

    private static final SnapshotParameters SNAPSHOT_PARAMETERS = new InitHelper<>(new SnapshotParameters()) {
        @Override
        public void init(SnapshotParameters instance) {
            instance.setFill(Color.TRANSPARENT);
        }
    }.get();

    public void changeWallpaper(Song song) {
        if (getProperties().getBooleanProperty("changeWallpaper", false) && Main.getInstance().isDonatorBuild()) {
            WALLPAPER_CREATOR.changeWallpaper(song, ivCover.snapshot(SNAPSHOT_PARAMETERS, null));
        }
    }

    public void changePlayingScreenBackground(Song prevSong, Song nextSong, SongSwitchDirection direction) {
        if (prevSong != null) {
            this.spRoot.setStyle(BackgroundColorable.toBackgroundStyle(prevSong));
        }

        StackPane spNew = new StackPane();
        spNew.setMaxWidth(Integer.MAX_VALUE);
        spNew.setMaxHeight(Integer.MAX_VALUE);

        this.spBase.widthProperty().addListener((observable, oldValue, newValue) -> {
            spNew.setMinWidth(newValue.doubleValue());
        });

        if (nextSong != null) {
            spNew.setStyle(BackgroundColorable.toBackgroundStyle(nextSong));
        }

        if (!this.firstBackgroundChange) {
            this.spRoot.getChildren().remove(0);
        }

        if (this.firstBackgroundChange) {
            this.firstBackgroundChange = false;
        }

        spNew.translateXProperty().set(direction.getStartValue(this.spRoot));
        this.spRoot.getChildren().add(0, spNew);

        Timeline timeline = new Timeline();
        timeline.getKeyFrames().add(
                new KeyFrame(Duration.millis(110),
                        new KeyValue(spNew.translateXProperty(),
                                direction.getEndValue(this.spRoot), Interpolator.EASE_IN)));
        timeline.play();
    }


    enum SongSwitchDirection {
        PREV, NEXT;

        public double getStartValue(Pane pane) {
            return this == NEXT ? pane.getWidth() : 0;
        }

        public double getEndValue(Pane pane) {
            return this == PREV ? pane.getWidth() : 0;
        }
    }
}
