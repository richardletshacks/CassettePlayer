package fe.materialplayer.controller.base

import com.jfoenix.controls.JFXRippler
import fe.materialplayer.scene.SceneManager
import javafx.fxml.Initializable

abstract class SidebarBindController : BaseController() {
    protected var sceneManager = SceneManager

    open fun bindSidebar(hamburger: JFXRippler?) {
        sceneManager.bindSidebarToggle(hamburger!!)
    }
}