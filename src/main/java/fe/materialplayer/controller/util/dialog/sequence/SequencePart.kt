package fe.materialplayer.controller.util.dialog.sequence

import fe.materialplayer.controller.base.dialog.UnclosableDialogController

data class SequencePart(val fxml: String,
                        val controller: UnclosableDialogController)
