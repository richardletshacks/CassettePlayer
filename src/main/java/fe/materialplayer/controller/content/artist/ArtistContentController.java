package fe.materialplayer.controller.content.artist;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXListView;
import com.jfoenix.controls.JFXPopup;
import fe.materialplayer.controller.content.ContentController;
import fe.materialplayer.controller.content.album.AlbumContentController;
import fe.materialplayer.controller.content.resize.ResizeButtonBinder;
import fe.materialplayer.controller.css.annotation.theme.ThemeBackgroundColor;
import fe.materialplayer.controller.css.annotation.theme.ThemeTextColor;
import fe.materialplayer.controller.misc.CrashViewerController;
import fe.materialplayer.controller.util.ItemFetcher;
import fe.materialplayer.controller.util.PluralHelper;
import fe.materialplayer.controller.util.SongSelector;
import fe.materialplayer.controller.util.cell.DefaultSongListCell;
import fe.materialplayer.controller.util.clickable.artist.ClickableArtistName;
import fe.materialplayer.controller.util.clickable.cover.ArtistAlbumCover;
import fe.materialplayer.controller.util.dialog.DialogUtil;
import fe.materialplayer.controller.util.popup.PopupState;
import fe.materialplayer.controller.util.popup.base.Popup;
import fe.materialplayer.controller.util.popup.impl.SongPopup;
import fe.materialplayer.data.indexable.Album;
import fe.materialplayer.data.indexable.Artist;
import fe.materialplayer.data.indexable.Song;
import fe.materialplayer.font.FontManager;
import fe.materialplayer.font.PlayerFont;
import fe.materialplayer.music.MusicLibrary;
import fe.materialplayer.util.DeclarationsKt;
import fe.materialplayer.util.StyleConstants;
import fe.materialplayer.util.Util;
import fe.materialplayer.util.artistimage.ArtistImageLoader;
import fe.materialplayer.util.color.BackgroundColorExtractor;
import fe.materialplayer.util.image.ImageUtil;
import fe.materialplayer.util.image.icon.ColoredIconCache;
import fe.materialplayer.util.properties.ColorThemeManager;
import fe.materialplayer.util.resource.ResourceBundleHelper;
import fe.materialplayer.util.resource.StylesheetUtil;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.TextFlow;
import javafx.stage.FileChooser;
import javafx.util.Duration;
import org.jetbrains.annotations.NotNull;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.sql.SQLException;
import java.util.*;
import java.util.stream.Collectors;

public class ArtistContentController extends ContentController {

    @FXML
    @ThemeTextColor
    private VBox vbLabels;

    @FXML
    private StackPane spArtist;

    @FXML
    private ImageView ivArtist;

    @FXML
    private TextFlow tfArtistName;

    @FXML
    private TextFlow tfMembers;

    @FXML
    private Label lbArtist;

    @FXML
    private Label lbSongNumber;

    @FXML
    private Label lbAlbumNumber;

    @FXML
    private Label lbLength;

    @FXML
    @ThemeBackgroundColor
    private ScrollPane spContent;

    @FXML
    private GridPane gpContent;

    @FXML
    private JFXButton btShuffleAll;

    @FXML
    private JFXButton btPlayNext;

    @FXML
    private JFXButton btAddToPlayingQueue;

    @FXML
    private JFXButton btAddToPlaylist;

    private JFXButton btAddArtistImage;
    private JFXButton btRemoveArtistImage;
    private JFXButton btRefreshArtistImage;

    private final Artist artist;
    private static final MusicLibrary LIBRARY = MusicLibrary.Companion.getInstance();

    private static final int IV_SIZE = 110;
    private static final int ROW_HEIGHT = 170;
    private static final int V_GAP = 7;
    private static final int H_GAP = 7;
    private static final int ALBUM_PADDING = 18;

    private static final PlayerFont HK_GROTESK = FontManager.Companion.getHK_GROTESK_SEMI_BOLD();

    private static final int FETCH_SIZE = 15;

    private Timeline scrollDownAnimation;

    private final ArtistImageLoader loadManager = ArtistImageLoader.getInstance();
    private static final ColoredIconCache ICON_ADD_CIRCLE = new ColoredIconCache(Map.of(Color.WHITE, "add_circle_white", Color.BLACK, "add_circle_black"));
    private static final ColoredIconCache ICON_REMOVE = new ColoredIconCache(Map.of(Color.WHITE, "delete_white", Color.BLACK, "delete_black"));
    private static final ColoredIconCache ICON_REFRESH = new ColoredIconCache(Map.of(Color.WHITE, "cloud_download_white", Color.BLACK, "cloud_download_black"));

    private HBox hbArtistImageControls;

    public ArtistContentController(Artist artist) {
        super(true, true);

        this.artist = artist;
        this.initArtistImageButtons();
    }

    public static void show(Artist artist, JFXPopup popup) {
        DialogUtil.loadDialog("content/ArtistContent", popup,
                new ArtistContentController(artist));
    }

    public void initArtistImageButtons() {
        this.btAddArtistImage = this.createArtistImageControlButton(ICON_ADD_CIRCLE, evt -> {
            this.ivArtist.setEffect(null);
            this.hbArtistImageControls.setVisible(false);
            FileChooser fc = new FileChooser();
            fc.setTitle(this.resources.getString("chooseImage"));
            fc.setSelectedExtensionFilter(new FileChooser.ExtensionFilter("Images (*.jpeg, *.jpg, *.png)", "*.jpeg", "*.jpg", "*.png"));

            File f = fc.showOpenDialog(this.spArtist.getScene().getWindow());
            if (f != null) {
                try {
                    if (this.artist.getImageFile() != null) {
                        this.artist.getImageFile().delete();
                    }

                    String fileName = f.getName();
                    File newImageFile = this.artist.updateImageMimeType(fileName.substring(fileName.lastIndexOf(".") + 1));

                    Files.copy(f.toPath(), newImageFile.toPath(), StandardCopyOption.REPLACE_EXISTING);

                    this.artist.setArtistImage(Util.useInputStream(new FileInputStream(newImageFile),
                            i -> new Image(i, Artist.DEFAULT_ARTIST_IMAGE_SIZE, Artist.DEFAULT_ARTIST_IMAGE_SIZE, false, false)));

                    LIBRARY.getArtistDao().update(this.artist);

                    this.updateArtistImage(this.artist.getArtistImage());
                } catch (IOException | SQLException e) {
                    e.printStackTrace();
                    new CrashViewerController("COPY_ARTIST_IMAGE", "An error occurred while copying the selected artist image to the artist's dir", e).show();
                }
            }
        });
        this.btRemoveArtistImage = this.createArtistImageControlButton(ICON_REMOVE, evt -> {
            this.ivArtist.setEffect(null);
            this.hbArtistImageControls.setVisible(false);
            this.artist.setArtistImage(Artist.DEFAULT_ARTIST_IMAGE);
            this.artist.getImageFile().delete();

            this.updateArtistImage(this.artist.getArtistImage());
        });
        this.btRefreshArtistImage = this.createArtistImageControlButton(ICON_REFRESH, evt -> {
            this.ivArtist.setEffect(null);
            this.hbArtistImageControls.setVisible(false);
            this.fetchArtistImage();
        });
    }

    public JFXButton createArtistImageControlButton(ColoredIconCache iconCache, EventHandler<MouseEvent> event) {
        JFXButton btn = new JFXButton(null);

        ImageView iv = ImageUtil.makeIv(19, () -> {
            if (this.artist.getArtistImage() != null) {
                BufferedImage img = SwingFXUtils.fromFXImage(this.artist.getArtistImage(), null);
                Color color = DeclarationsKt.getTextColor(BackgroundColorExtractor.extract(img));
                btn.setRipplerFill(color);

                return iconCache.getIcon(color);
            }

            return iconCache.getIcon(Color.WHITE);
        }, this.artist.artistImageProperty());

        btn.setOnMouseClicked(event);
        btn.setGraphic(iv);
        btn.setPadding(new Insets(4));

        btn.setStyle("-fx-background-radius: 20");
        btn.setMaxSize(19, 19);
        return btn;
    }

    public void updateArtistImage(Image img) {
        ImageUtil.roundImageWrapper(this.ivArtist, img, IV_SIZE);
    }

    public void fetchArtistImage() {
        this.ivArtist.setImage(ImageUtil.loadIcon("loading", "gif"));
        this.artist.setLoadingImage(true);

        this.artist.artistImageProperty().addListener(new ChangeListener<>() {
            @Override
            public void changed(ObservableValue<? extends Image> observable, Image oldValue, Image newValue) {
                if (newValue != null) {
                    Platform.runLater(() -> updateArtistImage(newValue));
                    artist.artistImageProperty().removeListener(this);
                }
            }
        });
        this.loadManager.nextArtist(this.artist);
    }

    @Override
    public void initialize(@NotNull URL location, @NotNull ResourceBundle resources) {
        super.initialize(location, resources);

        this.hbArtistImageControls = new HBox(btAddArtistImage, btRemoveArtistImage, btRefreshArtistImage);
        hbArtistImageControls.setVisible(false);

        this.spArtist.getChildren().add(hbArtistImageControls);
        hbArtistImageControls.setAlignment(Pos.CENTER);
        StackPane.setAlignment(hbArtistImageControls, Pos.CENTER);

        if (artist.getParentArtist() != null) {
            Label lbParentArtist = new ClickableArtistName(artist.getParentArtist(), this).createLabel();
            lbParentArtist.setFont(HK_GROTESK.get(18));

            //for some reason, the fontsize from the font is not used when the label is hovered, so we have to set it via css
            lbParentArtist.setStyle("-fx-font-size: 18");

            Label lbOf = new Label(resources.getString("of"));
            lbOf.setFont(HK_GROTESK.get(18));
            lbOf.setPadding(new Insets(0, 3, 0, 3));

            tfArtistName.getChildren().addAll(lbOf, lbParentArtist);
        }

        boolean hasMembers = !this.artist.getMemberArtists().isEmpty();
        if (hasMembers) {
            for (Artist member : this.artist.getMemberArtists()) {
                Label lbMember = new ClickableArtistName(member, this).createLabel();
                this.bindIcons(Map.of(lbMember, "artist"));

                if (!this.tfMembers.getChildren().isEmpty()) {
                    lbMember.setPadding(new Insets(0, 0, 0, 3));
                }

                tfMembers.getChildren().addAll(lbMember);
            }
        } else {
            this.tfMembers.setVisible(false);
            this.tfMembers.setManaged(false);
        }

        SongPopup popup = new SongPopup(new PopupState<>(), resources);

        ObservableList<Song> songs = FXCollections.observableList(new LinkedList<>());
        ResizeButtonBinder.INSTANCE.initButtons(List.of(
                this.btShuffleAll,
                this.btPlayNext,
                this.btAddToPlayingQueue,
                this.btAddToPlaylist
        ), songs, resources, ColorThemeManager.getAccentColor());

        this.ivArtist.setFitWidth(IV_SIZE);
        this.ivArtist.setFitHeight(IV_SIZE);

        this.spArtist.setOnMouseEntered(evt -> {
            if (!this.artist.isLoadingImage()) {
                this.ivArtist.setEffect(StyleConstants.BOX_BLUR);
                hbArtistImageControls.setVisible(true);
            }
        });

        this.spArtist.setOnMouseExited(evt -> {
            this.ivArtist.setEffect(null);
            hbArtistImageControls.setVisible(false);
        });

        this.scrollDownAnimation = new Timeline(
                new KeyFrame(Duration.millis(390),
                        new KeyValue(this.spContent.vvalueProperty(), 1)));

        if (!this.loadManager.isRunning()) {
            this.loadManager.start();
        }

        if (this.artist.getArtistImage() == null) {
            this.fetchArtistImage();
        } else {
            this.updateArtistImage(this.artist.getArtistImage());
        }

        this.lbArtist.setText(this.artist.getName());

        this.lbSongNumber.setText(PluralHelper.getString(resources, "%d %s", this.artist.getSongs(), "song", "songs"));
        this.lbAlbumNumber.setText(PluralHelper.getString(resources, "%d %s", this.artist.getAlbums(), "album", "albums"));
        this.lbLength.setText(this.artist.getFormattedLength());

        this.bindIcons(Map.of(
                this.lbSongNumber, "music_note",
                this.lbAlbumNumber, "album",
                this.lbLength, "calendar"));


        this.gpContent.setPadding(new Insets(0, ALBUM_PADDING, 0, ALBUM_PADDING));

        GridPane gp = new GridPane();
        gp.setAlignment(Pos.CENTER);
        gp.setPadding(new Insets(10, 0, 0, 0));
        gp.setVgap(V_GAP);


        List<AlbumGroup> albumGroups = new ArrayList<>();
        for (Album a : this.artist.getAlbums()) {
            albumGroups.add(new AlbumGroup(a));
        }

        albumGroups.sort(Comparator.comparing(AlbumGroup::getYear).reversed());

        List<VBox> clickableCovers = albumGroups.stream().map(ag -> {
            VBox box = new ArtistAlbumCover(ag.getAlbum()).build();
            box.setOnMouseClicked(evt -> {
                this.closeDialog();
                AlbumContentController.show(ag.getAlbum(), null);
            });

            return box;
        }).collect(Collectors.toList());

        this.gpRoot.widthProperty().addListener((observable, oldValue, newValue) -> {
            gp.getChildren().clear();
            gp.getRowConstraints().clear();
            gp.getColumnConstraints().clear();

            int coverGridColNum = (int) ((this.gpRoot.getWidth() - ALBUM_PADDING * 2) / (IV_SIZE + ALBUM_PADDING + H_GAP));

            int col = 0;
            int row = 0;
            for (int i = 0; i < coverGridColNum; i++) {
                ColumnConstraints cc = new ColumnConstraints();
                cc.setPercentWidth(100d / coverGridColNum);
                cc.setHalignment(HPos.CENTER);
                gp.getColumnConstraints().add(cc);
            }

            gp.getRowConstraints().add(this.createRowConstraint());

            for (VBox box : clickableCovers) {
                if (col > 0 && col % coverGridColNum == 0) {
                    row++;
                    col = 0;

                    gp.getRowConstraints().add(this.createRowConstraint());
                }

                gp.add(box, col++, row, 1, 1);
            }
        });

        // some albums do not "belong" to an artist, so we separately add the songs the artist is featured on
        for (AlbumGroup ag : albumGroups) {
            songs.addAll(ag.getSongs());
        }

        for (Song s : this.artist.getSongs()) {
            if (!songs.contains(s)) {
                songs.add(s);
            }
        }

        JFXListView<Song> lvSongs = this.createScrollList(songs, popup);

        int rowCounter = 0;
        if (!this.artist.getAlbums().isEmpty()) {
            Label lbAlbums = new Label(resources.getString("albums"));
            lbAlbums.setFont(HK_GROTESK.get(18));

            this.gpContent.add(lbAlbums, 0, rowCounter++);
            this.gpContent.add(gp, 0, rowCounter++);
        }

        if (!this.artist.getSongs().isEmpty()) {
            Label lbSongs = new Label(resources.getString("songs"));
            lbSongs.setFont(HK_GROTESK.get(18));

            this.gpContent.add(lbSongs, 0, rowCounter++);
            this.gpContent.add(lvSongs, 0, rowCounter++);
        }

        for (Artist groupMember : this.artist.getMemberArtists()) {
            JFXListView<Song> lvMemberSongs = this.createScrollList(FXCollections.observableList(groupMember.getSongs()), popup);

            if (!groupMember.getSongs().isEmpty()) {
                Label lbSongs = new Label(groupMember.getName());
                lbSongs.setFont(HK_GROTESK.get(18));

                this.bindIcons(Map.of(lbSongs, "artist"));

                this.gpContent.add(lbSongs, 0, rowCounter++);
                this.gpContent.add(lvMemberSongs, 0, rowCounter++);
            }
        }
    }

    private JFXListView<Song> createScrollList(ObservableList<Song> list, Popup<Song> popup) {
        JFXListView<Song> listView = new JFXListView<>();
//        listView.skinProperty().addListener((observable, o, n) -> {
//            ScrollBar verticalBar = (ScrollBar) listView.lookup(".scroll-bar:vertical");
//            verticalBar.valueProperty().addListener((obs, oldValue, newValue) -> {
//                if (this.spContent.getVvalue() != 1 && newValue.doubleValue() > oldValue.doubleValue()) {
//                    this.scrollDownAnimation.play();
//                }
//            });
//        });

        StylesheetUtil.applyStylesheet(listView, "listview");

        SongSelector songSelector = new SongSelector(listView, popup);

        ItemFetcher<Song> itemFetcher = new ItemFetcher<>(list, Comparator.comparing(Song::getTitle), false);
        itemFetcher.setListView(listView);
        itemFetcher.fetchItems(FETCH_SIZE, ItemFetcher.FetchType.INITIAL);

        listView.setItems(itemFetcher.getBaseList());
        listView.setOnScroll(evt -> itemFetcher.fetchItems(FETCH_SIZE, ItemFetcher.FetchType.SCROLL));
        listView.setCellFactory(c -> new DefaultSongListCell(Song.FormatStyle.TITLE_ALBUM, resources));
        listView.setOnMouseClicked(songSelector::select);

        return listView;
    }

    static class AlbumGroup {
        private final Album album;
        private final List<Song> songs = new ArrayList<>();

        public AlbumGroup(Album album) {
            this.album = album;
            this.songs.addAll(album.getSongs());
            this.songs.sort(Comparator.comparing(Song::getNumericTrack));
        }

        public Album getAlbum() {
            return album;
        }

        public String getYear() {
            return this.album.getYear();
        }

        public List<Song> getSongs() {
            return songs;
        }

    }

    private RowConstraints createRowConstraint() {
        return new RowConstraints(ROW_HEIGHT, ROW_HEIGHT, ROW_HEIGHT);
    }
}
