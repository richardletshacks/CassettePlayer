package fe.materialplayer.controller.sidebar;

import com.mpatric.mp3agic.Mp3File;
import com.mpatric.mp3agic.exception.InvalidDataException;
import com.mpatric.mp3agic.exception.UnsupportedTagException;
import fe.logger.Logger;
import fe.materialplayer.data.dao.helper.SongDaoHelper;
import fe.materialplayer.data.indexable.Album;
import fe.materialplayer.data.indexable.Artist;
import fe.materialplayer.data.indexable.Genre;
import fe.materialplayer.data.indexable.Song;
import fe.materialplayer.music.*;
import fe.materialplayer.music.remover.LibrarySongRemover;
import fe.materialplayer.util.HashHelper;
import fe.mp3taglib.ArtistTagHelper;
import fe.mp3taglib.TagInfo;
import fe.mp3taglib.TagParser;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.concurrent.Task;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.sql.SQLException;
import java.util.*;

public class SongUpdateTask extends Task<Void> {


    private final MusicLibrary library = MusicLibrary.Companion.getInstance();
    private final SongDaoHelper songDao = Objects.requireNonNull(this.library).getSongDao();

    private final Logger logger = new Logger("SongUpdateTask", true);

    @Override
    protected Void call() {
        Map<Song, Integer> changed = new HashMap<>();

        this.updateProgress(0, 100);

        TaskLoopHelper tlhHash = new TaskLoopHelper(this.songDao.getItems().size(), 0, 70);
        for (Song s : this.songDao.getItems()) {
            try {
                int hash = HashHelper.hashFile(s.getFile());
                if (hash != s.getHash()) {
                    changed.put(s, hash);
                    this.logger.print(Logger.Type.INFO, "Song %s (%d) is different from file (%d)", s, s.getHash(), hash);
                }

                this.updateProgress(tlhHash.next(), 100);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        this.updateProgress(70, 100);

        TaskLoopHelper tlhUpdate = new TaskLoopHelper(changed.entrySet().size(), 70, 30);
        for (Map.Entry<Song, Integer> ent : changed.entrySet()) {
            LibrarySongRemover lsr = new LibrarySongRemover();
            Song song = ent.getKey();
            try {
                boolean updated = this.updateSong(song, song.getFile(), lsr);
                if (updated) {
                    this.logger.print(Logger.Type.INFO, "%s has been updated", song);
                    song.setHash(ent.getValue());

                    this.songDao.updateAndRefresh(song);
                    lsr.executeAll();
                }

                this.updateProgress(tlhUpdate.next(), 100);
            } catch (InvalidDataException | IOException | UnsupportedTagException | SQLException e) {
                e.printStackTrace();
            }
        }

        this.updateProgress(100, 100);

        return null;
    }

    static class TaskLoopHelper {
        private final double startAt, increase;
        private int counter;

        public TaskLoopHelper(int size, double startAt, double progressCap) {
            this.increase = progressCap / size;
            this.startAt = startAt;
        }

        public double next() {
            return this.startAt + (this.increase * ++this.counter);
        }
    }

    public boolean updateSong(Song song, File file, LibrarySongRemover lsr) throws InvalidDataException, IOException, UnsupportedTagException, SQLException {
        Mp3File mp3File = new Mp3File(file);
        TagInfo tagInfo = TagParser.INSTANCE.getTags(mp3File, file.getName());
        if (tagInfo != null) {
            boolean modified = false;

            boolean artistChanged = false;
            boolean albumChanged = false;

            String title = tagInfo.getTitle();
            if (!song.getTitle().equals(title)) {
                this.logger.print(Logger.Type.INFO, "Title: current: %s, new: %s", song.getTitle(), title);
                song.setTitle(title);
                modified = true;
            }

            String year = tagInfo.getYear();
            if (!song.getYear().equals(year)) {
                this.logger.print(Logger.Type.INFO, "Year: current: %s, new: %s", song.getYear(), year);

                song.setYear(year);
                modified = true;
            }

            Map<String, List<String>> artistNames = tagInfo.getArtist();

            if (!song.getArtistName().equals(tagInfo.getOriginalArtistTagString())) {
                lsr.getArtistChecker().check(song);

                List<Artist> artists = new ArrayList<>();
                for (Map.Entry<String, List<String>> artistGroup : artistNames.entrySet()) {
                    String group = artistGroup.getKey();
                    Artist groupArtist = null;

                    if (!group.equals(ArtistTagHelper.NO_ARTIST_GROUP)) {
                        BooleanProperty groupArtistExisted = new SimpleBooleanProperty(true);
                        groupArtist = SongFactory.createArtist(group, groupArtistExisted);
                        if (!groupArtistExisted.get()) {
                            this.library.getArtistDao().createAndRefresh(groupArtist);
                        }

                        logger.print(Logger.Type.INFO, "\tFound group artist %s", groupArtist.getName());
                    }

                    for (String artistName : artistGroup.getValue()) {
                        BooleanProperty artistExisted = new SimpleBooleanProperty(true);
                        Artist artist = SongFactory.createArtist(artistName, artistExisted);
                        if (groupArtist != null) {
                            artist.setParentArtist(groupArtist);
                        }

                        if (!artistExisted.get()) {
                            this.library.getArtistDao().createAndRefresh(artist);
                        }

                        artists.add(artist);
                    }


                }

                logger.print(Logger.Type.INFO, "\tArtist: current: %s, new: %s", song.getArtistName(), Artist.createNameString(artists));

                song.setArtists(artists);
                modified = true;
                artistChanged = true;
            }

            BooleanProperty albumExisted = new SimpleBooleanProperty(true);

            String albumName = tagInfo.getAlbum();
            if (!song.getAlbumName().equals(albumName)) {
                lsr.getAlbumChecker().check(song);

                //remove the old album from the artists, albumChecker does NOT handle this
                for (Artist a : song.getAlbum().getArtists()) {
                    a.getAlbums().remove(song.getAlbum());
                }

                this.logger.print(Logger.Type.INFO, "Album: current: %s, new: %s", song.getAlbumName(), albumName);

                Album album = SongFactory.createAlbum(albumName, song.getArtists(), albumExisted, song.getYear());
                modified = true;

                File albumCoverDir = SongFactory.createCoverDir(song.getArtists().get(0).getName(), albumName);
                if (tagInfo.getAlbumImage() != null && album.getCoverDir() == null) {
                    CoverExtractTask cet = new CoverExtractTask(album, albumCoverDir, tagInfo.getAlbumImage());
                    if (cet.extract()) {
                        logger.print(Logger.Type.INFO, "Extracting cover succeeded");
                    } else {
                        logger.print(Logger.Type.ERROR, "Failed to extract cover for " + albumName);
                    }
                }


                if (!albumExisted.get()) {
                    album = this.library.getAlbumDao().createAndRefresh(album);
                }

                song.setAlbum(album);
                albumChanged = true;
            }

            if (artistChanged) {
                library.getArtistSongDao().batchCreateAndSetup(Collections.singletonList(song));
            }

            if (artistChanged || albumChanged) {
                //when the artist has changed, the album requires the new artists; if the album has changed,
                // it has also been refreshed (see above), so the artist list is not populated => populate it
                song.getAlbum().getArtists().clear();
                song.getAlbum().getArtists().addAll(song.getArtists());

                library.getArtistAlbumDao().batchCreateAndSetup(Collections.singletonList(song.getAlbum()), Collections.singletonList(song));
            }

            BooleanProperty genreExisted = new SimpleBooleanProperty(true);
            String genreName = tagInfo.getGenre();
            if (!song.getGenreName().equals(genreName)) {
                lsr.getGenreChecker().check(song);

                this.logger.print(Logger.Type.INFO, "Genre: current: %s, new: %s", song.getGenre(), genreName);

                Genre genre = SongFactory.createGenre(genreName, genreExisted);

                if (!genreExisted.get()) {
                    genre = this.library.getGenreDao().createAndRefresh(genre);
                }

                song.setGenre(genre);
                modified = true;
            }

            String track = tagInfo.getTrack();
            if (!song.getTrack().equals(track)) {
                this.logger.print(Logger.Type.INFO, "Track: current: %s, new: %s", song.getTrack(), track);

                song.setTrack(track);
                modified = true;
            }
            if (mp3File.hasId3v2Tag()) {
                File coverFile = song.getCoverFile(Song.CoverType.ORIGINAL);
                if (coverFile != null && tagInfo.getAlbumImage() != null) {
                    byte[] current = Files.readAllBytes(coverFile.toPath());

                    if (!Arrays.equals(tagInfo.getAlbumImage().getBytes(), current)) {
                        Album album = song.getAlbum();

                        this.logger.print(Logger.Type.INFO, "Song: %s, cover on disk is different from mp3 tag cover", song);

                        CoverExtractTask cet = new CoverExtractTask(album, new File(album.getCoverDir()), tagInfo.getAlbumImage());
                        if (cet.extract()) {
                            logger.print(Logger.Type.INFO, "Extracting cover succeeded");
                        } else {
                            logger.print(Logger.Type.ERROR, "Failed to extract cover for " + albumName);
                        }

                        album.setCoverMimeType(SongFactory.extractMimeType(mp3File.getId3v2Tag().getAlbumImageMimeType()));
                        List<Artist> artists = album.getArtists();
                        album = this.library.getAlbumDao().updateAndRefresh(album);
                        album.getArtists().addAll(artists);


                        modified = true;
                    }
                }
            }
            return modified;

        } else {
            this.logger.print(Logger.Type.ERROR, "Taginfo is null for %s", file);
            return false;
        }
    }
}
