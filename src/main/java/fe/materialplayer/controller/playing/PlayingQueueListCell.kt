package fe.materialplayer.controller.playing

import fe.materialplayer.controller.util.cell.impl.UpDownButtonCell
import fe.materialplayer.data.indexable.Song
import fe.materialplayer.data.indexable.base.BackgroundColorable
import fe.materialplayer.music.MusicPlayer
import fe.materialplayer.util.properties.ColorThemeManager
import javafx.beans.binding.Bindings
import javafx.geometry.HPos
import javafx.geometry.Pos
import javafx.scene.control.ContentDisplay
import javafx.scene.control.Label
import javafx.scene.control.Tooltip
import javafx.scene.paint.Color
import java.util.concurrent.Callable

class PlayingQueueListCell : UpDownButtonCell(Song.FormatStyle.TITLE_ARTIST_ALBUM) {
    private val cellTooltip = Tooltip()

    override fun bindLabel(label: Label, lineType: LineType, item: Song) {
        label.maxWidth = this.listView.width - 150
        label.textFillProperty().bind(
            Bindings.createObjectBinding(
                Callable {
                    if (this.selected.get() || this.hover.get()) {
                        return@Callable BackgroundColorable.getTextColor(item)
                    }

                    if (index - PLAYER.currentIndex < 1) {
                        return@Callable NEGATIVE_QUEUE_COLOR
                    }

                    return@Callable lineType.acquireColor.invoke()
                }, this.selected, this.hover, ColorThemeManager.theme, PLAYER.currentPointerProperty()
            )
        )
    }

    override fun updateItem(item: Song?, empty: Boolean) {
        super.updateItem(item, empty)
        if (empty || item == null) {
            text = null
            graphic = null
            tooltip = null
        } else {
            val buttonBox = makeButtonBox(item,
                { PLAYER.moveItem(index, -1) },
                { PLAYER.moveItem(index, 1) },
                { PLAYER.removeFromQueue(index) }
            )

            makeListeners(item)

            val vbox = this.makeLabelBox(item, item.getBoxLines(formatType))

            val displayIndexLabel = Label().apply {
                this.textProperty().bind(Bindings.createStringBinding({
                    "${index - PLAYER.currentIndex}"
                }, PLAYER.currentPointerProperty()))

                bindLabel(this, LineType.TITLE, item)

                alignment = Pos.CENTER
                setMinSize(20.0, 20.0)
            }

            val gp = makeBasicGrid(displayIndexLabel, vbox).also {
                val ccIv = it.columnConstraints[0]
                ccIv.halignment = HPos.CENTER

                with(calcIndexWidth().toDouble()) {
                    ccIv.minWidth = this
                    ccIv.prefWidth = this
                }

                it.add(buttonBox, 2, 0)
            }

            contentDisplay = ContentDisplay.GRAPHIC_ONLY
            graphic = gp

            tooltip = cellTooltip.apply {
                text = item.getBoxLines(formatType).getOneliner()
            }
        }
    }

    companion object {
        private const val BASE_WIDTH = 20

        //ghetto string width calculation lmao
        //2 chars = 20 width
        fun calcIndexWidth(): Int {
            var width = BASE_WIDTH
            (0..("${PLAYER.queue.size}".length - 2)).forEach { _ ->
                width += 5
            }

            return width
        }


        private val NEGATIVE_QUEUE_COLOR = Color.valueOf("#909090")
        private val PLAYER = MusicPlayer.getInstance()
    }
}