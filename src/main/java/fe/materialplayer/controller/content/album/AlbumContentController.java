package fe.materialplayer.controller.content.album;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXListView;
import com.jfoenix.controls.JFXPopup;
import fe.materialplayer.controller.content.ContentController;
import fe.materialplayer.controller.content.resize.ResizeButtonBinder;
import fe.materialplayer.controller.css.annotation.theme.ThemeTextColor;
import fe.materialplayer.controller.css.annotation.theme.ThemeBackgroundColor;
import fe.materialplayer.controller.util.PluralHelper;
import fe.materialplayer.controller.util.SongSelector;
import fe.materialplayer.controller.util.clickable.artist.ClickableArtistName;
import fe.materialplayer.controller.util.cell.DefaultSongListCell;
import fe.materialplayer.controller.util.dialog.DialogUtil;
import fe.materialplayer.controller.util.popup.PopupState;
import fe.materialplayer.controller.util.popup.impl.SongPopup;
import fe.materialplayer.data.indexable.Album;
import fe.materialplayer.data.indexable.Artist;
import fe.materialplayer.data.indexable.Song;
import fe.materialplayer.util.properties.ColorThemeManager;
import fe.materialplayer.util.image.ImageUtil;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.scene.text.TextFlow;
import org.jetbrains.annotations.NotNull;

import java.net.URL;
import java.util.*;

public class AlbumContentController extends ContentController {

    @FXML
    @ThemeTextColor
    private VBox vbLabels;

    @FXML
    @ThemeTextColor
    private Label lbSongs;

    @FXML
    private ImageView ivAlbum;

    @FXML
    private Label lbAlbum;

    @FXML
    private Tooltip tpAlbumName;

    @FXML
    private TextFlow tfArtists;

    @FXML
    private Label lbSongNumber;

    @FXML
    private Label lbLength;

    @FXML
    private Label lbYear;

    @FXML
    private JFXButton btShuffleAll;

    @FXML
    private JFXButton btPlayNext;

    @FXML
    private JFXButton btAddToPlayingQueue;

    @FXML
    private JFXButton btAddToPlaylist;

    @FXML
    @ThemeBackgroundColor
    private JFXListView<Song> lvSongs;

    private final ObservableList<Song> items = FXCollections.observableArrayList();

    private final Album album;

    private static final int IV_SIZE = 110;
    private static final int IMAGE_ROUNDING = 7;

    private SongSelector songSelector;

    public AlbumContentController(Album album) {
        super(true, true);
        this.album = album;
    }

    public static void show(Album album, JFXPopup popup) {
        DialogUtil.loadDialog("content/AlbumContent", popup, new AlbumContentController(album));
    }

    @Override
    public void initialize(@NotNull URL location, @NotNull ResourceBundle resources) {
        super.initialize(location, resources);

        SongPopup popup = new SongPopup(new PopupState<>(), resources);

        this.items.addAll(this.album.getSongs());
        this.items.sort(Comparator.comparing(Song::getTitle, String.CASE_INSENSITIVE_ORDER));
        this.items.sort(Comparator.comparing(Song::getNumericTrack));

        this.lvSongs.setItems(this.items);
        this.songSelector = new SongSelector(this.lvSongs, popup);

        ResizeButtonBinder.INSTANCE.initButtons(List.of(
                this.btShuffleAll,
                this.btPlayNext,
                this.btAddToPlayingQueue,
                this.btAddToPlaylist
        ), this.items, resources, ColorThemeManager.getAccentColor());

        this.ivAlbum.setFitWidth(IV_SIZE);
        this.ivAlbum.setFitHeight(IV_SIZE);

        this.ivAlbum.setImage(this.album.getCover(Song.CoverType.S160X160));
        ImageUtil.roundImage(this.ivAlbum, IMAGE_ROUNDING, IMAGE_ROUNDING);

        Map<Artist, List<Artist>> parentArtists = new HashMap<>();
        for (Artist a : this.album.getArtists()) {
            parentArtists.computeIfAbsent(a.getParentArtist(), s -> new ArrayList<>()).add(a);
        }

        for (Map.Entry<Artist, List<Artist>> ent : parentArtists.entrySet()) {
            for (int i = 0; i < ent.getValue().size(); i++) {
                Artist a = ent.getValue().get(i);

                Label lbArtist = new ClickableArtistName(a, this).createLabel();
                this.bindIcons(Map.of(lbArtist, "artist"));

                tfArtists.getChildren().add(lbArtist);
            }

            if (ent.getKey() != null) {
                tfArtists.getChildren().addAll(new Label("of"), new ClickableArtistName(ent.getKey(), this).createLabel());
            }
        }

        for (int i = 1; i < tfArtists.getChildren().size(); i++) {
            Label label = (Label) tfArtists.getChildren().get(i);
            label.setPadding(new Insets(0, 0, 0, 3));
        }

        this.lbAlbum.setText(this.album.getName());
        this.tpAlbumName.setText(this.album.getName());
        this.tpAlbumName.setStyle("-fx-text-fill: white");

        this.lbSongNumber.setText(PluralHelper.getString(resources, "%d %s", this.album.getSongs(), "song", "songs"));
        this.lbLength.setText(this.album.getFormattedLength());
        this.lbYear.setText(this.album.getYear());

        this.lvSongs.setCellFactory(arg0 -> new DefaultSongListCell(Song.FormatStyle.TITLE_ARTIST_ALBUM, resources));
        this.lvSongs.setOnMouseClicked(this.songSelector::select);

        this.bindIcons(Map.of(
                this.lbSongNumber, "music_note",
                this.lbLength, "timer",
                this.lbYear, "calendar"));
    }


}
