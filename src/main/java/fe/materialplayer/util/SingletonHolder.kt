package fe.materialplayer.util

import java.lang.Exception

/**
 * Taken from https://blog.mindorks.com/how-to-create-a-singleton-class-in-kotlin
 */
open class SingletonHolder<out T : Any, in A>(creator: (A) -> T) {
    private var creator: ((A) -> T)? = creator

    @Volatile
    private var instance: T? = null

    fun getInstance(): T {
        instance?.let {
            return it
        } ?: throw Exception("Singleton $creator has not been initialized yet")
    }

    fun createInstance(arg: A): T {
        val checkInstance = instance
        if (checkInstance != null) {
            return checkInstance
        }

        return synchronized(this) {
            val checkInstanceAgain = instance
            if (checkInstanceAgain != null) {
                checkInstanceAgain
            } else {
                val created = creator!!(arg)
                instance = created
                creator = null
                created
            }
        }
    }
}