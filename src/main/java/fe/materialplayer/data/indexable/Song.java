package fe.materialplayer.data.indexable;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import fe.materialplayer.controller.util.cell.FormattablePair;
import fe.materialplayer.data.indexable.base.Artistable;
import fe.materialplayer.data.indexable.base.BackgroundColorable;
import fe.materialplayer.data.persister.LocalDateTimePersister;
import fe.materialplayer.data.sort.SortType;
import fe.materialplayer.util.resource.ResourceBundleHelper;
import fe.materialplayer.util.Util;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.image.Image;
import javafx.scene.media.Media;
import javafx.scene.paint.Color;

import java.io.File;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@DatabaseTable(tableName = "song")
public class Song extends Artistable implements BackgroundColorable {
    private String[] compareValues;

    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField
    private String title;

    private final ObservableList<Artist> artists = FXCollections.observableArrayList();

    @DatabaseField(canBeNull = false, foreign = true, foreignAutoRefresh = true)
    private Album album;

    @DatabaseField(canBeNull = false, foreign = true, foreignAutoRefresh = true)
    private Genre genre;

    @DatabaseField
    private String track;

    @DatabaseField
    private double length;

    @DatabaseField
    private String path;

    private File file;

    @DatabaseField
    private int hash;

    @DatabaseField
    private boolean hidden;

    @DatabaseField(persisterClass = LocalDateTimePersister.class)
    private LocalDateTime favoritedAt;

    public Song() {
    }

    public Song(String title, List<Artist> artists, Album album, String track, Genre genre, double length, String path,
                int hash) {
        this.title = title;
        this.artists.addAll(artists);
        this.album = album;
        this.track = track;
        this.genre = genre;
        this.length = length;
        this.path = path;
        this.hash = hash;
    }

    public static final SortType<Song> DEFAULT_SORT_TYPE = new SortType<>("Title", Comparator.comparing(Song::getTitle, String.CASE_INSENSITIVE_ORDER));
    public static final List<SortType<Song>> SORT_TYPES = List.of(
            new SortType<>("Artist", Comparator.comparing(Song::getArtistName, String.CASE_INSENSITIVE_ORDER)),
            new SortType<>("Album", Comparator.comparing(Song::getAlbumName, String.CASE_INSENSITIVE_ORDER)),
            DEFAULT_SORT_TYPE,
            new SortType<>("Year", Comparator.comparing(Song::getYear, String.CASE_INSENSITIVE_ORDER)));


    public LocalDateTime getFavoritedAt() {
        return favoritedAt;
    }

    public boolean isFavorite() {
        return this.favoritedAt != null;
    }

    public Genre getGenre() {
        return genre;
    }

    public Album getAlbum() {
        return album;
    }

    public String getTrack() {
        if (this.track == null) {
            return "-";
        }

        return track;
    }

    public int getNumericTrack() {
        String track = this.getTrack();
        if (track != null) {
            try {
                return Integer.parseInt(this.getTrack());
            } catch (NumberFormatException ignored) {
            }
        }

        return -1;
    }

    public String getPath() {
        return path;
    }

    public int getHash() {
        return hash;
    }

    public String getTitle() {
        return title;
    }

    public String getGenreName() {
        if (this.genre == null) {
            return ResourceBundleHelper.getStringKey("unknownGenre");
        }

        return this.genre.getName();
    }

    public String getAlbumName() {
        if (this.album == null) {
            return ResourceBundleHelper.getStringKey("unknownAlbum");
        }

        return this.album.getName();
    }

    public String getYear() {
        if (this.album == null || this.album.getYear() == null) {
            return "Unknown Year";
        }

        return this.album.getYear();
    }

    public Color getTextColor() {
        return this.isWhiteText() ? Color.WHITE : Color.BLACK;
    }

    public boolean isWhiteText() {
        if (this.album == null) {
            return false;
        }

        return this.album.isWhiteText();
    }

    public Image loadSongCover(CoverType type) {
        return this.album.getCover(type);
    }

    public File getCoverFile(CoverType type) {
        return this.album.getCoverFile(type);
    }

    public String getCoverMimeType() {
        return this.album.getCoverMimeType();
    }

    public void setFavoritedAt(LocalDateTime favoritedAt) {
        this.favoritedAt = favoritedAt;
    }

    public void setPath(File file) {
        this.path = file.getAbsolutePath();
        if (this.file != null) {
            this.file = file;
        }
    }

    public File getFile() {
        if (this.file == null) {
            this.file = new File(this.path);
        }

        return file;
    }

    public Media toMedia() {
        return new Media(Util.fileToUriString(this.getFile()));
    }

    public boolean isHidden() {
        return hidden;
    }

    public void setHidden(boolean hidden) {
        this.hidden = hidden;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    public void addArtist(Artist... artists) {
        this.artists.addAll(artists);
    }


    public void setYear(String year) {
        if (this.album != null) {
            this.album.setYear(year);
        }
    }

    public void setGenre(Genre genre) {
        this.genre = genre;
    }

    public void setTrack(String track) {
        this.track = track;
    }

    public void setAlbum(Album album) {
        this.album = album;
    }

    public void setHash(int hash) {
        this.hash = hash;
    }

    @Override
    public boolean isSearchResultForText(String text) {
        return !this.hidden && super.isSearchResultForText(text);
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public String getHexBackgroundColor() {
        if (this.album == null) {
            return "#ffffff";
        }

        return this.album.getHexBackgroundColor();
    }

    @Override
    public Color getBackgroundColor() {
        if (this.album == null) {
            return Color.WHITE;
        }

        return this.album.getBackgroundColor();
    }

    @Override
    public ObservableList<Artist> getArtists() {
        return artists;
    }

    @Override
    public String getArtistName() {
        return Artist.createNameString(this.artists);
    }

    @Override
    public String toString() {
        return String.format("%s - %s by %s (%s)", this.title, this.getAlbumName(), this.getArtistName(), this.getYear());
    }

    @Override
    protected String[] compareArray() {
        return new String[]{this.title, this.getAlbumName(), this.getArtistName(), this.getGenreName()};
    }

    @Override
    public Collection<Song> toSongList() {
        return Collections.singleton(this);
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof Song) {
            return ((Song) o).id == this.id;
        }

        return false;
    }

    @Override
    public double getLength() {
        return this.length;
    }

    @Deprecated
    public String format(FormatStyle style) {
        switch (style) {
            case TITLE_ARTIST_ALBUM:
                return String.format("%s\n%s \uD83D\uDF84 %s", this.title, this.getArtistName(), this.getAlbumName());
            case TITLE_ALBUM:
                return String.format("%s\n%s", this.title, this.getAlbumName());
            case TITLE_DURATION:
                return String.format("%s\n%s", this.title, this.getFormattedLength());
        }

        return null;
    }

    public static final String SMALL_BLACK_CIRCLE = "⦁";

    public FormattablePair getBoxLines(FormatStyle style) {
        switch (style) {
            case TITLE_ARTIST_ALBUM:
                return new FormattablePair(this.title, String.format("%s %s %s", this.getArtistName(), SMALL_BLACK_CIRCLE, this.getAlbumName()));
            case TITLE_ALBUM:
                return new FormattablePair(this.title, this.getAlbumName());
            case TITLE_DURATION:
                return new FormattablePair(this.title, this.getFormattedLength());
        }

        return null;
    }

    public void setArtists(List<Artist> artists) {
        this.artists.clear();
        this.artists.addAll(artists);
    }

    public enum FormatStyle {
        TITLE_ARTIST_ALBUM, TITLE_ALBUM, ALBUM_ARTIST_SONG_NUM, TITLE_DURATION
    }

    public enum CoverType {
        ORIGINAL("original", -1),
        ORIGINAL_300X300("original", 300),
        S60X60("size_60", 60),
        S160X160("size_160", 160);

        private final String str;
        private final int size;

        CoverType(String str, int size) {
            this.str = str;
            this.size = size;
        }

        public int getSize() {
            return size;
        }

        @Override
        public String toString() {
            return this.str;
        }
    }
}
