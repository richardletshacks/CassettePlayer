package fe.materialplayer.controller.content.resize.button

import com.jfoenix.controls.JFXButton
import javafx.event.EventHandler
import javafx.scene.image.Image
import javafx.scene.input.MouseEvent

class DefaultResizeButton(button: JFXButton, event: EventHandler<in MouseEvent>, graphic: Image, text: String, maxSize: Int)
    : ResizeButton(button, event, graphic, text, maxSize) {
    init {
        increaseAnimation = makeTimeline(maxSize.toDouble())
        increaseAnimation!!.onFinished = EventHandler { this.button.text = this.text }

        button.onMouseEntered = EventHandler {
            decreaseAnimation.stop()
            increaseAnimation!!.play()
        }
    }
}