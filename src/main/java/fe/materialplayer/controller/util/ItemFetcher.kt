package fe.materialplayer.controller.util

import javafx.application.Platform
import javafx.collections.FXCollections
import javafx.collections.ListChangeListener
import javafx.collections.ObservableList
import javafx.collections.transformation.SortedList
import javafx.scene.control.ListView
import java.lang.Integer.min
import java.util.*


class ItemFetcher<T>(private val libraryList: ObservableList<T>,
                     var comparator: Comparator<T>, val autoRefetch: Boolean = false) {


    private var refetchQueued: Boolean = false
    private var fetching: Boolean = false
    private var pointer: Int = 0
    private var iterator: Iterator<T>
    var listView: ListView<T>? = null
        set(value) {
            field = value
            field!!.items = this.sortedViewItems

            if(this.refetchQueued){
                this.refetch()
                this.refetchQueued = false
            }
        }


    var sortedViewItems: SortedList<T>
    val baseList: ObservableList<T> = FXCollections.observableList(LinkedList())

    init {
        fetchers.add(this)

        this.sortedViewItems = this.newList(this.comparator)
        this.iterator = libraryList.listIterator()


        if (this.autoRefetch) {
            this.libraryList.addListener(ListChangeListener {
                this.refetch()
            })
        }
    }

    fun refetch() {
        if(this.listView == null && !this.refetchQueued){
            this.refetchQueued = true
        } else {
            Platform.runLater {
                this.reset(this.comparator)
                this.fetchItems(15, FetchType.CHANGE_LISTENER)
            }
        }
    }

    fun fetchItems(fetchSize: Int, fetchType: FetchType) {
        if (this.pointer < this.libraryList.size && fetchSize > 0) {
            this.fetching = true
            val to = min(this.pointer + fetchSize, this.libraryList.size)
            for (i in this.pointer until to) {
                try {
                    val next = this.iterator.next()
                    this.baseList.add(next)
                } catch (e: ConcurrentModificationException) {
                    //break loop if iterator throws exception because then we know libraryList changed; might not be best practice but fuck you
                    break;
                }
            }

            this.pointer += fetchSize
            this.fetching = false

        }
    }

    fun reset(comparator: Comparator<T>) {
        this.pointer = 0
        this.baseList.clear()
        this.iterator = this.libraryList.listIterator()
        this.comparator = comparator

        this.sortedViewItems = this.newList(comparator)
        this.listView?.items = this.sortedViewItems
    }

    private fun newList(comparator: Comparator<T>): SortedList<T> = SortedList(this.baseList, comparator)

    enum class FetchType {
        INITIAL, SCROLL, CHANGE_LISTENER;
    }

    companion object {
        private val fetchers = mutableListOf<ItemFetcher<out Any?>>()

        fun refetchAll() = fetchers.forEach {
            it.refetch()
        }
    }
}