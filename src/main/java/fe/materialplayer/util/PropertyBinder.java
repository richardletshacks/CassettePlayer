package fe.materialplayer.util;

import javafx.beans.Observable;
import javafx.beans.binding.Binding;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.ObjectBinding;
import javafx.beans.binding.StringBinding;
import javafx.scene.control.Label;
import javafx.scene.control.Labeled;
import javafx.scene.image.Image;
import javafx.scene.paint.Paint;
import javafx.scene.text.Text;

import java.util.concurrent.Callable;

public class PropertyBinder {

    public static void bindTextProperty(Callable<String> func, Observable dependency, Labeled... dependants) {
        PropertyBinder.bindTextProperty(Bindings.createStringBinding(func, dependency), dependants);
    }

    public static void bindTextProperty(StringBinding binding, Labeled... dependants) {
        for (Labeled dep : dependants) {
            dep.textProperty().bind(binding);
        }
    }
}
