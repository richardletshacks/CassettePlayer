package fe.materialplayer.util.image

import fe.materialplayer.data.indexable.Song.CoverType
import fe.materialplayer.music.SongFactory
import java.awt.image.BufferedImage
import java.io.File
import java.io.IOException
import javax.imageio.ImageIO

object ImageResizer {
    @Throws(IOException::class)
    fun resize(bi: BufferedImage, resizeSizes: Map<Int, CoverType>, dir: File, originalMimeType: String) {
        val width = bi.width
        val height = bi.height

        resizeSizes.forEach { (size, value) ->
            val file = SongFactory.createCoverFile(dir,
                    value.toString(),
                    originalMimeType)

            var w = size
            var h = size
            if (width > height) {
                h = size / (width / height)
            } else if (height > width) {
                w = size / (height / width)
            }

            with(BufferedImage(w, h, bi.type)) {
                this.createGraphics().also {
                    it.drawImage(bi, 0, 0, w, h, null)
                    it.dispose()
                }

                ImageIO.write(this, originalMimeType, file)
            }
        }
    }
}