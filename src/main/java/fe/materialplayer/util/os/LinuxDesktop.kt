package fe.materialplayer.util.os

enum class LinuxDesktop(val mprisSupport: Boolean, private vararg val desktopSess: String) {
    CINNAMON(true, "cinnamon", "cinnamon2d", "X-Cinnamon");

    companion object {
        fun detect(): LinuxDesktop? {
            return findBySess(listOfNotNull(
                    System.getenv("GDMSESSION"),
                    System.getenv("XDG_CURRENT_DESKTOP")
            ).firstOrNull())
        }

        fun findBySess(sess: String?): LinuxDesktop? {
            values().forEach {
                if (sess in it.desktopSess) {
                    return it
                }
            }

            return null
        }
    }
}