package fe.materialplayer.controller.content.genre;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXListView;
import fe.materialplayer.controller.content.ContentController;
import fe.materialplayer.controller.content.resize.ResizeButtonBinder;
import fe.materialplayer.controller.css.annotation.theme.ThemeBackgroundColor;
import fe.materialplayer.controller.css.annotation.theme.ThemeTextColor;
import fe.materialplayer.controller.util.ItemFetcher;
import fe.materialplayer.controller.util.PluralHelper;
import fe.materialplayer.controller.util.SongSelector;
import fe.materialplayer.controller.util.cell.DefaultSongListCell;
import fe.materialplayer.controller.util.dialog.DialogUtil;
import fe.materialplayer.controller.util.popup.PopupState;
import fe.materialplayer.controller.util.popup.impl.SongPopup;
import fe.materialplayer.data.indexable.Genre;
import fe.materialplayer.data.indexable.Song;
import fe.materialplayer.util.FormatUtil;
import fe.materialplayer.util.properties.ColorThemeManager;
import javafx.beans.binding.Bindings;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.VBox;
import org.jetbrains.annotations.NotNull;

import java.net.URL;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

public class GenreContentController extends ContentController {

    @FXML
    @ThemeTextColor
    private VBox vbLabels;

    @FXML
    @ThemeTextColor
    private Label lbSongs;

    @FXML
    private Label lbGenre;

    @FXML
    private Label lbSongNumber;

    @FXML
    private Label lbLength;

    @FXML
    private JFXButton btShuffleAll;

    @FXML
    private JFXButton btPlayNext;

    @FXML
    private JFXButton btAddToPlayingQueue;

    @FXML
    private JFXButton btAddToPlaylist;

    @FXML
    private JFXButton btDelete;

    @FXML
    private JFXButton btExport;

    @FXML
    @ThemeBackgroundColor
    private JFXListView<Song> lvSongs;

    private final Genre genre;

    private final IntegerProperty selectedIndex = new SimpleIntegerProperty();
    private final PopupState<Song> popupState = new PopupState<>();

    private static final int FETCH_SIZE = 15;
    private final ObservableList<Song> observableList = FXCollections.observableArrayList();

    private final ItemFetcher<Song> itemFetcher;

    private SongSelector songSelector;

    public GenreContentController(Genre genre) {
        super(true, true);

        this.genre = genre;
        this.observableList.addAll(genre.getSongs());
        this.itemFetcher = new ItemFetcher<>(this.observableList, Comparator.comparing(Song::getTitle), false);
    }

    public static void show(Genre genre) {
        DialogUtil.loadDialog("content/GenreContent",
                new GenreContentController(genre));
    }

    @Override
    public void initialize(@NotNull URL location, @NotNull ResourceBundle resources) {
        super.initialize(location, resources);

        this.itemFetcher.setListView(this.lvSongs);
        this.itemFetcher.fetchItems(FETCH_SIZE, ItemFetcher.FetchType.INITIAL);

        SongPopup popup = new SongPopup(this.popupState, resources);

        this.lvSongs.setItems(this.itemFetcher.getBaseList());
        this.songSelector = new SongSelector(this.lvSongs, popup);

        ResizeButtonBinder.INSTANCE.initButtons(List.of(
                this.btShuffleAll,
                this.btPlayNext,
                this.btAddToPlayingQueue,
                this.btAddToPlaylist
        ), observableList, resources, ColorThemeManager.getAccentColor());

        this.lbGenre.setText(this.genre.getName());

        this.lbSongNumber.textProperty().bind(Bindings.createStringBinding(() ->
                        PluralHelper.getString(resources, "%d %s", this.genre.getSongs(), "song", "songs"),
                this.genre.genreLengthProperty()));
        this.lbLength.textProperty().bind(Bindings.createStringBinding(() ->
                        FormatUtil.formatLength(this.genre.genreLengthProperty().get()),
                this.genre.genreLengthProperty()));

        this.bindIcons(Map.of(
                this.lbSongNumber, "music_note",
                this.lbLength, "timer"
        ));

        this.lvSongs.setCellFactory(c -> new DefaultSongListCell(Song.FormatStyle.TITLE_ARTIST_ALBUM, resources));
        this.lvSongs.setOnMouseClicked(event -> {
            int idx = this.lvSongs.getSelectionModel().getSelectedIndex();
            if (idx != -1) {
                this.selectedIndex.set(idx);
            }

            this.songSelector.select(event);
        });
    }

    @FXML
    public void lvSongsScrolled(ScrollEvent event) {
        this.itemFetcher.fetchItems(FETCH_SIZE, ItemFetcher.FetchType.SCROLL);
    }
}
