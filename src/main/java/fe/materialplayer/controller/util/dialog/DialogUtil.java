package fe.materialplayer.controller.util.dialog;

import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXPopup;
import fe.materialplayer.Main;
import fe.materialplayer.controller.MainController;
import fe.materialplayer.controller.base.dialog.ClosableDialogController;
import fe.materialplayer.controller.base.dialog.DialogController;
import fe.materialplayer.controller.base.dialog.UnclosableDialogController;
import fe.materialplayer.controller.misc.ConfirmActionController;
import fe.materialplayer.controller.view.playlist.controller.AddToPlaylistController;
import fe.materialplayer.data.indexable.Song;
import fe.materialplayer.data.indexable.base.Indexable;
import fe.materialplayer.scene.SceneManager;
import fe.materialplayer.util.resource.FxmlUtil;
import fe.materialplayer.util.resource.StylesheetUtil;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.concurrent.Task;
import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;

import java.io.IOException;
import java.util.List;
import java.util.Objects;

public class DialogUtil {
    private static final MainController MAIN_CONTROLLER = (MainController) Objects.requireNonNull(SceneManager.INSTANCE.findScene(MainController.class)).getController();

    public static void createAddToPlaylistDialog(Indexable indexable, JFXPopup popup) {
        DialogUtil.loadDialog("view/playlist/AddToPlaylist", popup, new AddToPlaylistController(indexable));
    }

    public static void createAddToPlaylistDialog(List<Song> songs, JFXPopup popup) {
        DialogUtil.loadDialog("view/playlist/AddToPlaylist", popup, new AddToPlaylistController(songs));
    }

    public static void createConfirmActionController(String resourceString, EventHandler<? super MouseEvent> handler) {
        DialogUtil.loadDialog("ConfirmAction", null, new ConfirmActionController(resourceString, handler));
    }

    public static JFXDialog loadDialog(String fxml, ClosableDialogController controller) {
        return DialogUtil.loadDialog(fxml, null, controller);
    }

    public static JFXDialog loadDialog(String fxml, JFXPopup popup, ClosableDialogController controller) {
        return DialogUtil.loadDialog(fxml, popup, controller, JFXDialog.DialogTransition.CENTER);
    }

    public static Task<JFXDialog> loadBackgroundDialog(String fxml, DialogController controller) {
        Task<JFXDialog> t = new Task<>() {
            @Override
            protected JFXDialog call() {
                JFXDialog d = loadDialog(fxml, null, controller, JFXDialog.DialogTransition.CENTER, false);
                if (d != null) {
                    Platform.runLater(d::show);
                }

                return d;
            }
        };
        new Thread(t).start();

        return t;
    }

    private static final int SCALING_PERCENTAGE = 90;

    public static JFXDialog loadDialog(String fxml, JFXPopup popup, DialogController controller, JFXDialog.DialogTransition transition, boolean show) {
        try {
            Pane pane = FxmlUtil.loadFxml(fxml, controller).load();
            if (controller.getScaled()) {
                pane.setMinWidth(getScaledWidth());
                pane.setMinHeight(getScaledHeight());

                //TODO: eval if these bindings need to be unbound on dialog close to avoid memory leak on dialog close
                pane.minHeightProperty().bind(Bindings.createDoubleBinding(DialogUtil::getScaledHeight, Main.getInstance().getStage().heightProperty()));
                pane.minWidthProperty().bind(Bindings.createDoubleBinding(DialogUtil::getScaledWidth, Main.getInstance().getStage().widthProperty()));
            }

            JFXDialog dialog = new JFXDialog(MAIN_CONTROLLER.getSpRoot(), pane, transition);
            if (controller != null) {
                controller.setDialog(dialog);
            }

            StylesheetUtil.applyStylesheet(dialog, "dialog");

            if (popup != null) {
                popup.hide();
            }

            if (show) {
                dialog.show();
            }

            return dialog;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static JFXDialog loadDialog(String fxml, JFXPopup popup, DialogController controller, JFXDialog.DialogTransition transition) {
        return loadDialog(fxml, popup, controller, transition, true);
    }

    private static double getScaledHeight() {
        return Main.getInstance().getStage().getHeight() / 100 * SCALING_PERCENTAGE;
    }

    private static double getScaledWidth() {
        return Main.getInstance().getStage().getWidth() / 100 * SCALING_PERCENTAGE;
    }

    public static void createUnclosableDialog(UnclosableDialogController controller, String fxml, JFXDialog.DialogTransition transition) {
        JFXDialog dialog = DialogUtil.loadDialog(fxml, null, controller, transition);
        controller.reset();

        if (dialog != null) {
            dialog.setOnDialogClosed(evt -> {
                if (!controller.hasMovedOn() && !controller.isDone()) {
                    DialogUtil.createUnclosableDialog(controller, fxml, transition);
                }
            });
        }
    }


}
