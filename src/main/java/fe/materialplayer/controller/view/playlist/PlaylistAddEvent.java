package fe.materialplayer.controller.view.playlist;

import fe.materialplayer.data.indexable.Playlist;
import javafx.event.Event;
import javafx.event.EventType;

public class PlaylistAddEvent extends Event {
    private final Playlist playlist;

    public PlaylistAddEvent(EventType<? extends Event> eventType, Playlist playlist) {
        super(eventType);
        
        this.playlist = playlist;
    }

    public Playlist getPlaylist() {
        return playlist;
    }
}
