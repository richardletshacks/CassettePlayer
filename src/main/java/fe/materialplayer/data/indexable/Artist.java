package fe.materialplayer.data.indexable;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import fe.materialplayer.controller.util.cell.FormattablePair;
import fe.materialplayer.data.indexable.base.Artistable;
import fe.materialplayer.data.indexable.base.IFormattedLines;
import fe.materialplayer.data.sort.SortType;
import fe.materialplayer.util.resource.ResourceBundleHelper;
import fe.materialplayer.util.Util;
import fe.mp3taglib.ArtistTagHelper;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.image.Image;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

import static fe.materialplayer.util.Util.useInputStream;

@DatabaseTable(tableName = "artist")
public class Artist extends Artistable implements IFormattedLines {
    @DatabaseField(generatedId = true)
    private int id;

    private final ObservableList<Song> songs = FXCollections.observableArrayList();
    private final ObservableList<Album> albums = FXCollections.observableArrayList();

    @DatabaseField
    private String name;

    @DatabaseField
    private String imagePath;

    @DatabaseField(foreign = true, foreignAutoRefresh = true)
    private Artist parentArtist;

    private final ObservableList<Artist> memberArtists = FXCollections.observableArrayList();

    @DatabaseField
    private String imageMimeType;

    private boolean loadingImage;


    public Artist(String name, String imagePath) {
        this.name = name;
        this.imagePath = imagePath;
        this.imageMimeType = SUPPORTED_ARTIST_IMAGE_FILE_EXTENSIONS.get(0);
    }

    public Artist() {
    }

    public static final SortType<Artist> DEFAULT_SORT_TYPE = new SortType<>("Name", Comparator.comparing(Artist::getName, String.CASE_INSENSITIVE_ORDER));
    public static final List<SortType<Artist>> SORT_TYPES = List.of(DEFAULT_SORT_TYPE);

    public static final int DEFAULT_ARTIST_IMAGE_SIZE = 110;
    public static final Image DEFAULT_ARTIST_IMAGE = useInputStream(Artist.class.getResourceAsStream("/icon/default_artist_art.png"),
            i -> new Image(i, DEFAULT_ARTIST_IMAGE_SIZE, DEFAULT_ARTIST_IMAGE_SIZE, false, false));

    private static final String ARTIST_IMAGE_BASE_NAME = "fetch";
    private static final List<String> SUPPORTED_ARTIST_IMAGE_FILE_EXTENSIONS = List.of("jpg", "jpeg", "png");

    private File imageFile;

    public Image downloadImage(String url) throws IOException {
        return useInputStream(new FileInputStream(Util.downloadFile(url, this.getImageFile())), Image::new);
    }

    public Image getArtistImage() {
        if (this.imagePath == null) {
            this.artistImage.set(DEFAULT_ARTIST_IMAGE);
            return DEFAULT_ARTIST_IMAGE;
        }

        if (this.artistImage.get() == null) {
            if (this.getImageFile().exists()) {
                try {
                    this.artistImage.set(useInputStream(new FileInputStream(this.imageFile), Image::new));
                } catch (FileNotFoundException ignored) {
                }
            }
        }

        return this.artistImage.get();
    }


    //will not update to database, you'll have to do that yourself
    public File updateImageMimeType(String imageMimeType) {
        this.imageMimeType = imageMimeType;
        this.imageFile = null;

        return this.getImageFile();
    }

    public File getImageFile() {
        if (this.imageFile == null) {
            File parentFolder = new File(this.imagePath);
            if (!parentFolder.exists()) {
                parentFolder.mkdirs();
            }

            this.imageFile = new File(parentFolder, String.format("%s.%s", ARTIST_IMAGE_BASE_NAME, imageMimeType));
        }

        return this.imageFile;
    }

    public void setParentArtist(Artist parentArtist) {
        this.parentArtist = parentArtist;
    }

    public ObservableList<Artist> getMemberArtists() {
        return memberArtists;
    }

    public Artist getParentArtist() {
        return parentArtist;
    }

    public void setArtistImage(Image artistImage) {
        // if newly fetched image is the same as the old default one, the imageview wouldnt update because the listener
        // would not be called, since the old and new values which are usually passed to the listener are the same.
        // here we work around that be first setting the artist image to null and then immediately setting it to the correct one.
        if (artistImage == DEFAULT_ARTIST_IMAGE) {
            this.artistImage.set(null);
        }

        this.artistImage.set(artistImage);
    }

    private final ObjectProperty<Image> artistImage = new SimpleObjectProperty<>();

    public ObjectProperty<Image> artistImageProperty() {
        return artistImage;
    }


    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public ObservableList<Album> getAlbums() {
        return albums;
    }

    public ObservableList<Song> getSongs() {
        return songs;
    }

    @Override
    protected String[] compareArray() {
        return new String[]{this.name};
    }

    @Override
    public Collection<Song> toSongList() {
        return this.getSongs();
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof Artist) {
            return ((Artist) o).id == this.id;
        }

        return false;
    }

    @Override
    public String toString() {
        return this.name;
    }

    @Override
    public List<Artist> getArtists() {
        return Collections.singletonList(this);
    }

    @Override
    public String getArtistName() {
        StringBuilder sb = new StringBuilder(this.name);
        if (this.getParentArtist() != null) {
            sb.append(" (").append(ResourceBundleHelper.getStringKey("of")).append(" ").append(this.getParentArtist().getName()).append(")");
        }

        return sb.toString();
    }

    public boolean isLoadingImage() {
        return loadingImage;
    }

    public void setLoadingImage(boolean loadingImage) {
        this.loadingImage = loadingImage;
    }

    public static String createNameString(List<Artist> artists) {
        if (artists.isEmpty()) {
            return ResourceBundleHelper.getStringKey("emptyArtist");
        }

        if (artists.get(0) == null || artists.get(0).getName() == null) {
            return ResourceBundleHelper.getStringKey("unknownArtist");
        }

        int noArtistGroup = 0;
        Map<String, List<String>> parentArtists = new HashMap<>();
        for (Artist a : artists) {
            boolean hasParentArtist = a.getParentArtist() != null;
            String parentName = hasParentArtist ? a.getParentArtist().getName() : ArtistTagHelper.NO_ARTIST_GROUP;
            if (!hasParentArtist) {
                noArtistGroup++;
            }

            parentArtists.computeIfAbsent(parentName, s -> new ArrayList<>()).add(a.getName());
        }

        if (parentArtists.size() == 1 && noArtistGroup == 1) {
            return artists.stream().map(Artist::getName).collect(Collectors.joining(", "));
        }

        List<String> tokenizedArtists = new ArrayList<>();
        List<String> noGroupArtists = parentArtists.get(ArtistTagHelper.NO_ARTIST_GROUP);
        if (noGroupArtists != null) {
            tokenizedArtists.addAll(noGroupArtists);
        }

        for (Map.Entry<String, List<String>> ent : parentArtists.entrySet()) {
            if (!ent.getKey().equals(ArtistTagHelper.NO_ARTIST_GROUP)) {
                StringBuilder sb = new StringBuilder();

                for (int i = 0; i < ent.getValue().size(); i++) {
                    sb.append(ent.getValue().get(i));

                    if (i < ent.getValue().size() - 1) {
                        sb.append(", ");
                    }
                }
                sb.append(" ").append(ResourceBundleHelper.getStringKey("of")).append(" ").append(ent.getKey());
                tokenizedArtists.add(sb.toString());
            }
        }

        return String.join(", ", tokenizedArtists);
    }

    @Override
    public FormattablePair getBoxLines() {
        return new FormattablePair(this.getArtistName(), String.format("%%s %s %%s", Song.SMALL_BLACK_CIRCLE));
    }
}
