package fe.materialplayer.controller.util.clickable.cover

import fe.materialplayer.data.indexable.Album
import fe.materialplayer.data.indexable.Song
import javafx.geometry.Insets
import javafx.scene.control.Label
import javafx.scene.layout.VBox

class ArtistAlbumCover(val album: Album) : TinyClickableCover(
        130.0, album.getCover(Song.CoverType.S160X160), album.hexBackgroundColor) {

    override fun build(): VBox {
        val lbName = Label(album.name)
        lbName.textFill = album.textColor
        lbName.padding = Insets(5.0, 0.0, 0.0, 5.0)

        val lbYear = Label(album.year)
        lbYear.textFill = album.textColor
        lbYear.padding = Insets(0.0, 0.0, 5.0, 5.0)

        return super.build(10.0, arrayOf(lbName, lbYear))
    }
}