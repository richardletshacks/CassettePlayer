package fe.materialplayer.controller.view.genre

import fe.materialplayer.controller.util.PluralHelper
import fe.materialplayer.controller.util.cell.SimpleLineBoxListCell
import fe.materialplayer.controller.view.artist.ArtistListCell
import fe.materialplayer.data.indexable.Genre
import fe.materialplayer.music.MusicPlayer
import fe.materialplayer.util.SnackbarHelper
import javafx.event.EventHandler
import javafx.scene.control.ContentDisplay
import javafx.scene.control.Label
import javafx.scene.control.ListCell
import javafx.scene.input.MouseButton
import javafx.scene.input.MouseEvent
import javafx.scene.layout.VBox
import java.util.*

class GenreListCell(val resourceBundle: ResourceBundle) : SimpleLineBoxListCell<Genre>() {

    companion object {
        private val PLAYER = MusicPlayer.getInstance()
    }

    override fun updateItem(item: Genre?, empty: Boolean) {
        super.updateItem(item, empty)

        if (empty || item == null) {
            text = null
            graphic = null
        } else {
            val vbox = this.makeLabelBox(item, item.boxLines.apply {
                this.formatValue(
                        PluralHelper.getString(resourceBundle, "%d %s", item.songs, "song", "songs"))
            })


            contentDisplay = ContentDisplay.GRAPHIC_ONLY
            graphic = vbox

//            this.onMouseClicked = EventHandler { evt: MouseEvent ->
//                if (evt.button == MouseButton.MIDDLE) {
//                    PLAYER.addNextSongs(item.songs.toList(), false)
//                    SnackbarHelper.createMinWidthTextBar(resourceBundle.getString("addedNextGenre"), 300.0)
//                }
//            }

        }
    }
}