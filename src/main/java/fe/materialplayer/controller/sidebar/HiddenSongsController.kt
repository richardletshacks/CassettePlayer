package fe.materialplayer.controller.sidebar

import com.jfoenix.controls.JFXListView
import com.jfoenix.controls.JFXPopup
import fe.materialplayer.controller.content.ContentController
import fe.materialplayer.controller.css.annotation.theme.ThemeTextColor
import fe.materialplayer.controller.util.ItemFetcher
import fe.materialplayer.controller.util.PluralHelper.getString
import fe.materialplayer.controller.util.cell.DefaultSongListCell
import fe.materialplayer.controller.util.popup.PopupState
import fe.materialplayer.controller.util.popup.PopupUtil
import fe.materialplayer.controller.util.popup.impl.EmptyPopupImpl
import fe.materialplayer.data.indexable.Song
import fe.materialplayer.music.MusicLibrary
import fe.materialplayer.util.FormatUtil
import javafx.collections.FXCollections
import javafx.event.EventHandler
import javafx.fxml.FXML
import javafx.scene.control.Label
import javafx.scene.input.MouseButton
import javafx.scene.input.MouseEvent
import javafx.scene.input.ScrollEvent
import javafx.scene.layout.VBox
import javafx.util.Callback
import java.net.URL
import java.util.*


class HiddenSongsController : ContentController() {
    @FXML
    @ThemeTextColor
    private lateinit var vbLabels: VBox

    @FXML
    @ThemeTextColor
    private lateinit var lbSongs: Label

    @FXML
    private lateinit var lbSongNumber: Label

    @FXML
    private lateinit var lbLength: Label

    @FXML
    private lateinit var lvSongs: JFXListView<Song>

    private val hiddenSongs = FXCollections.observableArrayList(LIBRARY.songDao.items.filter { s -> s.isHidden })
    private var itemFetcher: ItemFetcher<Song> = ItemFetcher(hiddenSongs, Comparator.comparing { obj: Song -> obj.title }, false)

    private val popupState = PopupState<Song>()

    private lateinit var popup: EmptyPopupImpl<Song>

    override fun initialize(location: URL, resources: ResourceBundle) {
        super.initialize(location, resources)

        itemFetcher.listView = lvSongs
        itemFetcher.fetchItems(FETCH_SIZE, ItemFetcher.FetchType.INITIAL)

        lbSongNumber.text = getString(resources, "%d %s", hiddenSongs, "song", "songs")
        lbLength.text = FormatUtil.formatLength(hiddenSongs.sumByDouble { s -> s.length })

        this.bindIcons(mapOf(lbSongNumber to "music_note", lbLength to "timer"))

        lvSongs.cellFactory = Callback { DefaultSongListCell(Song.FormatStyle.TITLE_ARTIST_ALBUM, resources) }

        popup = EmptyPopupImpl(this.popupState)
        popup.addItem(PopupUtil.createPopupButton(resources.getString("unhideSong"), "show_songs", EventHandler {
            val song = popupState.currentState!!

            song.isHidden = false
            LIBRARY.songDao.unhideSong(song)
            LIBRARY.songDao.update(song)

            LIBRARY.albumDao.refresh(song.album)
            song.artists.forEach {
                LIBRARY.artistDao.refresh(it)
            }

            ItemFetcher.refetchAll()

            hiddenSongs.remove(song)
        }))
    }

    @FXML
    fun lvSongsClicked(event: MouseEvent) {
        this.lvSongs.selectionModel.selectedItem?.let {
            if (event.button == MouseButton.SECONDARY) {
                popupState.currentState = it
                popup.build().show(this.lvSongs,
                        JFXPopup.PopupVPosition.TOP,
                        JFXPopup.PopupHPosition.LEFT, event.x, event.y)
            }
        }
    }

    @FXML
    fun lvSongsScrolled(event: ScrollEvent) {
        this.itemFetcher.fetchItems(FETCH_SIZE, ItemFetcher.FetchType.SCROLL)
    }

    companion object {
        private const val FETCH_SIZE = 15

        private val LIBRARY = MusicLibrary.getInstance()
    }
}