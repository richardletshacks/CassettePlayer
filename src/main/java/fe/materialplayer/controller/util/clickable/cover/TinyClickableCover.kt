package fe.materialplayer.controller.util.clickable.cover

import fe.materialplayer.util.Util.makeSimpleTimeline
import fe.materialplayer.util.image.ImageUtil
import javafx.animation.Animation
import javafx.application.Platform
import javafx.event.EventHandler
import javafx.scene.Node
import javafx.scene.effect.BlurType
import javafx.scene.effect.DropShadow
import javafx.scene.image.Image
import javafx.scene.layout.VBox
import javafx.scene.paint.Color
import javafx.util.Duration

abstract class TinyClickableCover(val size: Double, val image: Image,
                                  private val backgroundColor: String) {

    abstract fun build(): VBox

    protected fun build(jumpHeight: Double, components: Array<Node>): VBox {
        val iv = ImageUtil.makeIv(size.toInt())
        Platform.runLater { iv.image = image }

        val box = VBox(iv).apply {
            this.children.addAll(components)

            this.alignment = javafx.geometry.Pos.CENTER_LEFT
            this.style = "-fx-background-color: %s; -fx-background-radius: 3;".format(backgroundColor)

            this.minWidth = size
            this.maxWidth = size
            this.prefWidth = size

            this.effect = DropShadow(BlurType.GAUSSIAN, Color(0.0, 0.0, 0.0, 0.4),
                    10.0, 0.5, 0.0, 0.0)
        }


        val increaseAnimation: Animation = makeSimpleTimeline(hoverDuration, box.translateYProperty(), -jumpHeight)
        val decreaseAnimation: Animation = makeSimpleTimeline(hoverDuration, box.translateYProperty(), 0.0)

        box.onMouseEntered = EventHandler {
            decreaseAnimation.stop()
            increaseAnimation.play()
        }

        box.onMouseExited = EventHandler {
            increaseAnimation.stop()
            decreaseAnimation.play()
        }

        return box
    }

    companion object {
        private val hoverDuration = Duration.millis(90.0)
    }
}