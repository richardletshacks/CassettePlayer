package fe.materialplayer.controller.playing;

import fe.materialplayer.controller.css.StyleBinder;
import fe.materialplayer.controller.css.annotation.theme.ThemeBackgroundColor;
import fe.materialplayer.data.indexable.Song;
import fe.materialplayer.data.indexable.base.BackgroundColorable;
import fe.materialplayer.music.MusicPlayer;
import fe.materialplayer.util.properties.ColorThemeManager;
import fe.materialplayer.util.StyleConstants;
import fe.materialplayer.util.properties.ColorType;
import javafx.beans.binding.Bindings;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.effect.BlurType;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import org.jetbrains.annotations.NotNull;

import java.net.URL;
import java.util.ResourceBundle;

public class PlayingBarController extends PlayStatusDisplayer {

    @FXML
    @ThemeBackgroundColor(cssProperty = StyleConstants.CSS_BACKGROUND_COLOR_PROPERTY)
    private StackPane spPlaying;

    @FXML
    private Label lbTitle;

    @FXML
    private Label lbArtist;

    @FXML
    private ImageView ivCover;

    @FXML
    private StackPane spTitle;

    private final MusicPlayer player = MusicPlayer.getInstance();

    private final Song initialSong;
    private final EventHandler<? super MouseEvent> clickHandler;

    private final BooleanProperty titleMouseEntered = new SimpleBooleanProperty();

    public PlayingBarController(Song initialSong, EventHandler<? super MouseEvent> clickHandler) {
        this.initialSong = initialSong;
        this.clickHandler = clickHandler;
    }

    @Override
    public void initialize(@NotNull URL location, @NotNull ResourceBundle resources) {
        super.initialize(location, resources);

        this.spPlaying.setEffect(new DropShadow(BlurType.GAUSSIAN, new Color(0, 0, 0, 0.4), 10, 0.5, 0, 0));

        StyleBinder.bindPaintToComplexObjectProperty(Bindings.createObjectBinding(
                () -> this.titleMouseEntered.get() ? BackgroundColorable.getTextColor(this.player.getCurrentSong())
                        : ColorThemeManager.getTheme().get().getColor(ColorType.SECONDARY_TEXT_COLOR),
                ColorThemeManager.getTheme(), this.titleMouseEntered),
                this.lbTitle.textFillProperty(), this.lbArtist.textFillProperty());

        this.slPosition.skinProperty().addListener((observable, oldValue, newValue) -> {
            new StyleBinder(this.slPosition).bindToColorProperty(StyleConstants.CSS_PRIMARY_COLOR_VAR, ColorThemeManager.getPrimaryColor(), false).build();
        });

        StyleBinder.bindPaintToObjectColor(
                ColorThemeManager.getAccentColor(),
                this.btNext.ripplerFillProperty(),
                this.btPrevious.ripplerFillProperty(),
                this.btTogglePlay.ripplerFillProperty(),
                this.btVolume.ripplerFillProperty()
        );

        this.lbTitle.setText(this.initialSong.getTitle());
        this.lbArtist.setText(this.initialSong.getArtistName());

        this.ivCover.setImage(this.initialSong.loadSongCover(Song.CoverType.S60X60));
        this.slPosition.setMax(this.initialSong.getLength());

        this.lbEnd.setText(this.initialSong.getFormattedLength());

        this.spTitle.widthProperty().addListener((observable, oldValue, newValue) -> {
            this.spTitle.setOnMouseEntered(evt -> {
                this.spTitle.setStyle(BackgroundColorable.toBackgroundStyle(this.player.getCurrentSong()));
                this.titleMouseEntered.set(true);
            });

            this.spTitle.setOnMouseExited(evt -> {
                this.spTitle.setStyle(StyleConstants.TRANSPARENT_BACKGROUND);
                this.titleMouseEntered.set(false);
            });
        });

        this.player.nowPlayingProperty().addListener((observable, oldValue, song) -> {
            this.lbTitle.setText(song.getTitle());
            this.lbArtist.setText(song.getArtistName());
            this.ivCover.setImage(song.loadSongCover(Song.CoverType.S60X60));
        });

        this.ivCover.setOnMouseClicked(this.clickHandler);
        this.spTitle.setOnMouseClicked(this.clickHandler);
    }
}
