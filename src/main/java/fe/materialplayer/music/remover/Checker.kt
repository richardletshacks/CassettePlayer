package fe.materialplayer.music.remover

import fe.materialplayer.data.indexable.*
import fe.materialplayer.data.indexable.nm.ArtistAlbum
import fe.materialplayer.data.indexable.nm.ArtistSong
import fe.materialplayer.music.remover.LibrarySongRemover.Companion.LIBRARY
import java.util.*

abstract class Checker<T>(val lsr: LibrarySongRemover) {
    val removeList = LinkedList<T>()

    abstract fun check(song: Song)
    abstract fun execute()
}

abstract class UpdateChecker<T>(lsr: LibrarySongRemover) : Checker<T>(lsr) {
    val updateList = LinkedList<T>()
}

class NMChecker(lsr: LibrarySongRemover) : Checker<Unit>(lsr) {
    val removeArtistSongs = LinkedList<ArtistSong>()
    val removeArtistAlbums = LinkedList<ArtistAlbum>()

    override fun check(song: Song) {
        removeArtistSongs.addAll(LIBRARY.artistSongDao.findRelationSecond(song))
    }

    override fun execute() {
        LIBRARY.artistAlbumDao.batchDelete(removeArtistAlbums, true)
        LIBRARY.artistSongDao.batchDelete(removeArtistSongs, true)

        val updateArtists = lsr.artistChecker.updateList

        LIBRARY.artistSongDao.setup(LIBRARY.artistSongDao.queryRelatedByFirstCol(updateArtists).toMutableList())
        LIBRARY.artistAlbumDao.setup(LIBRARY.artistAlbumDao.queryRelatedByFirstCol(updateArtists).toMutableList())
    }

}

class AlbumChecker(lsr: LibrarySongRemover) : UpdateChecker<Album>(lsr) {

    override fun check(song: Song) {
        val state = LIBRARY.albumDao.checkRemoveAlbum(song)
        val album = song.album

        if (state == LibrarySongRemover.UpdateState.REMOVE) {
            lsr.nmChecker.removeArtistAlbums.addAll(LIBRARY.artistAlbumDao.findRelationSecond(album))
            removeList.add(album)
        } else {
            updateList.add(album)
        }
    }

    override fun execute() {
        LIBRARY.albumDao.batchDelete(removeList, true)
        LIBRARY.albumDao.performUncached {
            it.batchRefresh(updateList)
        }
    }
}

class ArtistChecker(lsr: LibrarySongRemover) : UpdateChecker<Artist>(lsr) {
    override fun check(song: Song) {
        LIBRARY.artistDao.getRemoveArtists(song).forEach { (artist, state) ->
            if (state == LibrarySongRemover.UpdateState.REMOVE) {
                lsr.nmChecker.removeArtistAlbums.addAll(LIBRARY.artistAlbumDao.findRelationFirst(artist))
                lsr.nmChecker.removeArtistSongs.addAll(LIBRARY.artistSongDao.findRelationFirst(artist))

                removeList.add(artist)
            } else {
                updateList.add(artist)
            }
        }
    }

    override fun execute() {
        LIBRARY.artistDao.batchDelete(removeList, true)
        LIBRARY.artistDao.performUncached {
            it.batchRefresh(updateList)
        }
    }

}

class GenreChecker(lsr: LibrarySongRemover) : Checker<Genre>(lsr) {
    override fun check(song: Song) {
        if (LIBRARY.genreDao.checkRemoveGenre(song)) {
            removeList.add(song.genre)
        }
    }

    override fun execute() {
        LIBRARY.genreDao.batchDelete(removeList, true)
    }
}