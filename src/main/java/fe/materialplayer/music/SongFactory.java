package fe.materialplayer.music;

import com.mpatric.mp3agic.Mp3File;
import com.mpatric.mp3agic.exception.InvalidDataException;
import com.mpatric.mp3agic.exception.UnsupportedTagException;
import fe.materialplayer.util.DeclarationsKt;
import fe.mp3taglib.*;
import fe.logger.Logger;
import fe.materialplayer.data.dao.helper.AlbumDaoHelper;
import fe.materialplayer.data.dao.helper.ArtistDaoHelper;
import fe.materialplayer.data.dao.helper.GenreDaoHelper;
import fe.materialplayer.data.indexable.*;
import fe.materialplayer.util.HashHelper;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.locks.ReentrantLock;

public class SongFactory {
    private static final Logger LOGGER = new Logger("SongFactory", true);
    private static final MusicLibrary LIBRARY = MusicLibrary.Companion.getInstance();

    private static final ArtistDaoHelper ARTIST_DAO_HELPER = Objects.requireNonNull(LIBRARY).getArtistDao();
    private static final AlbumDaoHelper ALBUM_DAO_HELPER = LIBRARY.getAlbumDao();
    private static final GenreDaoHelper GENRE_DAO_HELPER = LIBRARY.getGenreDao();

    public static String extractMimeType(String mimeType) {
        return mimeType.substring(mimeType.indexOf("/") + 1);
    }

    public static File createCoverFile(File dir, String name, String mimeType) {
        return new File(dir, String.format("%s.%s", name, mimeType));
    }

    private static final ReentrantLock ARTIST_LOCK = new ReentrantLock();
    private static final ReentrantLock GROUP_ARTIST_LOCK = new ReentrantLock();
    private static final ReentrantLock ALBUM_LOCK = new ReentrantLock();
    private static final ReentrantLock GENRE_LOCK = new ReentrantLock();

    public static Song createSong(File file, boolean doInsert) {
        LOGGER.print(Logger.Type.INFO, "Hashing %s", file);

        int hash = -1;
        try {
            hash = HashHelper.hashFile(file);
        } catch (IOException e) {
            e.printStackTrace();
        }

        LOGGER.print(Logger.Type.INFO, "Hashing done (%d)", hash);

        double duration = -1;
        try {
            Mp3File mp3File = new Mp3File(file);
            duration = mp3File.getLengthInMilliseconds() / 1000d;

            Optional<TagInfo> hasTagInfo = TagParser.getTagsJVM(mp3File, mp3File.getFilename());
            if (hasTagInfo.isPresent()) {
                TagInfo tagInfo = hasTagInfo.get();

                String albumName = tagInfo.getAlbum();
                LOGGER.print(Logger.Type.INFO, "Loading %s - %s by %s", tagInfo.getTitle(), albumName, tagInfo.getArtist());

                List<Artist> artists = new ArrayList<>();
                for (Map.Entry<String, List<String>> artistGroup : tagInfo.getArtist().entrySet()) {
                    String group = artistGroup.getKey();
                    Artist groupArtist = null;

                    if (!group.equals(ArtistTagHelper.NO_ARTIST_GROUP)) {
                        BooleanProperty groupArtistExisted = new SimpleBooleanProperty(true);
                        synchronized (GROUP_ARTIST_LOCK) {
                            groupArtist = SongFactory.createArtist(group, groupArtistExisted);
                            if (!groupArtistExisted.get()) {
                                if (doInsert) {
                                    LOGGER.print(Logger.Type.INFO, "Inserting group artist " + group);
                                    groupArtist = ARTIST_DAO_HELPER.createAndRefresh(groupArtist);
                                } else {
                                    LOGGER.print(Logger.Type.INFO, "Adding group artist " + group);
                                    ARTIST_DAO_HELPER.addToList(groupArtist);
                                }
                            }
                        }

                        LOGGER.print(Logger.Type.INFO, "\tFound group artist %s", groupArtist.getName());
                    }

                    for (String artistName : artistGroup.getValue()) {
                        BooleanProperty artistExisted = new SimpleBooleanProperty(true);
                        Artist artist = null;
                        synchronized (ARTIST_LOCK) {
                            artist = SongFactory.createArtist(artistName, artistExisted);
                            if (groupArtist != null) {
                                artist.setParentArtist(groupArtist);
                            }

                            if (!artistExisted.get()) {
                                if (doInsert) {
                                    LOGGER.print(Logger.Type.INFO, "Inserting artist " + artistName);
                                    artist = ARTIST_DAO_HELPER.createAndRefresh(artist);
                                } else {
                                    LOGGER.print(Logger.Type.INFO, "Adding artist " + artistName);
                                    ARTIST_DAO_HELPER.addToList(artist);
                                }
                            }
                        }

                        LOGGER.print(Logger.Type.INFO, "\tArtist = %s", artist.getName());
                        artists.add(artist);
                    }
                }

                BooleanProperty albumExisted = new SimpleBooleanProperty(true);
                Album album;
                synchronized (ALBUM_LOCK) {
                    album = SongFactory.createAlbum(albumName, artists, albumExisted, tagInfo.getYear());

                    File albumCoverDir = SongFactory.createCoverDir(artists.get(0).getName(), albumName);

                    LOGGER.print(Logger.Type.INFO, "\tAlbum = %s", album.getName());

                    ImageIO.setUseCache(false);
                    if (tagInfo.getAlbumImage() != null && album.getCoverDir() == null) {
                        CoverExtractTask cet = new CoverExtractTask(album, albumCoverDir, tagInfo.getAlbumImage());

                        if (cet.extract()) {
                            LOGGER.print(Logger.Type.INFO, "Extracting cover succeeded");
                        } else {
                            LOGGER.print(Logger.Type.ERROR, "Failed to extract cover for " + albumName);
                        }

                    }

                    album = SongFactory.doAlbumInsert(album, albumExisted, doInsert);
                }
                //we need to "reset" the lazily and inited hiddenSong list in Album since a new song has been added to it
                if (albumExisted.get() && doInsert) {
                    album.resetLazySongList();
                }

                BooleanProperty genreExisted = new SimpleBooleanProperty(true);
                String genreName = tagInfo.getGenre();
                Genre genre = null;
                synchronized (GENRE_LOCK) {
                    genre = SongFactory.createGenre(genreName, genreExisted);

                    LOGGER.print(Logger.Type.INFO, "\tGenre = %s (%s)", genreName, genre.getName());

                    if (!genreExisted.get()) {
                        if (doInsert) {
                            LOGGER.print(Logger.Type.INFO, "Inserting genre " + genreName);
                            genre = GENRE_DAO_HELPER.createAndRefresh(genre);
                        } else {
                            LOGGER.print(Logger.Type.INFO, "Adding genre " + genreName);
                            GENRE_DAO_HELPER.addToList(genre);
                        }
                    }
                }

                return new Song(tagInfo.getTitle(), artists, album, tagInfo.getTrack(), genre, duration, file.getAbsolutePath(), hash);
            }
        } catch (IOException | UnsupportedTagException | InvalidDataException | SQLException | NullPointerException e) {
            LOGGER.print(Logger.Type.ERROR, e);
        }

        LOGGER.print(Logger.Type.INFO, "Mp3 does not have a tag");

        BooleanProperty albumExisted = new SimpleBooleanProperty(true);
        Album album = SongFactory.createAlbum("Unknown Album", Collections.singletonList(ArtistDaoHelper.UNKNOWN_ARTIST), albumExisted, "Unknown Year");

        try {
            SongFactory.doAlbumInsert(album, albumExisted, doInsert);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }


        return new Song(file.getName(),
                Collections.singletonList(ArtistDaoHelper.UNKNOWN_ARTIST), album,
                null, GenreDaoHelper.getUNKNOWN_GENRE(), duration, file.getAbsolutePath(), hash);
    }

    public static Album doAlbumInsert(Album album, BooleanProperty albumExisted, boolean insert) throws SQLException {
        if (!albumExisted.get()) {
            if (insert) {
                LOGGER.print(Logger.Type.INFO, "Inserting album " + album.getName());
                return ALBUM_DAO_HELPER.createAndRefresh(album);
            } else {
                LOGGER.print(Logger.Type.INFO, "Adding album " + album.getName());
                ALBUM_DAO_HELPER.addToList(album);
            }
        }

        return album;
    }

    public static File createCoverDir(String artistName, String albumName) {
        File albumCoverDir = new File(Objects.requireNonNull(MusicLibrary.Companion.getInstance()).getAlbumCoverCache(),
                DeclarationsKt.encodeBase64(String.format("%s - %s", artistName, albumName).getBytes()));
        if (!albumCoverDir.exists()) {
            albumCoverDir.mkdirs();
        }

        return albumCoverDir;
    }

    public static Genre createGenre(String genreName, BooleanProperty existed) {
        Genre genre = GENRE_DAO_HELPER.existsGenre(genreName);
        if (genre == null) {
            existed.set(false);

            LOGGER.print(Logger.Type.INFO, "Creating new genre %s", genreName);
            genre = new Genre(genreName);
        }

        return genre;
    }

    public static Album createAlbum(String albumName, List<Artist> artists, BooleanProperty existed, String year) {
        Album album = ALBUM_DAO_HELPER.existsAlbum(albumName, year, artists);
        LOGGER.print(Logger.Type.INFO, "Album %s: %s", albumName, album);
        if (album == null) {
            existed.set(false);

            LOGGER.print(Logger.Type.INFO, "Creating new album %s", albumName);
            album = new Album(albumName, year);
            album.addArtist(artists);
        }

        return album;
    }

    public static Artist createArtist(String artistName, BooleanProperty existed) {
        Artist artist = ARTIST_DAO_HELPER.existsArtist(artistName);
        LOGGER.print(Logger.Type.INFO, "Artist %s: %s", artistName, artist);
        if (artistName != null) {
            File artistImageDir = new File(Objects.requireNonNull(MusicLibrary.Companion.getInstance()).getArtistCache(), DeclarationsKt.encodeBase64(artistName.getBytes()));
            if (!artistImageDir.exists()) {
                artistImageDir.mkdirs();
            }

            if (artist == null) {
                existed.set(false);

                LOGGER.print(Logger.Type.INFO, "Creating new artist %s", artistName);
                artist = new Artist(artistName, artistImageDir.getAbsolutePath());
            }
        }

        return artist;
    }

}
