package fe.materialplayer.util.properties

import fe.materialplayer.Main
import fe.materialplayer.controller.util.ComboboxNameKey
import fe.materialplayer.util.StyleConstants
import fe.materialplayer.util.luma
import javafx.beans.property.ObjectProperty
import javafx.beans.property.SimpleObjectProperty
import javafx.scene.paint.Color


object ColorThemeManager {
    const val PROP_THEME = "theme"
    const val PROP_PRIMARY_COLOR = "primaryColor"
    const val PROP_LIGHT_PRIMARY_COLOR = "lightPrimaryColor"
    const val PROP_ACCENT_COLOR = "accentColor"

    val properties = Properties.getInstance()
    private val donatorBuild = Main.getInstance().isDonatorBuild

    private fun getProperty(name: String) = if (donatorBuild) {
        properties.getStringProperty(name)
    } else null


    @JvmStatic
    val theme: ObjectProperty<PlayerTheme> = SimpleObjectProperty(getProperty(PROP_THEME)?.let {
        PlayerTheme.valueOf(it.toUpperCase())
    } ?: PlayerTheme.LIGHT)

    @JvmStatic
    val primaryColor: ObjectProperty<Color> = SimpleObjectProperty(getProperty(PROP_PRIMARY_COLOR)?.let {
        Color.valueOf(it)
    } ?: StyleConstants.DEFAULT_PRIMARY_COLOR)

    @JvmStatic
    val lightPrimaryColor: ObjectProperty<Color> = SimpleObjectProperty(getProperty(PROP_LIGHT_PRIMARY_COLOR)?.let {
        Color.valueOf(it)
    } ?: StyleConstants.DEFAULT_LIGHT_PRIMARY_COLOR)

    @JvmStatic
    val accentColor: ObjectProperty<Color> = SimpleObjectProperty(getProperty(PROP_ACCENT_COLOR)?.let {
        Color.valueOf(it)
    } ?: StyleConstants.DEFAULT_ACCENT_COLOR)
}


enum class PlayerTheme(
    private val nameKey: String,
    private val colorAdjustModifier: Int,
    val color: Map<ColorType, Color>
) : ComboboxNameKey {
    LIGHT(
        "lightTheme", -1, mapOf(
            ColorType.BACKGROUND_COLOR to Color.WHITE,
            ColorType.TEXT_COLOR to Color.BLACK,
            ColorType.SECONDARY_TEXT_COLOR to Color.BLACK
        )
    ),
    DARK(
        "darkTheme", 1, mapOf(
            ColorType.BACKGROUND_COLOR to Color.valueOf("#202020"),
            ColorType.TEXT_COLOR to Color.WHITE,
            ColorType.SECONDARY_TEXT_COLOR to Color.valueOf("#b3b3b3")
        )
    );

    override fun nameKey() = nameKey

    fun getColor(namedColor: ColorType): Color {
        return color[namedColor] ?: error("Theme does not specify requested color")
    }

    fun findIcon(name: String): String {
        return "${name}_${this.name.toLowerCase()}"
    }

    fun adjustedColor(toAdjust: Color): Color {
        val diff = toAdjust.luma - this.getColor(ColorType.BACKGROUND_COLOR).luma
        if (diff > COLOR_THRESHOLD) {
            return adjustColor(toAdjust, COLOR_ADJUST_CHANGE_AMOUNT * colorAdjustModifier)
        }

        return toAdjust
    }

    companion object {
        const val COLOR_ADJUST_CHANGE_AMOUNT = 0.3
        const val COLOR_THRESHOLD = -0.20

        fun adjustColor(col: Color, amount: Double): Color {
            return Color(
                (col.red + amount).coerceIn(0.0, 1.0),
                (col.green + amount).coerceIn(0.0, 1.0),
                (col.blue + amount).coerceIn(0.0, 1.0),
                1.0
            )
        }
    }
}

enum class ColorType {
    BACKGROUND_COLOR, TEXT_COLOR, SECONDARY_TEXT_COLOR;
}


