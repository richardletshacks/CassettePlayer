package fe.materialplayer.controller.view.playlist.validator;

import com.jfoenix.validation.base.ValidatorBase;
import fe.materialplayer.data.indexable.Playlist;
import fe.materialplayer.data.dao.helper.PlaylistDaoHelper;
import fe.materialplayer.music.MusicLibrary;
import javafx.scene.control.TextInputControl;

import java.util.Objects;

public class DuplicatePlaylistValidator extends ValidatorBase {

    private final MusicLibrary library = MusicLibrary.Companion.getInstance();
    private final PlaylistDaoHelper playlistDao = Objects.requireNonNull(this.library).getPlaylistDao();

    public DuplicatePlaylistValidator(String message) {
        super(message);
    }

    @Override
    protected void eval() {
        if (this.srcControl.get() instanceof TextInputControl) {
            TextInputControl textField = (TextInputControl) srcControl.get();
            String text = textField.getText();
            if (text != null) {
                this.hasErrors.set(this.playlistDao.getItems().stream().map(Playlist::getName).anyMatch(s -> s.trim().equalsIgnoreCase(text)));
            }
        }
    }
}
