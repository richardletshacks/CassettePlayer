package fe.materialplayer.data.indexable.base;

import fe.materialplayer.data.indexable.Song;
import fe.materialplayer.data.indexable.base.table.IdTable;
import fe.materialplayer.util.FormatUtil;

import java.sql.SQLOutput;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public abstract class Indexable implements IdTable {
    private String[] compareValues;
    private double length;
    private String formattedLength;

    public String getFormattedLength() {
        if (this.formattedLength == null) {
            this.length = FormatUtil.sumLength(this.toSongList());
            this.formattedLength = FormatUtil.formatLength(this.length);
        }

        return formattedLength;
    }

    public double getLength() {
        return length;
    }

    protected abstract String[] compareArray();

    public abstract Collection<Song> toSongList();

    public static List<Song> mapToSongs(List<? extends Indexable> indexables) {
        //this should, in theory, be faster since we a) dont use lambdas and b) collect to a linkedlist instead of an arraylist which Collectors#toList does
        List<Song> retList = new LinkedList<>();
        for (Indexable i : indexables) {
            retList.addAll(i.toSongList());
        }
        return retList;


//        return indexables.stream().map(Indexable::toSongList).flatMap(Collection::stream).collect(Collectors.toList());
    }

    @Override
    public abstract boolean equals(Object o);

    @Override
    public int hashCode() {
        return Arrays.hashCode(this.compareValues);
    }

    public boolean isSearchResultForText(String text) {
        if (this.compareValues == null) {
            this.compareValues = this.compareArray();
        }

        text = text.toLowerCase().trim();
        //does not work if values contain space, maybe use different token ???
//        if (text.contains(" ")) {
//            String[] parts = text.split(" ");
//
//            Map<String, Boolean> matches = new HashMap<>();
//            for(String val : this.values){
//                if(val != null){
//                    matches.put(val.toLowerCase(), false);
//                }
//            }
//
//            for (String part : parts) {
//                for(Map.Entry<String, Boolean> ent : matches.entrySet()){
//                    if(!ent.getValue() && ent.getKey().contains(part)){
//                        ent.setValue(true);
//                    }
//                }
//            }
//
//            return parts.length == matches.values().stream().filter(b -> b).count();
//        }

        for (String val : this.compareValues) {
            if (val != null && val.toLowerCase().contains(text)) {
                return true;
            }
        }

        return false;
    }

}
