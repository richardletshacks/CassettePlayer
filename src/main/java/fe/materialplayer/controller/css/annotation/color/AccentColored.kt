package fe.materialplayer.controller.css.annotation.color

@Target(AnnotationTarget.FIELD)
@Retention(AnnotationRetention.RUNTIME)
@MustBeDocumented
annotation class AccentColored(val cssProperty: String)