package fe.materialplayer.util.resource

import fe.materialplayer.util.properties.Properties
import java.util.*

object ResourceBundleHelper {

    private var bundle: ResourceBundle? = null
    private var currentLocale: Locale? = null

    @JvmStatic
    fun getBundle(): ResourceBundle {
        if (bundle == null) {
            currentLocale = Properties.getInstance().getStringProperty("lang").run {
                if (this != null) Locale.forLanguageTag(this) else Locale.getDefault()
            }

            bundle = ResourceBundle.getBundle("/language/language", currentLocale!!)
        }

        return bundle!!
    }

    @JvmStatic
    fun getStringKey(key: String): String {
        return getBundle().getString(key)
    }

    @JvmStatic
    fun reset() {
        bundle = null
    }

}