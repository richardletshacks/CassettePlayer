package fe.materialplayer.controller.firstrun

import com.google.gson.JsonPrimitive
import com.jfoenix.controls.JFXButton
import com.jfoenix.controls.JFXCheckBox
import com.jfoenix.controls.JFXProgressBar
import com.jfoenix.controls.JFXTextField
import com.jfoenix.controls.materialbutton.JFXContainedMaterialButton
import com.jfoenix.controls.materialbutton.JFXOutlinedMaterialButton
import fe.materialplayer.controller.base.dialog.UnclosableDialogController
import fe.materialplayer.controller.css.StyleBinder
import fe.materialplayer.controller.css.annotation.color.PrimaryColored
import fe.materialplayer.controller.css.annotation.property.IntCSSProperties
import fe.materialplayer.controller.css.annotation.property.IntCSSProperty
import fe.materialplayer.controller.css.annotation.text.PrimaryExtractedTextFill
import fe.materialplayer.controller.css.annotation.theme.ThemeBackgroundColor
import fe.materialplayer.controller.css.annotation.theme.ThemeSecondaryTextColor
import fe.materialplayer.controller.css.annotation.theme.ThemeTextColor
import fe.materialplayer.controller.util.ItemFetcher.Companion.refetchAll
import fe.materialplayer.util.StyleConstants
import fe.materialplayer.util.image.ImageUtil
import fe.materialplayer.util.properties.ColorThemeManager.accentColor
import fe.materialplayer.util.properties.ColorThemeManager.primaryColor
import fe.materialplayer.util.properties.ColorType
import javafx.beans.binding.Bindings
import javafx.beans.value.ChangeListener
import javafx.concurrent.WorkerStateEvent
import javafx.event.ActionEvent
import javafx.event.EventHandler
import javafx.fxml.FXML
import javafx.scene.control.Label
import javafx.scene.layout.GridPane
import javafx.stage.DirectoryChooser
import java.io.File
import java.net.URL
import java.util.*

class FirstRunIndexingController(doneListener: ChangeListener<Boolean>) : UnclosableDialogController() {
    init {
        this.done.addListener(doneListener)
    }

    @FXML
    private lateinit var lbWelcome: Label

    @FXML
    @ThemeTextColor
    private lateinit var gpLabels: GridPane

    @FXML
    @ThemeBackgroundColor(cssProperty = StyleConstants.CSS_BACKGROUND_COLOR_PROPERTY)
    @IntCSSProperties(
            IntCSSProperty(property = StyleConstants.CSS_BACKGROUND_RADIUS, 7),
            IntCSSProperty(property = StyleConstants.CSS_BORDER_RADIUS, 7))
    private lateinit var gpRoot: GridPane

    @FXML
    private lateinit var btBack: JFXButton

    @FXML
    @PrimaryColored
    @ThemeTextColor
    @ThemeSecondaryTextColor
    private lateinit var tfMusicDir: JFXTextField

    @FXML
    @PrimaryColored(cssProperty = StyleConstants.CSS_TEXT_FILL_PROPERTY)
    private lateinit var btSelect: JFXOutlinedMaterialButton

    @FXML
    @PrimaryColored(cssProperty = StyleConstants.CSS_BACKGROUND_COLOR_PROPERTY)
    @PrimaryExtractedTextFill(cssProperty = StyleConstants.CSS_TEXT_FILL_PROPERTY)
    private lateinit var btScan: JFXContainedMaterialButton

    @FXML
    private lateinit var lbPercentage: Label

    @FXML
    private lateinit var lbProgress: Label

    @FXML
    @ThemeTextColor
    @ThemeSecondaryTextColor
    private lateinit var cbSubDirs: JFXCheckBox

    @FXML
    @PrimaryColored
    private lateinit var pbScan: JFXProgressBar

    @FXML
    private lateinit var gpSelect: GridPane

    private var fileIndexTask: LibraryIndexTask? = null

    override fun initialize(location: URL, resources: ResourceBundle) {
        super.initialize(location, resources)

        StyleBinder.bindPaintToThemeColor(ColorType.TEXT_COLOR, this.lbWelcome.textFillProperty())

        this.btBack.graphic = ImageUtil.makeIconIv("back_grey", 17)
        this.tfMusicDir.focusColorProperty().bind(primaryColor)
        this.cbSubDirs.checkedColorProperty().bind(primaryColor)

        this.btBack.ripplerFillProperty().bind(accentColor)

        this.fileIndexTask?.let {
            this.tfMusicDir.text = it.dir.absolutePath
            this.cbSubDirs.isSelected = it.isRecursive

            bindProgress(it)
        }
    }

    @FXML
    fun btBackClicked(event: ActionEvent) {
        this.previous()
    }

    @FXML
    fun btScanClicked(event: ActionEvent) {
        val f = File(tfMusicDir.text)
        if (f.exists() && f.isDirectory) {
            if (this.fileIndexTask == null) {
                this.properties
                        .addProperty("musicDir", JsonPrimitive(f.absolutePath))
                        .addProperty("musicDirRecursive", JsonPrimitive(this.cbSubDirs.isSelected))
                        .save()

                this.fileIndexTask = LibraryIndexTask(this.resources, f, this.cbSubDirs.isSelected).apply {
                    onSucceeded = EventHandler { refetchAll() }
                }.also {
                    this.bindProgress(it)
                }

                this.btScan.isDisable = true

                Thread(fileIndexTask).start()
            }
        } else {
            this.lbProgress.text = resources.getString("dirNonExistent")
        }
    }

    private fun bindProgress(task: LibraryIndexTask) {
        this.lbPercentage.textProperty().bind(Bindings.createStringBinding({
            "${(task.progress * 100.0).toInt()}%"
        }, task.progressProperty()))

        this.lbProgress.textProperty().bind(task.messageProperty())
        this.pbScan.progressProperty().bind(task.progressProperty())

        task.addEventFilter(WorkerStateEvent.WORKER_STATE_SUCCEEDED) {
            this.lbProgress.textProperty().unbind()
            this.done.set(true)
            this.btScan.isDisable = false

            this.closeDialog()
        }
    }

    @FXML
    fun btSelectClicked(event: ActionEvent) {
        val dc = DirectoryChooser()

        //according to stackoverflow, this is not to correct approach to find the users home/music dir,
        //but I will not do some kind of retarded JNA bullshit to _just_ try to suggest the user their own fucking music folder
        val musicDir = File(System.getProperty("user.home"), "Music")
        if (musicDir.exists()) {
            dc.initialDirectory = musicDir
        }

        dc.title = this.resources.getString("selectMusicDir")
        dc.showDialog(this.tfMusicDir.scene.window)?.let {
            this.tfMusicDir.text = it.absolutePath
        }
    }
}