package fe.materialplayer.util.threading;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ThreadManager {
    private final Map<Thread, StoppableRunnable> runnableMap = new HashMap<>();
    private static ThreadManager instance;

    public static ThreadManager getInstance() {
        if (instance == null) {
            instance = new ThreadManager();
        }

        return instance;
    }

    private ThreadManager() {
    }

    public List<Thread> createThreads(StoppableRunnable... runnables) {
        List<Thread> threadList = new ArrayList<>();

        for (StoppableRunnable sr : runnables) {
            threadList.add(this.createThread(sr));
        }

        return threadList;
    }

    public Thread createThread(StoppableRunnable sr) {
        Thread t = new Thread(sr);
        this.runnableMap.put(t, sr);
        return t;
    }

    public void killThread(StoppableRunnable sr) {
        for (Map.Entry<Thread, StoppableRunnable> ent : this.runnableMap.entrySet()) {
            if (ent.getValue() == sr) {
                this.killThread(ent.getKey(), ent.getValue());
            }
        }
    }

    private void killThread(Thread t, StoppableRunnable sr) {
        sr.updateRunningStatus(false);
        t.interrupt();
    }

    public void killThreads() {
        for (Map.Entry<Thread, StoppableRunnable> ent : this.runnableMap.entrySet()) {
            this.killThread(ent.getKey(), ent.getValue());
        }
    }
}
