package fe.materialplayer.controller.css.annotation.property

@Target(AnnotationTarget.FIELD)
@Retention(AnnotationRetention.RUNTIME)
@MustBeDocumented
annotation class IntCSSProperty(val property: String, val value: Int)