package fe.materialplayer.controller.content.album

import fe.materialplayer.data.indexable.Song
import javafx.geometry.Insets
import javafx.geometry.Pos
import javafx.scene.control.ContentDisplay
import javafx.scene.control.Label
import javafx.scene.control.ListCell
import javafx.scene.layout.HBox


class AlbumContentSongListCell : ListCell<Song>() {
    override fun updateItem(item: Song?, empty: Boolean) {
        super.updateItem(item, empty)

        if (empty || item == null) {
            this.text = null
            this.graphic = null
        } else {
            val lbSong = Label(item.format(Song.FormatStyle.TITLE_DURATION))
            val lbTrack = Label(item.track)

            lbTrack.prefWidth = 20.0
            lbTrack.prefWidth = 20.0
            lbTrack.maxWidth = 20.0

            lbTrack.alignment = Pos.CENTER

            val box = HBox(lbTrack, lbSong)
            box.padding = Insets(0.0, 10.0, 0.0, 7.0)
            box.alignment = Pos.CENTER_LEFT
            box.spacing = 10.0

            contentDisplay = ContentDisplay.GRAPHIC_ONLY
            graphic = box
        }
    }
}