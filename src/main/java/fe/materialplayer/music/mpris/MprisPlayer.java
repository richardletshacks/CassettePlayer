package fe.materialplayer.music.mpris;

import fe.materialplayer.Main;
import fe.materialplayer.music.MusicPlayer;
import fe.materialplayer.util.InitHelper;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.util.Duration;
import kotlin.Metadata;
import org.freedesktop.dbus.connections.impl.DBusConnection;
import org.freedesktop.dbus.exceptions.DBusException;
import org.freedesktop.dbus.interfaces.Properties;
import org.freedesktop.dbus.types.Variant;
import org.mpris.MediaPlayer2;
import org.mpris.Player;

import java.nio.file.Paths;
import java.sql.DatabaseMetaData;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

public class MprisPlayer implements MediaPlayer2, Player, Properties {

    private DBusConnection connection;

    public static final String OBJECT_PATH = "/org/mpris/MediaPlayer2";

    private static final InitHelper<StringBuilder> PLAYER_DBUS_NAME = new InitHelper<>(new StringBuilder()) {
        @Override
        public void init(StringBuilder instance) {
            instance.append(String.format("%s.%s", OBJECT_PATH.replace("/", ".").substring(1), Main.APP_NAME));
            if (Main.getInstance().isDebug()) {
                instance.append(".debug");
            }
        }
    };

    private final MusicPlayer player;


    private final MprisProperty<String> playBackStatus = new MprisProperty<>("PlaybackStatus", MprisProperty.Interface.PLAYER, PlaybackStatus.STOPPED.toString());
    private final MprisProperty<Boolean> shuffle = new MprisProperty<>("Shuffle", MprisProperty.Interface.PLAYER, false);
    private final MprisProperty<Long> position = new MprisProperty<>("Position", MprisProperty.Interface.PLAYER, 0L);

    private final MprisProperty<Variant<?>> metadata = new MprisProperty<>("Metadata", MprisProperty.Interface.PLAYER, null);

    private final List<MprisProperty<?>> properties = List.of(
            this.playBackStatus,
            this.shuffle,
            this.metadata,
            this.position,
            new MprisProperty<>("LoopStatus", MprisProperty.Interface.PLAYER, "None"),
            new MprisProperty<>("Rate", MprisProperty.Interface.PLAYER, 1.0),
            new MprisProperty<>("Volume", MprisProperty.Interface.PLAYER, 1.0),
            new MprisProperty<>("MinimumRate", MprisProperty.Interface.PLAYER, 1.0),
            new MprisProperty<>("MaximumRate", MprisProperty.Interface.PLAYER, 1.0),
            new MprisProperty<>("CanControl", MprisProperty.Interface.PLAYER, true),
            new MprisProperty<>("CanGoNext", MprisProperty.Interface.PLAYER, true),
            new MprisProperty<>("CanGoPrevious", MprisProperty.Interface.PLAYER, true),
            new MprisProperty<>("CanPlay", MprisProperty.Interface.PLAYER, true),
            new MprisProperty<>("CanPause", MprisProperty.Interface.PLAYER, true),
            new MprisProperty<>("CanSeek", MprisProperty.Interface.PLAYER, true),
            new MprisProperty<>("CanQuit", MprisProperty.Interface.DEFAULT, true),
            new MprisProperty<>("FullScreen", MprisProperty.Interface.DEFAULT, false),
            new MprisProperty<>("CanSetFullscreen", MprisProperty.Interface.DEFAULT, false),
            new MprisProperty<>("CanRaise", MprisProperty.Interface.DEFAULT, true),
            new MprisProperty<>("HasTrackList", MprisProperty.Interface.DEFAULT, false),
            new MprisProperty<>("Identity", MprisProperty.Interface.DEFAULT, Main.APP_NAME),
            new MprisProperty<>("DesktopEntry", MprisProperty.Interface.DEFAULT, "file://" +
                    Objects.requireNonNull(Paths.get(System.getProperty("user.home"),
                            ".local", "share", "applications").toFile().listFiles())[0]),
            new MprisProperty<>("SupportedUriSchemes", MprisProperty.Interface.DEFAULT, new String[]{"file"}),
            new MprisProperty<>("SupportedMimeTypes", MprisProperty.Interface.DEFAULT, new String[]{"audio/mp3"})

    );

    public MprisPlayer(MusicPlayer player) {
        this.player = player;

        player.playerStoppedProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue && !oldValue) {
                this.stop();
            } else if (oldValue && !newValue) {
                this.connect();
                try {
                    this.metadata.updateValue(MetadataExtractor.toMetadata(player.getCurrentSong()));
                } catch (DBusException e) {
                    e.printStackTrace();
                }
            }
        });

        player.nowPlayingProperty().addListener((observable, oldValue, newValue) -> {
            try {
                this.metadata.updateValue(MetadataExtractor.toMetadata(newValue));
            } catch (DBusException e) {
                e.printStackTrace();
            }
        });


        player.playStatusProperty().addListener((observable, oldValue, newValue) -> {
            try {
                this.playBackStatus.updateValue((oldValue && !newValue ? PlaybackStatus.PAUSED : PlaybackStatus.PLAYING).toString());
            } catch (DBusException e) {
                e.printStackTrace();
            }
        });

        player.shuffleProperty().addListener((observable, oldValue, newValue) -> {
            try {
                this.shuffle.updateValue(newValue);
            } catch (DBusException e) {
                e.printStackTrace();
            }
        });

        this.position.valueProperty().bind(Bindings.createObjectBinding(
                () -> (long) player.currentSongPositionProperty().get() * MetadataExtractor.SECOND_TO_MICROSECOND,
                player.currentSongPositionProperty()
        ));
    }

    public void connect() {
        try {
            this.connection = DBusConnection.getConnection(DBusConnection.DBusBusType.SESSION);
            this.connection.exportObject(OBJECT_PATH, this);
            this.connection.requestBusName(PLAYER_DBUS_NAME.get().toString());

            MprisProperty.connect(this.connection);
        } catch (DBusException e) {
            e.printStackTrace();
        }
    }

    public void stop() {
        if (this.connection != null) {
            this.connection.disconnect();
        }
    }

    @Override
    public void Raise() {
        Platform.runLater(() -> Main.getInstance().show());
    }

    @Override
    public void Quit() {
        Platform.exit();
    }

    @Override
    public void Next() {
        Platform.runLater(this.player::next);
    }

    @Override
    public void Previous() {
        Platform.runLater(this.player::previous);
    }

    @Override
    public void Pause() {
        Platform.runLater(this.player::togglePlaying);
    }

    @Override
    public void PlayPause() {
        Platform.runLater(this.player::togglePlaying);
    }

    @Override
    public void Play() {
        Platform.runLater(this.player::togglePlaying);
    }

    @Override
    public void Stop() {
        Platform.runLater(this.player::clearQueue);
    }

    @Override
    public void Seek(long l) {
        long x = l / MetadataExtractor.SECOND_TO_MICROSECOND;
        Platform.runLater(() -> this.player.skipTo(Duration.seconds(x)));
    }

    @Override
    public void SetPosition(int TrackId, long Position) {
        System.out.println("test");
    }

    @Override
    public void OpenUri(String uri) {

    }

    @Override
    public boolean isRemote() {
        return false;
    }

    @Override
    public String getObjectPath() {
        return OBJECT_PATH;
    }

    @Override
    public <A> A Get(String interface_name, String property_name) {
        A response = (A) GetAll(interface_name).get(property_name);
        if (property_name.equalsIgnoreCase("Position")) {
            return (A) this.position.getValue();
        }

        return response;
    }

    @Override
    public <A> void Set(String interface_name, String property_name, A value) {
        if (property_name.equalsIgnoreCase("Volume")) {
            this.player.setVolume(Double.parseDouble(String.valueOf(value)));
        }

        System.out.println("set called for " + interface_name + " prop: " + property_name + " val " + value);
    }

    public enum PlaybackStatus {
        PLAYING("Playing"), PAUSED("Paused"), STOPPED("Stopped");

        String asString;

        PlaybackStatus(String str) {
            this.asString = str;
        }

        @Override
        public String toString() {
            return this.asString;
        }
    }


    @Override
    public Map<String, Variant<?>> GetAll(String interface_name) {
        return this.properties.stream().filter(p -> p.getInterface().str.equalsIgnoreCase(interface_name) && p.getValue() != null)
                .collect(Collectors.toMap(
                        MprisProperty::getName, MprisProperty::toVariant
                ));
    }
}
