package fe.materialplayer.controller.sidebar.language

import fe.materialplayer.controller.content.ContentController
import fe.materialplayer.controller.css.StyleBinder
import fe.materialplayer.music.MusicPlayer
import fe.materialplayer.scene.SceneManager
import fe.materialplayer.util.properties.ColorType
import fe.materialplayer.util.resource.ResourceBundleHelper
import javafx.event.EventHandler
import javafx.fxml.FXML
import javafx.scene.control.Label
import javafx.scene.layout.HBox
import javafx.scene.text.Font
import java.net.URL
import java.util.*

class ChangeLanguageController : ContentController(), LanguageBaseController {
    @FXML
    private lateinit var lbSelectLang: Label

    @FXML
    private lateinit var hbLanguages: HBox

    private val player = MusicPlayer.getInstance()

    override fun initialize(location: URL, resources: ResourceBundle) {
        super.initialize(location, resources)

        StyleBinder.bindPaintToThemeColor(ColorType.TEXT_COLOR, lbSelectLang.textFillProperty())

        //same problem as in SettingsController.kt .-.
        lbSelectLang.font = Font("HK Grotesk SemiBold", 16.0)

        this.addLanguages(this.hbLanguages).forEach {
            it.onMouseClicked = EventHandler {
                this.player.clearQueue()

                SceneManager.reload()
                this.closeDialog()
            }
        }
    }
}