package fe.materialplayer.controller.sidebar.language

import com.google.gson.JsonPrimitive
import fe.materialplayer.util.image.ImageUtil
import fe.materialplayer.util.properties.Properties
import fe.materialplayer.util.resource.ResourceBundleHelper
import javafx.scene.control.Hyperlink
import javafx.scene.input.MouseEvent
import javafx.scene.layout.HBox
import java.util.*

interface LanguageBaseController {

    fun addLanguages(hbLanguages: HBox): MutableList<Hyperlink> {
        val list = mutableListOf<Hyperlink>()

        AVAILABLE_LOCALES.forEach { (key, value) ->
            val hyperlink = Hyperlink(key, ImageUtil.makeIconIv("flag/%s".format(value.toLanguageTag()), 20)).apply {
                this.addEventHandler(MouseEvent.MOUSE_CLICKED) {
                    PROPERTIES.addProperty("lang", JsonPrimitive(value.toLanguageTag())).save()
                    ResourceBundleHelper.reset()
                }
            }

            hbLanguages.children.add(hyperlink)
            list += hyperlink
        }

        return list
    }

    companion object {
        private val PROPERTIES = Properties.getInstance()
        private val AVAILABLE_LOCALES = mapOf(
            "English" to Locale.ENGLISH,
            "Deutsch" to Locale.GERMAN
        )
    }
}