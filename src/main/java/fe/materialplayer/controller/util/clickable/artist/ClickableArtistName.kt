package fe.materialplayer.controller.util.clickable.artist

import fe.materialplayer.controller.base.dialog.DialogController
import fe.materialplayer.controller.content.artist.ArtistContentController
import fe.materialplayer.controller.css.applyStylesheet
import fe.materialplayer.data.indexable.Artist
import javafx.event.EventHandler
import javafx.scene.control.Label

class ClickableArtistName(val artist: Artist, private val dialogController: DialogController) {
    fun createLabel(): Label {
        return Label(artist.name)
            .apply {
                applyStylesheet("label-hover-underline")
                onMouseClicked = EventHandler {
                    dialogController.closeDialog()
                    ArtistContentController.show(artist, null)
                }
            }
    }
}