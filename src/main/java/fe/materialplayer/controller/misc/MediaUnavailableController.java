package fe.materialplayer.controller.misc;

import fe.materialplayer.controller.base.dialog.ClosableDialogController;
import fe.materialplayer.controller.content.ContentController;
import fe.materialplayer.data.indexable.Song;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import org.jetbrains.annotations.NotNull;

import java.net.URL;
import java.util.ResourceBundle;

public class MediaUnavailableController extends ContentController {
    @FXML
    private Label lbSongName;

    private final Song song;

    public MediaUnavailableController(Song song) {
        super(false, false);
        this.song = song;
    }

    @Override
    public void initialize(@NotNull URL location, @NotNull ResourceBundle resources) {
        this.lbSongName.setText(String.format("Song %s (%s) is unavailable, removing from library..", this.song, this.song.getPath()));
    }
}
