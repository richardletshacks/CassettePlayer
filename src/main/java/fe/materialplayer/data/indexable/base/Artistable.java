package fe.materialplayer.data.indexable.base;

import fe.materialplayer.data.indexable.Artist;

import java.util.List;

public abstract class Artistable extends Indexable {
    public abstract List<Artist> getArtists();
    public abstract String getArtistName();
}
