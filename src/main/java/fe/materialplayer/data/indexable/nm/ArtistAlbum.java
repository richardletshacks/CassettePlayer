package fe.materialplayer.data.indexable.nm;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import fe.materialplayer.data.indexable.Album;
import fe.materialplayer.data.indexable.Artist;
import fe.materialplayer.data.indexable.base.table.NMTable;
import kotlin.Pair;
import org.jetbrains.annotations.NotNull;

import java.util.Objects;

@DatabaseTable(tableName = "artist_album")
public class ArtistAlbum implements NMTable<Integer, Integer> {

    public static final String ARTIST_ID_COLUMN_NAME = "artist_id";
    public static final String ALBUM_ID_COLUMN_NAME = "album_id";

    @DatabaseField(foreign = true, uniqueCombo = true, columnName = ARTIST_ID_COLUMN_NAME)
    private Artist artist;

    @DatabaseField(foreign = true, uniqueCombo = true, columnName = ALBUM_ID_COLUMN_NAME)
    private Album album;


    public ArtistAlbum(Artist artist, Album album) {
        this.artist = artist;
        this.album = album;
    }

    public ArtistAlbum() {
    }

    public int getArtistId() {
        return this.artist.getId();
    }

    public int getAlbumId() {
        return this.album.getId();
    }

    public Artist getArtist() {
        return artist;
    }

    public void setArtist(Artist artist) {
        this.artist = artist;
    }

    public Album getAlbum() {
        return album;
    }

    public void setAlbum(Album album) {
        this.album = album;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof ArtistAlbum) {
            ArtistAlbum aa = (ArtistAlbum) o;
            return aa.getArtistId() == getArtistId() && aa.getAlbumId() == getAlbumId();
        }

        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(artist, album);
    }

    @NotNull
    @Override
    public Pair<Integer, Integer> colValues() {
        return new Pair<>(this.getArtistId(), this.getAlbumId());
    }


    @Override
    public String toString() {
        return "ArtistAlbum{" +
                "artist=" + artist +
                ", album=" + album +
                '}';
    }
}
