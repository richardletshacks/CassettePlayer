package fe.materialplayer.data.dao.helper

import fe.materialplayer.data.dao.base.NMDaoHelper
import fe.materialplayer.data.dao.base.SetupDao
import fe.materialplayer.data.dao.base.SetupDao.Companion.library
import fe.materialplayer.data.indexable.*
import fe.materialplayer.data.indexable.base.table.Table
import fe.materialplayer.data.indexable.nm.ArtistAlbum
import fe.materialplayer.data.indexable.nm.ArtistSong
import java.sql.SQLException
import java.util.*

class ArtistAlbumDaoHelper : NMDaoHelper<ArtistAlbum, Artist, Album>(
        ArtistAlbum::class.java,
        ArtistAlbum.ARTIST_ID_COLUMN_NAME to  ArtistAlbum.ALBUM_ID_COLUMN_NAME,
        ArtistAlbum.ALBUM_ID_COLUMN_NAME, { it.id }
), SetupDao {

    @Throws(SQLException::class)
    fun batchCreateAndSetup(albums: List<Album>, songs: List<Song>) {
        val artistAlbums = mutableListOf<ArtistAlbum>()
        for (album in albums) {
            for (artist in album.artists) {
                //when adding new media after the first initial scan, we check if the artist already has this album,
                // which indicates that the album must have already been in the database before, and therefore skipping the creation of it
                if (!artist.albums.contains(album)) {
                    val artistAlbum = ArtistAlbum(artist, album)
                    if (artistAlbum !in items) {
                        artistAlbums.add(artistAlbum)
                    }
                }
            }
        }

        this.addToList(artistAlbums)
        this.batchCreate(artistAlbums)
        this.setup(artistAlbums)

        //link album to all of its songs
        artistAlbums.forEach { album ->
            songs.forEach { song ->
                if (song.album.id == album.albumId) {
                    song.album = album.album
                }
            }
        }
    }

    override fun setup(queryResult: MutableList<out Table>) {
        queryResult.forEach {
            val artistAlbum = it as ArtistAlbum

            val album = library.albumDao.findItemById(artistAlbum.albumId)
            val artist = library.artistDao.findItemById(artistAlbum.artistId)

            artistAlbum.album = album
            artistAlbum.artist = artist

            artist?.let { art ->
                if (!art.albums.contains(album)) {
                    art.albums.add(album)
                }
            } ?: println("artist is null, this should never happen!")

            album?.let { alb ->
                if (!alb.artists.contains(artist)) {
                    alb.artists.add(artist)
                }
            } ?: println("album is null, this should never happen!")
        }
    }

    override fun findRelationFirst(first: Artist) = items.filter { it.artist == first }
    override fun findRelationSecond(second: Album) = items.filter { it.album == second }
}

