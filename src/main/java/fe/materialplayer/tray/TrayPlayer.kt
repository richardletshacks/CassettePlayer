package fe.materialplayer.tray

import fe.materialplayer.Main
import javafx.application.Platform
import java.awt.MenuItem
import java.awt.PopupMenu
import java.awt.SystemTray
import java.awt.TrayIcon
import javax.imageio.ImageIO

object TrayPlayer {
    private var trayIcon: TrayIcon? = null

    fun show(): Boolean {
        if (SystemTray.isSupported()) {
            val tray = SystemTray.getSystemTray()

            val pm = PopupMenu()
            val miShow = MenuItem("Show")
            miShow.addActionListener {
                Platform.runLater { Main.getInstance().show() }
            }

            val miExit = MenuItem("Exit")
            miExit.addActionListener {
                Platform.exit()
            }

            pm.add(miShow)
            pm.add(miExit)

            this.trayIcon = TrayIcon(ImageIO.read(TrayPlayer.javaClass.getResource("/icon/logo/ic_launcher_round_small.png")), Main.APP_NAME, pm)

            tray.add(trayIcon)
            return true
        }

        return false
    }

    fun remove() {
        if (SystemTray.isSupported()) {
            this.trayIcon?.let {
                SystemTray.getSystemTray().remove(it)
                this.trayIcon = null
            }
        }
    }

    fun isPresent() = trayIcon != null
}