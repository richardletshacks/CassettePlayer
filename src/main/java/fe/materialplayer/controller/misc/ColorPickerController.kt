package fe.materialplayer.controller.misc

import com.google.gson.JsonPrimitive
import com.jfoenix.controls.JFXColorPicker
import com.jfoenix.controls.JFXComboBox
import fe.materialplayer.Main
import fe.materialplayer.controller.css.AnnotationBinder
import fe.materialplayer.controller.css.annotation.color.PrimaryColored
import fe.materialplayer.controller.css.annotation.text.PrimaryExtractedTextFill
import fe.materialplayer.controller.css.annotation.theme.ThemeBackgroundColor
import fe.materialplayer.controller.css.annotation.theme.ThemeTextColor
import fe.materialplayer.controller.util.NamedKeyComboFactory
import fe.materialplayer.util.properties.ColorThemeManager
import fe.materialplayer.util.properties.ColorThemeManager.properties
import fe.materialplayer.util.properties.PlayerTheme
import fe.materialplayer.util.color.ColorPreset
import fe.materialplayer.util.hexString
import javafx.beans.property.ObjectProperty
import javafx.collections.FXCollections
import javafx.event.ActionEvent
import javafx.fxml.FXML
import javafx.fxml.Initializable
import javafx.geometry.Pos
import javafx.scene.control.ColorPicker
import javafx.scene.layout.HBox
import javafx.scene.paint.Color
import java.net.URL
import java.util.*

class ColorPickerController : Initializable {

    @FXML
    private lateinit var cpPrimary: JFXColorPicker

    @FXML
    private lateinit var cpLightPrimary: JFXColorPicker

    @FXML
    lateinit var cpAccent: JFXColorPicker

    @FXML
    @ThemeBackgroundColor
    @PrimaryColored
    @PrimaryExtractedTextFill
    private lateinit var cbPresets: JFXComboBox<ColorPreset>

    @FXML
    @ThemeBackgroundColor
    @PrimaryColored
    @PrimaryExtractedTextFill
    private lateinit var cbTheme: JFXComboBox<PlayerTheme>

    @FXML
    private lateinit var hbColorSelection: HBox

    override fun initialize(location: URL, resources: ResourceBundle) {
        AnnotationBinder.createBindings(this)

        cbPresets.focusColorProperty().bind(cpPrimary.valueProperty())

        val presetFactory = NamedKeyComboFactory<ColorPreset>(resources)
        val themeFactory = NamedKeyComboFactory<PlayerTheme>(resources)

        cbPresets.items = ColorPreset.presets
        ColorPreset.findCurrentPresetMatch()?.let {
            cbPresets.selectionModel.select(it)
        }

        cbPresets.buttonCell = presetFactory.call(null)
        cbPresets.cellFactory = presetFactory

        cbTheme.items = FXCollections.observableList(PlayerTheme.values().toList())
        cbTheme.selectionModel.select(ColorThemeManager.theme.get())
        cbTheme.buttonCell = themeFactory.call(null)
        cbTheme.cellFactory = themeFactory

        if (!Main.getInstance().isDonatorBuild) {
            cpPrimary.isDisable = true
            cpLightPrimary.isDisable = true
            cpAccent.isDisable = true
            cbPresets.isDisable = true
            cbTheme.isDisable = true
        }

        listOf(
                ColorPickHelper(cpPrimary, ColorThemeManager.primaryColor, ColorThemeManager.PROP_PRIMARY_COLOR),
                ColorPickHelper(cpLightPrimary, ColorThemeManager.lightPrimaryColor, ColorThemeManager.PROP_LIGHT_PRIMARY_COLOR),
                ColorPickHelper(cpAccent, ColorThemeManager.accentColor, ColorThemeManager.PROP_ACCENT_COLOR)
        ).forEach {
            it.colorPicker.value = it.color.get()
            it.colorPicker.valueProperty().addListener { _, _, newValue ->
                it.color.set(newValue)
                properties.addProperty(it.property, JsonPrimitive(it.color.get().hexString)).save()
            }
        }
    }

    @FXML
    fun cbThemeChanged(event: ActionEvent?) {
        ColorThemeManager.theme.set(cbTheme.value)
        properties.addProperty(ColorThemeManager.PROP_THEME, JsonPrimitive(ColorThemeManager.theme.get().toString())).save()
    }

    @FXML
    fun cbPresetsChanged(event: ActionEvent?) {
        with(cbPresets.value) {
            cpPrimary.value = this.primary
            cpLightPrimary.value = this.lightPrimary
            cpAccent.value = this.accent
        }
    }

    fun setPresetAlignment(alignment: Pos) {
        hbColorSelection.alignment = alignment
    }
}

data class ColorPickHelper(val colorPicker: ColorPicker, val color: ObjectProperty<Color>, val property: String)
