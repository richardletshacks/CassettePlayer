package fe.materialplayer.data.dao.helper

import fe.materialplayer.data.dao.DaoUtil
import fe.materialplayer.data.dao.base.IdDaoHelper
import fe.materialplayer.data.indexable.Genre
import fe.materialplayer.data.indexable.Song

class GenreDaoHelper : IdDaoHelper<Genre>(Genre::class.java, "name", true) {

    override fun queryAll(): MutableList<Genre> {
        //we override this method since songs are cached which means that if they are refreshed,
        // they do not pull a new instance of genre from the db, so in order to have a refreshed genre object in song,
        // we iterate over the songs of all genres and set the genre manually
        return super.queryAll().onEach { genre ->
            genre.songs.forEach { song ->
                song.genre = genre
            }
        }
    }

    fun existsGenre(genre: String?): Genre? {
//         synchronized(daoHelperLock) {
        return DaoUtil.createUnknownIfNotExists(genre, UNKNOWN_GENRE, this.items)
            ?: this.items.find { it.name.equals(genre, ignoreCase = true) }
//        }
    }

    fun checkRemoveGenre(song: Song): Boolean {
        return song.genre.run {
            this != null && this.songs.size == 1
        }
    }

    companion object {
        @JvmStatic
        val UNKNOWN_GENRE = Genre("Unknown Genre")
    }
}