package fe.materialplayer.font

import fe.materialplayer.util.SingletonHolder
import java.io.File

class FontManager private constructor(baseDir: File) {
    var fontDir: File = File(baseDir, "fonts").also {
        it.mkdirs()
    }
    private val fontSize = 20

    private val fonts = mutableListOf(ROBOTO, HK_GROTESK_SEMI_BOLD, HK_GROTESK)

    companion object : SingletonHolder<FontManager, File>(::FontManager) {
        private const val basePath = "/font"

        val ROBOTO = PlayerFont("Roboto", "$basePath/roboto/Roboto-Regular.ttf", FontType.INTERNAL)
        val HK_GROTESK_SEMI_BOLD = PlayerFont("HK Grotesk SemiBold", "$basePath/hkgrotesk/hkgrotesk-semibold.ttf", FontType.INTERNAL)
        val HK_GROTESK = PlayerFont("HK Grotesk", "$basePath/hkgrotesk/hkgrotesk-regular.ttf", FontType.INTERNAL)
    }

    fun loadFonts() {
        fonts.forEach { it.load(fontSize) }
    }

//    fun findByName(name: String): PlayerFont? {
//        return fonts.find {
//            it.name == name
//        }
//    }
}