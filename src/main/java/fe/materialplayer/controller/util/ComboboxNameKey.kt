package fe.materialplayer.controller.util

import fe.materialplayer.controller.css.StyleBinder
import fe.materialplayer.controller.css.bindStyle
import fe.materialplayer.util.properties.ColorThemeManager
import fe.materialplayer.util.properties.ColorType
import fe.materialplayer.util.StyleConstants
import javafx.scene.control.ListCell
import javafx.scene.control.ListView
import javafx.util.Callback
import java.util.*

interface ComboboxNameKey {
    fun nameKey(): String
}

class NamedKeyComboFactory<T : ComboboxNameKey>(val resources: ResourceBundle) : Callback<ListView<T>, ListCell<T>> {
    private var hasAppliedCSS = false

    override fun call(param: ListView<T>?): ListCell<T> {
        if (param != null && !hasAppliedCSS) {
            param.bindStyle()
                    .applyStyleSheet("combobox-content")
                    .bindToColorProperty(StyleConstants.CSS_PRIMARY_COLOR_VAR, ColorThemeManager.primaryColor, false)
                    .bindToThemeColor(StyleConstants.CSS_BACKGROUND_COLOR_VAR, ColorType.BACKGROUND_COLOR).build()

            hasAppliedCSS = true
        }

        return object : ListCell<T>() {
            override fun updateItem(item: T?, empty: Boolean) {
                super.updateItem(item, empty)
                text = if (item != null && !empty) {
                    resources.getString(item.nameKey())
                } else {
                    null
                }

                StyleBinder.bindPaintToThemeColor(ColorType.TEXT_COLOR, textFillProperty())
            }
        }
    }
}