package fe.materialplayer.controller.base

data class SearchResult<T>(val searchText: String?, val state: SearchResultState, val results: MutableList<T>?)

enum class SearchResultState {
    RESET, FOUND, NOT_FOUND, TEXT_NULL
}