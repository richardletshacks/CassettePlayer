package fe.materialplayer.data.indexable.nm;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import fe.materialplayer.data.indexable.Playlist;
import fe.materialplayer.data.indexable.Song;
import fe.materialplayer.data.indexable.base.table.IdTable;
import fe.materialplayer.data.indexable.base.table.NMTable;
import kotlin.Pair;
import org.jetbrains.annotations.NotNull;

@DatabaseTable(tableName = "playlist_song")
public class PlaylistSong implements NMTable<Integer, Integer> {

    public static final String PLAYLIST_ID_COLUMN_NAME = "playlist_id";
    public static final String INDEX_COLUMN_NAME = "index";

    @DatabaseField(foreign = true, uniqueCombo = true, columnName = PLAYLIST_ID_COLUMN_NAME)
    private Playlist playlist;

    @DatabaseField(foreign = true)
    private Song song;

    @DatabaseField(uniqueCombo = true, columnName = INDEX_COLUMN_NAME)
    private int index;

    public PlaylistSong() { }

    public PlaylistSong(Playlist playlist, Song song, int index) {
        this.playlist = playlist;
        this.song = song;
        this.index = index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public void setPlaylist(Playlist playlist) {
        this.playlist = playlist;
    }

    public void setSong(Song song) {
        this.song = song;
    }

    public Playlist getPlaylist() {
        return playlist;
    }

    public int getPlaylistId(){
        return this.playlist.getId();
    }

    public int getSongId(){
        return this.song.getId();
    }

    public Song getSong() {
        return song;
    }

    public int getIndex() {
        return index;
    }

    @NotNull
    @Override
    public Pair<Integer, Integer> colValues() {
        return new Pair<>(this.getPlaylistId(), this.index);
    }
}
