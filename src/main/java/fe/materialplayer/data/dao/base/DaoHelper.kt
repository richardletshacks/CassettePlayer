package fe.materialplayer.data.dao.base

import com.j256.ormlite.dao.Dao
import com.j256.ormlite.dao.DaoManager
import com.j256.ormlite.stmt.DeleteBuilder
import com.j256.ormlite.stmt.UpdateBuilder
import com.j256.ormlite.support.ConnectionSource
import fe.materialplayer.data.indexable.base.table.Table
import fe.materialplayer.data.sort.SortType
import javafx.collections.FXCollections
import javafx.collections.ObservableList
import java.sql.SQLException
import java.util.*
import java.util.concurrent.locks.ReentrantLock


abstract class DaoHelper<T : Table>(
    val clazz: Class<T>,
    private val orderCol: String,
    private val useCache: Boolean = false
) {

    protected lateinit var dao: Dao<T, Int>

    val items: ObservableList<T> = FXCollections.observableList(LinkedList())

    open fun makeDao(source: ConnectionSource) {
        this.dao = DaoManager.createDao(source, clazz)
        this.dao.setObjectCache(useCache)
    }

    //TODO: improve? disabling the cache clears it, is that intended?
    fun performUncached(func: (daoHelper: DaoHelper<T>) -> Unit) {
        this.useCache(false)
        func.invoke(this)
        this.useCache(true)
    }

    fun useCache(cache: Boolean) {
        this.dao.setObjectCache(cache)
    }

    open fun addToList(item: T) {
        this.items.add(item)
    }

    fun addToList(list: List<T>) {
        list.forEach {
            addToList(it)
        }
    }

    fun removeFromList(list: List<T>) {
        list.forEach {
            removeFromList(it)
        }
    }

    open fun removeFromList(item: T) {
        items.remove(item)
    }

    @Throws(SQLException::class)
    open fun queryAll(): MutableList<T> {
        val builder = this.dao.queryBuilder()
        builder.orderBy(this.orderCol, true)

        val result = this.dao.query(builder.prepare())
        this.addToList(result)

        return result
    }

    @Throws(SQLException::class)
    open fun createAndRefresh(item: T): T {
        this.create(item)
        return this.refresh(item)
    }

    @Throws(SQLException::class)
    open fun updateAndRefresh(item: T): T {
        this.update(item)
        return this.refresh(item)
    }

    @Throws(SQLException::class)
    abstract fun refresh(item: T): T

    fun getSortedCopy(sortType: SortType<T>): LinkedList<T> {
        return LinkedList(this.items).apply {
            this.sortWith(sortType.getSortComparator())
        }
    }

    @Throws(SQLException::class)
    fun batchRefresh(itemList: List<T>) = this.dao.callBatchTasks {
        for (item in itemList) {
            this.refresh(item)
        }
    }!!

    @Throws(SQLException::class)
    open fun delete(item: T, removeItem: Boolean = false): Int {
        val int = this.dao.delete(item)
        if (removeItem) {
            this.removeFromList(item)
        }

        return int
    }

    @Throws(SQLException::class)
    fun create(item: T) = this.dao.create(item)

    @Throws(SQLException::class)
    fun update(item: T) = this.dao.update(item)

    fun batchDelete(itemList: List<T>, removeItems: Boolean = false) {
        this.dao.callBatchTasks {
            itemList.forEach { item ->
                try{
                    this.delete(item)
                }catch(e: SQLException){
                    e.printStackTrace()
                }
            }
        }

        if (removeItems) {
            this.removeFromList(itemList)
        }
    }

    fun batchCreateExisting() = this.batchCreate(this.items)

    fun batchCreate(itemList: MutableList<T>) = this.dao.callBatchTasks {
        itemList.forEach { item ->
            try {
                this.create(item)
            } catch (e: SQLException) {
                e.printStackTrace()
            }
        }
    }!!

    fun updateBuilder(): UpdateBuilder<T, Int> = this.dao.updateBuilder()
    fun deleteBuilder(): DeleteBuilder<T, Int> = this.dao.deleteBuilder()
}
