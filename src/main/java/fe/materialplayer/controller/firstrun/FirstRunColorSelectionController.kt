package fe.materialplayer.controller.firstrun

import com.jfoenix.controls.JFXButton
import fe.materialplayer.Main
import fe.materialplayer.controller.base.dialog.UnclosableDialogController
import fe.materialplayer.controller.css.StyleBinder
import fe.materialplayer.controller.css.annotation.color.PrimaryColored
import fe.materialplayer.controller.css.annotation.property.IntCSSProperties
import fe.materialplayer.controller.css.annotation.property.IntCSSProperty
import fe.materialplayer.controller.css.annotation.text.PrimaryExtractedTextFill
import fe.materialplayer.controller.css.annotation.theme.ThemeBackgroundColor
import fe.materialplayer.controller.css.annotation.theme.ThemeTextColor
import fe.materialplayer.controller.misc.ColorPickerController
import fe.materialplayer.util.StyleConstants
import fe.materialplayer.util.properties.ColorType
import fe.materialplayer.util.image.ImageUtil
import fe.materialplayer.util.resource.FxmlUtil
import javafx.event.ActionEvent
import javafx.fxml.FXML
import javafx.geometry.Pos
import javafx.scene.control.Label
import javafx.scene.layout.GridPane
import javafx.scene.layout.StackPane
import javafx.scene.layout.VBox
import javafx.scene.text.Text
import java.net.URL
import java.util.*


class FirstRunColorSelectionController : UnclosableDialogController() {

    @FXML
    @ThemeBackgroundColor(cssProperty = StyleConstants.CSS_BACKGROUND_COLOR_PROPERTY)
    @IntCSSProperties(
            IntCSSProperty(property = StyleConstants.CSS_BACKGROUND_RADIUS, 7),
            IntCSSProperty(property = StyleConstants.CSS_BORDER_RADIUS, 7))
    private lateinit var gpRoot: GridPane

    @FXML
    private lateinit var lbWelcome: Label

    @FXML
    private lateinit var txColor: Text

    @FXML
    @ThemeTextColor
    private lateinit var spColorSelection: StackPane

    @FXML
    @PrimaryColored(cssProperty = StyleConstants.CSS_BACKGROUND_COLOR_PROPERTY)
    @PrimaryExtractedTextFill(cssProperty = StyleConstants.CSS_TEXT_FILL_PROPERTY)
    private lateinit var btNext: JFXButton

    @FXML
    private lateinit var btBack: JFXButton

    override fun initialize(location: URL, resources: ResourceBundle) {
        super.initialize(location, resources)

        StyleBinder.bindPaintToThemeColor(ColorType.TEXT_COLOR,
                lbWelcome.textFillProperty(),
                txColor.fillProperty())

        btBack.graphic = ImageUtil.makeIconIv("back_grey", 17)

        if (!Main.getInstance().isDonatorBuild) {
            txColor.text = resources.getString("donatorRestricted")
        }

        val colorSelectionController = ColorPickerController()
        val colorSelection = FxmlUtil.loadFxml("ColorPicker", colorSelectionController).load<VBox>()
        colorSelectionController.setPresetAlignment(Pos.CENTER)

        spColorSelection.children.add(colorSelection)

        btBack.ripplerFillProperty().bind(colorSelectionController.cpAccent.valueProperty())
    }

    @FXML
    fun btBackClicked(event: ActionEvent?) {
        this.previous()
    }

    @FXML
    fun btNextClicked(event: ActionEvent?) {
        properties.save()
        this.next()
    }
}