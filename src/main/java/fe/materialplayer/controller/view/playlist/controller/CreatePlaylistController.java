package fe.materialplayer.controller.view.playlist.controller;

import com.jfoenix.controls.JFXTextField;
import com.jfoenix.controls.materialbutton.JFXContainedMaterialButton;
import com.jfoenix.controls.materialbutton.JFXTextMaterialButton;
import fe.materialplayer.controller.content.ContentController;
import fe.materialplayer.controller.css.annotation.color.LightPrimaryColored;
import fe.materialplayer.controller.css.annotation.color.PrimaryColored;
import fe.materialplayer.controller.css.annotation.text.PrimaryExtractedTextFill;
import fe.materialplayer.controller.css.annotation.theme.ThemeSecondaryTextColor;
import fe.materialplayer.controller.css.annotation.theme.ThemeTextColor;
import fe.materialplayer.controller.view.playlist.PlaylistAddEvent;
import fe.materialplayer.controller.view.playlist.validator.PlaylistNameValidatorHelper;
import fe.materialplayer.data.indexable.Playlist;
import fe.materialplayer.music.MusicLibrary;
import fe.materialplayer.util.StyleConstants;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import org.jetbrains.annotations.NotNull;

import java.net.URL;
import java.util.ResourceBundle;

public class CreatePlaylistController extends ContentController {

    @FXML
    @ThemeTextColor
    private Label lbCreatePlaylist;

    @FXML
    @PrimaryColored
    @ThemeTextColor
    @ThemeSecondaryTextColor
    private JFXTextField tfPlaylistName;

    @FXML
    @ThemeTextColor(cssProperty = StyleConstants.CSS_TEXT_FILL_PROPERTY)
    @LightPrimaryColored
    private JFXTextMaterialButton btCancel;

    @FXML
    @PrimaryColored(cssProperty = StyleConstants.CSS_BACKGROUND_COLOR_PROPERTY)
    @PrimaryExtractedTextFill(cssProperty = StyleConstants.CSS_TEXT_FILL_PROPERTY)
    private JFXContainedMaterialButton btCreate;

    private final EventHandler<PlaylistAddEvent> onPlaylistAdd;

    private final MusicLibrary library = MusicLibrary.Companion.getInstance();

    public CreatePlaylistController() {
       this(null);
    }

    public CreatePlaylistController(EventHandler<PlaylistAddEvent> onPlaylistAdd) {
        super(false, false);
        this.onPlaylistAdd = onPlaylistAdd;
    }

    @Override
    public void initialize(@NotNull URL location, @NotNull ResourceBundle resources) {
        super.initialize(location, resources);

        new PlaylistNameValidatorHelper(this.tfPlaylistName, resources);
        this.gpRoot.addEventFilter(KeyEvent.KEY_PRESSED, event -> {
            if (event.getCode() == KeyCode.ENTER) {
                this.btCreateClicked(null);
                event.consume();
            }
        });
    }

    @FXML
    public void btCancelClicked(ActionEvent event) {
        this.closeDialog();
    }


    @FXML
    public void btCreateClicked(ActionEvent event) {
        if (this.tfPlaylistName.validate()) {
            Playlist p = new Playlist(this.tfPlaylistName.getText());
//            try {
            this.library.getPlaylistDao().createPlaylist(p);
//            } catch (SQLException e) {
//                this.closeDialog();
//                new CrashViewerController("CREATE_PLAYLIST", String.format("Error while creating playlist %s", p), e).show();
//            }

            if (this.onPlaylistAdd != null) {
                PlaylistAddEvent evt = new PlaylistAddEvent(EventType.ROOT, p);
                this.onPlaylistAdd.handle(evt);
            }

            this.closeDialog();
        }
    }
}
