package fe.materialplayer.util.resource;

import javafx.concurrent.Task;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.concurrent.ExecutionException;
import java.util.function.Consumer;

public class FxmlUtil {

    public static FXMLLoader loadFxml(String res) {
        return new FXMLLoader(FxmlUtil.class.getResource(String.format("/fxml/%s.fxml", res)),
                ResourceBundleHelper.getBundle());
    }

    public static FXMLLoader loadFxml(String res, Initializable controller) {
        FXMLLoader loader = FxmlUtil.loadFxml(res);
        if (controller != null) {
            loader.setController(controller);
        }

        return loader;
    }

    public static <T> Task<T> loadNonBlockingFxml(String res, Initializable controller, Consumer<T> onLoaded) {
        Task<T> task = new Task<>() {
            @Override
            protected T call() throws Exception {
                return loadFxml(res, controller).load();
            }
        };

        task.setOnSucceeded(event -> {
            try {
                onLoaded.accept(task.get());
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        });

        new Thread(task).start();

        return task;
    }

    public static Stage createStageFromFxml(String res, double width, double height) throws IOException {
        return FxmlUtil.createStageFromFxml(res, null, width, height);
    }

    public static Stage createStageFromFxml(String res, Initializable controller, double width, double height) throws IOException {
        Stage stage = new Stage();
        stage.setScene(new Scene(FxmlUtil.loadFxml(res, controller).load(), width, height));

        return stage;
    }
}
