package fe.materialplayer.controller.view.album

import fe.materialplayer.controller.util.PluralHelper
import fe.materialplayer.controller.util.cell.impl.ColorableCell
import fe.materialplayer.data.indexable.Album
import fe.materialplayer.data.indexable.Song
import fe.materialplayer.music.MusicPlayer
import fe.materialplayer.util.SnackbarHelper.createMinWidthTextBar
import fe.materialplayer.util.image.ImageUtil
import javafx.application.Platform
import javafx.event.EventHandler
import javafx.scene.control.ContentDisplay
import javafx.scene.input.MouseButton
import javafx.scene.input.MouseEvent
import javafx.scene.layout.HBox
import java.util.*


class AlbumListCell(val resourceBundle: ResourceBundle) : ColorableCell<Album>() {

    companion object {
        private val PLAYER = MusicPlayer.getInstance()
    }

    override fun updateItem(item: Album?, empty: Boolean) {
        super.updateItem(item, empty)
        if (empty || item == null || item.songs == null) {
            text = null
            graphic = null
        } else {
            val iv = ImageUtil.makeIv(35)
            val vbox = this.makeLabelBox(item, item.boxLines.apply {
                this.formatValue(PluralHelper.getString(resourceBundle, "%d %s", item.songs, "song", "songs"))
            })

            Platform.runLater { iv.image = item.getCover(Song.CoverType.S60X60) }
            this.makeListeners(item)

            contentDisplay = ContentDisplay.GRAPHIC_ONLY
            graphic = HBox(iv, vbox).apply {
                this.spacing = 10.0
            }

            this.onMouseClicked = EventHandler { evt: MouseEvent ->
                if (evt.button == MouseButton.MIDDLE) {
                    PLAYER.addNextSongs(item.songs, false)
                    createMinWidthTextBar(resourceBundle.getString("addedNextAlbum"), 300.0)
                }
            }
        }
    }
}