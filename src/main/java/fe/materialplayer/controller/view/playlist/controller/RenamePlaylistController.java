package fe.materialplayer.controller.view.playlist.controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import fe.materialplayer.controller.content.ContentController;
import fe.materialplayer.controller.css.annotation.color.AccentColored;
import fe.materialplayer.controller.css.annotation.color.PrimaryColored;
import fe.materialplayer.controller.css.annotation.text.AccentExtractedTextFill;
import fe.materialplayer.controller.css.annotation.theme.ThemeSecondaryTextColor;
import fe.materialplayer.controller.css.annotation.theme.ThemeTextColor;
import fe.materialplayer.controller.view.playlist.validator.PlaylistNameValidatorHelper;
import fe.materialplayer.data.indexable.Playlist;
import fe.materialplayer.music.MusicLibrary;
import fe.materialplayer.util.StyleConstants;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import org.jetbrains.annotations.NotNull;

import java.net.URL;
import java.util.ResourceBundle;

public class RenamePlaylistController extends ContentController {

    @FXML
    @ThemeTextColor(cssProperty = StyleConstants.CSS_TEXT_FILL_PROPERTY)
    private Label lbRename;

    @FXML
    @PrimaryColored
    @ThemeTextColor
    @ThemeSecondaryTextColor
    private JFXTextField tfPlaylistName;

    @FXML
    @PrimaryColored(cssProperty = StyleConstants.CSS_BACKGROUND_COLOR_PROPERTY)
    @AccentExtractedTextFill(cssProperty = StyleConstants.CSS_TEXT_FILL_PROPERTY)
    private JFXButton btCancel;

    @FXML
    @PrimaryColored(cssProperty = StyleConstants.CSS_BACKGROUND_COLOR_PROPERTY)
    @AccentExtractedTextFill(cssProperty = StyleConstants.CSS_TEXT_FILL_PROPERTY)
    private JFXButton btRename;

    private final Playlist playlist;

    private final MusicLibrary library = MusicLibrary.Companion.getInstance();

    public RenamePlaylistController(Playlist playlist) {
        super(false, false);

        this.playlist = playlist;
    }

    @Override
    public void initialize(@NotNull URL location, @NotNull ResourceBundle resources) {
        super.initialize(location, resources);

        new PlaylistNameValidatorHelper(this.tfPlaylistName, resources);
    }

    @FXML
    public void btCancelClicked(ActionEvent event) {
        this.closeDialog();
    }


    @FXML
    public void btRenameClicked(ActionEvent event) {
        if (this.tfPlaylistName.validate()) {
            this.library.getPlaylistDao().renamePlaylist(this.playlist, this.tfPlaylistName.getText());
            this.closeDialog();
        }
    }
}
