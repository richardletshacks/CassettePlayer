package fe.materialplayer.util;

import javafx.scene.effect.BoxBlur;
import javafx.scene.paint.Color;

public class StyleConstants {

    public static final String CSS_PRIMARY_COLOR_VAR = "-primary-color";
    public static final String CSS_LIGHT_PRIMARY_COLOR_VAR = "-light-primary-color";
    public static final String CSS_TEXT_FILL_VAR = "-text-fill";
    public static final String CSS_SECONDARY_TEXT_FILL_VAR = "-secondary-text-fill";
    public static final String CSS_BACKGROUND_COLOR_VAR = "-background-color";

    public static final String CSS_BACKGROUND_RADIUS = "-fx-background-radius";
    public static final String CSS_BORDER_RADIUS = "-fx-border-radius";
    public static final String CSS_BACKGROUND_COLOR_PROPERTY = "-fx-background-color";
    public static final String CSS_TEXT_FILL_PROPERTY = "-fx-text-fill";

    public static final String TRANSPARENT_BACKGROUND = CSS_BACKGROUND_COLOR_PROPERTY + ": transparent";
    public static final String LIGHT_GRAY_BACKGROUND = CSS_BACKGROUND_COLOR_PROPERTY+ ": lightgray";


    public static final Color DEFAULT_PRIMARY_COLOR = Color.valueOf("#496cf8");
    public static final Color DEFAULT_LIGHT_PRIMARY_COLOR = Color.valueOf("#a3b5f8");
    public static final Color DEFAULT_ACCENT_COLOR = Color.valueOf("#f50057");

    public static final Color SUB_TITLE_COLOR = Color.valueOf("#757575");

    public static final BoxBlur BOX_BLUR = new InitHelper<>(new BoxBlur()) {
        @Override
        public void init(BoxBlur instance) {
            instance.setWidth(7.5);
            instance.setHeight(7.5);
            instance.setIterations(10);
        }
    }.get();

    public static String toBackgroundStyle(String str) {
        return String.format("-fx-background-color: %s", str);
    }
}
