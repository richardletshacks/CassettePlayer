package fe.materialplayer.controller.css

import fe.materialplayer.controller.css.annotation.color.AccentColored
import fe.materialplayer.controller.css.annotation.color.LightPrimaryColored
import fe.materialplayer.controller.css.annotation.color.PrimaryColored
import fe.materialplayer.controller.css.annotation.property.IntCSSProperties
import fe.materialplayer.controller.css.annotation.text.AccentExtractedTextFill
import fe.materialplayer.controller.css.annotation.text.LightPrimaryExtractedTextFill
import fe.materialplayer.controller.css.annotation.text.PrimaryExtractedTextFill
import fe.materialplayer.controller.css.annotation.theme.ThemeBackgroundColor
import fe.materialplayer.controller.css.annotation.theme.ThemeSecondaryTextColor
import fe.materialplayer.controller.css.annotation.theme.ThemeTextColor
import fe.materialplayer.util.properties.ColorThemeManager
import fe.materialplayer.util.properties.ColorType
import javafx.fxml.Initializable
import javafx.scene.Node
import java.lang.reflect.Field

object AnnotationBinder {
    fun createBindings(controller: Initializable){
        var clazz: Class<*>? = controller::class.java
        val classes = mutableListOf<Field>(*clazz!!.declaredFields)

        while (clazz?.superclass.also { clazz = it } != null) {
            classes += clazz!!.declaredFields
        }

        classes.forEach { field ->
            field.isAccessible = true

            val fieldValue = field.get(controller)
            if (fieldValue is Node) {
                val binder = StyleBinder(fieldValue)
                field.annotations.forEach { annotation ->
                    when (annotation) {
                        is AccentColored -> {
                            binder.bindToColorProperty(annotation.cssProperty, ColorThemeManager.accentColor, false)
                        }

                        is LightPrimaryColored -> {
                            binder.bindToColorProperty(annotation.cssProperty, ColorThemeManager.lightPrimaryColor, false)
                        }

                        is PrimaryColored -> {
                            binder.bindToColorProperty(annotation.cssProperty, ColorThemeManager.primaryColor, false)
                        }

                        is ThemeBackgroundColor -> {
                            binder.bindToThemeColor(annotation.cssProperty, ColorType.BACKGROUND_COLOR)
                        }

                        is ThemeTextColor -> {
                            binder.bindToThemeColor(annotation.cssProperty, ColorType.TEXT_COLOR)
                        }

                        is ThemeSecondaryTextColor -> {
                            binder.bindToThemeColor(annotation.cssProperty, ColorType.SECONDARY_TEXT_COLOR)
                        }

                        is AccentExtractedTextFill -> {
                            binder.bindToColorProperty(annotation.cssProperty, ColorThemeManager.accentColor, true)
                        }

                        is PrimaryExtractedTextFill -> {
                            binder.bindToColorProperty(annotation.cssProperty, ColorThemeManager.primaryColor, true)
                        }

                        is LightPrimaryExtractedTextFill -> {
                            binder.bindToColorProperty(annotation.cssProperty, ColorThemeManager.lightPrimaryColor, true)
                        }

                        is IntCSSProperties -> {
                            annotation.properties.forEach {
                                binder.setIntCSSProperty(it.property, it.value)
                            }
                        }
                    }
                }

                binder.build()
            }
        }
    }
}