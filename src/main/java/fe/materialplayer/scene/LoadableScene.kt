package fe.materialplayer.scene

import fe.materialplayer.controller.base.SidebarBindController
import fe.materialplayer.util.resource.FxmlUtil
import javafx.beans.property.ObjectProperty
import javafx.beans.property.SimpleObjectProperty
import javafx.scene.Node

class LoadableScene(val name: String,
                    val fxml: String,
                    val controller: SidebarBindController) {

    var currentScene: ObjectProperty<Node> = SimpleObjectProperty()

    fun load(): ObjectProperty<Node> {
        this.currentScene.get()?.let {
            return this.currentScene
        }

        return this.reload()
    }

    fun reload(): ObjectProperty<Node> {
        this.currentScene.set(FxmlUtil.loadFxml(this.fxml, this.controller).load())
        return this.currentScene
    }

    fun getNode(): Node = this.currentScene.get()
}