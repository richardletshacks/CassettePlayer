package fe.materialplayer.controller.view.playlist.controller;

import com.jfoenix.controls.JFXListView;
import com.jfoenix.controls.materialbutton.*;
import fe.materialplayer.controller.base.IndexController;
import fe.materialplayer.controller.base.OptionsBindableController;
import fe.materialplayer.controller.content.playlist.PlaylistContentController;
import fe.materialplayer.controller.css.annotation.color.AccentColored;
import fe.materialplayer.controller.css.annotation.color.LightPrimaryColored;
import fe.materialplayer.controller.css.annotation.color.PrimaryColored;
import fe.materialplayer.controller.css.annotation.theme.ThemeTextColor;
import fe.materialplayer.controller.css.annotation.theme.ThemeBackgroundColor;
import fe.materialplayer.controller.misc.ConfirmActionController;
import fe.materialplayer.controller.misc.CrashViewerController;
import fe.materialplayer.controller.util.ItemFetcher;
import fe.materialplayer.controller.util.dialog.DialogUtil;
import fe.materialplayer.controller.util.popup.PopupState;
import fe.materialplayer.controller.util.popup.PopupUtil;
import fe.materialplayer.controller.util.popup.impl.EmptyPopupImpl;
import fe.materialplayer.controller.view.playlist.PlaylistListCell;
import fe.materialplayer.data.indexable.Playlist;
import fe.materialplayer.data.sort.SortType;
import fe.materialplayer.music.MusicLibrary;
import fe.materialplayer.svg.SVGDrawer;
import fe.materialplayer.util.SnackbarHelper;
import fe.materialplayer.util.StyleConstants;
import fe.materialplayer.util.properties.ColorThemeManager;
import javafx.beans.Observable;
import javafx.beans.property.SimpleObjectProperty;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.*;

public class PlaylistViewController extends IndexController<Playlist> implements OptionsBindableController {

    @FXML
    private JFXTextMaterialButton btAddPlaylist;

    @FXML
    private StackPane spRoot;

    @FXML
    @ThemeBackgroundColor
    @PrimaryColored
    @LightPrimaryColored
    @ThemeTextColor
    private JFXListView<Playlist> lvPlaylists;

    private final MusicLibrary library = MusicLibrary.Companion.getInstance();
    private SortType<Playlist> sortMode = Playlist.DEFAULT_SORT_TYPE;

    private final PopupState<Playlist> popupState = new PopupState<>();
    public EmptyPopupImpl<Playlist> popup;

    public PlaylistViewController() {
        super(Playlist.DEFAULT_SORT_TYPE, MusicLibrary.Companion.getInstance().getPlaylistDao());

        this.itemFetcher = new ItemFetcher<>(this.daoHelper.getItems(), this.sortMode.getSortComparator(), true);
    }

    @Override
    public void initialize(@NotNull URL location, @NotNull ResourceBundle resources) {
        super.initialize(location, resources);
        this.initListViewPadding(this.lvPlaylists);

        this.btAddPlaylist.textFillProperty().bind(ColorThemeManager.getAccentColor());
        this.btAddPlaylist.setExtraGraphic(
                SVGDrawer.draw(SVGDrawer.ADD_ICON, 12, Map.of(
                        0, new SimpleObjectProperty<>(Color.TRANSPARENT),
                        1, ColorThemeManager.getAccentColor()
                ))
        );

        EventHandler<MouseEvent> deletePlaylist = delPlaylistEvt -> DialogUtil.createConfirmActionController("confirmDeletePlaylist", confirmEvt -> {
            try {
                Objects.requireNonNull(this.library).deletePlaylist(Objects.requireNonNull(this.popupState.getCurrentState()));
            } catch (SQLException e) {
                e.printStackTrace();
                new CrashViewerController("DELETE_PLAYLIST", String.format("Error while deleting playlist %s", this.popupState.getCurrentState()), e).show();
            }
        });

        this.popup = (EmptyPopupImpl<Playlist>) Objects.requireNonNull(new EmptyPopupImpl<>(this.popupState)
                .addItem(PopupUtil.createPopupButton(resources.getString("rename"), "edit",
                        evt -> DialogUtil.loadDialog("view/playlist/RenamePlaylist", null,
                                new RenamePlaylistController(this.popupState.getCurrentState())))))
                .addItem(PopupUtil.createPopupButton(resources.getString("delete"), "delete", deletePlaylist));

        this.sortMode = Playlist.DEFAULT_SORT_TYPE;

        this.itemFetcher.setListView(this.lvPlaylists);
        this.itemFetcher.fetchItems(FETCH_SIZE, ItemFetcher.FetchType.INITIAL);

        this.lvPlaylists.setCellFactory(ar -> new PlaylistListCell(resources));
    }

    @FXML
    public void lvPlaylistsClicked(MouseEvent event) {
        Playlist p = this.lvPlaylists.getSelectionModel().getSelectedItem();
        if (p != null) {
            if (event.getButton() == MouseButton.PRIMARY && event.getClickCount() == 2) {
                PlaylistContentController.show(p);
            } else if (event.getButton() == MouseButton.SECONDARY) {
                this.popupState.setCurrentState(p);
                this.popup.buildAndShow(this.lvPlaylists, event, null);
            }
        }
    }

    @FXML
    public void lvPlaylistsScrolled(ScrollEvent event) {
        this.itemFetcher.fetchItems(FETCH_SIZE, ItemFetcher.FetchType.SCROLL);
    }

    @FXML
    public void btAddPlaylistClicked(ActionEvent event) {
        DialogUtil.loadDialog("view/playlist/CreatePlaylist", null,
                new CreatePlaylistController());
    }

    @Override
    public void handleOptionsClick(MouseEvent event) {
        try {
            this.showPopup(Playlist.SORT_TYPES, this.sortMode, (Node) event.getSource());
        } catch (IOException e) {
            e.printStackTrace();
            new CrashViewerController("SHOW_OPTIONS_POPUP",
                    String.format("Error while showing options popup for sort mode %s", this.sortMode), e).show();
        }
    }

    @Override
    public void handleSearchTextChanged(Observable observable, String oldValue, String newValue) {
        this.execSearch(newValue, this.lvPlaylists);
    }
}


