package fe.materialplayer.util;

import fe.materialplayer.data.indexable.Playlist;
import fe.materialplayer.data.indexable.Song;

public class M3UUtil {
    public static String exportPlaylist(Playlist playlist) {
        StringBuilder sb = new StringBuilder("#EXTM3U").append("\n");
        for (Song song : playlist.getSongs()) {
            sb.append("#EXTINF:");
            sb.append((int) song.getLength()).append(",");
            sb.append(String.format("%s- %s", song.getArtistName(), song.getTitle()));
            sb.append("\n");
            sb.append(Util.fileToUriString(song.getFile()));
            sb.append("\n");
        }

        return sb.toString();
    }
}
