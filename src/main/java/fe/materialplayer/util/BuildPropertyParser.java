package fe.materialplayer.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

public class BuildPropertyParser {
    private static final String DELIMITER = "=";

    public static Map<String, String> parse(String res) {
        Map<String, String> properties = new HashMap<>();
        try (BufferedReader br = new BufferedReader(new InputStreamReader(BuildPropertyParser.class
                .getResourceAsStream(res)))) {
            String line;
            while ((line = br.readLine()) != null) {
                if (line.contains(DELIMITER)) {
                    String[] spl = line.split(DELIMITER);
                    properties.put(spl[0], spl[1]);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }


        return properties;
    }
}
