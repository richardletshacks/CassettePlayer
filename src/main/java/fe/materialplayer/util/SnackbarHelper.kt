package fe.materialplayer.util

import com.jfoenix.controls.JFXSnackbar
import fe.materialplayer.Main
import fe.materialplayer.controller.css.applyStylesheet
import javafx.geometry.Insets
import javafx.geometry.Pos
import javafx.scene.control.Label
import javafx.scene.layout.HBox
import javafx.scene.paint.Color
import java.lang.Double.max

object SnackbarHelper {
    private val textBarPane by lazy {
        Main.getInstance().spFileDragRoot
    }

    @JvmStatic
    fun createMinWidthTextBar(text: String, minWidth: Double) {
        with(textToLabel(text)) {
            createSnackbar(this, max(minWidth, this.layoutBounds.width))
        }
    }

    @JvmStatic
    fun createSimpleTextBar(text: String, fixedWidth: Double) {
        createSnackbar(textToLabel(text), fixedWidth)
    }

    private fun textToLabel(text: String) = Label(text).apply {
        this.textFill = Color.WHITE
    }

    private fun createSnackbar(label: Label, width: Double) {
        val hBox = HBox(label).apply {
            this.padding = Insets(0.0, 0.0, 0.0, 10.0)
            this.alignment = Pos.CENTER_LEFT
            this.prefWidth = width
            this.prefHeight = 45.0
        }

        JFXSnackbar(textBarPane, 7).apply {
            this.applyStylesheet("snackbar")
        }.enqueue(JFXSnackbar.SnackbarEvent(hBox))
    }
}