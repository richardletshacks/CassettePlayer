package fe.materialplayer.data.indexable.base.table;

public interface IdTable extends Table {
    int getId();
}
