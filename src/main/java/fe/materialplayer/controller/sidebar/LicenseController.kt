package fe.materialplayer.controller.sidebar

import fe.materialplayer.controller.content.ContentController
import fe.materialplayer.controller.css.StyleBinder
import fe.materialplayer.controller.css.annotation.theme.ThemeBackgroundColor
import fe.materialplayer.controller.css.annotation.theme.ThemeTextColor
import fe.materialplayer.font.FontManager
import fe.materialplayer.util.Util
import fe.materialplayer.util.properties.ColorType
import javafx.fxml.FXML
import javafx.scene.control.Hyperlink
import javafx.scene.control.Label
import javafx.scene.control.ScrollPane
import javafx.scene.text.Font
import javafx.scene.text.Text
import javafx.scene.text.TextFlow
import java.net.URL
import java.util.*

class LicenseController : ContentController() {
    @FXML
    @ThemeBackgroundColor
    private lateinit var spLicenses: ScrollPane

    @FXML
    @ThemeTextColor
    private lateinit var lbLicenses: Label

    @FXML
    private lateinit var tfLicenses: TextFlow

    override fun initialize(location: URL, resources: ResourceBundle) {
        super.initialize(location, resources)

        dependencies.forEach { dep ->
            val textHeader = Text(dep.name)
            textHeader.font = HK_GROTESK.get(15.0)

            val hyperlinkDependency = Hyperlink("${dep.url}\n")
            hyperlinkDependency.font = HK_GROTESK.get(15.0)
            hyperlinkDependency.setOnAction { Util.openUrl(dep.url) }

            val textLicense = Text(LicenseController::class.java.getResourceAsStream(dep.getLicense()).use {
                it.bufferedReader().readLines().joinToString(separator = "\n")
            } + "\n")
            textLicense.font = COURIER_NEW_12

            StyleBinder.bindPaintToThemeColor(ColorType.TEXT_COLOR, textLicense.fillProperty())
            tfLicenses.children.addAll(textHeader, hyperlinkDependency, textLicense)
        }

    }

    companion object {
        private val HK_GROTESK = FontManager.HK_GROTESK_SEMI_BOLD
        private val COURIER_NEW_12 = Font.font("Courier New", 12.0)

        private val dependencies = listOf(
                Dependency("AnimateFX", "https://github.com/Typhon0/AnimateFX", License.APACHE_2),
                Dependency("Gson", "https://github.com/google/gson", License.APACHE_2),
                Dependency("JFoenix", "https://github.com/jfoenixadmin/JFoenix", License.APACHE_2),
                Dependency("lz4", "https://github.com/lz4/lz4-java", License.APACHE_2),
                Dependency("MaterialIcons", "https://material.io/resources/icons", License.APACHE_2),
                Dependency("SQLite-JDBC", "https://github.com/xerial/sqlite-jdbc", License.APACHE_2),
                Dependency("color-thief-java", "https://github.com/SvenWoltmann/color-thief-java", License.CREATIVE_COMMONS_PUBLIC),
                Dependency("dbus-java", "https://github.com/hypfvieh/dbus-java", License.ACADEMIC_FREE_2_1),
                Dependency("JSemver", "https://github.com/zafarkhaja/jsemver", License.CUSTOM),
                Dependency("mp3agic", "https://github.com/mpatric/mp3agic", License.CUSTOM),
                Dependency("ORMLite-JDBC", "https://github.com/j256/ormlite-jdbc", License.CUSTOM),
                Dependency("TwelveMonkeys", "https://github.com/haraldk/TwelveMonkeys", License.CUSTOM),
                Dependency("Flags", "https://www.flaticon.com", License.CUSTOM),
                Dependency("LoadingIO", "https://loading.io", License.CUSTOM),
                Dependency("HK Grotesk Font", "https://hanken.co/products/hk-grotesk", License.SIL_OPEN_FONT)
        )
    }

    data class Dependency(val name: String, val url: String, val license: License) {
        fun getLicense() = license.getLicense(this)
    }

    enum class License {
        APACHE_2,
        CREATIVE_COMMONS_PUBLIC,
        ACADEMIC_FREE_2_1,
        SIL_OPEN_FONT,
        CUSTOM;

        open fun getLicense(dependency: Dependency): String {
            return if (this != CUSTOM) {
                "/license/${name.toLowerCase()}.license"
            } else {
                "/license/dependency/${dependency.name.toLowerCase()}.license"
            }
        }
    }
}

