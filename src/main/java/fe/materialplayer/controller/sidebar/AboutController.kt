package fe.materialplayer.controller.sidebar

import com.jfoenix.controls.materialbutton.JFXOutlinedMaterialButton
import fe.materialplayer.Main
import fe.materialplayer.controller.content.ContentController
import fe.materialplayer.controller.css.annotation.color.PrimaryColored
import fe.materialplayer.controller.css.annotation.theme.ThemeTextColor
import fe.materialplayer.controller.util.dialog.DialogUtil
import fe.materialplayer.util.StyleConstants
import fe.materialplayer.util.Util
import fe.materialplayer.util.image.ImageUtil
import javafx.event.ActionEvent
import javafx.fxml.FXML
import javafx.geometry.Pos
import javafx.scene.control.Hyperlink
import javafx.scene.control.Label
import javafx.scene.image.Image
import javafx.scene.image.ImageView
import javafx.scene.layout.HBox
import javafx.scene.layout.VBox
import java.net.URL
import java.time.format.DateTimeFormatter
import java.util.*

class AboutController : ContentController() {
    @FXML
    @ThemeTextColor
    private lateinit var vbInfo: VBox

    @FXML
    @ThemeTextColor
    private lateinit var vbCredits: VBox

    @FXML
    private lateinit var lbVersion: Label

    @FXML
    private lateinit var lbBuiltAt: Label

    @FXML
    private lateinit var lbDonator: Label

    @FXML
    private lateinit var ivLogo: ImageView

    @FXML
    private lateinit var ivAuthor: ImageView

    @FXML
    private lateinit var ivPhonograph: ImageView

    @FXML
    private lateinit var vbPoweredby: VBox

    @FXML
    @PrimaryColored(cssProperty = StyleConstants.CSS_TEXT_FILL_PROPERTY)
    val btLicenses = JFXOutlinedMaterialButton("Show licenses", null)

    override fun initialize(location: URL, resources: ResourceBundle) {
        super.initialize(location, resources)

        this.lbVersion.text = "${Main.getInstance().version} (${Main.getInstance().fullVersion})"

        if (!Main.getInstance().isDonatorBuild) {
            this.lbDonator.isVisible = false
        }

        this.lbBuiltAt.text = String.format("%s %s", resources.getString("builtAt"), Main.getInstance().builtAt.format(DTF))

        bindIcons(mapOf(
                this.lbVersion to "info",
                this.lbBuiltAt to "datetime"
        ))

        this.ivLogo.image = ImageUtil.loadIcon("logo/ic_launcher_round")

        ImageUtil.roundImageWrapper(this.ivAuthor, Image(GITLAB_IMAGE_URL, true), IMG_SIZE)
        ImageUtil.roundImageWrapper(this.ivPhonograph, ImageUtil.load("/logo/phonograph.png"), IMG_SIZE)


        POWERED_BY.forEach { (key, value) ->
            val iv = ImageUtil.makeIv(IMG_SIZE)
            ImageUtil.roundImageWrapper(iv, ImageUtil.load("/logo/${key.toLowerCase()}.png"), IMG_SIZE)


            val hyperlink = Hyperlink()
            hyperlink.text = key
            hyperlink.setOnAction { Util.openUrl(value) }

            val hbox = HBox(iv, hyperlink)
            hbox.alignment = Pos.CENTER_LEFT
            hbox.prefHeight = 50.0

            vbPoweredby.children += hbox
        }

        vbPoweredby.children += Label("...and a few more")
        btLicenses.setOnMouseClicked {
            DialogUtil.loadDialog("main/sidebar/Licenses", null, LicenseController())
        }

        vbPoweredby.children += btLicenses
    }

    @FXML
    fun hlAuthorClicked(event: ActionEvent) {
        Util.openUrl(GITLAB_URL)
    }

    @FXML
    fun hlPhonographClicked(event: ActionEvent) {
        Util.openUrl(PHONOGRAPH_URL)
    }

    companion object {
        private const val IMG_SIZE = 40
        private val DTF: DateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")

        private val POWERED_BY = mapOf(
                "JFoenix" to "https://github.com/jfoenixadmin/JFoenix",
                "MaterialIcons" to "https://material.io/resources/icons",
                "ORMLite" to "https://github.com/j256/ormlite-jdbc",
                "mp3agic" to "https://github.com/mpatric/mp3agic"
        )

        private const val PHONOGRAPH_URL = "https://github.com/kabouzeid/Phonograph"
        private const val GITLAB_URL = "https://gitlab.com/grrfe"
        private const val GITLAB_IMAGE_URL = "https://gitlab.com/uploads/-/system/user/avatar/3886345/avatar.png"
    }

}

