package fe.materialplayer.util.wallpaper

import fe.materialplayer.Main
import fe.materialplayer.data.indexable.Song
import fe.materialplayer.util.os.OSHelper
import fe.materialplayer.util.SingletonHolder
import javafx.embed.swing.SwingFXUtils
import javafx.scene.image.WritableImage
import javafx.scene.paint.Color
import java.awt.Dimension
import java.awt.Toolkit
import java.awt.image.BufferedImage
import java.io.File
import javax.imageio.ImageIO

class WallpaperChanger private constructor(baseDir: File) {
    private var wallpaperDir: File = File(baseDir, ".wallpaper").also {
        it.mkdirs()
    }

    private var currentChanger: WallpaperProvider? = null
    private var startupWallpaper: File? = null

    init {
        when (Main.getInstance().currentOS) {
            OSHelper.OS.LINUX -> {
                Main.getInstance().linuxDesktop?.let { de ->
                    LINUX_WALLPAPER_CHANGERS.find { it.linuxDesktop == de }?.let {
                        currentChanger = it
                        startupWallpaper = it.getCurrent()
                    }
                }
            }

            OSHelper.OS.WINDOWS -> {
                currentChanger = WindowsWallpaperProvider.also {
                    startupWallpaper = it.getCurrent()
                }
            }
            else -> {

            }
        }
    }

    companion object : SingletonHolder<WallpaperChanger, File>(::WallpaperChanger) {
        val SCREEN_SIZE: Dimension = Toolkit.getDefaultToolkit().screenSize
        private val LINUX_WALLPAPER_CHANGERS = listOf(CinnamonWallpaperProvider)
        private var HAS_CHANGED = false
    }

    fun changeWallpaper(song: Song, coverImage: WritableImage) {
        currentChanger?.let { changer ->
            val file = File(wallpaperDir, "${song.hash}.png")
            if (!file.exists()) {
                val swingAlbumCover = SwingFXUtils.fromFXImage(coverImage, null)

                val outputImage = BufferedImage(SCREEN_SIZE.width, SCREEN_SIZE.height, swingAlbumCover.type)
                outputImage.createGraphics().run {
                    this.color = java.awt.Color.decode(song.hexBackgroundColor.substring(0, 7))
                    this.fillRect(0, 0, SCREEN_SIZE.width, SCREEN_SIZE.height)

                    this.drawImage(
                        swingAlbumCover,
                        SCREEN_SIZE.width / 2 - swingAlbumCover.width / 2,
                        SCREEN_SIZE.height / 2 - swingAlbumCover.height / 2,
                        swingAlbumCover.width,
                        swingAlbumCover.height,
                        null
                    )

                    this.color = if (song.isWhiteText) java.awt.Color.WHITE else java.awt.Color.BLACK
                    val str = "${song.artistName} - ${song.title}"

                    this.drawString(
                        str,
                        SCREEN_SIZE.width / 2 - this.fontMetrics.stringWidth(str) / 2,
                        SCREEN_SIZE.height / 2 + swingAlbumCover.height / 2 + 10
                    )
                    this.dispose()
                }

                ImageIO.write(outputImage, "png", file)
            }

            changer.change(file)

            if (!HAS_CHANGED) {
                HAS_CHANGED = true
            }
        }
    }

    fun resetWallpaper() {
        if (HAS_CHANGED) {
            startupWallpaper?.let {
                currentChanger?.change(it)
            }
        }
    }
}