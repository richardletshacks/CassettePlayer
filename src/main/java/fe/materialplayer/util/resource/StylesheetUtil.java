package fe.materialplayer.util.resource;

import javafx.scene.Parent;

public class StylesheetUtil {

    public static String getStylesheet(String name) {
        return StylesheetUtil.class.getResource(String.format("/css/%s.css", name)).toExternalForm();
    }

    public static void applyStylesheet(Parent component, String styleSheet){
        component.getStylesheets().add(StylesheetUtil.getStylesheet(styleSheet));
    }
}

