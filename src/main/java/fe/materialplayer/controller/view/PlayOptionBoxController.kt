package fe.materialplayer.controller.view

import com.jfoenix.controls.materialbutton.JFXTextMaterialButton
import fe.materialplayer.controller.css.AnnotationBinder
import fe.materialplayer.controller.css.annotation.color.AccentColored
import fe.materialplayer.controller.util.dialog.DialogUtil
import fe.materialplayer.data.indexable.Song
import fe.materialplayer.data.indexable.base.Indexable
import fe.materialplayer.music.MusicPlayer
import fe.materialplayer.svg.SVGDrawer
import fe.materialplayer.util.StyleConstants
import fe.materialplayer.util.properties.ColorThemeManager
import javafx.beans.property.SimpleObjectProperty
import javafx.event.ActionEvent
import javafx.fxml.FXML
import javafx.fxml.Initializable
import javafx.scene.paint.Color
import java.net.URL
import java.util.*


abstract class PlayOptionBoxController<T : Indexable> : Initializable {
    @FXML
    @AccentColored(cssProperty = StyleConstants.CSS_LIGHT_PRIMARY_COLOR_VAR)
    lateinit var btPlayAll: JFXTextMaterialButton

    @FXML
    lateinit var btShuffleAll: JFXTextMaterialButton

    @FXML
    lateinit var btPlayNext: JFXTextMaterialButton

    @FXML
    lateinit var btAddToQueue: JFXTextMaterialButton

    @FXML
    lateinit var btAddToPlaylist: JFXTextMaterialButton

    private val player = MusicPlayer.getInstance()

    override fun initialize(location: URL?, resources: ResourceBundle?) {
        mapOf(
                btPlayAll to SVGDrawer.PLAY_ICON,
                btShuffleAll to SVGDrawer.SHUFFLE_ICON,
                btPlayNext to SVGDrawer.QUEUE_ADD_NEXT,
                btAddToQueue to SVGDrawer.PLAYLIST_PLAY,
                btAddToPlaylist to SVGDrawer.PLAYLIST_PLUS
        ).forEach { (btn, icon) ->
            btn.setExtraGraphic(SVGDrawer.draw(icon, 11.0, mapOf(
                    0 to SimpleObjectProperty(Color.TRANSPARENT),
                    1 to ColorThemeManager.accentColor)))
            btn.textFillProperty().bind(ColorThemeManager.accentColor)
        }
    }

    @FXML
    fun btAddToPlaylistClicked(event: ActionEvent) {
        DialogUtil.createAddToPlaylistDialog(this.mapSongs(), null)
    }

    @FXML
    fun btPlayAllClicked(event: ActionEvent?) {
        player.requeue(mapSongs(), 0)
    }

    @FXML
    fun btShuffleAllClicked(event: ActionEvent?) {
        player.clearQueue()
        player.addSongs(mapSongs(), true)
        player.playCurrent()
    }

    @FXML
    fun btPlayNextClicked(event: ActionEvent?) {
        player.addNextSongs(mapSongs(), false)
    }

    @FXML
    fun btAddToQueueClicked(event: ActionEvent?) {
        player.addSongs(mapSongs(), false)
    }

    private fun mapSongs(): MutableList<Song> = Indexable.mapToSongs(provideSongs())

    abstract fun provideSongs(): List<T>


}