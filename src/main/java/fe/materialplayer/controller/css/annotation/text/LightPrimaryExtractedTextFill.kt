package fe.materialplayer.controller.css.annotation.text

import fe.materialplayer.util.StyleConstants

@Target(AnnotationTarget.FIELD)
@Retention(AnnotationRetention.RUNTIME)
@MustBeDocumented
annotation class LightPrimaryExtractedTextFill(val cssProperty: String = StyleConstants.CSS_LIGHT_PRIMARY_COLOR_VAR)