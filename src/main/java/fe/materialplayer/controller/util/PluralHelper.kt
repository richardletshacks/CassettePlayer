package fe.materialplayer.controller.util

import java.util.*

object PluralHelper {

    @JvmStatic
    fun getString(resourceBundle: ResourceBundle, formatString: String, listItems: Collection<Any>, vararg options: String): String {
        return listItems.size.let { size ->
            formatString.format(size, getString(resourceBundle, size, *options))
        }
    }

    @JvmStatic
    fun getString(resourceBundle: ResourceBundle, listItems: List<Any>, vararg options: String): String {
        return getString(resourceBundle, listItems.size, *options)
    }

    @JvmStatic
    fun getString(resourceBundle: ResourceBundle, amount: Int, vararg options: String): String {
       return resourceBundle.getString(options[if (amount == 1) 0 else 1])
    }
}