package fe.materialplayer.controller.util.popup

import fe.materialplayer.data.indexable.Song
import fe.materialplayer.data.indexable.base.Indexable
import java.util.List.of

class PopupState<T : Indexable> {

    var currentState: T? = null

    fun getSongs(): MutableList<Song> = this.currentState!!.toSongList().toMutableList()
}