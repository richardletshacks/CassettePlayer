package fe.materialplayer.controller.content.playlist;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXListView;
import fe.materialplayer.controller.content.ContentController;
import fe.materialplayer.controller.content.resize.ResizeButtonBinder;
import fe.materialplayer.controller.content.resize.button.DefaultResizeButton;
import fe.materialplayer.controller.css.annotation.color.AccentColored;
import fe.materialplayer.controller.css.annotation.theme.ThemeBackgroundColor;
import fe.materialplayer.controller.css.annotation.theme.ThemeTextColor;
import fe.materialplayer.controller.misc.CrashViewerController;
import fe.materialplayer.controller.util.ItemFetcher;
import fe.materialplayer.controller.util.PluralHelper;
import fe.materialplayer.controller.util.SongSelector;
import fe.materialplayer.controller.util.dialog.DialogUtil;
import fe.materialplayer.controller.util.popup.PopupState;
import fe.materialplayer.controller.util.popup.PopupUtil;
import fe.materialplayer.controller.util.popup.impl.SongPopup;
import fe.materialplayer.controller.view.playlist.controller.ExportPlaylistController;
import fe.materialplayer.data.indexable.Playlist;
import fe.materialplayer.data.indexable.Song;
import fe.materialplayer.music.MusicLibrary;
import fe.materialplayer.util.FormatUtil;
import fe.materialplayer.util.StyleConstants;
import fe.materialplayer.util.image.ImageUtil;
import fe.materialplayer.util.properties.ColorThemeManager;
import javafx.beans.binding.Bindings;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import org.jetbrains.annotations.NotNull;

import java.net.URL;
import java.sql.SQLException;
import java.util.*;

public class PlaylistContentController extends ContentController {

    @FXML
    @ThemeTextColor
    private VBox vbLabels;

    @FXML
    @ThemeTextColor
    private Label lbSongs;

    @FXML
    private Label lbPlaylist;

    @FXML
    private Label lbSongNumber;

    @FXML
    private Label lbLength;

    @FXML
    private JFXButton btShuffleAll;

    @FXML
    private JFXButton btPlayNext;

    @FXML
    private JFXButton btAddToPlayingQueue;

    @FXML
    private JFXButton btAddToPlaylist;

    @FXML
    @AccentColored(cssProperty = StyleConstants.CSS_BACKGROUND_COLOR_PROPERTY)
    private JFXButton btDelete;

    @FXML
    @AccentColored(cssProperty = StyleConstants.CSS_BACKGROUND_COLOR_PROPERTY)
    private JFXButton btExport;

    @FXML
    @ThemeBackgroundColor
    private JFXListView<Song> lvSongs;

    private final Playlist playlist;

    private static final MusicLibrary LIBRARY = MusicLibrary.Companion.getInstance();

    private final IntegerProperty selectedIndex = new SimpleIntegerProperty();

    private final PopupState<Song> popupState = new PopupState<>();

    private static final int FETCH_SIZE = 15;
    private final ItemFetcher<Song> itemFetcher;


    private SongSelector songSelector;

    public PlaylistContentController(Playlist playlist) {
        super(true, true);

        this.playlist = playlist;
        this.itemFetcher = new ItemFetcher<>(this.playlist.getSongs(), Comparator.comparing(Song::getTitle), false);
    }

    public static void show(Playlist playlist) {
        DialogUtil.loadDialog("content/PlaylistContent",
                new PlaylistContentController(playlist));
    }

    @Override
    public void initialize(@NotNull URL location, @NotNull ResourceBundle resources) {
        super.initialize(location, resources);

        this.itemFetcher.setListView(this.lvSongs);
        this.itemFetcher.fetchItems(FETCH_SIZE, ItemFetcher.FetchType.INITIAL);

        EventHandler<MouseEvent> deletePlaylistSong = playlistSongDel -> DialogUtil.createConfirmActionController("confirmDeletePlaylistSong", confirmEvt -> {
            try {
                this.playlist.removeIndex(this.selectedIndex.get());
                this.itemFetcher.refetch();
            } catch (SQLException e) {
                e.printStackTrace();
                new CrashViewerController("REMOVE_INDEX_FROM_PLAYLIST",
                        String.format("Error while removing index %d from playlist %s", this.selectedIndex.get(), this.playlist), e).show();
            }
        });

        SongPopup popup = (SongPopup) new SongPopup(this.popupState, resources)
                .addItem(PopupUtil.createPopupButton(resources.getString("removeFromPlaylist"), "remove", deletePlaylistSong), 1);

        this.lvSongs.setItems(this.itemFetcher.getBaseList());
        this.songSelector = new SongSelector(this.lvSongs, popup);

        ResizeButtonBinder.INSTANCE.initButtons(List.of(
                this.btShuffleAll,
                this.btPlayNext,
                this.btAddToPlayingQueue,
                this.btAddToPlaylist
        ), this.playlist.getSongs(), resources, ColorThemeManager.getAccentColor());

        this.btDelete.setTextFill(Color.WHITE);
        this.btExport.setTextFill(Color.WHITE);

        new DefaultResizeButton(this.btDelete, delPlaylistEvt -> DialogUtil.createConfirmActionController("confirmDeletePlaylist", confirmEvt -> {
            try {
                LIBRARY.deletePlaylist(this.playlist);
                this.closeDialog();
            } catch (SQLException e) {
                e.printStackTrace();
                new CrashViewerController("DELETE_PLAYLIST", String.format("Error while deleting playlist %s", this.playlist.getName()), e).show();
            }
        }), ImageUtil.loadIcon("delete_dark"), resources.getString("deletePlaylist"), 150);

        new DefaultResizeButton(this.btExport, evt -> {
            DialogUtil.loadDialog("view/playlist/ExportPlaylist", null, new ExportPlaylistController(this.playlist));
        }, ImageUtil.loadIcon("export_dark"), resources.getString("exportPlaylist"), 150);

        this.lbPlaylist.setText(this.playlist.getName());

        this.lbSongNumber.textProperty().bind(Bindings.createStringBinding(() ->
                        PluralHelper.getString(resources, "%d %s", this.playlist.getSongs(), "song", "songs"),
                this.playlist.playlistLengthProperty()));

        this.lbLength.textProperty().bind(Bindings.createStringBinding(() ->
                        FormatUtil.formatLength(this.playlist.playlistLengthProperty().get()),
                this.playlist.playlistLengthProperty()));

        this.bindIcons(Map.of(this.lbSongNumber, "music_note", this.lbLength, "timer"));

        this.lvSongs.setCellFactory(c -> new PlaylistContentListCell(this.playlist, this.itemFetcher));
        this.lvSongs.setOnMouseClicked(event -> {
            int idx = this.lvSongs.getSelectionModel().getSelectedIndex();
            if (idx != -1) {
                this.selectedIndex.set(idx);
            }

            this.songSelector.select(event);
        });
    }

    @FXML
    public void lvSongsScrolled(ScrollEvent event) {
        this.itemFetcher.fetchItems(FETCH_SIZE, ItemFetcher.FetchType.SCROLL);
    }
}
