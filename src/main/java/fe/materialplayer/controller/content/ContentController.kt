package fe.materialplayer.controller.content

import fe.materialplayer.controller.base.dialog.ClosableDialogController
import fe.materialplayer.controller.css.annotation.property.IntCSSProperties
import fe.materialplayer.controller.css.annotation.property.IntCSSProperty
import fe.materialplayer.controller.css.annotation.theme.ThemeBackgroundColor
import fe.materialplayer.util.StyleConstants
import fe.materialplayer.util.image.ImageUtil
import javafx.fxml.FXML
import javafx.scene.control.Label
import javafx.scene.layout.GridPane

abstract class ContentController(closeBtn: Boolean = true, scaled: Boolean = false) :
    ClosableDialogController(closeBtn, scaled) {
    @FXML
    @ThemeBackgroundColor(cssProperty = StyleConstants.CSS_BACKGROUND_COLOR_PROPERTY)
    @IntCSSProperties(
        IntCSSProperty(property = StyleConstants.CSS_BACKGROUND_RADIUS, 7),
        IntCSSProperty(property = StyleConstants.CSS_BORDER_RADIUS, 7)
    )
    protected lateinit var gpRoot: GridPane

    protected fun bindIcons(map: Map<Label, String>) {
        map.forEach { (key, value) ->
            key.graphic = ImageUtil.makeThemeBoundIv(value, OVERVIEW_ICON_SIZE)
        }
    }

    companion object {
        @JvmStatic
        val OVERVIEW_ICON_SIZE = 18
    }
}