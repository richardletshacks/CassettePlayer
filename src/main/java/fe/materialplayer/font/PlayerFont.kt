package fe.materialplayer.font

import fe.materialplayer.util.Util
import javafx.scene.text.Font

import java.io.File
import java.io.IOException


data class PlayerFont(val name: String, val path: String, val type: FontType) {
    val fontCache = mutableMapOf<Double, Font>()

    fun load(size: Int) = type.load(this.path, size)

    fun get(size: Double): Font {
        return fontCache.getOrPut(size, { Font.font(name, size) })
    }
}

enum class FontType {
    INTERNAL {
        override fun load(path: String, size: Int) = PlayerFont::class.java.getResourceAsStream(path).use {
            Font.loadFont(it, size.toDouble())
        }
    },
    EXTERNAL {
        override fun load(path: String, size: Int): Font = Font.loadFont(
                Util.fileToUriString(File(path)), size.toDouble())
    },
    INSTALLED {
        override fun load(path: String, size: Int): Font = Font.font(path, size.toDouble())
    };

    abstract fun load(path: String, size: Int): Font
}