package fe.materialplayer.util.color

import de.androidpit.colorthief.ColorThief
import fe.materialplayer.util.luma
import javafx.scene.paint.Color
import java.awt.image.BufferedImage
import java.lang.Double.min


object BackgroundColorExtractor {

    private val luma0 = Color(0.007843137, 0.007843137, 0.007843137, 1.0)
    private const val LUMA_THRESHOLD = 0.078431373
    private const val BRIGHTNESS_MULTIPLIER = 0.015686275

    @JvmStatic
    fun extract(bufferedImage: BufferedImage): Color {
        val rgb: IntArray = ColorThief.getColor(bufferedImage)
        var color = Color(rgb[0] / 255.0, rgb[1] / 255.0, rgb[2] / 255.0, 1.0)

        if (color.luma < LUMA_THRESHOLD) {
            if (color.luma == 0.0) {
                color = luma0
            }

            color = Color(this.brighter(color.red), this.brighter(color.green), this.brighter(color.blue), 1.0)
        }

        return color
    }

    private fun brighter(i: Double): Double = min(1.0, i * BRIGHTNESS_MULTIPLIER)
}