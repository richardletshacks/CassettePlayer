package fe.materialplayer.util.image;

import fe.materialplayer.util.properties.ColorThemeManager;
import fe.materialplayer.util.Util;
import javafx.beans.Observable;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.ObjectBinding;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.SnapshotParameters;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.Callable;
import java.util.function.Consumer;
import java.util.function.Supplier;

public class ImageUtil {

    public static ImageView makeThemeBoundIv(String name, int size, Observable... extraDeps) {
        return ImageUtil.makeIv(size, ImageUtil.createIconBinding(name, extraDeps));
    }

    public static ImageView makeThemeBoundIv(Supplier<String> name, int size, Observable... extraDeps) {
        return ImageUtil.makeIv(size, ImageUtil.createIconBinding(name, extraDeps));
    }

    public static ObjectBinding<Image> createIconBinding(Supplier<String> name, Observable... customDeps) {
        return Bindings.createObjectBinding(() -> ImageUtil.getThemedIcon(name.get()), ImageUtil.makeObservableThemeArray(customDeps));
    }

    public static ObjectBinding<Image> createIconBinding(String name, Observable... customDeps) {
        return Bindings.createObjectBinding(() -> ImageUtil.getThemedIcon(name), ImageUtil.makeObservableThemeArray(customDeps));
    }

    public static Image getThemedIcon(String name){
        return ImageUtil.loadIcon(ColorThemeManager.getTheme().get().findIcon(name));
    }

    public static Observable[] makeObservableThemeArray(Observable... customDeps) {
        Observable[] dependencies = Arrays.copyOf(customDeps, customDeps.length + 1);
        dependencies[customDeps.length] = ColorThemeManager.getTheme();

        return dependencies;
    }

    public static ImageView makeIconIv(String icon, String ext, int size) {
        return ImageUtil.makeIv(ImageUtil.loadIcon(icon, ext), size);
    }

    public static ImageView makeIconIv(String icon, int size) {
        return ImageUtil.makeIv(ImageUtil.loadIcon(icon), size);
    }

    public static ImageView makeIv(Image img, int size) {
        ImageView iv = ImageUtil.makeIv(size);
        iv.setImage(img);
        return iv;
    }

    public static ImageView makeIv(int size) {
        ImageView iv = new ImageView();
        if (size != -1) {
            iv.setFitHeight(size);
            iv.setFitWidth(size);
        }

        return iv;
    }

    public static ImageView makeIv(int size, Callable<Image> func, Observable dependency) {
        return ImageUtil.makeIv(size, Bindings.createObjectBinding(func, dependency));
    }

    public static ImageView makeIv(int size, ObservableValue<? extends Image> binding) {
        ImageView iv = ImageUtil.makeIv(size);
        iv.imageProperty().bind(binding);

        return iv;
    }

    public static Image load(String resourceLocation) {
        return Util.useInputStream(ImageUtil.class.getResourceAsStream(resourceLocation), Image::new);
    }

    public static Image loadIcon(String icon, String ext) {
        return ImageUtil.load(String.format("/icon/%s.%s", icon, ext));
    }

    public static Image loadIcon(String icon) {
        return ImageUtil.loadIcon(icon, "png");
    }

    public static void roundImageWrapper(ImageView iv, Image img, int widthHeight) {
        ImageUtil.roundImageWrapper(iv, img, widthHeight, widthHeight);
    }

    public static void roundImageWrapper(ImageView iv, Image img, int width, int height) {
        ChangeListener<Number> listener = (observable, oldValue, newValue) -> {
            if (newValue.doubleValue() == 1d) {
                iv.setImage(img);
                ImageUtil.roundImage(iv, width, height);
            }
        };

        if (img.isBackgroundLoading()) {
            img.progressProperty().addListener(listener);
        } else {
            listener.changed(null, -1, 1d);
        }
    }

    public static void roundImage(ImageView iv, int width, int height) {
        Rectangle clip = new Rectangle(
                iv.getFitWidth(), iv.getFitHeight()
        );
        clip.setArcWidth(width);
        clip.setArcHeight(height);
        iv.setClip(clip);

        SnapshotParameters parameters = new SnapshotParameters();
        parameters.setFill(Color.TRANSPARENT);
        WritableImage image = iv.snapshot(parameters, null);

        iv.setClip(null);
        iv.setImage(image);
    }
}
