package fe.materialplayer.controller.util

import com.jfoenix.controls.JFXListView
import com.jfoenix.controls.JFXPopup
import fe.materialplayer.controller.util.popup.base.Popup
import fe.materialplayer.controller.util.popup.PopupState
import fe.materialplayer.data.indexable.Song
import fe.materialplayer.music.MusicPlayer
import javafx.collections.ObservableList
import javafx.scene.input.MouseButton
import javafx.scene.input.MouseEvent


class SongSelector(private val lvSongs: JFXListView<Song>,
                   val popup: Popup<Song>) {

    val popupState: PopupState<Song> = popup.state

    private val player: MusicPlayer = MusicPlayer.getInstance()

    fun select(event: MouseEvent) {
        val song: Song? = this.lvSongs.selectionModel.selectedItem

        if (event.button === MouseButton.PRIMARY && event.clickCount == 2) {
            this.player.requeue(lvSongs.items, this.lvSongs.selectionModel.selectedIndex)
        } else if (event.button === MouseButton.SECONDARY) {
            song.let {
                this.popupState.currentState = it
                this.popup.buildAndShow(lvSongs, event)
            }
        }
    }
}