package fe.materialplayer.data.dao.helper

import fe.materialplayer.data.dao.base.IdDaoHelper
import fe.materialplayer.data.indexable.Playlist

class PlaylistDaoHelper : IdDaoHelper<Playlist>(Playlist::class.java, "name") {
    fun createPlaylist(p: Playlist) {
        this.items.add(p)
        this.create(p)
    }

    fun renamePlaylist(playlist: Playlist, name: String?) {
        playlist.name = name
        this.update(playlist)
    }
}
