package fe.materialplayer.controller.view.genre

import com.jfoenix.controls.JFXListView
import fe.materialplayer.controller.base.IndexController
import fe.materialplayer.controller.base.OptionsBindableController
import fe.materialplayer.controller.content.genre.GenreContentController
import fe.materialplayer.controller.css.annotation.color.LightPrimaryColored
import fe.materialplayer.controller.css.annotation.color.PrimaryColored
import fe.materialplayer.controller.css.annotation.theme.ThemeBackgroundColor
import fe.materialplayer.controller.css.annotation.theme.ThemeTextColor
import fe.materialplayer.controller.misc.CrashViewerController
import fe.materialplayer.controller.util.ItemFetcher
import fe.materialplayer.controller.view.PlayOptionBoxController
import fe.materialplayer.data.indexable.Genre
import fe.materialplayer.music.MusicLibrary
import fe.materialplayer.util.SnackbarHelper
import fe.materialplayer.util.resource.FxmlUtil
import javafx.beans.Observable
import javafx.fxml.FXML
import javafx.scene.Node
import javafx.scene.input.MouseButton
import javafx.scene.input.MouseEvent
import javafx.scene.input.ScrollEvent
import javafx.scene.layout.GridPane
import javafx.scene.layout.HBox
import java.io.IOException
import java.net.URL
import java.util.*
import java.util.function.Consumer

class GenreViewController : IndexController<Genre>(Genre.DEFAULT_SORT_TYPE, MusicLibrary.getInstance().genreDao), OptionsBindableController {
    init {
        itemFetcher = ItemFetcher(this.daoHelper.items, sortMode.comparator, false)
    }

    @FXML
    private lateinit var gpRoot: GridPane

    @FXML
    @ThemeBackgroundColor
    @PrimaryColored
    @LightPrimaryColored
    @ThemeTextColor
    private lateinit var lvGenres: JFXListView<Genre>

    override fun initialize(location: URL, resources: ResourceBundle) {
        super.initialize(location, resources)
        initListViewPadding(this.lvGenres)

        FxmlUtil.loadNonBlockingFxml<HBox>("view/PlayOptionBox", object : PlayOptionBoxController<Genre>() {
            override fun provideSongs() = searchFilteredItemsOrAll
        }, { node -> gpRoot.add(node, 0, 0) })

        this.itemFetcher.listView = lvGenres
        this.itemFetcher.fetchItems(FETCH_SIZE, ItemFetcher.FetchType.INITIAL)

        this.lvGenres.setCellFactory { GenreListCell(resources) }
    }

    @FXML
    fun lvGenresClicked(event: MouseEvent) {
        this.lvGenres.selectionModel.selectedItem?.let {
            if (event.button == MouseButton.PRIMARY && event.clickCount == 2) {
                GenreContentController.show(it)
            }
        }

    }

    @FXML
    fun lvGenresScrolled(event: ScrollEvent) {
        this.itemFetcher.fetchItems(FETCH_SIZE, ItemFetcher.FetchType.SCROLL)
    }

    override fun handleSearchTextChanged(observable: Observable?, oldValue: String?, newValue: String?) {
        execSearch(newValue, this.lvGenres)
    }

    override fun handleOptionsClick(event: MouseEvent?) {
        try {
            showPopup(Genre.SORT_TYPES, this.sortMode, event!!.source as Node)
        } catch (e: IOException) {
            e.printStackTrace()
            CrashViewerController("SHOW_OPTIONS_POPUP", String.format("Error while showing options popup for sort mode %s", sortMode), e).show()
        }
    }
}