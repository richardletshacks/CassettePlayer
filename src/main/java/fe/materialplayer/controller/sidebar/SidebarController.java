package fe.materialplayer.controller.sidebar;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXProgressBar;
import fe.materialplayer.Main;
import fe.materialplayer.controller.base.SidebarBindController;
import fe.materialplayer.controller.css.StyleBinder;
import fe.materialplayer.controller.css.annotation.theme.ThemeBackgroundColor;
import fe.materialplayer.controller.util.ItemFetcher;
import fe.materialplayer.controller.util.dialog.DialogUtil;
import fe.materialplayer.music.MusicLibrary;
import fe.materialplayer.util.StyleConstants;
import fe.materialplayer.util.Util;
import fe.materialplayer.util.image.ImageUtil;
import fe.materialplayer.util.properties.ColorThemeManager;
import fe.materialplayer.util.properties.ColorType;
import fe.materialplayer.util.resource.StylesheetUtil;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import kotlin.io.FilesKt;
import org.jetbrains.annotations.NotNull;

import java.net.URL;
import java.util.Map;
import java.util.Objects;
import java.util.ResourceBundle;

public class SidebarController extends SidebarBindController {

    private final JFXDrawer drawer;

    @FXML
    @ThemeBackgroundColor(cssProperty = StyleConstants.CSS_BACKGROUND_COLOR_PROPERTY)
    private GridPane gpRoot;

    @FXML
    private JFXButton btRescan;

    @FXML
    private VBox vbRescan;

    @FXML
    private JFXButton btMoveLibrary;

    @FXML
    private JFXButton btShowHidden;

    @FXML
    private JFXButton btOpenFolder;

    @FXML
    private JFXButton btSettings;

    @FXML
    private JFXButton btReset;

    @FXML
    private JFXButton btAbout;

    public SidebarController(JFXDrawer drawer) {
        this.drawer = drawer;
    }

    @Override
    public void initialize(@NotNull URL location, @NotNull ResourceBundle resources) {
        super.initialize(location, resources);

        for (Map.Entry<JFXButton, String> ent : Map.of(
                this.btRescan, "refresh",
                this.btMoveLibrary, "copy",
                this.btShowHidden, "show_songs",
                this.btOpenFolder, "folder",
                this.btSettings, "settings",
                this.btReset, "reset",
                this.btAbout, "help").entrySet()) {
            JFXButton btn = ent.getKey();

            btn.setGraphic(ImageUtil.makeIv(20, () -> ImageUtil.loadIcon(ColorThemeManager.getTheme().get().findIcon(ent.getValue())), ColorThemeManager.getTheme()));
            btn.setTextFill(ColorThemeManager.getTheme().get().getColor(ColorType.TEXT_COLOR));

            ColorThemeManager.getTheme().addListener((observable, oldValue, newValue) -> {
                btn.setTextFill(newValue.getColor(ColorType.TEXT_COLOR));
            });
        }
    }

    @FXML
    public void btRescanClicked(ActionEvent event) {
        JFXProgressBar pbRescan = new JFXProgressBar();
        StylesheetUtil.applyStylesheet(pbRescan, "progressbar");
        new StyleBinder(pbRescan)
                .bindToColorProperty(StyleConstants.CSS_PRIMARY_COLOR_VAR, ColorThemeManager.getPrimaryColor(), false)
                .build();

        this.vbRescan.getChildren().add(pbRescan);

        SongUpdateTask songUpdateTask = new SongUpdateTask();
        songUpdateTask.setOnSucceeded(evt -> {
            this.vbRescan.getChildren().remove(pbRescan);
            ItemFetcher.Companion.refetchAll();
        });
        pbRescan.progressProperty().bind(songUpdateTask.progressProperty());

        new Thread(songUpdateTask).start();
    }

    @FXML
    public void btShowHiddenClicked(ActionEvent event) {
        this.drawer.close();
        DialogUtil.loadDialog("main/sidebar/ShowHidden", null, new HiddenSongsController());
    }

    @FXML
    public void btOpenFolderClicked(ActionEvent event) {
        this.drawer.close();
        Util.openDir(Main.getInstance().getDir());
    }

    @FXML
    public void btResetClicked(ActionEvent event) {
        this.drawer.close();
        DialogUtil.createConfirmActionController("resetConfirmation", evt -> {
            Objects.requireNonNull(MusicLibrary.Companion.getInstance()).closeConnection();
            FilesKt.deleteRecursively(Main.getInstance().getDir());

            Platform.exit();
        });
    }

    @FXML
    public void btSettingsClicked(ActionEvent event) {
        Task<JFXDialog> d = DialogUtil.loadBackgroundDialog("main/sidebar/Settings", new SettingsController());
        d.setOnSucceeded(evt -> this.drawer.close());
    }

    @FXML
    public void btAboutClicked(ActionEvent event) {
        this.drawer.close();
        DialogUtil.loadDialog("main/sidebar/About", new AboutController());
    }


    @FXML
    public void btMoveLibraryClicked(ActionEvent event) {

    }
}
