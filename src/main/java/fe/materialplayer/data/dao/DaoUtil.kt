package fe.materialplayer.data.dao

import javafx.collections.ObservableList

object DaoUtil {
    fun <T> createUnknownIfNotExists(item: String?, unknown: T, items: ObservableList<T>): T? {
        if (item == null) {
            if(unknown !in items){
                items.add(unknown)
            }

            return unknown
        }

        return null
    }
}