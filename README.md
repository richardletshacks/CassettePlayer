![Logo](readme_res/title.png)

MusicPlayer written in JavaFX. The purpose of this application is to resemble the look and feel as well as the "playing behavior" of [Phonograph](https://github.com/kabouzeid/Phonograph) as close as possible.


## Features

* Scan MP3 files from a folder/(+subfolders)
* Extract MP3 tags (Group by artist, album, genre)
* Artist image fetching (from Deezer)
* Themes
* Playlist support
* Playing queue
* Parse tracks with featured artists/group members
* Linux MPRIS desktop integration
* Windows mediakey support


## Downloads 

Windows, Linux (deb + universal binary)

[Releases](https://gitlab.com/grrfe/CassettePlayer/-/releases)

## Media

![Screenshot1](readme_res/1.png)
![Screenshot2](readme_res/2.png)
![Screenshot1](readme_res/3.png)
![Screenshot1](readme_res/4.png)
![Screenshot2](readme_res/5.png)
![Screenshot1](readme_res/6.png)
![Screenshot2](readme_res/7.png)
![Screenshot2](readme_res/8.png)

## Tips

* Mid-click songs to add them as the next song in the playing queue
* Hold CTRL in Album/Artist-View and hover over "Add to playing queue" or "Play next" to add them shuffled. 

## Artist image fetching

When the artist list is first opened by the user, artist images for the artists in their music library are fetched from 
Deezer using the following API url: `http://api.deezer.com/search/artist?q=ARTIST_NAME`. No additional data is sent (but please note that deezer might log your IP address/query etc). 
This behavior is defined in [ArtistImageLoader.java](src/main/java/fe/materialplayer/util/artistimage/ArtistImageLoader.java).

## Featured artists / groups

Have you ever had your library fucked up by broken MP3 tags like `Artist X feat. Artist Y` which show up as an own artist in your artist overview?
I have, and I hated it, which is the reason I built a "featured artist" parser for CassettePlayer.

The parser supports the following "formats":

* Title tags may contain `ft.`, `feat.` or `featuring.` followed by a list of features separated by `,`, `;`, `&` or ` x `; 
These "feature delimiters" may also be surrounded by parenthesis.
* Artist tags may contain multiple artists separated by `,`, `;`, `&` or ` x `
* Artist/Title tags may contain `of` or `from` followed by a the name of the group they are a member of (for example, if an artist is part of a band/group) but has solo songs; 
If `of` or `from` is detected, all artists **before** it are added as members of the group, for example: `Artist X, Artist Y of Group Z` would mean that both `X` and `Y` are members of `Z`. 
So, if a song is from multiple group members and one artist which isn't a member of a group, the group members must be first in the title tag like so: `X, Y of Z, A` which marks `A` as an 
independent artist. One could also include the groupmembers in the title tag using the feature delimiters above, and the independent artist in the title

**Examples**

| Title tag                            | Artist tag                            | Resulting artist list in CassettePlayer (comma separated)              |
|--------------------------------------|---------------------------------------|--------------------------------------------------------------------|
| The Middle feat. Maren Morris & Grey | Zedd                                  | Zeed, Maren Morris, Grey                                           |
| Welcome to Chilis (feat. bbno$)      | Yung Gravy                            | Yung Gravy, bbno$                                                  |
| Empire feat. Minnie of (G)I-DLE      | WENGIE                                | WENGIE, Minnie (member of group (G)I-DLE)                          |
| Really Really                        | Siyeon, Sua, Yoohyeon of Dreamcatcher | Siyeon (Dreamcatcher), Sua (Dreamcatcher), Yoohyeon (Dreamcatcher) |

## Caveats

### Supported formats

Currently, only MP3 files are supported, but since the "playing-backend" (which is the JavaFX Media-API) supports more formats,
as can be seen [here](https://docs.oracle.com/javafx/2/api/javafx/scene/media/package-summary.html#SupportedMediaTypes),
support for different formats might be added in the future; Also, switching to vlc4j might be considered in the future, which enables us to play virtually any audio format thanks to vlc. 

### CPU/Ram usage

The cpu/ram use might use a lot of ram (~300mb on average in my testing) and quite a bit of your precious CPU; This might have something todo with Java(FX), not quite sure though; I'm working on that!

### Dark mode 

Dark mode might look very shitty right now since I do not like darkmode that much. Will probably fixed in the future. 

## Donations

Want to support me and also unlock donator-mode features? Hit me up at `grrfe [at] 420blaze.it` :)

(yes, you could also just compile the player from source with donator-mode enabled, go ahead of you know how to do that)
