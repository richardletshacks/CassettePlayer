package fe.materialplayer.controller.util.popup.impl

import com.jfoenix.controls.JFXButton
import fe.materialplayer.controller.content.album.AlbumContentController
import fe.materialplayer.controller.util.ItemFetcher
import fe.materialplayer.controller.util.dialog.DialogUtil
import fe.materialplayer.controller.util.popup.PopupState
import fe.materialplayer.controller.util.popup.PopupUtil
import fe.materialplayer.data.indexable.Song
import fe.materialplayer.music.MusicLibrary
import fe.materialplayer.util.Util
import java.util.*

class SongPopup(override val state: PopupState<Song>,
                private val resourceBundle: ResourceBundle) : ArtistablePopup<Song>(state, resourceBundle) {

    private var btGoToAlbum: JFXButton = PopupUtil.createPopupButton(this.resourceBundle.getString("goToAlbum"), "album")
    private var btHideSong: JFXButton = PopupUtil.createPopupButton(this.resourceBundle.getString("hideSong"), "hide_song")
    private var btOpenDir: JFXButton = PopupUtil.createPopupButton(this.resourceBundle.getString("openFolder"), "folder")

    init {
        this.btGoToAlbum.setOnMouseClicked { AlbumContentController.show(state.currentState!!.album, this.popup) }
        this.btHideSong.setOnMouseClicked {
            val song = state.currentState!!

            song.isHidden = true
            library.songDao.hideSong(song)
            library.songDao.update(song)

            library.albumDao.refresh(song.album)
            song.artists.forEach {
                library.artistDao.refresh(it)
            }

            ItemFetcher.refetchAll()
        }

        this.btOpenDir.setOnMouseClicked { Util.openDir(state.currentState!!.file.parentFile) }

        this.addItem(this.btGoToAlbum, this.btHideSong, this.btOpenDir)
    }

    override fun showPopup() {
        super.showPopup()

        this.btGoToAlbum.text = String.format(this.resourceBundle.getString("goToPhrase"), this.state.currentState!!.albumName)
    }

    companion object {
        private val library = MusicLibrary.getInstance()
    }
}