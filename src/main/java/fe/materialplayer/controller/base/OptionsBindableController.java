package fe.materialplayer.controller.base;

import javafx.beans.Observable;
import javafx.scene.input.MouseEvent;

public interface OptionsBindableController {
    void handleOptionsClick(MouseEvent event);
    void handleSearchTextChanged(Observable observable, String oldValue, String newValue);
}
