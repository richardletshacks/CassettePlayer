package fe.materialplayer.data.dao.helper

import fe.materialplayer.data.dao.base.IdDaoHelper
import fe.materialplayer.data.indexable.Song
import javafx.collections.FXCollections
import javafx.collections.ObservableList
import java.io.File
import java.nio.file.Path
import java.util.*

class SongDaoHelper : IdDaoHelper<Song>(Song::class.java, "title", true) {
    val nonHiddenItems: ObservableList<Song> = FXCollections.observableList(LinkedList())
    val favoriteSongs: ObservableList<Song> = FXCollections.observableList(LinkedList())

    fun favoriteSong(song: Song) {
        this.favoriteSongs.add(song)
    }

    fun unfavoriteSong(song: Song) {
        this.favoriteSongs.remove(song)
    }

    fun findSongByPath(path: Path): Song? {
        return findSongByPath(path.toString())
    }

    fun findSongByFile(file: File): Song? {
        return findSongByPath(file.absolutePath)
    }

    fun findSongByPath(path: String): Song? {
        for (s in this.items) {
            if (s.path == path) {
                return s
            }
        }

        return null
    }

    fun hideSong(song: Song) {
        this.nonHiddenItems.remove(song)
    }

    fun unhideSong(song: Song) {
        this.nonHiddenItems.add(song)
    }

    override fun removeFromList(item: Song) {
        super.removeFromList(item)
        this.nonHiddenItems.remove(item)
        this.favoriteSongs.remove(item)
    }

    override fun queryAll(): MutableList<Song> {
        val result = super.queryAll()
        this.nonHiddenItems.addAll(this.items.filter { s -> !s.isHidden })
        this.favoriteSongs.addAll(this.items.filter { s -> s.isFavorite })

        return result
    }

    override fun createAndRefresh(item: Song): Song {
        val song = super.createAndRefresh(item)

        if (!song.isHidden) {
            this.nonHiddenItems.add(song)
        }

        if (song.isFavorite) {
            this.favoriteSongs.add(song)
        }

        return song
    }

    override fun updateAndRefresh(item: Song): Song {
        val song = super.updateAndRefresh(item)
        if (!song.isHidden) {
            this.nonHiddenItems.add(song)
        }

        if (song.isFavorite) {
            this.favoriteSongs.add(song)
        }

        return song
    }
}