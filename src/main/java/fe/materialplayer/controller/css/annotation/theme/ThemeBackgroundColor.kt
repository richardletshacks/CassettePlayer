package fe.materialplayer.controller.css.annotation.theme

import fe.materialplayer.util.StyleConstants

@Target(AnnotationTarget.FIELD)
@Retention(AnnotationRetention.RUNTIME)
@MustBeDocumented
annotation class ThemeBackgroundColor(val cssProperty: String = StyleConstants.CSS_BACKGROUND_COLOR_VAR)