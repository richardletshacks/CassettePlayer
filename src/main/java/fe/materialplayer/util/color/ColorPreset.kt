package fe.materialplayer.util.color

import fe.materialplayer.controller.util.ComboboxNameKey
import fe.materialplayer.util.properties.ColorThemeManager
import fe.materialplayer.util.StyleConstants
import javafx.collections.FXCollections
import javafx.collections.ObservableList
import javafx.scene.paint.Color

data class ColorPreset(val nameKey: String, val primary: Color, val lightPrimary: Color, val accent: Color) : ComboboxNameKey {

    constructor(nameKey: String, primary: String, lightPrimary: String, accent: String)
            : this(nameKey, Color.valueOf(primary), Color.valueOf(lightPrimary), Color.valueOf(accent))

    override fun nameKey() = nameKey

    fun isMatch(primary: Color, lightPrimary: Color, accent: Color) = this.primary == primary && this.lightPrimary == lightPrimary && this.accent == accent

    companion object {
        @JvmStatic
        val presets: ObservableList<ColorPreset> = FXCollections.observableList(listOf(
                ColorPreset("presetDefault", StyleConstants.DEFAULT_PRIMARY_COLOR, StyleConstants.DEFAULT_LIGHT_PRIMARY_COLOR, StyleConstants.DEFAULT_ACCENT_COLOR),
                ColorPreset("presetSummer", "#2195f2", "#90caf9", "#fec007"),
                ColorPreset("presetDreary", "#607c8a", "#cfd8dc", "#fe9700"),
                ColorPreset("presetGoogleMusic", "#ef6c00", "#f28328", "#9fa8da")
        ))

        @JvmStatic
        fun findCurrentPresetMatch(): ColorPreset? {
            return findPresetMatch(ColorThemeManager.primaryColor.get(), ColorThemeManager.lightPrimaryColor.get(), ColorThemeManager.accentColor.get())
        }

        @JvmStatic
        fun findPresetMatch(primary: Color, lightPrimary: Color, accent: Color): ColorPreset? {
            presets.forEach {
                if (it.isMatch(primary, lightPrimary, accent)) {
                    return it
                }
            }

            return null
        }
    }
}

