package fe.materialplayer.controller.util.popup.impl

import com.jfoenix.controls.JFXButton
import fe.materialplayer.controller.content.artist.ArtistContentController
import fe.materialplayer.controller.util.dialog.DialogUtil
import fe.materialplayer.controller.util.popup.PopupState
import fe.materialplayer.controller.util.popup.PopupUtil
import fe.materialplayer.controller.util.popup.base.BasicPlayPopup
import fe.materialplayer.data.indexable.Artist
import fe.materialplayer.data.indexable.base.Artistable
import java.util.*

open class ArtistablePopup<T : Artistable>(override val state: PopupState<T>,
                                           private val resourceBundle: ResourceBundle) : BasicPlayPopup<T>(state, resourceBundle) {

    private val artistBtns = mutableListOf<JFXButton>()

    override fun showPopup() {
        super.showPopup()

        this.removeItems(artistBtns)
        artistBtns.clear()

        this.state.currentState!!.artists.forEach { artist ->
            addGoToBtn(artist)
        }

        this.state.currentState!!.artists.filter { it.parentArtist != null }.map { it.parentArtist }.distinct().forEach {artist ->
            addGoToBtn(artist)
        }
    }

    fun addGoToBtn(artist: Artist){
        val btGoToArtist = PopupUtil.createPopupButton(
                String.format(this.resourceBundle.getString("goToPhrase"), artist.name),
                "artist")
        btGoToArtist.setOnMouseClicked {
            ArtistContentController.show(artist, this.popup)
        }

        artistBtns.add(btGoToArtist)
        this.addItem(btGoToArtist, this.nodeSize() - 2)
    }
}