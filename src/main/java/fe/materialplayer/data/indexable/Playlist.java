package fe.materialplayer.data.indexable;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import fe.materialplayer.data.dao.helper.PlaylistSongDaoHelper;
import fe.materialplayer.data.indexable.base.Indexable;
import fe.materialplayer.data.indexable.nm.PlaylistSong;
import fe.materialplayer.data.persister.StringPropertyPersister;
import fe.materialplayer.data.sort.SortType;
import fe.materialplayer.data.persister.LocalDateTimePersister;
import fe.materialplayer.music.MusicLibrary;
import fe.materialplayer.util.FormatUtil;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;

import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.*;

@DatabaseTable(tableName = "playlist")
public class Playlist extends Indexable {
    private final static MusicLibrary LIBRARY = MusicLibrary.Companion.getInstance();

    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField(persisterClass = StringPropertyPersister.class)
    private final StringProperty name = new SimpleStringProperty();

    @DatabaseField(persisterClass = LocalDateTimePersister.class)
    private LocalDateTime createdAt;

    private final ObservableList<Song> songs = FXCollections.observableArrayList();
    private final DoubleProperty playlistLength = new SimpleDoubleProperty();

    private final PlaylistSongDaoHelper playlistSongDaoHelper = LIBRARY.getPlaylistSongDao();

    public Playlist(String name) {
        this.name.set(name);
        this.createdAt = LocalDateTime.now();
    }

    public Playlist() {
    }

    {
        this.songs.addListener((ListChangeListener<Song>) c -> this.playlistLength.set(FormatUtil.sumLength(this.songs)));
    }

    public StringProperty nameProperty() {
        return name;
    }

    public DoubleProperty playlistLengthProperty() {
        return playlistLength;
    }

    public static final SortType<Playlist> DEFAULT_SORT_TYPE = new SortType<>("Name", Comparator.comparing(Playlist::getName, String.CASE_INSENSITIVE_ORDER));
    public static final List<SortType<Playlist>> SORT_TYPES = List.of(
            DEFAULT_SORT_TYPE,
            new SortType<>("Created", Comparator.comparing(Playlist::getCreatedAt))
    );


    public ObservableList<Song> getSongs() {
        return songs;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public String getName() {
        return name.get();
    }

    @Override
    protected String[] compareArray() {
        return new String[]{this.getName()};
    }

    @Override
    public Collection<Song> toSongList() {
        return this.songs;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof Playlist) {
            return ((Playlist) o).id == this.id;
        }

        return false;
    }

    @Override
    public String toString() {
        return this.name.get();
    }

    public int getId() {
        return id;
    }

    public void loadSongs(List<PlaylistSong> songs) {
        songs.sort(Comparator.comparing(PlaylistSong::getIndex));
        for (PlaylistSong ps : songs) {
            this.songs.add(ps.getSong());
        }

        this.highestIndex = this.songs.size() + DB_INDEX_START;
    }

    //db indices start at 1 to enable easy swapping by setting to -value
    private static final int DB_INDEX_START = 1;
    private int highestIndex = DB_INDEX_START;

    public void addSongs(List<Song> songs) throws Exception {
        List<PlaylistSong> playlistSongs = new ArrayList<>();
        for (Song s : songs) {
            this.songs.add(s);

            playlistSongs.add(new PlaylistSong(this, s, this.highestIndex++));
        }

        this.playlistSongDaoHelper.batchCreate(playlistSongs);
    }

    public int moveItem(int index, int modifier) throws SQLException {
        int swap = index + modifier;
        if (swap >= 0 && swap < this.songs.size()) {
            Collections.swap(this.songs, index, swap);

            ///increase by 1 since list counting starts at one and passed index is from selected listitem which starts at 0
            index++;
            swap++;

            this.playlistSongDaoHelper.swapPlaylistItems(this, index, swap);
            return swap - 1;
        }

        return -1;
    }

    public void removeAllSong(Song song) throws SQLException {
        int idx;
        while ((idx = this.songs.indexOf(song)) != -1) {
            this.removeIndex(idx);
        }
    }

    public void removeIndex(int index) throws SQLException {
        if (index >= 0 && index < this.songs.size()) {
            this.songs.remove(index);

            //translate to db index which start at 1
            this.playlistSongDaoHelper.deletePlaylistItem(this, index + DB_INDEX_START);
            for (int i = index + 1; i <= this.songs.size(); i++) {
                this.playlistSongDaoHelper.movePlaylistItemDown(this, i + DB_INDEX_START);
            }

            this.highestIndex--;
        }
    }
}
