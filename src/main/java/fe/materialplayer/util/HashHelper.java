package fe.materialplayer.util;


import net.jpountz.xxhash.StreamingXXHash32;
import net.jpountz.xxhash.XXHashFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class HashHelper {
    private static final XXHashFactory FACTORY = XXHashFactory.fastestInstance();
    private static final int SEED = 0x23872f4;

    public static int hashFile(File f) throws IOException {
        FileInputStream fis = new FileInputStream(f);
        StreamingXXHash32 hash32 = FACTORY.newStreamingHash32(SEED);

        byte[] buffer = new byte[8192];
        int i;
        while((i = fis.read(buffer)) != -1){
            hash32.update(buffer, 0, i);
        }

        return hash32.getValue();
    }
}
