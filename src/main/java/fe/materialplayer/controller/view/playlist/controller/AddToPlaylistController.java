package fe.materialplayer.controller.view.playlist.controller;

import com.jfoenix.controls.JFXListView;
import com.jfoenix.controls.materialbutton.JFXContainedMaterialButton;
import com.jfoenix.controls.materialbutton.JFXTextMaterialButton;
import fe.materialplayer.controller.content.ContentController;
import fe.materialplayer.controller.css.annotation.color.AccentColored;
import fe.materialplayer.controller.css.annotation.color.LightPrimaryColored;
import fe.materialplayer.controller.css.annotation.color.PrimaryColored;
import fe.materialplayer.controller.css.annotation.text.PrimaryExtractedTextFill;
import fe.materialplayer.controller.css.annotation.theme.ThemeBackgroundColor;
import fe.materialplayer.controller.css.annotation.theme.ThemeTextColor;
import fe.materialplayer.controller.misc.CrashViewerController;
import fe.materialplayer.controller.util.PluralHelper;
import fe.materialplayer.controller.util.dialog.DialogUtil;
import fe.materialplayer.data.dao.helper.PlaylistDaoHelper;
import fe.materialplayer.data.indexable.Playlist;
import fe.materialplayer.data.indexable.Song;
import fe.materialplayer.data.indexable.base.Indexable;
import fe.materialplayer.music.MusicLibrary;
import fe.materialplayer.svg.SVGDrawer;
import fe.materialplayer.util.SnackbarHelper;
import fe.materialplayer.util.StyleConstants;
import fe.materialplayer.util.properties.ColorThemeManager;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import org.jetbrains.annotations.NotNull;

import java.net.URL;
import java.util.*;

public class AddToPlaylistController extends ContentController {

    @FXML
    @ThemeTextColor(cssProperty = StyleConstants.CSS_TEXT_FILL_PROPERTY)
    @LightPrimaryColored
    private JFXTextMaterialButton btCancel;

    @FXML
    @PrimaryColored(cssProperty = StyleConstants.CSS_BACKGROUND_COLOR_PROPERTY)
    @PrimaryExtractedTextFill(cssProperty = StyleConstants.CSS_TEXT_FILL_PROPERTY)
    private JFXContainedMaterialButton btAdd;

    @FXML
    @AccentColored(cssProperty = StyleConstants.CSS_TEXT_FILL_PROPERTY)
    @LightPrimaryColored
    private JFXTextMaterialButton btCreatePlaylist;

    @FXML
    @ThemeBackgroundColor
    @PrimaryColored
    @LightPrimaryColored
    @ThemeTextColor
    private JFXListView<Playlist> lvPlaylists;

    @FXML
    @ThemeTextColor
    private Label lbSelectPlaylist;

    private ObservableList<Playlist> playlists;

    private final MusicLibrary library = MusicLibrary.Companion.getInstance();
    private final PlaylistDaoHelper playlistDao = Objects.requireNonNull(this.library).getPlaylistDao();

    private final List<Song> songs = new ArrayList<>();
    private ResourceBundle resources;

    public AddToPlaylistController(Indexable indexable) {
        this(indexable.toSongList());
    }

    public AddToPlaylistController(Collection<Song> songs) {
        super(false, false);
        this.songs.addAll(songs);
    }

    @Override
    public void initialize(@NotNull URL location, @NotNull ResourceBundle resources) {
        super.initialize(location, resources);

        this.resources = resources;
        this.lvPlaylists.setItems(this.playlistDao.getItems());

        this.btCreatePlaylist.setExtraGraphic(
                SVGDrawer.draw(SVGDrawer.ADD_ICON, 12, Map.of(
                        0, new SimpleObjectProperty<>(Color.TRANSPARENT),
                        1, ColorThemeManager.getAccentColor()
                ))
        );

        this.gpRoot.addEventFilter(KeyEvent.KEY_PRESSED, event -> {
            if (event.getCode() == KeyCode.ENTER) {
                this.btAddClicked(null);
                event.consume();
            }
        });
    }

    public void addSongsToPlaylist(Playlist p) {
        if (p != null) {
            try {
                p.addSongs(this.songs);

                SnackbarHelper.createMinWidthTextBar(String.format("%s %s",
                        PluralHelper.getString(this.resources, "%d %s", this.songs, "song", "songs"),
                        PluralHelper.getString(this.resources, this.songs, "addedToPlaylistSingular", "addedToPlaylistPlural")
                ), 300);
            } catch (Exception e) {
                e.printStackTrace();
                new CrashViewerController("ADD_SONGS_TO_PLAYLIST",
                        String.format("Error while adding %s songs to playlist %s", this.songs, p), e).show();
            }
        }

        this.closeDialog();
    }

    @FXML
    public void btCreatePlaylistClicked(ActionEvent event) {
        DialogUtil.loadDialog("view/playlist/CreatePlaylist", null,
                new CreatePlaylistController(evt -> this.addSongsToPlaylist(evt.getPlaylist())));
    }

    @FXML
    public void btAddClicked(ActionEvent event) {
        this.addSongsToPlaylist(this.lvPlaylists.getSelectionModel().getSelectedItem());
    }

    @FXML
    public void lvPlaylistsClicked(MouseEvent event) {
        if (event.getButton() == MouseButton.PRIMARY && event.getClickCount() == 2) {
            this.addSongsToPlaylist(this.lvPlaylists.getSelectionModel().getSelectedItem());
        }
    }

    @FXML
    public void btCancelClicked(ActionEvent event) {
        this.closeDialog();
    }
}
