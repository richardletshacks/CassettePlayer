package fe.materialplayer.controller.css.annotation.color

import fe.materialplayer.util.StyleConstants

@Target(AnnotationTarget.FIELD)
@Retention(AnnotationRetention.RUNTIME)
@MustBeDocumented
annotation class LightPrimaryColored(val cssProperty: String = StyleConstants.CSS_LIGHT_PRIMARY_COLOR_VAR)