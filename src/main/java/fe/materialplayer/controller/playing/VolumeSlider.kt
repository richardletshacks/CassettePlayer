package fe.materialplayer.controller.playing

import com.jfoenix.controls.JFXButton
import com.jfoenix.controls.JFXPopup
import com.jfoenix.controls.JFXSlider
import fe.materialplayer.controller.css.StyleBinder
import fe.materialplayer.controller.css.bindStyle
import fe.materialplayer.music.MusicPlayer
import fe.materialplayer.util.StyleConstants
import fe.materialplayer.util.image.ImageUtil
import fe.materialplayer.util.properties.ColorThemeManager.primaryColor
import fe.materialplayer.util.properties.ColorThemeManager.accentColor
import fe.materialplayer.util.properties.ColorType
import javafx.beans.value.ObservableValue
import javafx.geometry.*
import javafx.scene.control.Skin
import javafx.scene.layout.ColumnConstraints
import javafx.scene.layout.GridPane
import javafx.scene.layout.Priority
import javafx.scene.layout.RowConstraints
import javafx.scene.paint.Color

class VolumeSlider(
    val btVolume: JFXButton,
    private val popupVPosition: JFXPopup.PopupVPosition,
    private val yOffset: Int
) {
    private val slider = JFXSlider()
    private var popup: JFXPopup? = null

    companion object {
        const val WIDTH = 40.0
        const val BUTTON_ROW_HEIGHT = 30.0
        const val DEFAULT_HEIGHT = 220.0
        const val PADDING_TOP_BOTTOM = 3.0
        const val ICON_SIZE = 18
        const val VOLUME_CHANGE_ON_INVOKE = 5

        val PLAYER: MusicPlayer = MusicPlayer.getInstance()
    }

    fun buildOrUpdateAndShow(volumeColor: Color?): JFXPopup {
        slider.value = PLAYER.volume * 100

        popup?.let { popup ->
            volumeColor?.let { color -> StyleBinder.setSliderColor(slider, color) }
            return popup.also {
                this.showPopup(popup)
            }
        }

        slider.bindStyle()
            .applyStyleSheet("sliderstyle")
            .bindToColorProperty(StyleConstants.CSS_PRIMARY_COLOR_VAR, primaryColor)
            .build()

        if (volumeColor != null) {
            slider.skinProperty().addListener { _: ObservableValue<out Skin<*>?>?, _: Skin<*>?, _: Skin<*>? ->
                StyleBinder.setSliderColor(slider, volumeColor)
            }
        }

        val insets = Insets(PADDING_TOP_BOTTOM, 0.0, PADDING_TOP_BOTTOM, 0.0)
        val gp = GridPane().apply {
            this.columnConstraints.add(ColumnConstraints(WIDTH, WIDTH, WIDTH, Priority.NEVER, HPos.CENTER, true))

            val btnRowConstraint = RowConstraints(BUTTON_ROW_HEIGHT)
            val sliderRowConstraint = RowConstraints(
                DEFAULT_HEIGHT - BUTTON_ROW_HEIGHT * 2,
                DEFAULT_HEIGHT - BUTTON_ROW_HEIGHT * 2,
                Double.MAX_VALUE,
                Priority.ALWAYS,
                VPos.CENTER,
                true
            )

            this.rowConstraints.addAll(btnRowConstraint, sliderRowConstraint, btnRowConstraint)

            listOf(
                makeButton(VolumeChange.PLUS),
                slider,
                makeButton(VolumeChange.MINUS)
            ).forEachIndexed { index, control ->
                this.addRow(index, control)
            }

            this.bindStyle()
                .bindToThemeColor(StyleConstants.CSS_BACKGROUND_COLOR_PROPERTY, ColorType.BACKGROUND_COLOR).build()

            this.prefWidth = WIDTH
            this.prefHeight = DEFAULT_HEIGHT

            this.alignment = Pos.CENTER
            this.padding = insets
        }

        GridPane.setMargin(slider, insets)

        slider.valueProperty().addListener { _: ObservableValue<out Number>?, _: Number?, newValue: Number ->
            PLAYER.volume = newValue.toDouble() / 100
        }
        slider.orientation = Orientation.VERTICAL

        popup = JFXPopup(gp).also {
            this.showPopup(it)
        }

        return popup as JFXPopup
    }

    private fun makeButton(change: VolumeChange): JFXButton {
        return JFXButton(null, ImageUtil.makeThemeBoundIv(change.icon, ICON_SIZE)).apply {
            StyleBinder.bindPaintToObjectColor(accentColor, this.ripplerFillProperty())

            setMinSize(BUTTON_ROW_HEIGHT, BUTTON_ROW_HEIGHT)
            setPrefSize(BUTTON_ROW_HEIGHT, BUTTON_ROW_HEIGHT)
            this.setOnMouseClicked {
                change.invoke(slider)
                it.consume()
            }
        }
    }

    enum class VolumeChange(val icon: String) {
        PLUS("plus2") {
            override fun invoke(slider: JFXSlider) {
                slider.value = (slider.value + VOLUME_CHANGE_ON_INVOKE).coerceAtMost(100.0)
            }
        },
        MINUS("remove2") {
            override fun invoke(slider: JFXSlider) {
                slider.value = (slider.value - VOLUME_CHANGE_ON_INVOKE).coerceAtLeast(0.0)
            }
        };

        abstract fun invoke(slider: JFXSlider)
    }

    fun showPopup(popup: JFXPopup) {
        popup.show(
            this.btVolume,
            this.popupVPosition,
            JFXPopup.PopupHPosition.LEFT, 0.0,
            this.yOffset.toDouble()
        )
    }

}