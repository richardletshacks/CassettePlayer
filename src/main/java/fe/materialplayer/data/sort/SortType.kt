package fe.materialplayer.data.sort

import java.util.*

class SortType<T>(val name: String, val comparator: Comparator<T>) {
    var reverse = false
    
    fun getSortComparator(): Comparator<T> = if (reverse) comparator.reversed() else comparator
}