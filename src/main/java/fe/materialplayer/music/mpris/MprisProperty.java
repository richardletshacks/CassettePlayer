package fe.materialplayer.music.mpris;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import org.freedesktop.dbus.connections.impl.DBusConnection;
import org.freedesktop.dbus.exceptions.DBusException;
import org.freedesktop.dbus.interfaces.Properties;
import org.freedesktop.dbus.types.Variant;

import java.util.Collections;
import java.util.Map;

public class MprisProperty<T> {

    private final Interface iface;
    private String name;
    private ObjectProperty<T> value;

    private static DBusConnection connectedTo;

    public MprisProperty(String name, Interface iface, T initialValue) {
        this.name = name;
        this.value = new SimpleObjectProperty<>(initialValue);
        this.iface = iface;

        this.value.addListener((observable, oldValue, newValue) -> {
            try {
                this.sendUpdate();
            } catch (DBusException e) {
                e.printStackTrace();
            }
        });
    }

    public static void connect(DBusConnection connection) {
        MprisProperty.connectedTo = connection;
    }

    public String getName() {
        return name;
    }

    public Variant<T> toVariant() {
        if (this.value.get() instanceof Variant) {
            return (Variant<T>) this.value.get();
        }

        return new Variant<T>(this.value.get());
    }

    public ObjectProperty<T> valueProperty() {
        return value;
    }

    public T getValue() {
        return value.get();
    }

    public void setValue(T value) {
        this.value.set(value);
    }

    public void updateValue(T newValue) throws DBusException {
        if (connectedTo != null && connectedTo.isConnected()) {
            this.value.setValue(newValue);
            this.sendUpdate();
        }
    }

    private void sendUpdate() throws DBusException {
        if (connectedTo != null && connectedTo.isConnected()) {
            connectedTo.sendMessage(new Properties.PropertiesChanged(
                    MprisPlayer.OBJECT_PATH,
                    this.iface.toString(),
                    Map.of(this.name, this.toVariant()),
                    Collections.emptyList())
            );
        }
    }

    public Interface getInterface() {
        return iface;
    }

    public enum Interface {
        DEFAULT("org.mpris.MediaPlayer2"),
        PLAYER("org.mpris.MediaPlayer2.Player");

        String str;

        Interface(String s) {
            this.str = s;
        }

        @Override
        public String toString() {
            return this.str;
        }
    }
}
