package fe.materialplayer.data.dao.helper

import fe.materialplayer.data.dao.base.IdDaoHelper
import fe.materialplayer.data.indexable.Album
import fe.materialplayer.data.indexable.Artist
import fe.materialplayer.data.indexable.Song
import fe.materialplayer.music.remover.LibrarySongRemover
import java.sql.SQLException

class AlbumDaoHelper : IdDaoHelper<Album>(Album::class.java, "name", true) {

    companion object {
        const val UNKNOWN_ALBUM = "Unknown Album"
    }

    fun existsAlbum(album: String?, year: String, artists: List<Artist>): Album? {
        return this.items.find { a ->
            a.name.equals(album, ignoreCase = true) && (
                    (!a.name.equals(UNKNOWN_ALBUM, ignoreCase = true) && containsAtLeastOne(a, artists))
                            || (a.year == year && compareArtistNames(a.artists, artists))
                    )
        }
    }

    //since this is used in the songfactory, the albums do not receive the updated artists with the correct ids yet, so .equals of the artist fails;
    //even if we used the refreshed artist instances, it would still not work when a song has been added afterwards since they are not refreshed
    // immediately like they are on the first run
    private fun compareArtistNames(artists1: List<Artist>, artists2: List<Artist>): Boolean {
        var counter = 0
        if (artists1.size != artists2.size) {
            return false
        }

        artists1.forEach { art1 ->
            if(preCorrectIdContains(artists2, art1) != null){
                counter++
            }
        }

        return counter == artists1.size
    }

    private fun containsAtLeastOne(album: Album, artists: List<Artist>): Boolean {
        return artists.find {
            preCorrectIdContains(album.artists, it) != null
        } != null
    }

    private fun preCorrectIdContains(list: List<Artist>, artist: Artist): Artist? {
        return list.find {
            preCorrectIdCompare(it, artist)
        }
    }

    private fun preCorrectIdCompare(artist1: Artist, artist2: Artist): Boolean {
        return artist1.name == artist2.name
    }

    @Throws(SQLException::class)
    override fun refresh(item: Album): Album {
        val artists = item.artists
        this.removeFromList(item)

        return this.querySpecific(item.id).also {
            it.addArtist(artists)
            this.addToList(it)
        }
    }


    fun checkRemoveAlbum(song: Song): LibrarySongRemover.UpdateState {
        val alb = song.album
        return if (alb.songs.size == 1) {
            LibrarySongRemover.UpdateState.REMOVE
        } else {
            LibrarySongRemover.UpdateState.UPDATE
        }
    }
}