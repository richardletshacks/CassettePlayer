package fe.materialplayer.controller.util.index;

import fe.logger.Logger;
import fe.materialplayer.data.indexable.Song;
import fe.materialplayer.music.SongFactory;
import kotlin.Pair;

import java.io.File;
import java.util.concurrent.Callable;

public class ParseTask implements Callable<Pair<Song, Long>> {
    private final Logger logger;
    private final File songFound;
    private final boolean insert;

    public ParseTask(Logger logger, File songFound, boolean insert) {
        this.logger = logger;
        this.songFound = songFound;
        this.insert = insert;
    }

    @Override
    public Pair<Song, Long> call() {
        long startedAt = System.currentTimeMillis();
        this.logger.print(Logger.Type.INFO, "Creating song %s using song factory", songFound);
        Song song = SongFactory.createSong(songFound, insert);
        long elapsedTime = System.currentTimeMillis() - startedAt;
        this.logger.print(Logger.Type.INFO, "Created song in %d", elapsedTime);

        return new Pair<>(song, elapsedTime);
    }
}
