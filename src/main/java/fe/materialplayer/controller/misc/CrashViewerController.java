package fe.materialplayer.controller.misc;

import com.google.gson.JsonObject;
import com.jfoenix.controls.JFXTextArea;
import fe.materialplayer.controller.content.ContentController;
import fe.materialplayer.controller.css.annotation.theme.ThemeTextColor;
import fe.materialplayer.controller.css.annotation.theme.ThemeBackgroundColor;
import fe.materialplayer.controller.util.dialog.DialogUtil;
import fe.materialplayer.util.StyleConstants;
import javafx.fxml.FXML;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import org.jetbrains.annotations.NotNull;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URL;
import java.util.Base64;
import java.util.ResourceBundle;

public class CrashViewerController extends ContentController {
    @FXML
    private GridPane gpRoot;

    @FXML
    @ThemeTextColor
    private VBox vbLabels;

    @FXML
    @ThemeBackgroundColor
    @ThemeTextColor(cssProperty = StyleConstants.CSS_TEXT_FILL_PROPERTY)
    private JFXTextArea taException;

    @FXML
    private Label lbErrorCode;

    @FXML
    private Hyperlink hlCopyAgain;

    private final String errorCode;
    private final String exceptionText;
    private final String copyText;

    public CrashViewerController(String errorCode, String extraText, Exception e) {
        this.errorCode = errorCode;
        this.exceptionText = CrashViewerController.exceptionToString(e);

        JsonObject obj = new JsonObject();
        obj.addProperty("errorCode", this.errorCode);
        obj.addProperty("extraText", extraText);
        obj.addProperty("text", this.exceptionText);

        this.copyText = Base64.getEncoder().encodeToString(obj.toString().getBytes());
        this.copyToClipboard();
    }

    public void show() {
        DialogUtil.loadDialog("CrashViewer", null, this);
    }

    @Override
    public void initialize(@NotNull URL location, @NotNull ResourceBundle resources) {
        super.initialize(location, resources);

        this.taException.setText(this.exceptionText);
        this.lbErrorCode.setText(String.format(resources.getString("errorCodePhrase"), this.errorCode));

        this.hlCopyAgain.setOnMouseClicked(evt -> this.copyToClipboard());
    }


    public void copyToClipboard() {
        ClipboardContent content = new ClipboardContent();
        content.putString(this.copyText);

        Clipboard.getSystemClipboard().setContent(content);
    }

    public static String exceptionToString(Exception exception) {
        if (exception == null) {
            return "";
        }

        StringWriter sw = new StringWriter();
        exception.printStackTrace(new PrintWriter(sw));

        return sw.toString();
    }
}
