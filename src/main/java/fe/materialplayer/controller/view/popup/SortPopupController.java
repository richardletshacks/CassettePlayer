package fe.materialplayer.controller.view.popup;

import com.jfoenix.controls.JFXPopup;
import com.jfoenix.controls.JFXRadioButton;
import fe.materialplayer.controller.base.IndexController;
import fe.materialplayer.data.indexable.base.Indexable;
import fe.materialplayer.data.sort.SortType;
import fe.materialplayer.util.properties.ColorThemeManager;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

public class SortPopupController<T extends Indexable> implements Initializable {

    @FXML
    private JFXRadioButton rbAscending;

    @FXML
    private JFXRadioButton rbDescending;

    @FXML
    private GridPane gpRoot;

    private JFXPopup popup;

    @FXML
    private VBox vbRadioButtons;

    private final IndexController<T> parent;
    private final SortType<T> currentMode;
    private final List<SortType<T>> sortTypes;


    public SortPopupController(IndexController<T> parent, SortType<T> currentMode, List<SortType<T>> sortTypes) {
        this.parent = parent;
        this.currentMode = currentMode;
        this.sortTypes = sortTypes;
    }

    public void setPopup(JFXPopup popup) {
        this.popup = popup;
    }

    public boolean isReverse(ToggleGroup toggleGroup) {
        return toggleGroup.getSelectedToggle() == this.rbDescending;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        ToggleGroup tgType = new ToggleGroup();
        this.rbAscending.setToggleGroup(tgType);
        this.rbDescending.setToggleGroup(tgType);

        this.rbAscending.selectedColorProperty().bind(ColorThemeManager.getPrimaryColor());
        this.rbDescending.selectedColorProperty().bind(ColorThemeManager.getPrimaryColor());

        tgType.selectToggle(this.currentMode.getReverse() ? this.rbDescending : this.rbAscending);

        this.gpRoot.setPrefHeight(this.sortTypes.size() * 10);

        ToggleGroup tgSort = new ToggleGroup();
        for (SortType<T> st : this.sortTypes) {
            JFXRadioButton btn = new JFXRadioButton(st.getName());
            btn.selectedColorProperty().bind(ColorThemeManager.getPrimaryColor());
            btn.setToggleGroup(tgSort);

            if (this.currentMode == st) {
                tgSort.selectToggle(btn);
            }

            btn.setOnMouseClicked(evt -> {
                st.setReverse(this.isReverse(tgType));

                this.parent.updateSorting(st);
                if (this.parent.isSearching()) {
                    this.parent.reRunSearch();
                }
                this.popup.hide();
            });

            this.vbRadioButtons.getChildren().add(btn);
        }
    }
}
