package fe.materialplayer.util

import fe.materialplayer.controller.misc.CrashViewerController
import javafx.animation.KeyFrame
import javafx.animation.KeyValue
import javafx.animation.Timeline
import javafx.beans.property.DoubleProperty
import javafx.util.Duration
import java.awt.Desktop
import java.io.*
import java.net.URISyntaxException
import java.net.URL

object Util {

    //TODO: FIND A WAY TO IMPROVE THIS
    @JvmStatic
    fun isMp3File(file: File) = file.isFile && file.extension == "mp3"

    @JvmStatic
    fun isMp3File(list: List<File>): Boolean {
        list.forEach {
            if (!isMp3File(it)) {
                return false
            }
        }

        return true
    }

    @JvmStatic
    fun scanSubDir(dir: File, recursive: Boolean): List<File> {
        return if (recursive) {
            dir.walkTopDown().filter { isMp3File(it) }.toList()
        } else dir.listFiles()?.filter { isMp3File(it) }?.toList() ?: listOf()
    }

    @JvmStatic
    fun openUrl(url: String) {
        Thread {
            try {
                Desktop.getDesktop().browse(URL(url).toURI())
            } catch (e: IOException) {
                e.printStackTrace()
            } catch (e: URISyntaxException) {
                e.printStackTrace()
            }
        }.start()
    }

    @JvmStatic
    fun openDir(file: File) {
        Thread {
            try {
                Desktop.getDesktop().open(file)
            } catch (e: IOException) {
                e.printStackTrace()
                CrashViewerController("FOLDER_OPEN", "An error happened while opening a directory", e).show()
            }
        }.start()
    }

    @JvmStatic
    fun unNull(str: String?): String {
        return str ?: ""
    }

    @JvmStatic
    fun makeSimpleTimeline(duration: Duration, doubleProperty: DoubleProperty, endValue: Double): Timeline {
        return Timeline(KeyFrame(duration, KeyValue(doubleProperty, endValue)))
    }

    @JvmStatic
    inline fun <T> useInputStream(stream: InputStream, useFn: (closeable: InputStream) -> T): T {
        return stream.use {
            useFn(it)
        }
    }

    @JvmStatic
    inline fun <R, A : Closeable, B : Closeable> multiUse(resourceA: A, resourceB: B, useFn: (a: A, b: B) -> R): R {
        resourceA.use { aUse ->
            resourceB.use { bUse ->
                return useFn(aUse, bUse)
            }
        }
    }

    @JvmStatic
    @Throws(IOException::class)
    fun downloadFile(url: String, f: File): File {
        multiUse(URL(url).openStream(), f.outputStream()) { i, o -> i.copyTo(o) }
        return f
    }

    @JvmStatic
    fun getComputerName(): String {
        val env = System.getenv()
        val name = env["COMPUTERNAME"]
        return name ?: env.getOrDefault("HOSTNAME", "Unknown computer")
    }

    @JvmStatic
    fun fileToUriString(file: File): String {
        var uri = file.toURI().toString()
        if (uri.startsWith("file:")) {
            uri = uri.replace("file:", "file://")
        }

        return uri
    }
}