package fe.materialplayer.controller.base

import fe.materialplayer.controller.css.AnnotationBinder
import fe.materialplayer.util.properties.Properties
import javafx.fxml.Initializable
import java.net.URL
import java.util.*

abstract class BaseController : Initializable {
    protected lateinit var resources: ResourceBundle
    val properties = Properties.getInstance()

    override fun initialize(location: URL, resources: ResourceBundle) {
        this.resources = resources
        AnnotationBinder.createBindings(this)
    }
}