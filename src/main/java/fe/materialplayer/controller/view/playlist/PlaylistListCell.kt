package fe.materialplayer.controller.view.playlist

import fe.materialplayer.data.indexable.Playlist
import fe.materialplayer.music.MusicPlayer
import fe.materialplayer.util.SnackbarHelper.createMinWidthTextBar
import fe.materialplayer.util.image.ImageUtil
import fe.materialplayer.util.properties.ColorThemeManager.theme
import fe.materialplayer.util.properties.PlayerTheme
import javafx.event.EventHandler
import javafx.geometry.Pos
import javafx.scene.control.ContentDisplay
import javafx.scene.control.Label
import javafx.scene.control.ListCell
import javafx.scene.image.ImageView
import javafx.scene.input.MouseButton
import javafx.scene.input.MouseEvent
import javafx.scene.layout.HBox
import java.util.*
import java.util.concurrent.Callable


class PlaylistListCell(val resourceBundle: ResourceBundle) : ListCell<Playlist>() {

    companion object {
        private val PLAYER = MusicPlayer.getInstance()
        private val ICON_MAP = PlayerTheme.values().map {
            it to ImageUtil.loadIcon(it.findIcon("playlist"))
        }.toMap()
    }

    override fun updateItem(item: Playlist?, empty: Boolean) {
        super.updateItem(item, empty)
        if (empty || item == null) {
            text = null
            graphic = null
        } else {
            val label = Label()
            label.textProperty().bind(item.nameProperty())

            val iv: ImageView = ImageUtil.makeIv(
                    25, Callable {
                return@Callable ICON_MAP[theme.get()]
            }, theme)

            val box = HBox(iv, label)

            box.alignment = Pos.CENTER_LEFT
            box.spacing = 10.0

            contentDisplay = ContentDisplay.GRAPHIC_ONLY
            graphic = box

            this.onMouseClicked = EventHandler { evt: MouseEvent ->
                if (evt.button == MouseButton.MIDDLE) {
                    PLAYER.addNextSongs(item.songs, false)
                    createMinWidthTextBar(resourceBundle.getString("addedNextPlaylist"), 300.0)
                }
            }
        }
    }
}