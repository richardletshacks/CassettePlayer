package fe.materialplayer.controller.base;

import com.jfoenix.controls.JFXListView;
import com.jfoenix.controls.JFXPopup;
import fe.materialplayer.controller.css.annotation.theme.ThemeTextColor;
import fe.materialplayer.controller.util.ItemFetcher;
import fe.materialplayer.controller.view.popup.SortPopupController;
import fe.materialplayer.data.dao.base.IdDaoHelper;
import fe.materialplayer.data.indexable.base.Indexable;
import fe.materialplayer.data.sort.SortType;
import fe.materialplayer.music.MusicLibrary;
import fe.materialplayer.music.MusicPlayer;
import fe.materialplayer.util.resource.FxmlUtil;
import javafx.beans.binding.Bindings;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Region;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.function.Consumer;

public abstract class IndexController<T extends Indexable> extends SidebarBindController {

    @FXML
    protected BorderPane bpSearchHelper;

    @ThemeTextColor
    private Label lbNoResults;

    protected static final int FETCH_SIZE = 15;

    protected MusicLibrary library = MusicLibrary.Companion.getInstance();
    protected MusicPlayer player = MusicPlayer.getInstance();

    protected ItemFetcher<T> itemFetcher;
    protected SortType<T> sortMode;
    protected IdDaoHelper<T> daoHelper;

    private static final Insets NO_PADDING = new Insets(0, 0, 0, 0);
    private static final Insets PLAYING_BAR_PADDING = new Insets(0, 0, 40, 0);

    public IndexController(SortType<T> sortMode, IdDaoHelper<T> daoHelper) {
        this.sortMode = sortMode;
        this.daoHelper = daoHelper;
    }

    @Override
    public void initialize(@NotNull URL location, @NotNull ResourceBundle resources) {
        super.initialize(location, resources);

        this.bpSearchHelper.setPickOnBounds(false);
        this.lbNoResults = new Label(this.resources.getString("noResults"));
    }

    public void initListViewPadding(JFXListView<T> listView) {
        listView.paddingProperty().bind(Bindings.createObjectBinding(
                () -> player.playerStoppedProperty().get() ? NO_PADDING : PLAYING_BAR_PADDING, player.playerStoppedProperty()
        ));
    }

    public void updateSorting(SortType<T> sortMode) {
        this.sortMode = sortMode;

        this.itemFetcher.reset(this.sortMode.getSortComparator());
        this.itemFetcher.fetchItems(FETCH_SIZE, ItemFetcher.FetchType.INITIAL);
    }

    public void showPopup(List<SortType<T>> sortTypeList, SortType<T> currentMode, Node source) throws IOException {
        SortPopupController<T> svopc = new SortPopupController<>(this, currentMode, sortTypeList);

        FxmlUtil.loadNonBlockingFxml("view/popup/SortPopupController", svopc, node -> {
            JFXPopup popup = new JFXPopup((Region) node);
            svopc.setPopup(popup);

            popup.show(source,
                    JFXPopup.PopupVPosition.TOP,
                    JFXPopup.PopupHPosition.RIGHT,
                    -12,
                    15);
        });
    }

    public boolean isSearching() {
        String searchText = this.currentSearchResult.getSearchText();
        return searchText != null && !searchText.isEmpty();
    }

    public void reRunSearch() {
        if (this instanceof OptionsBindableController) {
            ((OptionsBindableController) this).handleSearchTextChanged(null, "", this.currentSearchResult.getSearchText());
        }
    }

    protected SearchResult<T> currentSearchResult = new SearchResult<>(null, SearchResultState.RESET, null);

    protected void execSearch(String searchText, JFXListView<T> listView) {
        this.bpSearchHelper.setCenter(null);

        if (searchText != null) {
            if (searchText.isEmpty()) {
                listView.setItems(itemFetcher.getSortedViewItems());
                this.currentSearchResult = new SearchResult<>(searchText, SearchResultState.RESET, null);
            } else {
                ObservableList<T> searchList = FXCollections.observableArrayList();
                listView.setItems(searchList);
                for (T item : this.daoHelper.getItems()) {
                    if (item.isSearchResultForText(searchText)) {
                        searchList.add(item);
                    }
                }
                if (searchList.isEmpty()) {
                    this.bpSearchHelper.setCenter(this.lbNoResults);
                    this.currentSearchResult = new SearchResult<>(searchText, SearchResultState.NOT_FOUND, null);
                } else {
                    searchList.sort(this.itemFetcher.getComparator());
                    this.currentSearchResult = new SearchResult<>(searchText, SearchResultState.FOUND, searchList);
                }
            }
        } else {
            this.currentSearchResult = new SearchResult<>(null, SearchResultState.TEXT_NULL, null);
        }
    }

    protected List<T> getSearchFilteredItemsOrAll() {
        return this.currentSearchResult.getState() != SearchResultState.FOUND
                ? this.daoHelper.getSortedCopy(this.sortMode) : this.currentSearchResult.getResults();
    }
}


