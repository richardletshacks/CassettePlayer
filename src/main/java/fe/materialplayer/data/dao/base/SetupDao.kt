package fe.materialplayer.data.dao.base

import fe.materialplayer.data.indexable.base.table.Table
import fe.materialplayer.music.MusicLibrary

interface SetupDao {
    fun setup(queryResult: MutableList<out Table>)

    companion object {
        val library = MusicLibrary.getInstance()
    }
}