package fe.materialplayer.controller.util.cell.impl

import fe.materialplayer.controller.util.cell.SimpleLineBoxListCell
import fe.materialplayer.data.indexable.base.BackgroundColorable
import fe.materialplayer.util.StyleConstants
import fe.materialplayer.util.properties.ColorThemeManager
import javafx.beans.binding.Bindings
import javafx.beans.property.BooleanProperty
import javafx.beans.property.SimpleBooleanProperty
import javafx.beans.value.ObservableValue
import javafx.event.EventHandler
import javafx.scene.control.Label
import java.util.concurrent.Callable

open class ColorableCell<T : BackgroundColorable> : SimpleLineBoxListCell<T>() {
    protected var selected: BooleanProperty = SimpleBooleanProperty()
    protected var hover: BooleanProperty = SimpleBooleanProperty()

    init {
        this.prefWidth = 0.0
    }

    override fun bindLabel(label: Label, lineType: LineType, item: T) {
        label.maxWidth = this.listView.width - 150
        label.textFillProperty().bind(Bindings.createObjectBinding(
                Callable {
                    if (this.selected.get() || this.hover.get()) {
                        return@Callable BackgroundColorable.getTextColor(item)
                    }

                    return@Callable lineType.acquireColor.invoke()
                }, this.selected, this.hover, ColorThemeManager.theme
        ))
    }

    fun makeListeners(item: BackgroundColorable?) {
        this.onMouseEntered = EventHandler {
            if (!selected.get() && item != null && this.graphic != null) {
                hover.set(true)
                this.style = BackgroundColorable.toBackgroundStyle(item)
            }
        }

        this.onMouseExited = EventHandler {
            if (!selected.get() && item != null) {
                hover.set(false)
                this.style = StyleConstants.TRANSPARENT_BACKGROUND
            }
        }

        this.selectedProperty().addListener { _: ObservableValue<out Boolean>?, oldValue: Boolean, newValue: Boolean ->
            if (!oldValue && newValue) {
                this.style = BackgroundColorable.toBackgroundStyle(item)
                selected.set(true)
            }

            if (oldValue && !newValue) {
                this.style = StyleConstants.TRANSPARENT_BACKGROUND
                selected.set(false)
                hover.set(false)
            }
        }
    }
}