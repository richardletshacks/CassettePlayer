package fe.materialplayer.controller.view.song;

import com.jfoenix.controls.JFXListView;
import fe.materialplayer.controller.base.IndexController;
import fe.materialplayer.controller.base.OptionsBindableController;
import fe.materialplayer.controller.css.StyleBinder;
import fe.materialplayer.controller.css.annotation.theme.ThemeTextColor;
import fe.materialplayer.controller.css.annotation.theme.ThemeBackgroundColor;
import fe.materialplayer.controller.misc.CrashViewerController;
import fe.materialplayer.controller.util.ItemFetcher;
import fe.materialplayer.controller.util.cell.DefaultSongListCell;
import fe.materialplayer.controller.util.popup.PopupState;
import fe.materialplayer.controller.util.popup.impl.SongPopup;
import fe.materialplayer.controller.view.PlayOptionBoxController;
import fe.materialplayer.controller.view.TopFavoriteController;
import fe.materialplayer.data.dao.helper.SongDaoHelper;
import fe.materialplayer.data.indexable.Song;
import fe.materialplayer.music.MusicLibrary;
import fe.materialplayer.music.MusicPlayer;
import fe.materialplayer.util.properties.ColorType;
import fe.materialplayer.util.StyleConstants;
import fe.materialplayer.util.resource.FxmlUtil;
import javafx.beans.Observable;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.*;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.net.URL;
import java.util.*;

public class SongViewController extends IndexController<Song> implements OptionsBindableController {
    @FXML
    private RowConstraints rcFavorites;

    @ThemeTextColor
    private Label lbFavorites;

    @ThemeTextColor
    private Label lbSongs;

    @FXML
    private GridPane gpRoot;

    @FXML
    @ThemeBackgroundColor
    private JFXListView<Song> lvSongs;


    private final MusicPlayer player = MusicPlayer.getInstance();

    private final PopupState<Song> popupState = new PopupState<>();
    private SongPopup popup;

    private final SongDaoHelper songDao;


    public SongViewController() {
        super(Song.DEFAULT_SORT_TYPE, MusicLibrary.Companion.getInstance().getSongDao());

        this.songDao = (SongDaoHelper) this.daoHelper;
        this.itemFetcher = new ItemFetcher<>(this.songDao.getNonHiddenItems(), this.sortMode.getSortComparator(), false);
    }

    public void addControlBox(PlayOptionBoxController<Song> playOptionBoxController, Label lbPrepend, int col, int row, GridPane gp) {
        try {
            HBox hbSongControl = FxmlUtil.loadFxml("view/PlayOptionBox", playOptionBoxController).load();
            hbSongControl.getChildren().add(0, lbPrepend);
            gp.add(hbSongControl, col, row);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public GridPane addFavoriteBox() {
        try {
            GridPane gpFavorites;
            this.gpRoot.add(gpFavorites = FxmlUtil.loadFxml("view/TopFavoriteDisplay",
                    new TopFavoriteController(rcFavorites, lbSongs, songDao.getFavoriteSongs())).load(), 0, 0);

            return gpFavorites;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void initialize(@NotNull URL location, @NotNull ResourceBundle resources) {
        super.initialize(location, resources);
        this.initListViewPadding(this.lvSongs);

        this.lbSongs = new Label(resources.getString("songs"));
        this.lbFavorites = new Label(resources.getString("favorites"));

        new StyleBinder(this.lbSongs, this.lbFavorites)
                .applyStyleSheet("content-label")
                .setIntCSSProperty("-fx-font-size", 15)
                .bindToThemeColor(StyleConstants.CSS_TEXT_FILL_VAR, ColorType.TEXT_COLOR)
                .build();

        GridPane gpFavorites = this.addFavoriteBox();
        this.addControlBox(new PlayOptionBoxController<>() {
            @NotNull
            @Override
            public List<Song> provideSongs() {
                return getSearchFilteredItemsOrAll();
            }
        }, this.lbSongs, 0, 1, this.gpRoot);

        this.addControlBox(new PlayOptionBoxController<>() {
            @NotNull
            @Override
            public List<Song> provideSongs() {
                return songDao.getFavoriteSongs();
            }
        }, this.lbFavorites, 0, 0, gpFavorites);

        this.popup = new SongPopup(this.popupState, resources);

        this.itemFetcher.setListView(this.lvSongs);
        this.itemFetcher.fetchItems(FETCH_SIZE, ItemFetcher.FetchType.INITIAL);

        this.lvSongs.setCellFactory(arg0 -> new DefaultSongListCell(Song.FormatStyle.TITLE_ARTIST_ALBUM, resources));
    }

    @FXML
    public void lvSongsClicked(MouseEvent event) {
        Song s = this.lvSongs.getSelectionModel().getSelectedItem();
        if (s != null) {
            if (event.getButton() == MouseButton.PRIMARY && event.getClickCount() == 2) {
                List<Song> queueList = this.getSearchFilteredItemsOrAll();

                this.player.requeue(
                        queueList,
                        queueList.indexOf(s));
            } else if (event.getButton() == MouseButton.SECONDARY) {
                this.popupState.setCurrentState(s);
                this.popup.buildAndShow(this.lvSongs, event, null);
            }
        }
    }

    @FXML
    public void lvSongsScrolled(ScrollEvent event) {
        this.itemFetcher.fetchItems(FETCH_SIZE, ItemFetcher.FetchType.SCROLL);
    }

    @Override
    public void handleOptionsClick(MouseEvent event) {
        try {
            this.showPopup(Song.SORT_TYPES, this.sortMode, (Node) event.getSource());
        } catch (IOException e) {
            e.printStackTrace();
            new CrashViewerController("SHOW_OPTIONS_POPUP",
                    String.format("Error while showing options popup for sort mode %s", this.sortMode), e).show();
        }
    }

    @Override
    public void handleSearchTextChanged(Observable observable, String oldValue, String newValue) {
        this.execSearch(newValue, this.lvSongs);
    }
}


