package fe.materialplayer.controller.css.annotation.theme

import fe.materialplayer.util.StyleConstants

@Target(AnnotationTarget.FIELD)
@Retention(AnnotationRetention.RUNTIME)
@MustBeDocumented
annotation class ThemeTextColor(val cssProperty: String = StyleConstants.CSS_TEXT_FILL_VAR)