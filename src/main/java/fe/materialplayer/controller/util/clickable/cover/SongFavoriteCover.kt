package fe.materialplayer.controller.util.clickable.cover

import fe.materialplayer.data.indexable.Song
import javafx.geometry.Insets
import javafx.scene.control.Label
import javafx.scene.layout.VBox

class SongFavoriteCover(val song: Song) : TinyClickableCover(
        SIZE, song.album.getCover(Song.CoverType.S160X160), song.hexBackgroundColor) {

    override fun build(): VBox {
        val lbName = Label(song.title).apply {
            this.textFill = song.textColor
            this.padding = Insets(0.0, 0.0, 0.0, 5.0)
        }

        return super.build(5.0, arrayOf(lbName)).apply {
            this.maxHeight = MAX_VBOX_HEIGHT
        }
    }

    companion object {
        const val SIZE = 75.0
        const val MAX_VBOX_HEIGHT = 90.0
    }
}