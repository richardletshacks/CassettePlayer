package fe.materialplayer.controller.content.resize.button

import com.jfoenix.controls.JFXButton
import fe.materialplayer.util.Util
import javafx.animation.Animation
import javafx.event.EventHandler
import javafx.scene.image.Image
import javafx.scene.image.ImageView
import javafx.scene.input.MouseEvent
import javafx.util.Duration

abstract class ResizeButton(val button: JFXButton, val event: EventHandler<in MouseEvent>, val graphic: Image, val text: String, val maxSize: Int) {

    val duration: Duration = Duration.millis(100.0)

    protected var increaseAnimation: Animation? = null
    protected var decreaseAnimation: Animation = makeTimeline(minSize)

    init {
        val iv = ImageView(graphic)
        iv.fitHeight = iconSize
        iv.fitWidth = iconSize

        button.graphic = iv
        button.onMouseClicked = event

        decreaseAnimation.onFinished = EventHandler { button.text = null }
        button.onMouseExited = EventHandler {
            increaseAnimation!!.stop()
            decreaseAnimation.play()
        }
    }

    fun makeTimeline(size: Double): Animation {
        return Util.makeSimpleTimeline(this.duration, button.minWidthProperty(), size)
    }

    companion object {
        private const val minSize = 30.0
        private const val iconSize = 18.0
    }
}