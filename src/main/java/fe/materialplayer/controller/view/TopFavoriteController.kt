package fe.materialplayer.controller.view

import fe.materialplayer.controller.util.clickable.cover.SongFavoriteCover
import fe.materialplayer.data.indexable.Song
import fe.materialplayer.music.MusicPlayer
import fe.materialplayer.util.SnackbarHelper
import fe.materialplayer.util.Util.makeSimpleTimeline
import fe.materialplayer.util.image.ImageUtil
import javafx.animation.Timeline
import javafx.collections.ListChangeListener
import javafx.collections.ObservableList
import javafx.event.EventHandler
import javafx.fxml.FXML
import javafx.fxml.Initializable
import javafx.geometry.Insets
import javafx.geometry.Pos
import javafx.scene.control.Label
import javafx.scene.input.MouseEvent
import javafx.scene.layout.*
import javafx.util.Duration
import java.net.URL
import java.util.*

class TopFavoriteController(private val rcFavorites: RowConstraints, val lbName: Label, val favoriteItems: ObservableList<Song>) : Initializable {
    companion object {
        private const val FAVORITE_ROW_HEIGHT = 130
        private const val FAVORITE_HOVER_INSET = 150
        private const val BASE_MILLI_DURATION = 90.0
        private val PLAYER = MusicPlayer.getInstance()
    }

    private lateinit var resources: ResourceBundle

    @FXML
    private lateinit var gpFavorites: GridPane

    @FXML
    private lateinit var spFavorites: StackPane

    @FXML
    private lateinit var hbFavorites: HBox

    @FXML
    private lateinit var bpFavoriteIndicator: BorderPane

    private val favoriteMap = mutableMapOf<Song, VBox>()
    private var currentFavoriteTimeline: Timeline? = null

    private var showableCovers = 0

    override fun initialize(location: URL, resources: ResourceBundle) {
        this.resources = resources

        bpFavoriteIndicator.isPickOnBounds = false

        val listChangeListener = ListChangeListener { change: ListChangeListener.Change<out Song>? ->
            val visible: Boolean = !favoriteItems.isEmpty()
            val height = if (visible) FAVORITE_ROW_HEIGHT else 0
            this.lbName.isVisible = visible
            this.lbName.isManaged = visible
            gpFavorites.isVisible = visible

            this.rcFavorites.prefHeight = height.toDouble()
            this.rcFavorites.minHeight = height.toDouble()
            this.rcFavorites.maxHeight = height.toDouble()

            this.rcFavorites.prefHeightProperty().set(height.toDouble())
            this.rcFavorites.minHeightProperty().set(height.toDouble())
            this.rcFavorites.maxHeightProperty().set(height.toDouble())

            if (change != null) {
                change.next()

                if (change.wasAdded()) {
                    this.addFavorites(change.addedSubList as List<Song>)
                    this.attemptCreateRightArrow()
                } else if (change.wasRemoved()) {
                    change.removed.forEach { song ->
                        this.favoriteMap[song]?.let {
                            hbFavorites.children.remove(it)
                            this.favoriteMap.remove(song)
                        }

                        if (this.favoriteMap.size <= this.showableCovers) {
                            this.removeRightArrow()
                        }
                    }
                }
            } else {
                this.addFavorites(favoriteItems)
            }
        }

        //wait for width to be set since attemptCreateRightArrow() depends on correct width which is set by javafx later (javafx works in mysterious ways)
        hbFavorites.widthProperty().addListener { _, oldValue, _ ->
            if (oldValue.toDouble() == 0.0) {
                this.attemptCreateRightArrow()
            }
        }

        spFavorites.sceneProperty().addListener { _, _, newVal ->
            if (newVal != null) {
                //not perfectly accurate since the last cover doesnt need spacing, but should still be fine
                val spacePerCover = SongFavoriteCover.SIZE + hbFavorites.spacing
                this.showableCovers = (newVal.width / spacePerCover).toInt()

                newVal.widthProperty().addListener { _, _, newValue ->
                    this.showableCovers = (newValue.toDouble() / spacePerCover).toInt()
                }
            }
        }

        spFavorites.onMouseEntered = EventHandler { evt: MouseEvent ->
            if (this.showableCovers < this.favoriteMap.size) {
                if (spFavorites.scene.width - evt.x <= FAVORITE_HOVER_INSET) {
                    //calculates the space difference between all covers and the amount of covers which can be shown at once,
                    // might not be perfect since showableCovers is not calculated perfectly
                    val spaceDiff: Double = this.calculateFavoriteCoverWidth(this.favoriteMap.size) - this.calculateFavoriteCoverWidth(this.showableCovers)

                    //factor 5 was found through trial and error, please dont judge me .-.
                    this.currentFavoriteTimeline = makeSimpleTimeline(
                            Duration.millis(BASE_MILLI_DURATION * this.favoriteMap.size),
                            hbFavorites.translateXProperty(),
                            -(spaceDiff - hbFavorites.spacing * 5)
                    ).also {
                        it.play()
                    }

                    this.removeRightArrow()
                } else if (evt.x <= FAVORITE_HOVER_INSET) {
                    this.currentFavoriteTimeline = makeSimpleTimeline(
                            Duration.millis(BASE_MILLI_DURATION * this.favoriteMap.size),
                            hbFavorites.translateXProperty(),
                            0.0
                    ).also {
                        it.play()
                    }
                }
            }
        }

        spFavorites.onMouseExited = EventHandler {
            this.currentFavoriteTimeline?.stop()
        }

        favoriteItems.sortedBy {
            it.favoritedAt
        }

        listChangeListener.onChanged(null)
        favoriteItems.addListener(listChangeListener)
    }

    private fun removeRightArrow() {
        if (bpFavoriteIndicator.right != null) {
            bpFavoriteIndicator.right = null
        }
    }

    private fun attemptCreateRightArrow() {
        if (showableCovers < favoriteMap.size) {
            val pane = StackPane(ImageUtil.makeIconIv("arrow_right_white", 20)).apply {
                this.style = "-fx-background-radius: 100; -fx-background-color: #373737"
                this.setPrefSize(25.0, 25.0)
                this.setMaxSize(25.0, 25.0)
            }

            val box = VBox(pane).apply {
                this.alignment = Pos.BOTTOM_CENTER
                this.padding = Insets(0.0, 5.0, 0.0, 0.0)
            }

            bpFavoriteIndicator.right = box
        }
    }

    private fun calculateFavoriteCoverWidth(size: Int): Double {
        return size * SongFavoriteCover.SIZE + (size - 1) * hbFavorites.spacing
    }

    fun addFavorites(favorites: List<Song>) {
        favorites.forEach { s ->
            with(SongFavoriteCover(s).build()) {
                this.onMouseClicked = EventHandler {
                    PLAYER.addNextSongs(listOf(s), false)
                    SnackbarHelper.createMinWidthTextBar(resources.getString("addedNextSong"), 300.0)
                }

                favoriteMap[s] = this
                hbFavorites.children.add(0, this)
            }
        }
    }
}