package fe.materialplayer.util;

import fe.materialplayer.data.indexable.Song;
import javafx.util.Duration;

import java.util.Collection;

public class FormatUtil {

    public static double sumLength(Collection<Song> songs) {
        return songs.stream().mapToDouble(Song::getLength).sum();
    }

    public static String formatLength(double timeSeconds) {
        int minutes = 0;
        int hours = 0;
        int seconds = (int) (timeSeconds % 60);
        if (timeSeconds > 60) {
            minutes = (int) (timeSeconds / 60);
            if (minutes > 60) {
                hours = minutes / 60;
                minutes = minutes % 60;
            }
        }

        if (hours > 0) {
            return String.format("%02d:%02d:%02d", hours, minutes, seconds);
        }

        return String.format("%d:%02d", minutes, seconds);
    }

    public static String formatLength(Duration duration) {
        return FormatUtil.formatLength(duration.toSeconds());
    }
}
