package fe.materialplayer.scene

import com.jfoenix.controls.JFXDrawer
import com.jfoenix.controls.JFXDrawersStack
import com.jfoenix.controls.JFXRippler
import fe.materialplayer.controller.MainController
import fe.materialplayer.controller.base.SidebarBindController
import fe.materialplayer.controller.sidebar.SidebarController
import fe.materialplayer.controller.view.album.AlbumViewController
import fe.materialplayer.controller.view.artist.ArtistViewController
import fe.materialplayer.controller.view.genre.GenreViewController
import fe.materialplayer.controller.view.playlist.controller.PlaylistViewController
import fe.materialplayer.controller.view.song.SongViewController
import javafx.event.EventHandler
import javafx.stage.Stage


object SceneManager {
    private val drawerStack = JFXDrawersStack()
    private val dwSideBar = JFXDrawer()

    private var scenes: MutableList<LoadableScene>? = null;

    private var stage: Stage? = null

    fun setupStack(stage: Stage): JFXDrawersStack {
        this.stage = stage

        val main = LoadableScene("Main", "main/Main", MainController())
        val sidebar = LoadableScene("Sidebar", "main/Sidebar", SidebarController(this.dwSideBar))
        this.scenes = mutableListOf(
                main,
                LoadableScene("SongView", "view/song/SongView", SongViewController()),
                LoadableScene("AlbumView", "view/album/AlbumView", AlbumViewController()),
                LoadableScene("ArtistView", "view/artist/ArtistView", ArtistViewController()),
                LoadableScene("GenreView", "view/genre/GenreView", GenreViewController()),
                LoadableScene("PlaylistView", "view/playlist/PlaylistView", PlaylistViewController()),
                sidebar
        )

        sidebar.currentScene.addListener { _, _, _ ->
            this.dwSideBar.setSidePane(sidebar.getNode())
        }

        sidebar.load()

        this.dwSideBar.defaultDrawerSize = 200.0
        this.dwSideBar.isResizeContent = false
        this.dwSideBar.isOverLayVisible = true
        this.dwSideBar.isResizableOnDrag = true

        this.showController(main)

        return this.drawerStack
    }

    fun reload() = this.scenes?.forEach {
        it.reload()
    }

    fun findScene(clazz: Class<out SidebarBindController>): LoadableScene? {
        this.scenes?.forEach {
            if (it.controller.javaClass == clazz) {
                return it
            }
        }

        return null
    }

    private fun findScene(name: String): LoadableScene? {
        this.scenes?.forEach { scene ->
            if (scene.name.equals(name, ignoreCase = true)) {
                return scene
            }
        }

        return null
    }

    private fun showController(scene: LoadableScene) {
        scene.currentScene.addListener { _, _, _ -> this.drawerStack.content = scene.getNode() }
        scene.load()
    }

    private fun closeSidebar() = this.dwSideBar.close()

    fun bindSidebarToggle(hamburger: JFXRippler) {
        hamburger.onMouseClicked = EventHandler {
            this.drawerStack.toggle(this.dwSideBar)
        }
    }
}