package fe.materialplayer.data.indexable.base;

import fe.materialplayer.controller.util.cell.FormattablePair;

public interface IFormattedLines {
    FormattablePair getBoxLines();
}
