package fe.materialplayer.data.persister;

import com.j256.ormlite.field.FieldType;
import com.j256.ormlite.field.SqlType;
import com.j256.ormlite.field.types.BaseDataType;
import com.j256.ormlite.support.DatabaseResults;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.sql.SQLException;

public class StringPropertyPersister extends BaseDataType {
    private static final StringPropertyPersister SINGLETON = new StringPropertyPersister();

    private StringPropertyPersister() {
        super(SqlType.STRING, new Class<?>[]{StringProperty.class});
    }

    public StringPropertyPersister(SqlType sqlType, Class<?>[] classes) {
        super(sqlType, classes);
    }

    public static StringPropertyPersister getSingleton() {
        return SINGLETON;
    }

    @Override
    public Object parseDefaultString(FieldType fieldType, String defaultStr) throws SQLException {
        return new SimpleStringProperty();
    }

    @Override
    public Object resultToSqlArg(FieldType fieldType, DatabaseResults results, int columnPos) throws SQLException {
        return new SimpleStringProperty(results.getString(columnPos).trim());
    }

    @Override
    public Object javaToSqlArg(FieldType fieldType, Object javaObject) throws SQLException {
        return ((SimpleStringProperty) javaObject).getValue();
    }
}
