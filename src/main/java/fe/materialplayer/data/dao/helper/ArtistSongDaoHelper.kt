package fe.materialplayer.data.dao.helper

import fe.materialplayer.data.dao.base.NMDaoHelper
import fe.materialplayer.data.dao.base.SetupDao
import fe.materialplayer.data.dao.base.SetupDao.Companion.library
import fe.materialplayer.data.indexable.Artist
import fe.materialplayer.data.indexable.nm.ArtistSong
import fe.materialplayer.data.indexable.Song
import fe.materialplayer.data.indexable.base.table.Table
import java.sql.SQLException
import java.util.*

class ArtistSongDaoHelper : NMDaoHelper<ArtistSong, Artist, Song>(ArtistSong::class.java,
        Pair(ArtistSong.ARTIST_ID_COLUMN_NAME, ArtistSong.SONG_ID_COLUMN_NAME),
        ArtistSong.SONG_ID_COLUMN_NAME, { it.id }), SetupDao {

    @Throws(SQLException::class)
    fun batchCreateAndSetup(songs: List<Song>) {
        val artistSongs = mutableListOf<ArtistSong>()
        for (s in songs) {
            for (a in s.artists) {
                val artistSong = ArtistSong(a, s)

                if(artistSong !in items){
                    artistSongs.add(artistSong)
                }
            }

            s.artists.clear()
        }

        this.addToList(artistSongs)
        this.batchCreate(artistSongs)
        this.setup(artistSongs)
    }

    override fun setup(queryResult: MutableList<out Table>) {
        queryResult.forEach {
            val artistSong = it as ArtistSong

            val song = library.songDao.findItemById(artistSong.songId)
            val artist = library.artistDao.findItemById(artistSong.artistId)

            artistSong.song = song
            artistSong.artist = artist

            artist?.let { art ->
                if (!art.songs.contains(song)) {
                    art.songs.add(song)
                }
            } ?: println("artist is null, this should never happen!")

            song?.let { so ->
                if (!so.artists.contains(artist)) {
                    so.artists.add(artist)
                }
            } ?: println("song is null, this should never happen!")
        }
    }

    override fun findRelationFirst(first: Artist) = items.filter { it.artist == first }
    override fun findRelationSecond(second: Song) = items.filter { it.song == second }

}

