package fe.materialplayer.controller.view.artist

import fe.materialplayer.controller.util.PluralHelper
import fe.materialplayer.controller.util.cell.SimpleLineBoxListCell
import fe.materialplayer.controller.view.album.AlbumListCell
import fe.materialplayer.data.indexable.Artist
import fe.materialplayer.data.indexable.Song
import fe.materialplayer.data.indexable.base.BackgroundColorable
import fe.materialplayer.music.MusicPlayer
import fe.materialplayer.util.SnackbarHelper.createMinWidthTextBar
import fe.materialplayer.util.artistimage.ArtistImageLoader
import fe.materialplayer.util.image.ImageUtil
import fe.materialplayer.util.properties.ColorThemeManager
import javafx.application.Platform.runLater
import javafx.beans.binding.Bindings
import javafx.beans.value.ChangeListener
import javafx.beans.value.ObservableValue
import javafx.event.EventHandler
import javafx.scene.control.ContentDisplay
import javafx.scene.control.Label
import javafx.scene.control.ListCell
import javafx.scene.image.Image
import javafx.scene.image.ImageView
import javafx.scene.input.MouseButton
import javafx.scene.input.MouseEvent
import javafx.scene.layout.HBox
import java.util.*


class ArtistListCell(val resourceBundle: ResourceBundle) : SimpleLineBoxListCell<Artist>() {
    private val loadManager = ArtistImageLoader.getInstance()

    override fun updateItem(item: Artist?, empty: Boolean) {
        super.updateItem(item, empty)
        if (empty || item == null) {
            text = null
            graphic = null
        } else {
            val vbox = this.makeLabelBox(item, item.boxLines.apply {
                this.formatValue(
                        PluralHelper.getString(resourceBundle, "%d %s", item.albums, "album", "albums"),
                        PluralHelper.getString(resourceBundle, "%d %s", item.songs, "song", "songs")
                )
            })

            val iv: ImageView = ImageUtil.makeIv(IV_SIZE)
            val box = HBox(iv, vbox).apply {
                this.spacing = 10.0
            }

            if (item.artistImage != null) {
                ImageUtil.roundImageWrapper(iv, item.artistImage, IV_SIZE)
            } else {
                runLater { iv.image = LOADING_GIF }
                this.loadManager.nextArtist(item)

                val listener = object : ChangeListener<Image?> {
                    override fun changed(observable: ObservableValue<out Image?>, oldValue: Image?, newValue: Image?) {
                        if (newValue != null) {
                            runLater { ImageUtil.roundImageWrapper(iv, newValue, IV_SIZE) }
                            item.artistImageProperty().removeListener(this)
                        }
                    }
                }

                item.artistImageProperty().addListener(listener)
            }

            if (!this.loadManager.isRunning) {
                this.loadManager.start()
            }

            contentDisplay = ContentDisplay.GRAPHIC_ONLY
            graphic = box

//            this.onMouseClicked = EventHandler { evt: MouseEvent ->
//                if (evt.button == MouseButton.MIDDLE) {
//                    PLAYER.addNextSongs(item.songs, false)
//                    createMinWidthTextBar(resourceBundle.getString("addedNextArtist"), 300.0)
//                }
//            }
        }
    }

    companion object {
        private const val IV_SIZE = 35
        private val LOADING_GIF: Image = ImageUtil.loadIcon("loading_small", "gif")
        private val PLAYER = MusicPlayer.getInstance()
    }
}