package fe.materialplayer.data.indexable.base.table

fun interface NMTable<X, Y> : Table {
    fun colValues(): Pair<X, Y>
}