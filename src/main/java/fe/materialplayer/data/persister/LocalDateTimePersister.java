package fe.materialplayer.data.persister;

import com.j256.ormlite.field.FieldType;
import com.j256.ormlite.field.SqlType;
import com.j256.ormlite.field.types.DateTimeType;
import com.j256.ormlite.support.DatabaseResults;

import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class LocalDateTimePersister extends DateTimeType {
    private static final LocalDateTimePersister SINGLETON = new LocalDateTimePersister();
    public static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ISO_DATE_TIME;

    private LocalDateTimePersister() {
        super(SqlType.STRING, new Class<?>[]{LocalDateTime.class});
    }

    public LocalDateTimePersister(SqlType sqlType, Class<?>[] classes) {
        super(sqlType, classes);
    }

    public static LocalDateTimePersister getSingleton() {
        return SINGLETON;
    }

    @Override
    public Object parseDefaultString(FieldType fieldType, String defaultStr) {
        return LocalDateTime.MIN;
    }

    @Override
    public Object resultToSqlArg(FieldType fieldType, DatabaseResults results, int columnPos) throws SQLException {
        String date = results.getString(columnPos);
        if(date == null){
            return null;
        }

        return LocalDateTime.parse(date.trim(), DATE_TIME_FORMATTER);
    }

    @Override
    public Object javaToSqlArg(FieldType fieldType, Object javaObject) {
        LocalDateTime ldt = (LocalDateTime) javaObject;
        return ldt.format(DATE_TIME_FORMATTER);
    }

    @Override
    public Object sqlArgToJava(FieldType fieldType, Object sqlArg, int columnPos) {
        return LocalDateTime.parse(String.valueOf(sqlArg), DATE_TIME_FORMATTER);
    }
}
