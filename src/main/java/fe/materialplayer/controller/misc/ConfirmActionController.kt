package fe.materialplayer.controller.misc

import com.jfoenix.controls.JFXButton
import com.jfoenix.controls.materialbutton.JFXOutlinedMaterialButton
import fe.materialplayer.controller.content.ContentController
import fe.materialplayer.controller.css.StyleBinder
import fe.materialplayer.controller.css.annotation.color.PrimaryColored
import fe.materialplayer.controller.css.annotation.text.PrimaryExtractedTextFill
import fe.materialplayer.controller.css.annotation.theme.ThemeTextColor
import fe.materialplayer.util.StyleConstants
import fe.materialplayer.util.properties.ColorType
import javafx.event.ActionEvent
import javafx.event.EventHandler
import javafx.fxml.FXML
import javafx.scene.control.Label
import javafx.scene.input.MouseEvent
import javafx.scene.text.Text
import java.net.URL
import java.util.*


class ConfirmActionController(private val resourceString: String, private val continueHandler: EventHandler<in MouseEvent>) : ContentController() {
    @FXML
    @ThemeTextColor
    private lateinit var lbConfirmAction: Label

    @FXML
    private lateinit var txAction: Text

    @FXML
    @PrimaryColored(cssProperty = StyleConstants.CSS_TEXT_FILL_PROPERTY)
    private lateinit var btCancel: JFXOutlinedMaterialButton

    @FXML
    @PrimaryColored(cssProperty = StyleConstants.CSS_BACKGROUND_COLOR_PROPERTY)
    @PrimaryExtractedTextFill(cssProperty = StyleConstants.CSS_TEXT_FILL_PROPERTY)
    private lateinit var btContinue: JFXButton

    override fun initialize(location: URL, resources: ResourceBundle) {
        super.initialize(location, resources)
        
        this.txAction.text = resources.getString(resourceString)
        StyleBinder.bindPaintToThemeColor(ColorType.TEXT_COLOR,
                this.txAction.fillProperty(),
                this.btContinue.textFillProperty(),
                this.btCancel.textFillProperty())

        this.btContinue.addEventHandler(MouseEvent.MOUSE_CLICKED, continueHandler)
        this.btContinue.setOnMouseClicked {
            this.closeDialog()
        }
    }

    @FXML
    fun btCancelClicked(event: ActionEvent) {
        this.closeDialog()
    }
}