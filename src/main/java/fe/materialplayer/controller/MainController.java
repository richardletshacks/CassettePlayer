package fe.materialplayer.controller;

import animatefx.animation.FadeInUp;
import animatefx.animation.FadeOutDown;
import animatefx.animation.SlideInRight;
import animatefx.animation.SlideOutRight;
import com.google.gson.JsonPrimitive;
import com.jfoenix.controls.*;
import fe.materialplayer.controller.base.OptionsBindableController;
import fe.materialplayer.controller.base.SidebarBindController;
import fe.materialplayer.controller.css.StyleBinder;
import fe.materialplayer.controller.css.annotation.color.LightPrimaryColored;
import fe.materialplayer.controller.css.annotation.color.PrimaryColored;
import fe.materialplayer.controller.css.annotation.theme.ThemeBackgroundColor;
import fe.materialplayer.controller.css.annotation.theme.ThemeTextColor;
import fe.materialplayer.controller.firstrun.FirstRunColorSelectionController;
import fe.materialplayer.controller.firstrun.FirstRunIndexingController;
import fe.materialplayer.controller.firstrun.FirstRunSelectLang;
import fe.materialplayer.controller.libchanges.LibraryChangesController;
import fe.materialplayer.controller.misc.CrashViewerController;
import fe.materialplayer.controller.playing.PlayingBarController;
import fe.materialplayer.controller.playing.PlayingScreenController;
import fe.materialplayer.controller.util.ItemFetcher;
import fe.materialplayer.controller.util.dialog.DialogUtil;
import fe.materialplayer.controller.util.dialog.sequence.SequenceManager;
import fe.materialplayer.controller.util.dialog.sequence.SequencePart;
import fe.materialplayer.controller.view.album.AlbumViewController;
import fe.materialplayer.controller.view.artist.ArtistViewController;
import fe.materialplayer.controller.view.genre.GenreViewController;
import fe.materialplayer.controller.view.playlist.controller.PlaylistViewController;
import fe.materialplayer.controller.view.song.SongViewController;
import fe.materialplayer.font.FontManager;
import fe.materialplayer.font.PlayerFont;
import fe.materialplayer.music.MusicLibrary;
import fe.materialplayer.music.MusicPlayer;
import fe.materialplayer.music.remover.LibrarySongRemover;
import fe.materialplayer.scene.LoadableScene;
import fe.materialplayer.svg.SVGDrawer;
import fe.materialplayer.util.StyleConstants;
import fe.materialplayer.util.Util;
import fe.materialplayer.util.directory.DirectoryListener;
import fe.materialplayer.util.image.ImageUtil;
import fe.materialplayer.util.properties.ColorThemeManager;
import fe.materialplayer.util.properties.ColorType;
import fe.materialplayer.util.properties.Properties;
import fe.materialplayer.util.resource.FxmlUtil;
import fe.materialplayer.util.threading.ThreadManager;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.ResourceBundle;

public class MainController extends SidebarBindController  {
    @FXML
    @ThemeBackgroundColor(cssProperty = StyleConstants.CSS_BACKGROUND_COLOR_PROPERTY)
    private JFXRippler riMenu;

    @FXML
    @ThemeBackgroundColor
    @ThemeTextColor
    private JFXHamburger hmMenu;

    @FXML
    private JFXRippler riOptions;

    @FXML
    @ThemeBackgroundColor
    @ThemeTextColor
    private JFXHamburger hmOptions;

    @FXML
    private JFXTextField tfSearchbar;

    @FXML
    @PrimaryColored
    @ThemeBackgroundColor
    @ThemeTextColor
    private HBox hbSearch;

    @FXML
    private BorderPane bpMusicControl;

    @FXML
    @PrimaryColored(cssProperty = StyleConstants.CSS_BACKGROUND_COLOR_PROPERTY)
    private GridPane gpTop;

    @FXML
    @ThemeBackgroundColor(cssProperty = StyleConstants.CSS_BACKGROUND_COLOR_PROPERTY)
    private GridPane gpTopSearch;

    @FXML
    private BorderPane bpSearchIcon;

    @FXML
    @ThemeBackgroundColor(cssProperty = StyleConstants.CSS_BACKGROUND_COLOR_PROPERTY)
    private StackPane spRoot;

    @FXML
    @PrimaryColored
    @LightPrimaryColored
    @ThemeTextColor
    @ThemeBackgroundColor
    private JFXTabPane tbCategories;

    @FXML
    @ThemeTextColor(cssProperty = StyleConstants.CSS_TEXT_FILL_PROPERTY)
    private Label lbCassette;

    @FXML
    private Tab tbSongs;

    @FXML
    private Tab tbAlbums;

    @FXML
    private Tab tbArtists;

    @FXML
    private Tab tbGenres;

    @FXML
    private Tab tbPlaylists;

    @FXML
    private VBox vbIndexProgress;

    @FXML
    private Label lbIndexProgress;

    private final MusicPlayer player = MusicPlayer.getInstance();
    private final MusicLibrary library = Objects.requireNonNull(MusicLibrary.Companion.getInstance());
    private final ThreadManager threadManager = ThreadManager.getInstance();
    private final Properties properties = Objects.requireNonNull(Properties.Companion.getInstance());

    private boolean musicBarShown;

    private StackPane spPlayingScreen;
    private StackPane spPlayingBar;
    private PlayingScreenController playingScreenController;

    private static final String TF_SEARCH_FOCUS_CLASS = "focusedbox";
    private static final Color TF_SEARCH_NON_FOCUSSED = Color.valueOf("#9e9e9e");
    private static final PlayerFont HK_GROTESK_SEMI_BOLD = FontManager.Companion.getHK_GROTESK_SEMI_BOLD();

    private boolean setupDialogShown;

    @Override
    public void initialize(@NotNull URL location, @NotNull ResourceBundle resources) {
        super.initialize(location, resources);

        ObjectProperty<Color> searchIconColor = new SimpleObjectProperty<>();
        this.tfSearchbar.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue) {
                this.hbSearch.getStyleClass().add(TF_SEARCH_FOCUS_CLASS);
                searchIconColor.set(ColorThemeManager.getPrimaryColor().get());
            } else {
                this.hbSearch.getStyleClass().remove(TF_SEARCH_FOCUS_CLASS);
                searchIconColor.set(TF_SEARCH_NON_FOCUSSED);
            }
        });

        this.tfSearchbar.setFont(HK_GROTESK_SEMI_BOLD.get(15.5));

        this.bpSearchIcon.setCenter(SVGDrawer.draw(SVGDrawer.SEARCH_ICON, 13, Map.of(
                0, new SimpleObjectProperty<>(Color.TRANSPARENT),
                1, searchIconColor)
        ));

        this.vbIndexProgress.setVisible(false);

        this.bindSidebar(this.riMenu);
        this.bpMusicControl.setPickOnBounds(false);

        this.spRoot.setOnKeyPressed(evt -> {
            if (evt.isControlDown() && evt.getCode() == KeyCode.F) {
                this.tfSearchbar.requestFocus();
            }
        });

        JFXButton btSearch = this.makeSearchButton();
        this.tfSearchbar.setOnKeyTyped(event -> {
            if (this.setupDialogShown) {
                this.tfSearchbar.setText(null);
                event.consume();
            }
        });

        this.tfSearchbar.setOnKeyPressed(event -> {
            if (!this.setupDialogShown && !(this.tfSearchbar.getText() == null || this.tfSearchbar.getText().isEmpty()) && event.getCode() == KeyCode.ENTER) {
                this.currentController.handleSearchTextChanged(null, "", this.tfSearchbar.getText());
            }
        });

        this.tfSearchbar.textProperty().addListener((observable, oldValue, newValue) -> {
            if(!this.setupDialogShown){
                newValue = Util.unNull(newValue);
                oldValue = Util.unNull(oldValue);

                this.currentController.handleSearchTextChanged(observable, oldValue, newValue);

                if (!newValue.isEmpty() && oldValue.isEmpty()) {
                    if (!this.hbSearch.getChildren().contains(btSearch)) {
                        this.hbSearch.getChildren().add(btSearch);
                        new SlideInRight(btSearch).setSpeed(5).play();
                    }
                } else if (newValue.isEmpty() && !oldValue.isEmpty()) {
                    SlideOutRight slideOutRight = new SlideOutRight(btSearch);
                    slideOutRight.setSpeed(5).setOnFinished(v -> this.hbSearch.getChildren().remove(btSearch));
                    slideOutRight.play();
                }
            }
        });

        this.player.queueSizeProperty().addListener((observable, oldValue, newValue) -> {
            if (!this.musicBarShown) {
                if (this.spPlayingBar == null) {
                    this.spPlayingBar = this.createPlayingBar();
                }

                this.bpMusicControl.setBottom(this.spPlayingBar);
                this.musicBarShown = true;
            }

            if (this.player.queueSizeProperty().get() == 0 && this.musicBarShown) {
                this.bpMusicControl.setBottom(null);
                this.musicBarShown = false;
            }
        });

        if (Objects.requireNonNull(this.library).getFirstRun()) {
            this.setupDialogShown = true;
            SequenceManager sequence = new SequenceManager(SequenceManager.Direction.HORIZONTAL);

            SequencePart selectLang = new SequencePart("firstrun/FirstRunSelectLang", new FirstRunSelectLang());
            SequencePart selectColor = new SequencePart("firstrun/FirstRunColorSelection", new FirstRunColorSelectionController());

            SequencePart libraryIndex = new SequencePart("firstrun/FirstRunIndexing", new FirstRunIndexingController((observable, oldValue, newValue) -> {
                this.createDirectoryListener(getMusicDir());
                this.properties.addProperty("libraryScanningDone", new JsonPrimitive(true)).save();
                this.setupDialogShown = false;

                //cleanup (high ram usage after first run indexing, with -XX:+UseG1GC enabled this seems to work
                // properly and give the used memory back to the system)
                System.gc();
            }));

            sequence.start(selectLang, selectColor, libraryIndex);
        } else {
            File dir = getMusicDir();
            this.library.scanMusicDir(dir, this.properties.getBooleanProperty("musicDirRecursive", false));

            //do a little changelistener fuckery so we dont have to create yet _another_ fucking method
            ChangeListener<Boolean> onDone = (observable, oldValue, newValue) -> {
                try {
                    new LibrarySongRemover().removeNonExistentLibrarySongs();
                    ItemFetcher.Companion.refetchAll();
                } catch (Exception e) {
                    new CrashViewerController("CHECK_REMOVED_SONGS", "Something went wrong while checking for removed songs", e).show();
                }

                this.createDirectoryListener(dir);
            };

            List<File> files = this.library.checkForNewSongs();
            if (!files.isEmpty()) {
                DialogUtil.createUnclosableDialog(new LibraryChangesController(files, onDone), "LibraryChanges", JFXDialog.DialogTransition.RIGHT);
            } else {
                //invoke state change to "execute" our listener
                onDone.changed(null, false, true);
            }
        }

    }

    public File getMusicDir() {
        return new File(Objects.requireNonNull(this.properties.getStringProperty("musicDir")));
    }

    public JFXButton makeSearchButton() {
        JFXButton btn = new JFXButton(null, ImageUtil.makeThemeBoundIv("clear", 15));
        btn.setStyle("-fx-background-radius: 20");
        btn.setPrefSize(15, 15);
        btn.setMaxSize(15, 15);
        StyleBinder.bindPaintToThemeColor(ColorType.TEXT_COLOR, btn.ripplerFillProperty());

        btn.setPadding(new Insets(4));
        btn.setOnMouseClicked(evt -> this.tfSearchbar.setText(""));

        return btn;
    }


    public void createDirectoryListener(File dir) {
        this.threadManager.createThread(new DirectoryListener(dir)).start();
    }

    public BorderPane getBpMusicControl() {
        return bpMusicControl;
    }

    public StackPane getSpRoot() {
        return spRoot;
    }

    public void disablePlayingScreen() {
        if (this.spPlayingScreen != null) {
            FadeOutDown fadeOutDown = new FadeOutDown(this.spPlayingScreen);
            fadeOutDown.setSpeed(4.5).setOnFinished(evt -> this.spRoot.getChildren().remove(this.spPlayingScreen));
            fadeOutDown.play();
        }
    }

    public StackPane createPane(String res, Initializable controller) {
        try {
            return FxmlUtil.loadFxml(res, controller).load();
        } catch (IOException e) {
            e.printStackTrace();
            new CrashViewerController("LOAD_FXML",
                    String.format("Error while loading %s with controller %s", res, controller), e).show();
        }

        return null;
    }

    public StackPane createPlayingBar() {
        if (this.spPlayingScreen == null) {
            this.spPlayingScreen = this.createPlayingScreen();
        }

        return this.createPane("main/PlayingBar",
                new PlayingBarController(this.player.getCurrentSong(), event -> {
                    if (!this.spRoot.getChildren().contains(this.spPlayingScreen)) {
                        this.spRoot.getChildren().add(this.spPlayingScreen);
                        new FadeInUp(this.spPlayingScreen).setSpeed(4.5).play();

                        this.playingScreenController.addKeyListener();
                    }
                }));
    }

    public StackPane createPlayingScreen() {
        return this.createPane("main/PlayingScreen",
                playingScreenController = new PlayingScreenController(this.player.getCurrentSong(), event -> this.disablePlayingScreen()));
    }


    private void setContent(Tab tab, Node content) {
        this.tfSearchbar.setText("");

        AnchorPane.setTopAnchor(content, 0d);
        AnchorPane.setRightAnchor(content, 0d);
        AnchorPane.setLeftAnchor(content, 0d);
        AnchorPane.setBottomAnchor(content, 0d);

        AnchorPane anchorPane = (AnchorPane) tab.getContent();
        anchorPane.getChildren().clear();
        anchorPane.getChildren().add(content);
    }

    private LoadableScene sceneFinder(Class<? extends SidebarBindController> clazz) {
        LoadableScene loadableScene = this.getSceneManager().findScene(clazz);
        if (Objects.requireNonNull(loadableScene).getController() instanceof OptionsBindableController) {
            this.currentController = (OptionsBindableController) loadableScene.getController();
            this.riOptions.setOnMouseClicked(this.currentController::handleOptionsClick);
        }

        return loadableScene;
    }

    private void bindSceneToTab(Class<? extends SidebarBindController> clazz, Tab tab) {
        LoadableScene scene = this.sceneFinder(clazz);
        scene.getCurrentScene().addListener((observable, oldValue, newValue) -> {
            this.setContent(tab, scene.getNode());
        });

        scene.load();
        this.setContent(tab, scene.getNode());
    }

    private OptionsBindableController currentController;

    /**
     * -------------------------------------------
     * IMPORTANT: USE !EVENT! AND NOT ActionEvent!
     * -------------------------------------------
     */

    @FXML
    public void tbSongsChanged(Event event) {
        this.bindSceneToTab(SongViewController.class, this.tbSongs);
    }

    @FXML
    public void tbAlbumsChanged(Event event) {
        this.bindSceneToTab(AlbumViewController.class, this.tbAlbums);
    }

    @FXML
    public void tbArtistsChanged(Event event) {
        this.bindSceneToTab(ArtistViewController.class, this.tbArtists);
    }

    @FXML
    public void tbGenresChanged(Event event) {
        this.bindSceneToTab(GenreViewController.class, this.tbGenres);
    }

    @FXML
    public void tbPlaylistsChanged(Event event) {
        this.bindSceneToTab(PlaylistViewController.class, this.tbPlaylists);
    }
}
