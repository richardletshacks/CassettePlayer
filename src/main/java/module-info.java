module musicplayer.main {
    requires javafx.controls;
    requires com.jfoenix;
    requires Logger;
    requires java.sql;
    requires java.desktop;
    requires javafx.fxml;
    requires java.sql.rowset;
    requires ormlitebuild;
    requires AnimateFX;
    requires java.semver;
    requires imageio.jpeg;
    requires colorthief;
    requires org.lz4.java;
    requires dbus.java;
    requires annotations;
    requires mp3agic;
    requires com.google.gson;
    requires javafx.media;
    requires mp3taglib;
    requires javafx.swing;
    requires jkeymaster;
    requires kotlin.stdlib;

    opens fe.materialplayer.controller to javafx.fxml;
    opens fe.materialplayer.controller.base.dialog to javafx.fxml;
    opens fe.materialplayer.controller.view.song to javafx.fxml;
    opens fe.materialplayer.controller.view.artist to javafx.fxml;
    opens fe.materialplayer.controller.view.album to javafx.fxml;
    opens fe.materialplayer.controller.view.playlist to javafx.fxml;
    opens fe.materialplayer.controller.view.playlist.controller to javafx.fxml;
    opens fe.materialplayer.controller.view.popup to javafx.fxml;
    opens fe.materialplayer.controller.view.genre to javafx.fxml;


    opens fe.materialplayer.controller.sidebar.language to javafx.fxml;

    opens fe.materialplayer.controller.content.album to javafx.fxml;
    opens fe.materialplayer.controller.content.artist to javafx.fxml;
    opens fe.materialplayer.controller.content.genre to javafx.fxml;
    opens fe.materialplayer.controller.content.playlist to javafx.fxml;
    opens fe.materialplayer.controller.content to javafx.fxml;

    opens fe.materialplayer.controller.base to javafx.fxml;
    opens fe.materialplayer.controller.playing to javafx.fxml;
    opens fe.materialplayer.controller.sidebar to javafx.fxml;
    opens fe.materialplayer.controller.misc to javafx.fxml;

    exports fe.materialplayer.controller.firstrun to javafx.fxml;
    opens fe.materialplayer.controller.firstrun to javafx.fxml;

    opens fe.materialplayer.controller.libchanges to javafx.fxml;

    opens fe.materialplayer.data.indexable to ormlitebuild;
    exports fe.materialplayer.data.indexable to ormlitebuild;
    exports fe.materialplayer.data.persister to ormlitebuild;

    exports fe.materialplayer.controller.view to javafx.fxml;
    opens fe.materialplayer.controller.view to javafx.fxml;

    exports fe.materialplayer.data.indexable.nm to ormlitebuild;
    opens fe.materialplayer.data.indexable.nm to ormlitebuild;

    exports fe.materialplayer.data.sort;
    exports fe.materialplayer.font;
    exports fe.materialplayer.util;
    exports org.mpris;
    exports fe.materialplayer;
}

