package fe.materialplayer.controller.playing;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXPopup;
import com.jfoenix.controls.JFXSlider;
import fe.materialplayer.controller.base.BaseController;
import fe.materialplayer.controller.css.StyleBinder;
import fe.materialplayer.music.MusicPlayer;
import fe.materialplayer.util.*;
import fe.materialplayer.util.image.ImageUtil;
import fe.materialplayer.util.properties.ColorType;
import javafx.beans.binding.Bindings;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.paint.Color;
import javafx.util.Duration;
import org.jetbrains.annotations.NotNull;

import java.net.URL;
import java.util.Map;
import java.util.ResourceBundle;

public class PlayStatusDisplayer extends BaseController {

    @FXML
    protected JFXSlider slPosition;

    @FXML
    protected Label lbStart;

    @FXML
    protected Label lbEnd;

    @FXML
    protected JFXButton btPrevious;

    @FXML
    protected JFXButton btTogglePlay;

    @FXML
    protected JFXButton btNext;

    @FXML
    protected JFXButton btVolume;

    private static final double CLICK_THRESHOLD = 0.25;
    private static final Map<Boolean, String> PLAY_STATUS = Map.of(
            false, "play2",
            true, "pause"
    );

    private static final Map<Boolean, String> VOLUME_STATUS = Map.of(
            false, "volume_normal",
            true, "volume_muted"
    );

    protected MusicPlayer player = MusicPlayer.getInstance();
    private JFXPopup.PopupVPosition popupVPosition = JFXPopup.PopupVPosition.BOTTOM;
    private int yOffset = -40;

    private Color volumeColor;
    private boolean dragging;

    private static final int ICON_SIZE = 25;

    public PlayStatusDisplayer(JFXPopup.PopupVPosition vPosition, int yOffset) {
        this.popupVPosition = vPosition;
        this.yOffset = yOffset;
    }

    public PlayStatusDisplayer() {
    }

    @Override
    public void initialize(@NotNull URL location, @NotNull ResourceBundle resources) {
        super.initialize(location, resources);

        this.btNext.setGraphic(ImageUtil.makeThemeBoundIv("next", ICON_SIZE));
        this.btPrevious.setGraphic(ImageUtil.makeThemeBoundIv("previous", ICON_SIZE));

        this.btTogglePlay.setGraphic(ImageUtil.makeThemeBoundIv(() -> PLAY_STATUS.get(this.player.playStatusProperty().get()), ICON_SIZE, this.player.playStatusProperty()));
        this.btVolume.setGraphic(ImageUtil.makeThemeBoundIv(() -> {
            //round double val to 2 decimals e.g. 0.00 or
            return VOLUME_STATUS.get((Math.round(this.player.getVolume() * 100) / 100d) == 0);
        }, ICON_SIZE, this.player.volumeProperty()));

        PropertyBinder.bindTextProperty(() ->
                        FormatUtil.formatLength(this.player.currentSongPositionProperty().doubleValue()),
                this.player.currentSongPositionProperty(),
                this.lbStart);

        StyleBinder.bindPaintToThemeColor(ColorType.SECONDARY_TEXT_COLOR, this.lbStart.textFillProperty(), this.lbEnd.textFillProperty());

        this.slPosition.setValueFactory(v -> Bindings.createStringBinding(() -> FormatUtil.formatLength(slPosition.getValue()), slPosition.valueProperty()));
        this.player.currentSongPositionProperty().addListener((observable, oldValue, newValue) -> {
            if (!this.dragging) {
                this.slPosition.setValue(newValue.doubleValue());
            }
        });

        this.player.nowPlayingProperty().addListener((observable, oldValue, song) -> {
            this.slPosition.setMax(song.getLength());
            this.lbEnd.setText(song.getFormattedLength());
        });

        this.slPosition.setValue(0);
        this.slPosition.valueChangingProperty().addListener((observable, oldValue, newValue) -> {
            this.dragging = newValue;
            if (!this.dragging) {
                this.player.skipTo(Duration.seconds(this.slPosition.getValue()));
            }
        });

        this.slPosition.valueProperty().addListener((observable, oldValue, newValue) -> {
            if (Math.abs(newValue.doubleValue() - oldValue.doubleValue()) > CLICK_THRESHOLD && !this.dragging) {
                this.player.skipTo(Duration.seconds(newValue.doubleValue()));
            }
        });
    }

    @FXML
    public void btNextClicked(ActionEvent event) {
        this.player.next();
    }

    @FXML
    public void btPreviousClicked(ActionEvent event) {
        this.player.previous();
    }

    @FXML
    public void btTogglePlayClicked(ActionEvent event) {
        this.player.togglePlaying();
    }

    private VolumeSlider volumeSlider;

    @FXML
    public void btVolumeClicked(ActionEvent event) {
        if (this.volumeSlider == null) {
            this.volumeSlider = new VolumeSlider(btVolume, popupVPosition, yOffset);
        }

        this.volumeSlider.buildOrUpdateAndShow(this.volumeColor);
    }

    public void setVolumeColor(Color volumeColor) {
        this.volumeColor = volumeColor;
    }
}


