package fe.materialplayer.controller.util.cell.impl

import com.jfoenix.controls.JFXButton
import fe.materialplayer.controller.css.StyleBinder
import fe.materialplayer.controller.playing.PlayingScreenController
import fe.materialplayer.data.indexable.Song
import fe.materialplayer.data.indexable.base.BackgroundColorable
import fe.materialplayer.music.MusicLibrary
import fe.materialplayer.music.MusicPlayer
import fe.materialplayer.util.properties.ColorThemeManager
import fe.materialplayer.util.image.ImageUtil
import fe.materialplayer.util.image.icon.ColoredIconCache
import fe.materialplayer.util.properties.Properties
import javafx.event.EventHandler
import javafx.geometry.HPos
import javafx.scene.Node
import javafx.scene.image.ImageView
import javafx.scene.input.MouseEvent
import javafx.scene.layout.ColumnConstraints
import javafx.scene.layout.GridPane
import javafx.scene.layout.HBox
import javafx.scene.layout.Priority
import javafx.scene.paint.Color
import java.time.LocalDateTime

open class UpDownButtonCell(formatType: Song.FormatStyle) : CoverGridCell(formatType) {
    val properties = Properties.getInstance()
    private val songDaoHelper = MusicLibrary.getInstance().songDao

    private fun makeButton(iv: ImageView): JFXButton {
        return JFXButton(null, iv).apply {
            this.setPrefSize(10.0, 10.0)
            this.setMaxSize(10.0, 10.0)
        }
    }

    fun makeButtonBox(
        song: Song,
        upClicked: EventHandler<MouseEvent>,
        downClicked: EventHandler<MouseEvent>,
        removeClicked: EventHandler<MouseEvent>
    ): HBox {
        val color = BackgroundColorable.getTextColor(song)

        val ivUp = ImageUtil.makeIv(UP_ICON.getIcon(color), ICON_SIZE)
        val ivDown = ImageUtil.makeIv(DOWN_ICON.getIcon(color), ICON_SIZE)
        val ivRemove = ImageUtil.makeIv(REMOVE_ICON.getIcon(color), ICON_SIZE)

        val btnUp = this.makeButton(ivUp)
        btnUp.visibleProperty().bind(this.hover)
        btnUp.onMouseClicked = upClicked

        val btnDown = this.makeButton(ivDown)
        btnDown.visibleProperty().bind(this.hover)
        btnDown.onMouseClicked = downClicked

        val btnRemove = this.makeButton(ivRemove)
        btnRemove.visibleProperty().bind(this.hover)
        btnRemove.onMouseClicked = removeClicked

        StyleBinder.bindPaintToObjectColor(
            ColorThemeManager.primaryColor,
            btnUp.ripplerFillProperty(), btnDown.ripplerFillProperty(), btnRemove.ripplerFillProperty()
        )

        return HBox(btnUp, btnDown, btnRemove)
    }

    fun makeBasicGrid(nodeLeft: Node, nodeRight: Node): GridPane {
        val gp = super.makeBasicGrid(nodeLeft, ICON_SIZE.toDouble(), nodeRight)
        val ccButtons = ColumnConstraints(ICON_SIZE * 5.0)

        ccButtons.hgrow = Priority.NEVER
        ccButtons.halignment = HPos.CENTER

        gp.columnConstraints.add(ccButtons)
        return gp
    }

    companion object {
        const val ICON_SIZE = 20

        val DOWN_ICON = ColoredIconCache(mapOf(Color.WHITE to "arrow_down_white", Color.BLACK to "arrow_down_black"))
        val UP_ICON = ColoredIconCache(mapOf(Color.WHITE to "arrow_up_white", Color.BLACK to "arrow_up_black"))
        val REMOVE_ICON = ColoredIconCache(mapOf(Color.WHITE to "remove_dark", Color.BLACK to "remove_light"))

        private val FAVORITE_FILLED =
            ColoredIconCache(mapOf(Color.WHITE to "favorite_filled_white", Color.BLACK to "favorite_filled_black"))
        private val FAVORITE_EMPTY =
            ColoredIconCache(mapOf(Color.WHITE to "favorite_empty_white", Color.BLACK to "favorite_empty_black"))

        fun favoriteHelper(bool: Boolean) = if (bool) FAVORITE_FILLED else FAVORITE_EMPTY
    }
}