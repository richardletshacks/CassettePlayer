package fe.materialplayer.util

abstract class InitHelper<T>(val instance: T) {
    abstract fun init(instance: T)

    fun get(): T {
        init(instance).also {
            return instance
        }
    }
}
