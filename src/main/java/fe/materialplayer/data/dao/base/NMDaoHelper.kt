package fe.materialplayer.data.dao.base

import fe.materialplayer.data.indexable.base.table.NMTable
import java.sql.SQLException

abstract class NMDaoHelper<T : NMTable<out Any, out Any>, X, Y>(clazz: Class<T>, private val colNames: Pair<String, String>,
                                                                orderCol: String, private val firstColValIdProvider: (X) -> Int) : DaoHelper<T>(clazz, orderCol) {
    @Throws(SQLException::class)
    override fun refresh(item: T): T {
        this.removeFromList(item)
        val newItem = this.querySpecific(item)
        this.addToList(item)

        return newItem
    }

    override fun delete(item: T, removeItem: Boolean): Int {
        val deleteBuilder = this.deleteBuilder()
        deleteBuilder.where().eq(colNames.first, item.colValues().first).and().eq(colNames.second, item.colValues().second)
        deleteBuilder.delete()

        if (removeItem) {
            this.removeFromList(item)
        }

        return 1
    }

    @Throws(SQLException::class)
    fun queryWhereColValue(colName: String, value: Any): MutableList<T> {
        return this.dao.queryForFieldValues(mapOf(colName to value))
    }

    @Throws(SQLException::class)
    private fun querySpecific(item: T): T {
        val values = item.colValues()

        val map = mapOf(
                colNames.first to values.first,
                colNames.second to values.second
        )

        val list = this.dao.queryForFieldValues(map)
        if (list.size != 1) {
            error("Invalid list size: ${list.size}")
        }

        return list[0]
    }

    private fun querySpecific(id: Int): T = this.dao.queryForId(id)

    abstract fun findRelationFirst(first: X): List<T>
    abstract fun findRelationSecond(second: Y): List<T>

    @Throws(SQLException::class)
    fun queryRelatedByFirstCol(firstColVal: X): MutableList<T> {
        return this.queryWhereColValue(colNames.first, this.firstColValIdProvider(firstColVal))
    }


    @Throws(SQLException::class)
    fun queryRelatedByFirstCol(firstColValues: List<X>): List<T> {
        return firstColValues.map {
            this.queryRelatedByFirstCol(it)
        }.flatten()
    }
}
