package fe.materialplayer.controller.content.resize

import com.jfoenix.controls.JFXButton
import fe.materialplayer.controller.content.resize.button.CtrlResizeButton
import fe.materialplayer.controller.content.resize.button.DefaultResizeButton
import fe.materialplayer.controller.css.StyleBinder
import fe.materialplayer.controller.util.dialog.DialogUtil
import fe.materialplayer.data.indexable.Song
import fe.materialplayer.music.MusicPlayer
import fe.materialplayer.util.StyleConstants
import fe.materialplayer.util.image.ImageUtil
import javafx.beans.property.ObjectProperty
import javafx.scene.paint.Color
import javafx.scene.text.Text
import java.util.*

object ResizeButtonBinder {
    private val player = MusicPlayer.getInstance()

    private val IMAGE_CACHE = mapOf(
            "shuffle" to ImageUtil.loadIcon("shuffle"),
            "play" to ImageUtil.loadIcon("play_dark"),
            "add_queue" to ImageUtil.loadIcon("add_to_playing_queue_dark"),
            "playlist_add" to ImageUtil.loadIcon("playlist_add_dark")
    )

    fun initButtons(bindTo: List<JFXButton>, items: List<Song>, resourceBundle: ResourceBundle, backgroundColor: ObjectProperty<Color>) {
        bindTo.forEach {
            StyleBinder(it)
                    .bindToColorProperty(StyleConstants.CSS_BACKGROUND_COLOR_PROPERTY, backgroundColor, false).build()
            it.textFill = Color.WHITE
        }

        DefaultResizeButton(bindTo[0], { player.requeueShuffled(items) }, IMAGE_CACHE.getValue("shuffle"), resourceBundle.getString("shuffleAll"), 120)

        val shuffleText = resourceBundle.getString("shuffled")
        val width = Text(shuffleText).layoutBounds.width.toInt()

        CtrlResizeButton(bindTo[1], { evt -> player.addNextSongs(items, evt.isControlDown) },
                IMAGE_CACHE.getValue("play"), resourceBundle.getString("playNext"), shuffleText, width, 110)
        CtrlResizeButton(bindTo[2], { evt -> player.addSongs(items, evt.isControlDown) },
                IMAGE_CACHE.getValue("add_queue"), resourceBundle.getString("addToPlayingQueue"), shuffleText, width, 190)
        CtrlResizeButton(bindTo[3], { evt ->
            DialogUtil.createAddToPlaylistDialog(if (evt.isControlDown) items.shuffled() else items, null)
        }, IMAGE_CACHE.getValue("playlist_add"), resourceBundle.getString("addToPlaylist"), shuffleText, width, 170)
    }

}