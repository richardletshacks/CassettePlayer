package fe.materialplayer.controller.css

import com.jfoenix.controls.JFXSlider
import fe.materialplayer.util.*
import fe.materialplayer.util.properties.ColorThemeManager
import fe.materialplayer.util.properties.ColorType
import fe.materialplayer.util.properties.PlayerTheme
import fe.materialplayer.util.resource.StylesheetUtil
import javafx.beans.Observable
import javafx.beans.binding.Binding
import javafx.beans.binding.Bindings
import javafx.beans.property.ObjectProperty
import javafx.scene.Node
import javafx.scene.Parent
import javafx.scene.paint.Color
import javafx.scene.paint.Paint
import java.util.*
import java.util.concurrent.Callable

class StyleBinder(private vararg val nodes: Node) {

    private val initList = mutableListOf<String>()
    private var stylesheet: String? = null
    private val cache = mutableMapOf<String, String>()

    fun applyStyleSheet(stylesheet: String): StyleBinder {
        this.stylesheet = stylesheet
        return this
    }

    fun setIntCSSProperty(property: String, num: Int): StyleBinder {
        initList += "$property: $num"
        return this
    }

    fun setStringCSSProperty(property: String, s: String): StyleBinder {
        initList += toCSSString(property, s)
        return this
    }

    fun bindToThemeColor(property: String, namedColor: ColorType): StyleBinder {
        bindHelper(property) { it.getColor(namedColor) }
        return this
    }

    private fun bindHelper(property: String, fn: (PlayerTheme) -> Color): StyleBinder {
        this.cacheColor(property, fn(theme.get()))

        theme.addListener { _, _, newVal ->
            cache[property] = fn(newVal).hexString
            nodes.forEach {
                it.style = mapToString(cache)
            }
        }

        return this
    }

    private fun cacheColor(property: String, color: Color) {
        with(color.hexString) {
            initList += "$property: $this"
            cache[property] = this
        }
    }

    fun bindToColorProperty(
        property: String,
        color: ObjectProperty<Color>,
        extractTextColor: Boolean = false
    ): StyleBinder {
        with(color.get()) {
            cacheColor(property, if (extractTextColor) this.textColor else this)
        }

        color.addListener { _, _, newVal ->
            cache[property] = (if (extractTextColor) newVal.textColor else newVal).hexString
            nodes.forEach {
                it.style = mapToString(cache)
            }
        }

        return this
    }

    fun build() {
        nodes.forEach { node ->
            stylesheet?.let { sh ->
                if (node is Parent) {
                    node.applyStylesheet(sh)
                }
            }

            node.style = listToString(initList)
        }
    }


    private fun mapToString(map: MutableMap<String, String>): String {
        return map.map {
            "${it.key}:${it.value}"
        }.joinToString(separator = "; ")
    }

    private fun listToString(list: MutableList<String>): String {
        return list.joinToString(
            separator = "; "
        )
    }

    companion object {
        private val fillProperties = mutableMapOf<ObjectProperty<Paint>, ColorType>()
        val theme = ColorThemeManager.theme

        init {
            theme.addListener { _, _, _ ->
                fillProperties.forEach {
                    it.key.set(theme.value.getColor(it.value))
                }
            }
        }

        @JvmStatic
        fun bindPaintToComplexObjectProperty(binding: Binding<Paint>, vararg properties: ObjectProperty<Paint>) {
            properties.forEach {
                it.bind(binding)
            }
        }

        @JvmStatic
        fun bindPaintToComplexObjectProperty(
            func: Callable<Paint>,
            dependency: Observable,
            vararg properties: ObjectProperty<Paint>
        ) {
            bindPaintToComplexObjectProperty(Bindings.createObjectBinding(func, dependency), *properties)
        }

        @JvmStatic
        fun bindPaintToObjectColor(colorProp: ObjectProperty<Color>, vararg properties: ObjectProperty<Paint>) {
            bindPaintToComplexObjectProperty(Callable {
                return@Callable colorProp.get()
            }, colorProp, *properties)
        }

        @JvmStatic
        fun bindPaintToThemeColor(namedColor: ColorType, vararg properties: ObjectProperty<Paint>) {
            properties.forEach {
                it.set(theme.value.getColor(namedColor))
                fillProperties[it] = namedColor
            }
        }

        private val SLIDER_LOOKUP = arrayOf(".colored-track", ".thumb", ".animated-thumb")

        @JvmStatic
        fun setSliderColor(slider: JFXSlider, color: Color) {
            SLIDER_LOOKUP.forEach { sel ->
                slider.lookup(sel)?.let { node ->
                    node.style = toCSSColorString(StyleConstants.CSS_BACKGROUND_COLOR_PROPERTY, color)
                }
            }
        }

        @JvmStatic
        fun toCSSColorString(property: String, color: Color) = toCSSString(property, color.hexString)

        @JvmStatic
        fun toCSSString(property: String, value: String) = "$property: $value"
    }
}

fun Node.bindStyle() = StyleBinder(this)

fun Parent.applyStylesheet(styleSheet: String) {
    StylesheetUtil.applyStylesheet(this, styleSheet)
}