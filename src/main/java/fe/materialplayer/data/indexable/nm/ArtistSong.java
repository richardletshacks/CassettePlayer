package fe.materialplayer.data.indexable.nm;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import fe.materialplayer.data.indexable.Artist;
import fe.materialplayer.data.indexable.Song;
import fe.materialplayer.data.indexable.base.table.NMTable;
import kotlin.Pair;
import org.jetbrains.annotations.NotNull;

import java.util.Objects;

@DatabaseTable(tableName = "artist_song")
public class ArtistSong implements NMTable<Integer, Integer> {

    public static final String ARTIST_ID_COLUMN_NAME = "artist_id";
    public static final String SONG_ID_COLUMN_NAME = "song_id";

    @DatabaseField(foreign = true, uniqueCombo = true, columnName = ARTIST_ID_COLUMN_NAME)
    private Artist artist;

    @DatabaseField(foreign = true, uniqueCombo = true, columnName = SONG_ID_COLUMN_NAME)
    private Song song;


    public ArtistSong(Artist artist, Song song) {
        this.artist = artist;
        this.song = song;
    }

    public ArtistSong() {
    }

    public int getArtistId() {
        return this.artist.getId();
    }

    public int getSongId() {
        return this.song.getId();
    }

    public Artist getArtist() {
        return artist;
    }

    public void setArtist(Artist artist) {
        this.artist = artist;
    }

    public Song getSong() {
        return song;
    }

    public void setSong(Song song) {
        this.song = song;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof ArtistSong) {
            ArtistSong as = (ArtistSong) o;
            return as.getArtistId() == getArtistId() && as.getSongId() == getSongId();
        }

        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(artist, song);
    }

    @NotNull
    @Override
    public Pair<Integer, Integer> colValues() {
        return new Pair<>(this.getArtistId(), this.getSongId());
    }

    @Override
    public String toString() {
        return "ArtistSong{" +
                "artist=" + artist +
                ", song=" + song +
                '}';
    }
}
