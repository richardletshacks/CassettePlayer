package fe.materialplayer.controller.view.album;

import com.jfoenix.controls.JFXListView;
import fe.materialplayer.controller.base.IndexController;
import fe.materialplayer.controller.base.OptionsBindableController;
import fe.materialplayer.controller.content.album.AlbumContentController;
import fe.materialplayer.controller.css.annotation.theme.ThemeBackgroundColor;
import fe.materialplayer.controller.misc.CrashViewerController;
import fe.materialplayer.controller.util.ItemFetcher;
import fe.materialplayer.controller.util.popup.PopupState;
import fe.materialplayer.controller.util.popup.impl.ArtistablePopup;
import fe.materialplayer.controller.view.PlayOptionBoxController;
import fe.materialplayer.data.indexable.Album;
import fe.materialplayer.music.MusicLibrary;
import fe.materialplayer.util.SnackbarHelper;
import fe.materialplayer.util.resource.FxmlUtil;
import javafx.beans.Observable;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.GridPane;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.concurrent.ExecutionException;
import java.util.function.Consumer;

public class AlbumViewController extends IndexController<Album> implements OptionsBindableController {

    @FXML
    private GridPane gpRoot;

    @FXML
    @ThemeBackgroundColor
    private JFXListView<Album> lvAlbums;

    private final PopupState<Album> popupState = new PopupState<>();
    private ArtistablePopup<Album> popup;


    public AlbumViewController() {
        super(Album.DEFAULT_SORT_TYPE, MusicLibrary.Companion.getInstance().getAlbumDao());
        this.itemFetcher = new ItemFetcher<>(this.daoHelper.getItems(), this.sortMode.getSortComparator(), false);
    }

    @Override
    public void initialize(@NotNull URL location, @NotNull ResourceBundle resources) {
        super.initialize(location, resources);
        this.initListViewPadding(this.lvAlbums);

        this.popup = new ArtistablePopup<>(this.popupState, resources);

        FxmlUtil.loadNonBlockingFxml("view/PlayOptionBox", new PlayOptionBoxController<Album>() {
            @NotNull
            @Override
            public List<Album> provideSongs() {
                return getSearchFilteredItemsOrAll();
            }
        }, node -> gpRoot.add((Node) node, 0, 0));

        this.itemFetcher.setListView(this.lvAlbums);
        this.itemFetcher.fetchItems(FETCH_SIZE, ItemFetcher.FetchType.INITIAL);

        this.lvAlbums.setCellFactory(arg0 -> new AlbumListCell(resources));
    }

    @FXML
    public void lvAlbumsClicked(MouseEvent event) {
        Album a = this.lvAlbums.getSelectionModel().getSelectedItem();
        if (a != null) {
            if (event.getButton() == MouseButton.PRIMARY && event.getClickCount() == 2) {
                AlbumContentController.show(a, null);
            } else if (event.getButton() == MouseButton.SECONDARY) {
                this.popupState.setCurrentState(a);
                this.popup.buildAndShow(this.lvAlbums, event, null);
            }
        }
    }

    @FXML
    public void lvAlbumsScrolled(ScrollEvent event) {
        this.itemFetcher.fetchItems(FETCH_SIZE, ItemFetcher.FetchType.SCROLL);
    }

    @Override
    public void handleOptionsClick(MouseEvent event) {
        try {
            this.showPopup(Album.SORT_TYPES, this.sortMode, (Node) event.getSource());
        } catch (IOException e) {
            e.printStackTrace();
            new CrashViewerController("SHOW_OPTIONS_POPUP",
                    String.format("Error while showing options popup for sort mode %s", this.sortMode), e).show();
        }
    }

    @Override
    public void handleSearchTextChanged(Observable observable, String oldValue, String newValue) {
        this.execSearch(newValue, this.lvAlbums);
    }
}


