package fe.materialplayer.data.indexable;

import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;
import fe.materialplayer.controller.util.cell.FormattablePair;
import fe.materialplayer.data.dao.helper.GenreDaoHelper;
import fe.materialplayer.data.indexable.base.IFormattedLines;
import fe.materialplayer.data.indexable.base.Indexable;
import fe.materialplayer.data.sort.SortType;
import fe.materialplayer.util.FormatUtil;
import fe.materialplayer.util.resource.ResourceBundleHelper;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

@DatabaseTable(tableName = "genre")
public class Genre extends Indexable implements IFormattedLines {

    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField
    private String name;

    @ForeignCollectionField(foreignFieldName = "genre")
    private ForeignCollection<Song> songs;

    private final DoubleProperty genreLength = new SimpleDoubleProperty();

    public Genre(String name) {
        this.name = name;
    }

    public Genre() {
    }

    public static final SortType<Genre> DEFAULT_SORT_TYPE = new SortType<>("Name", Comparator.comparing(Genre::getName, String.CASE_INSENSITIVE_ORDER));
    public static final List<SortType<Genre>> SORT_TYPES = List.of(DEFAULT_SORT_TYPE);


    public DoubleProperty genreLengthProperty() {
        if (this.genreLength.get() == 0) {
            this.genreLength.set(FormatUtil.sumLength(this.songs));
        }

        return genreLength;
    }

    public int getId() {
        return id;
    }

    public String getNonTranslatedName(){
        return this.name;
    }

    public String getName() {
        if (this == GenreDaoHelper.getUNKNOWN_GENRE() || this.name.equals(GenreDaoHelper.getUNKNOWN_GENRE().getNonTranslatedName())) {
            return ResourceBundleHelper.getBundle().getString("unknownGenre");
        }

        return name;
    }

    public ForeignCollection<Song> getSongs() {
        return songs;
    }

    @Override
    protected String[] compareArray() {
        return new String[]{this.name};
    }

    @Override
    public Collection<Song> toSongList() {
        return this.songs;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof Genre) {
            return ((Genre) o).name.equalsIgnoreCase(this.name);
        }

        return false;
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public FormattablePair getBoxLines() {
        return new FormattablePair(this.getName(), "%s");
    }
}
