package fe.materialplayer.music;

import com.tulskiy.keymaster.common.MediaKey;
import fe.logger.Logger;
import fe.materialplayer.Main;
import fe.materialplayer.data.indexable.Song;
import fe.materialplayer.music.mpris.MprisPlayer;
import fe.materialplayer.util.FormatUtil;
import fe.materialplayer.util.os.OSHelper;
import fe.materialplayer.util.StyleConstants;
import fe.materialplayer.util.image.ImageUtil;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.DoubleBinding;
import javafx.beans.binding.NumberExpressionBase;
import javafx.beans.property.*;
import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.scene.image.ImageView;
import javafx.scene.media.Media;
import javafx.scene.media.MediaException;
import javafx.scene.media.MediaPlayer;
import javafx.util.Duration;

import java.applet.Applet;
import java.util.*;

import com.tulskiy.keymaster.common.Provider;
import org.jetbrains.annotations.NotNull;

import javax.sound.sampled.AudioSystem;

public class MusicPlayer {

    private final Logger logger = new Logger("MusicPlayer", true);

    private static MediaPlayer mediaPlayer;

    private static MusicPlayer instance;
    private MprisPlayer mprisPlayer;
    private Provider hotKeyProvider;

    //we just assume that every linux distro has mpris support
    public static final boolean MPRIS_SUPPORT = Main.getInstance().getCurrentOS() == OSHelper.OS.LINUX;

    private final ObservableList<Song> queue = FXCollections.observableArrayList();
    private final DoubleProperty queueLength = new SimpleDoubleProperty();
    private final DoubleProperty volume = new SimpleDoubleProperty(0.5d);
    private final ObjectProperty<Song> nowPlaying = new SimpleObjectProperty<>();
    private final DoubleProperty currentSongPosition = new SimpleDoubleProperty();
    private final IntegerProperty queueSize = new SimpleIntegerProperty();
    private final BooleanProperty queueChanged = new SimpleBooleanProperty();

    private final BooleanProperty playStatus = new SimpleBooleanProperty();
    private final IntegerProperty currentPointer = new SimpleIntegerProperty();

    private final BooleanProperty playerStopped = new SimpleBooleanProperty();

    private final BooleanProperty shuffleLoop = new SimpleBooleanProperty();
    private final List<Song> shuffleLoopRestore = new ArrayList<>();

    private final BooleanProperty shuffle = new SimpleBooleanProperty();
    private final List<Song> shuffleRestore = new ArrayList<>();
    private int shufflePointer;

    private final ObjectProperty<LoopMode> loopMode = new SimpleObjectProperty<>(LoopMode.NO_LOOP);

    private MusicPlayer() {
        //TODO: maybe optimize?
        ChangeListener<? super Number> lengthListener = (observable, oldValue, newValue) -> {
            if (this.currentPointer.get() < this.queue.size()) {
                this.queueLength.set(FormatUtil.sumLength(this.queue.subList(this.currentPointer.get(), this.queue.size())));
            }
        };

        this.queueSize.addListener(lengthListener);
        this.currentPointer.addListener(lengthListener);
        this.playerStopped.set(true);
    }

    interface RunLaterHotKeyListener {
        void onHotKey();
    }

    private static final Map<MediaKey, RunLaterHotKeyListener> MEDIA_HOTKEY_LISTENERS = Map.of(
            MediaKey.MEDIA_NEXT_TRACK, () -> instance.next(),
            MediaKey.MEDIA_PREV_TRACK, () -> instance.previous(),
            MediaKey.MEDIA_PLAY_PAUSE, () -> instance.togglePlaying(),
            MediaKey.MEDIA_STOP, () -> instance.stopPlayer());

    public static synchronized MusicPlayer getInstance() {
        if (instance == null) {
            instance = new MusicPlayer();
            if (MusicPlayer.MPRIS_SUPPORT) {
                instance.mprisPlayer = new MprisPlayer(instance);
            } else {
                instance.hotKeyProvider = Provider.getCurrentProvider(false);
                for (Map.Entry<MediaKey, RunLaterHotKeyListener> ent : MEDIA_HOTKEY_LISTENERS.entrySet()) {
                    instance.hotKeyProvider.register(ent.getKey(), hk -> Platform.runLater(() -> ent.getValue().onHotKey()));
                }
            }
        }

        return instance;
    }

    public enum LoopMode {
        NO_LOOP(ImageUtil.makeThemeBoundIv("repeat", 25), StyleConstants.TRANSPARENT_BACKGROUND),
        LOOP_ALL(ImageUtil.makeThemeBoundIv("repeat", 25), StyleConstants.LIGHT_GRAY_BACKGROUND),
        LOOP_CURRENT(ImageUtil.makeThemeBoundIv("repeat_one", 25), StyleConstants.LIGHT_GRAY_BACKGROUND);

        private final ImageView iv;
        private final String backgroundColor;

        LoopMode(ImageView iv, String backgroundColor) {
            this.iv = iv;
            this.backgroundColor = backgroundColor;
        }

        public String getBackgroundColor() {
            return backgroundColor;
        }

        public ImageView getImageView() {
            return iv;
        }

        public LoopMode next() {
            return LoopMode.values()[(this.ordinal() + 1) % LoopMode.values().length];
        }

    }

    public LoopMode switchMode(LoopMode loopMode) {
        this.loopMode.set(loopMode);
        return loopMode;
    }

    public void skipTo(Duration duration) {
        if (MusicPlayer.mediaPlayer != null && !duration.greaterThan(MusicPlayer.mediaPlayer.getMedia().getDuration())) {
            MusicPlayer.mediaPlayer.seek(duration);
        }
    }

    private final ChangeListener<Duration> currentTimeListener = (v, oldTime, newTime) -> this.currentSongPosition.set(newTime.toSeconds());

    public void playCurrent() {
        if (this.currentPointer.get() < this.queue.size()) {
            if (this.playerStopped.get()) {
                this.playerStopped.set(false);
            }

            if (mediaPlayer == null) {
                Song s = this.queue.get(this.currentPointer.get());

                try {

                    mediaPlayer = new MediaPlayer(s.toMedia());
                    this.currentSongPosition.bind(Bindings.createDoubleBinding(
                            () -> mediaPlayer != null ? mediaPlayer.currentTimeProperty().get().toSeconds() : 0,
                            mediaPlayer.currentTimeProperty()
                    ));

                    mediaPlayer.setVolume(this.volume.get());
                    this.nowPlaying.set(s);

                    mediaPlayer.play();

                    this.playStatus.set(true);
                    this.logger.print(Logger.Type.INFO, "Playing next song (%s)", s);

                    mediaPlayer.setOnEndOfMedia(() -> {
                        this.logger.print(Logger.Type.INFO, "End of song reached, playing next in queue");

                        if (this.loopMode.get() != LoopMode.LOOP_CURRENT) {
                            this.currentPointer.set(this.currentPointer.get() + 1);
                        }

                        if (this.loopMode.get() == LoopMode.NO_LOOP && this.shuffleLoop.get() && this.hasReachedQueueEnd()) {
                            this.logger.print(Logger.Type.INFO, "Shuffle loop enabled, end of queue reached, shuffling..");
                            Collections.shuffle(this.queue);
                            this.currentPointer.set(0);
                        }

                        if (this.loopMode.get() == LoopMode.LOOP_ALL && this.hasReachedQueueEnd()) {
                            this.currentPointer.set(0);
                        }

                        this.currentSongPosition.unbind();

                        this.stopPlayer();
                        this.playCurrent();
                    });
                } catch (MediaException e) {
                    e.printStackTrace();
                }
            }
        } else {
            this.stopPlayer();
            this.logger.print(Logger.Type.INFO, "End of queue reached");
        }
    }

    private boolean hasReachedQueueEnd() {
        return this.currentPointer.get() == this.queue.size();
    }

    public void toggleShuffleLoop() {
        this.setShuffleLoop(!this.shuffleLoop.get());
    }

    public void setShuffleLoop(boolean shuffleLoop) {
        boolean wasShuffleLoop = this.shuffleLoop.get();
        this.shuffleLoop.set(shuffleLoop);
        if (this.shuffleLoop.get() && !wasShuffleLoop) {
            this.shuffleLoopRestore.clear();
            this.shuffleLoopRestore.addAll(this.queue);
        } else if (!this.shuffleLoop.get() && wasShuffleLoop) {
            this.queue.clear();
            this.queue.addAll(this.shuffleLoopRestore);
        }
    }

    public void toggleShuffle() {
        this.setShuffle(!this.shuffle.get());
    }

    public void setShuffle(boolean shuffle) {
        boolean wasShuffle = this.shuffle.get();
        this.shuffle.set(shuffle);
        if (this.shuffle.get() && !wasShuffle) {
            this.shuffleRestore.clear();
            this.shuffleRestore.addAll(this.queue);

            List<Song> toPlay = new ArrayList<>(this.queue);
            toPlay.remove(this.currentPointer.get());

            this.shufflePointer = this.currentPointer.get();

            if (this.currentPointer.get() + 1 < this.queue.size()) {
                this.queue.remove(this.currentPointer.get() + 1, this.queue.size());
            }

            this.queue.remove(0, this.currentPointer.get());

            Collections.shuffle(toPlay);
            this.queue.addAll(toPlay);
            this.currentPointer.set(0);
        } else if (!this.shuffle.get() && wasShuffle) {
            this.queue.clear();
            this.queue.addAll(this.shuffleRestore);
            this.nowPlaying.set(this.nowPlaying.get());

            this.currentPointer.set(this.shufflePointer + this.currentPointer.get());
        }

        this.queueChanged.set(true);
    }

    public void togglePlaying() {
        if (mediaPlayer != null) {
            if (mediaPlayer.getStatus() == MediaPlayer.Status.PLAYING) {
                mediaPlayer.pause();
                this.playStatus.set(false);
            } else if (mediaPlayer.getStatus() == MediaPlayer.Status.PAUSED) {
                mediaPlayer.play();
                this.playStatus.set(true);
            }
        } else {
            if (this.currentPointer.get() == this.queue.size()) {
                this.currentPointer.set(this.currentPointer.get() - 1);
            }

            this.playCurrent();
        }
    }

    private int stopCounter = 0;
    private static final int INVOKE_GC_AFTER_X_STOPS = 15;

    public void stopPlayer() {
        if (mediaPlayer != null) {
            this.playStatus.set(false);

            mediaPlayer.currentTimeProperty().removeListener(this.currentTimeListener);
            mediaPlayer.setOnEndOfMedia(null);

            mediaPlayer.stop();
            mediaPlayer.dispose();
            mediaPlayer = null;

            this.stopCounter++;
            if (this.stopCounter % INVOKE_GC_AFTER_X_STOPS == 0) {
                this.logger.print(Logger.Type.INFO, "Explicitly calling gc to give back memory to system..");
                System.gc();
            }
        }
    }

    public void previous() {
        boolean canPrev = this.currentPointer.get() - 1 >= 0;

        this.currentPointer.set(canPrev ? this.currentPointer.get() - 1 : this.queue.size() - 1);
        this.stopPlayer();
        this.playCurrent();
    }

    public void next() {
        boolean canNext = this.currentPointer.get() + 1 < this.queue.size();

        this.currentPointer.set(canNext ? this.currentPointer.get() + 1 : 0);
        if (this.currentPointer.get() == 0 && this.loopMode.get() == LoopMode.NO_LOOP && this.shuffleLoop.get()) {
            Collections.shuffle(this.queue);
        }

        this.stopPlayer();
        this.playCurrent();
    }

    public void requeue(List<Song> items, int index) {
        this.clearQueue();
        this.addSongs(items, false);
        this.jumpToIndex(index);
        this.playCurrent();
    }


    public void requeueShuffled(List<Song> items) {
        this.clearQueue();
        this.addSongs(items, true);
        this.playCurrent();
    }

    public void jumpToIndex(int index) {
        if (index < this.queue.size()) {
            this.currentPointer.set(index);
            this.stopPlayer();
            this.playCurrent();
        }
    }

    public void clearQueue() {
        if (this.queue.size() > 0) {
            this.playerStopped.set(true);
        }

        this.queue.clear();
        this.queueSize.set(0);
        this.currentPointer.set(0);

        this.shuffleRestore.clear();
        this.shuffle.set(false);
        this.shufflePointer = 0;

        this.stopPlayer();
    }

    public boolean moveItem(int index, int modifier) {
        boolean includedCurrent = false;
        if (index == this.currentPointer.get()) {
            return false;
        }

        int toIndex = index + modifier;
        if (toIndex >= 0 && toIndex < this.queue.size()) {
            if (toIndex == this.currentPointer.get()) {
                this.currentPointer.set(this.currentPointer.get() - modifier);
                includedCurrent = true;
            }

            Song s = this.queue.remove(index);
            this.queue.add(toIndex, s);

            this.queueChanged.set(true);
        }

        return includedCurrent;
    }

    private List<Song> shuffleList(List<Song> songs) {
        List<Song> shuffleSongs = new ArrayList<>(songs);

        Collections.shuffle(shuffleSongs);
        return shuffleSongs;
    }

    public void addSongs(List<Song> songs, boolean shuffled) {
        this.addSongs(shuffled ? this.shuffleList(songs) : songs, this.queue.size());
    }

    public void addNextSongs(List<Song> songs, boolean shuffled) {
        if (this.queue.isEmpty()) {
            this.currentPointer.set(0);
        }

        this.addSongs(shuffled ? this.shuffleList(songs) : songs, this.queue.isEmpty() ? 0 : this.currentPointer.get() + 1);
    }

    public void addSongs(List<Song> songs, int firstPosition) {
        for (int i = 0; i < songs.size(); i++) {
            this.addSong(songs.get(i), firstPosition + i);
        }
    }

    public void addSong(Song song, int position) {
        if (this.queue.isEmpty() && position > 0) {
            position = 0;
        }

        this.queue.add(position, song);
        this.queueSize.set(this.queue.size());
    }

    public void removeFromQueue(int selectedIndex) {
        if (selectedIndex >= 0 && selectedIndex < this.queue.size()) {
            this.queue.remove(selectedIndex);
            this.queueSize.set(this.queue.size());
        }
    }

    public Song getCurrentSong() {
        if (this.queue.isEmpty()) {
            return null;
        }

        return this.queue.get(this.currentPointer.get());
    }

    public int getCurrentIndex() {
        return this.currentPointer.get();
    }

    public void setVolume(double volume) {
        if (MusicPlayer.mediaPlayer != null) {
            MusicPlayer.mediaPlayer.setVolume(volume);
        }

        this.volume.set(volume);
        //force update since properties only update when the val is accessed .-.
        this.volume.get();
    }

    public double getVolume() {
        if (MusicPlayer.mediaPlayer != null) {
            return MusicPlayer.mediaPlayer.getVolume();
        }

        return 0.7d;
    }

    public void removeAllOccurrences(@NotNull List<Song> songs) {
        if (songs.contains(this.getCurrentSong())) {
            Platform.runLater(this::next);
        }

        for (Song song : songs) {
            Platform.runLater(() -> {
                this.queue.removeIf(s -> s == song);
            });
        }
    }

    public NumberExpressionBase queueLengthWithoutFirstProperty() {
        if (!this.queue.isEmpty()) {
            return this.queueLength.subtract(this.queue.get(0).getLength());
        }

        return this.queueLength;
    }

    public IntegerProperty currentPointerProperty() {
        return currentPointer;
    }

    public DoubleProperty volumeProperty() {
        return volume;
    }

    public BooleanProperty playerStoppedProperty() {
        return playerStopped;
    }

    public BooleanProperty queueChangedProperty() {
        return queueChanged;
    }

    public DoubleProperty queueLengthProperty() {
        return queueLength;
    }

    public BooleanProperty playStatusProperty() {
        return playStatus;
    }

    public ObjectProperty<LoopMode> loopModeObjectProperty() {
        return loopMode;
    }

    public IntegerProperty queueSizeProperty() {
        return queueSize;
    }

    public DoubleProperty currentSongPositionProperty() {
        return currentSongPosition;
    }

    public ObjectProperty<Song> nowPlayingProperty() {
        return nowPlaying;
    }

    public BooleanProperty shuffleLoopProperty() {
        return shuffleLoop;
    }

    public BooleanProperty shuffleProperty() {
        return shuffle;
    }

    public MprisPlayer getMprisPlayer() {
        return mprisPlayer;
    }

    public Provider getHotKeyProvider() {
        return hotKeyProvider;
    }

    public ObservableList<Song> getQueue() {
        return queue;
    }
}
