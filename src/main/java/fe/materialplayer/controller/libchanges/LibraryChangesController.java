package fe.materialplayer.controller.libchanges;

import com.jfoenix.controls.JFXProgressBar;
import fe.materialplayer.controller.base.dialog.UnclosableDialogController;
import fe.materialplayer.controller.css.StyleBinder;
import fe.materialplayer.controller.css.annotation.color.PrimaryColored;
import fe.materialplayer.controller.util.ItemFetcher;
import fe.materialplayer.util.StyleConstants;
import fe.materialplayer.util.properties.ColorType;
import javafx.beans.binding.Bindings;
import javafx.beans.value.ChangeListener;
import javafx.concurrent.WorkerStateEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

public class LibraryChangesController extends UnclosableDialogController {

    @FXML
    private StackPane spRoot;

    @FXML
    private Label lbPercentage;

    @FXML
    @PrimaryColored
    private JFXProgressBar pbScan;

    @FXML
    private Label lbProgress;

    private final List<File> files;

    public LibraryChangesController(List<File> files, ChangeListener<Boolean> doneListener) {
        this.files = files;
        this.getDone().addListener(doneListener);
    }

    @Override
    public void initialize(@NotNull URL location, @NotNull ResourceBundle resources) {
        super.initialize(location, resources);

        new StyleBinder(this.spRoot)
                .bindToThemeColor(StyleConstants.CSS_BACKGROUND_COLOR_PROPERTY, ColorType.BACKGROUND_COLOR)
                .setIntCSSProperty(StyleConstants.CSS_BACKGROUND_RADIUS, 7)
                .setIntCSSProperty(StyleConstants.CSS_BORDER_RADIUS, 7)
                .build();

        LibraryChangesIndexTask changesIndexTask = new LibraryChangesIndexTask(resources, this.files);


        this.lbPercentage.textProperty().bind(Bindings.createStringBinding(() ->
                        String.format("%d%%", (int) (changesIndexTask.getProgress() * 100d)),
                changesIndexTask.progressProperty()));

        this.lbProgress.textProperty().bind(changesIndexTask.messageProperty());
        this.pbScan.progressProperty().bind(changesIndexTask.progressProperty());

        changesIndexTask.addEventFilter(WorkerStateEvent.WORKER_STATE_SUCCEEDED, evt -> {
            this.lbProgress.textProperty().unbind();
            this.getDone().set(true);
            ItemFetcher.Companion.refetchAll();

            this.closeDialog();
        });

        new Thread(changesIndexTask).start();
    }
}
