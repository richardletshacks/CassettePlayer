package org.mpris;

import org.freedesktop.dbus.annotations.DBusInterfaceName;
import org.freedesktop.dbus.exceptions.DBusException;
import org.freedesktop.dbus.interfaces.DBusInterface;
import org.freedesktop.dbus.messages.DBusSignal;

@DBusInterfaceName("org.mpris.MediaPlayer2.Player")
public interface Player extends DBusInterface {

    class Seeked extends DBusSignal {
        private final long position;

        public Seeked(String path, long position) throws DBusException {
            super(path, position);
            this.position = position;

            System.out.println("seeked2 to " + position);
        }
    }

    void Next();

    void Previous();

    void Pause();

    void PlayPause();

    void Play();

    void Stop();

    void Seek(long Offset);

    void OpenUri(String uri);

    void SetPosition(int TrackId, long Position);
}
