package fe.materialplayer.util.wallpaper

import fe.materialplayer.util.os.OSHelper
import fe.materialplayer.util.Util
import fe.materialplayer.util.os.LinuxDesktop
import java.io.File

abstract class WallpaperProvider(os: OSHelper.OS) {
    abstract fun change(file: File)

    abstract fun getCurrent(): File
}

abstract class LinuxWallpaperProvider(val linuxDesktop: LinuxDesktop) : WallpaperProvider(OSHelper.OS.LINUX)

object WindowsWallpaperProvider : WallpaperProvider(OSHelper.OS.WINDOWS) {
    override fun change(file: File) {
//        TODO("Not yet implemented")
    }

    override fun getCurrent(): File {
        return File(".")
//        TODO("Not yet implemented")
    }
}

object CinnamonWallpaperProvider : LinuxWallpaperProvider(LinuxDesktop.CINNAMON) {
    private const val WALLPAPER_CHANGE_CMD = "gsettings set org.cinnamon.desktop.background picture-uri  \"%s\""
    private const val GET_WALLPAPER = "gsettings get org.cinnamon.desktop.background picture-uri"

    private const val FILE_PREFIX = "'file://"

    override fun change(file: File) {
        Runtime.getRuntime().exec(String.format(WALLPAPER_CHANGE_CMD, Util.fileToUriString(file)))
    }

    override fun getCurrent() = File(Runtime.getRuntime().exec(GET_WALLPAPER).inputStream.bufferedReader().use { buff ->
        buff.readLine().let { line ->
            line.substring(FILE_PREFIX.length, line.length - 1)
        }
    })
}