package fe.materialplayer.controller.content.playlist

import fe.materialplayer.controller.misc.CrashViewerController
import fe.materialplayer.controller.util.ItemFetcher
import fe.materialplayer.controller.util.cell.impl.UpDownButtonCell
import fe.materialplayer.data.indexable.Playlist
import fe.materialplayer.data.indexable.Song
import javafx.scene.control.ContentDisplay
import javafx.scene.control.Label
import javafx.scene.image.ImageView
import java.sql.SQLException
import java.util.*


class PlaylistContentListCell(private val playlist: Playlist, private val itemFetcher: ItemFetcher<Song>) :
    UpDownButtonCell(Song.FormatStyle.TITLE_ARTIST_ALBUM) {

    override fun updateItem(item: Song?, empty: Boolean) {
        super.updateItem(item, empty)
        if (empty || item == null) {
            text = null
            graphic = null
        } else {
            val label: Label = this.makeLabel(item)
            val iv: ImageView = this.makeAlbumCover(item, 35, Song.CoverType.S60X60)
            val buttonBox = makeButtonBox(item,
                {
                    try {
                        this.doSwap(-1)
                    } catch (e: SQLException) {
                        e.printStackTrace()
                        CrashViewerController(
                            "MOVE_INDEX_DOWN_IN_PLAYLIST",
                            String.format("Error while moving index %d down in playlist %s", index, this.playlist), e
                        ).show()
                    }
                },
                {
                    try {
                        this.doSwap(1)
                    } catch (e: SQLException) {
                        e.printStackTrace()
                        CrashViewerController(
                            "MOVE_INDEX_UP_IN_PLAYLIST",
                            String.format("Error while moving index %d up in playlist %s", index, this.playlist), e
                        ).show()
                    }
                },
                {
                    this.playlist.removeIndex(index)
                    this.itemFetcher.refetch()
                }
            )
            this.makeListeners(item)

            val gp = this.makeBasicGrid(iv, label)
            gp.add(buttonBox, 2, 0)

            contentDisplay = ContentDisplay.GRAPHIC_ONLY
            graphic = gp
        }
    }

    private fun doSwap(modifier: Int) {
        val swap = this.playlist.moveItem(index, modifier)
        if (swap != -1) {
            Collections.swap(this.itemFetcher.baseList, index, swap)
        }
    }
}