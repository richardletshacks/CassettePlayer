package fe.materialplayer.util.image.icon

import fe.materialplayer.util.image.ImageUtil
import javafx.scene.paint.Color

class ColoredIconCache(iconNames: Map<Color, String>) {
    private val icons = iconNames.mapValues {
        ImageUtil.loadIcon(it.value)
    }

    fun getIcon(color: Color) = icons[color]
}