package fe.materialplayer.controller.libchanges;

import fe.materialplayer.controller.util.index.IndexTask;
import fe.materialplayer.data.dao.helper.*;
import fe.materialplayer.data.indexable.Album;
import fe.materialplayer.data.indexable.Song;
import fe.materialplayer.music.MusicLibrary;

import java.io.File;
import java.sql.SQLException;
import java.util.*;
import java.util.stream.Collectors;

public class LibraryChangesIndexTask extends IndexTask<List<Song>> {
    private final List<File> files;
    private final MusicLibrary library = MusicLibrary.Companion.getInstance();
    private final ArtistSongDaoHelper artistSongDao = Objects.requireNonNull(this.library).getArtistSongDao();
    private final ArtistAlbumDaoHelper artistAlbumDao = Objects.requireNonNull(this.library).getArtistAlbumDao();

    private final SongDaoHelper songDao = Objects.requireNonNull(this.library).getSongDao();

    public LibraryChangesIndexTask(ResourceBundle resourceBundle, List<File> files) {
        super(resourceBundle);
        this.files = files;
    }

    @Override
    protected List<Song> call() throws Exception {
        List<Song> songs = this.parseSongs(this.files, true);

        List<Song> newSongs = new ArrayList<>();
        for (Song s : songs) {
            newSongs.add(this.songDao.createAndRefresh(s));
        }

        List<Album> albums = newSongs.stream().map(Song::getAlbum).distinct().collect(Collectors.toCollection(LinkedList::new));
        try {
            this.artistSongDao.batchCreateAndSetup(newSongs);
            this.artistAlbumDao.batchCreateAndSetup(albums, newSongs);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return songs;
    }
}
