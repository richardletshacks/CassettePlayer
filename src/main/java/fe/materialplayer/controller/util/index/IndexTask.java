package fe.materialplayer.controller.util.index;

import fe.logger.Logger;
import fe.materialplayer.data.indexable.Song;
import fe.materialplayer.music.MusicLibrary;
import fe.materialplayer.util.Util;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.concurrent.Task;
import kotlin.Pair;

import java.io.File;
import java.sql.SQLException;
import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.concurrent.*;

public abstract class IndexTask<T> extends Task<T> {
    private final double searchWeight;
    private final double indexingWeight;
    private final ResourceBundle resourceBundle;

    private Duration estimatedTime;
    private final List<Long> elapsed = new LinkedList<>();
    private final MusicLibrary library = Objects.requireNonNull(MusicLibrary.Companion.getInstance());

    protected final Logger logger = new Logger("IndexHelper", true);

    private static final int PARSE_THREADS = 4;

    private double maxProgress;
    private double currentProgress;

    public IndexTask(ResourceBundle resourceBundle, double searchWeight, double indexingWeight) {
        this.resourceBundle = resourceBundle;
        this.searchWeight = searchWeight;
        this.indexingWeight = indexingWeight;
    }

    public IndexTask(ResourceBundle resourceBundle) {
        this(resourceBundle, 0.25, 1);
    }

    public List<Song> parseSongs(List<File> foundSongs, boolean insert) throws Exception {
        if (foundSongs.isEmpty()) {
            this.estimatedTime = Duration.ZERO;
            this.updateProgress(1, 1);
            return List.of();
        }

        this.updateMessage("Found " + foundSongs.size() + " songs");
        Thread.sleep(500);

        this.updateProgress(this.currentProgress,  this.maxProgress += foundSongs.size() * this.indexingWeight);

        ExecutorService executorService = Executors.newFixedThreadPool(PARSE_THREADS);
        CompletionService<Pair<Song, Long>> completionService = new ExecutorCompletionService<>(executorService);

        long start = System.currentTimeMillis();

        for (File foundSong : foundSongs) {
            completionService.submit(new ParseTask(logger, foundSong, insert));
        }

        executorService.shutdown();

        List<Song> songs = new LinkedList<>();
        int errors = 0;
        while ((songs.size() + errors) < foundSongs.size()) {
            Future<Pair<Song, Long>> resultFuture = completionService.take();
            try {
                Pair<Song, Long> pair = resultFuture.get();

                Song song = pair.getFirst();

                this.updateProgress(this.currentProgress += 1 * this.indexingWeight, this.maxProgress);
                this.updateMessage(this.estimatedTime == null ?
                        "Parsing " + song :
                        String.format(this.resourceBundle.getString("parsing"), song, this.estimatedTime.toString().substring(2)));

                this.elapsed.add(pair.getSecond());

                OptionalDouble avg = this.elapsed.stream().mapToLong(v -> v).average();

                int itemsLeft = foundSongs.size() - 1 - songs.size();
                this.estimatedTime = Duration.of(((long) (avg.orElse(0) * itemsLeft)) / PARSE_THREADS, ChronoUnit.MILLIS);

                songs.add(song);
            } catch (Exception e) {
                //if this exception is thrown, something is probably very fucking wrong
                e.printStackTrace();
                this.logger.print(Logger.Type.ERROR, e);
                this.updateMessage("An error occurred: " + e.getMessage());

                errors++;
            }
        }

        this.logger.print(Logger.Type.INFO, "Scanned all songs, elapsed time: %s (errors: %d)", Duration.of(System.currentTimeMillis() - start, ChronoUnit.MILLIS), errors);

        return songs;
    }

    public void storeSongs(List<Song> songs) {
        this.updateMessage("Storing parsed data..");
        try {
            this.library.setupNMFirstRun(songs);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public List<File> scanSubDir(File dir, boolean recursive) {
        List<File> songFiles = new ArrayList<>();

        File[] children = dir.listFiles();
        if (children != null) {
            this.maxProgress += (children.length * this.searchWeight);
            this.updateProgress(this.currentProgress, this.maxProgress);

            this.updateMessage("Searching for files in " + dir);
            for (File file : children) {
                this.currentProgress += (1 * this.searchWeight);
                this.updateProgress(this.currentProgress, this.maxProgress);

                if (file.isDirectory() && recursive) {
                    songFiles.addAll(this.scanSubDir(file, true));
                } else if (Util.isMp3File(file)) {
                    songFiles.add(file);
                    this.updateMessage("Found file " + file);
                }
            }
        }

        return songFiles;
    }
}
