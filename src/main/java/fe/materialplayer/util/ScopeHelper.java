package fe.materialplayer.util;

import java.util.function.Consumer;

public class ScopeHelper {
    public static <T> void with(T value, Consumer<T> consumer) {
        consumer.accept(value);
    }
}
