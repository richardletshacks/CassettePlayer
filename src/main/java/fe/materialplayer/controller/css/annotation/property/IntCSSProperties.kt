package fe.materialplayer.controller.css.annotation.property

@Target(AnnotationTarget.FIELD)
@Retention(AnnotationRetention.RUNTIME)
@MustBeDocumented
annotation class IntCSSProperties(vararg val properties: IntCSSProperty)