package fe.materialplayer.util.directory

import fe.logger.Logger
import fe.materialplayer.controller.libchanges.LibraryChangesIndexTask
import fe.materialplayer.controller.util.ItemFetcher
import fe.materialplayer.data.indexable.Song
import fe.materialplayer.music.MusicLibrary
import fe.materialplayer.music.remover.LibrarySongRemover
import fe.materialplayer.util.resource.ResourceBundleHelper
import fe.materialplayer.util.SnackbarHelper
import fe.materialplayer.util.Util
import fe.materialplayer.util.partitionMap
import fe.materialplayer.util.properties.Properties
import fe.materialplayer.util.threading.StoppableRunnable
import javafx.application.Platform
import java.io.File
import java.nio.file.*
import java.util.*
import java.util.concurrent.TimeUnit
import java.util.stream.Collectors

class DirectoryListener(dir: File) : StoppableRunnable() {
    private val library = MusicLibrary.getInstance()
    private val properties = Properties.getInstance()

    private val logger = Logger("DirectoryListener")

    private val watcher = FileSystems.getDefault().newWatchService()
    private val rootPath: Path = dir.toPath()

    val dirs = (if (properties.getBooleanProperty("musicDirRecursive")) {
        Files.walk(rootPath).filter { Files.isDirectory(it) }.collect(Collectors.toList())
    } else listOf(rootPath)).also { paths ->
        paths.forEach { p ->
            logger.print(Logger.Type.INFO, "Registering fs listener on $p")
            p.register(this.watcher, StandardWatchEventKinds.ENTRY_DELETE, StandardWatchEventKinds.ENTRY_CREATE, StandardWatchEventKinds.ENTRY_MODIFY)
        }
    }

    private fun resolve(filename: Path, kind: WatchEvent.Kind<out Any>): Path? {
        dirs.forEach { dir ->
            with(dir.resolve(filename)) {
                when (kind) {
                    StandardWatchEventKinds.ENTRY_CREATE -> {
                        if (Files.exists(this)) {
                            return this
                        }
                    }

                    StandardWatchEventKinds.ENTRY_DELETE -> {
                        library.songDao.findSongByPath(this)?.let {
                            return this
                        }
                    }
                }
            }
        }

        return null
    }

    private val eventQueue = LinkedList<DirectoryEvent>()
    private val THRESHOLD = 3 * 1000L

    private var lastWatchEvent = 0L

    override fun run() {
        while (this.running) {
            try {
                with(this.watcher.poll(THRESHOLD, TimeUnit.MILLISECONDS)) {
                    if ((lastWatchEvent != 0L) && System.currentTimeMillis() > (lastWatchEvent + THRESHOLD)) {
                        //copy list so it wont get mutated in further change events while we work with the current queue
                        processQueue(eventQueue.toList().also {
                            eventQueue.clear()
                        })

                        lastWatchEvent = 0L
                    }

                    this?.let {
                        logger.print(Logger.Type.INFO, "Polling key")

                        for (event in this.pollEvents()) {
                            val kind = event.kind()
                            logger.print(Logger.Type.INFO, "Kind: $kind")
                            if (kind != StandardWatchEventKinds.OVERFLOW) {
                                val filename = event.context() as Path
                                resolve(filename, kind).also { logger.print(Logger.Type.INFO, "Resolved path to $it") }?.let { child ->
                                    val fileName = child.fileName.toString()
                                    if (!fileName.contains("tmp")) {
                                        if ((kind == StandardWatchEventKinds.ENTRY_CREATE && Util.isMp3File(child.toFile())) || kind == StandardWatchEventKinds.ENTRY_DELETE) {
                                            eventQueue.push(DirectoryEvent(kind, child))
                                            lastWatchEvent = System.currentTimeMillis()
                                        }
                                    }
                                }
                            }

                            if (!this.reset()) {
                                break
                            }
                        }
                    }
                }
            } catch (e: InterruptedException) {
                return
            }
        }
    }

    private fun processQueue(eventQueue: List<DirectoryEvent>) {
        logger.print(Logger.Type.INFO, "Processing queue with ${eventQueue.size} items")

        val (createdList, deletedList) = eventQueue.partitionMap({ it.file }, { it.eventKind == StandardWatchEventKinds.ENTRY_CREATE })

        val resourceBundle = ResourceBundleHelper.getBundle()
        if (createdList.isNotEmpty()) {
            val libraryChangesIndexTask = LibraryChangesIndexTask(resourceBundle, createdList)
            libraryChangesIndexTask.setOnSucceeded {
                ItemFetcher.refetchAll()

                displayTextBar(resourceBundle, libraryChangesIndexTask.get(), Pair("songAdded", "songCountAdded"))
            }

            Thread(libraryChangesIndexTask).start()
        }

        with(deletedList.mapNotNull { this.library.songDao.findSongByFile(it) }) {
            if (this.isNotEmpty()) {
                LibrarySongRemover().removeSongs(this)
                ItemFetcher.refetchAll()

                displayTextBar(resourceBundle, this, Pair("songRemoved", "songCountRemoved"))
            }
        }
    }

    private fun displayTextBar(resourceBundle: ResourceBundle, list: List<Song>, pair: Pair<String, String>) {
        Platform.runLater { SnackbarHelper.createMinWidthTextBar(formatString(resourceBundle, list, pair), if(list.size == 1) 700.0 else 300.0) }
    }

    private fun formatString(resourceBundle: ResourceBundle, list: List<Song>, keys: Pair<String, String>): String {
        return if (list.size == 1) {
            resourceBundle.getString(keys.first).format(list[0].toString())
        } else resourceBundle.getString(keys.second).format(list.size)
    }
}

data class DirectoryEvent(val eventKind: WatchEvent.Kind<out Any>, val child: Path) {
    val file: File = child.toFile()
}