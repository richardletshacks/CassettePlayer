package fe.materialplayer.controller.util.popup.base

import fe.materialplayer.controller.util.dialog.DialogUtil
import fe.materialplayer.controller.util.popup.PopupState
import fe.materialplayer.controller.util.popup.PopupUtil
import fe.materialplayer.data.indexable.base.Indexable
import fe.materialplayer.music.MusicPlayer
import javafx.event.EventHandler
import java.util.*

abstract class BasicPlayPopup<T : Indexable>(state: PopupState<T>,
                                             val resources: ResourceBundle) : Popup<T>(state) {
    private val player: MusicPlayer = MusicPlayer.getInstance()

    init {
        this.createBasicPlayPopup()
    }

    open fun createPlayNext(): BasicPlayPopup<T> {
        val btn = PopupUtil.createPopupButton(this.resources.getString("playNext"), "play")
        btn.onMouseClicked = EventHandler {
            this.player.addNextSongs(this.state.getSongs(), false)
            this.popup?.hide()
        }

        this.addItem(btn)
        return this
    }

    open fun createAddToPlayingQueue(): BasicPlayPopup<T> {
        val btn = PopupUtil.createPopupButton(this.resources.getString("addToPlayingQueue"), "add_to_playing_queue")
        btn.onMouseClicked = EventHandler {
            this.player.addSongs(this.state.getSongs(), false)
            this.popup?.hide()
        }

        this.addItem(btn)
        return this
    }

    open fun createAddToPlaylist(): BasicPlayPopup<T> {
        val btn = PopupUtil.createPopupButton(this.resources.getString("addToPlaylistDotted"), "playlist_add")
        btn.onMouseClicked = EventHandler {
            DialogUtil.createAddToPlaylistDialog(this.state.currentState, this.popup)
        }

        this.addItem(btn)
        return this
    }

    private fun createBasicPlayPopup(): BasicPlayPopup<T> = this.createPlayNext()
            .createAddToPlayingQueue()
            .createAddToPlaylist()
}