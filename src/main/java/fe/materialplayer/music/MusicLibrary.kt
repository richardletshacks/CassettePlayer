package fe.materialplayer.music

import com.j256.ormlite.jdbc.JdbcConnectionSource
import com.j256.ormlite.stmt.DeleteBuilder
import com.j256.ormlite.table.TableUtils
import fe.logger.Logger
import fe.materialplayer.data.dao.base.SetupDao
import fe.materialplayer.data.dao.helper.*
import fe.materialplayer.data.indexable.*
import fe.materialplayer.data.indexable.nm.PlaylistSong
import fe.materialplayer.util.SingletonHolder
import fe.materialplayer.util.Util
import fe.materialplayer.util.properties.Properties
import java.io.File
import java.sql.SQLException
import java.util.*
import kotlin.collections.ArrayList

class MusicLibrary private constructor(dir: File) {


    private var dbFile: File = File(dir, "database.sqlite")
    private var connectionSource = JdbcConnectionSource("jdbc:sqlite:" + dbFile.absolutePath)

    var artistCache: File
    var albumCoverCache: File
    var firstRun: Boolean

    val songDao = SongDaoHelper()
    val artistDao = ArtistDaoHelper()
    val albumDao = AlbumDaoHelper()
    val artistAlbumDao = ArtistAlbumDaoHelper()
    val artistSongDao = ArtistSongDaoHelper()
    val playlistDao = PlaylistDaoHelper()
    val playlistSongDao = PlaylistSongDaoHelper()
    val genreDao = GenreDaoHelper()

    /**
     * always make sure these items are in the correct order - loading songs before the corresponding albums/artists/etc fucks up everything
     */
    private val daoHelper = listOf(
            this.artistDao,
            this.albumDao,
            this.genreDao,
            this.songDao,
            this.artistSongDao,
            this.artistAlbumDao,
            this.playlistDao,
            this.playlistSongDao
    )

    private val existingFiles = mutableListOf<File>()
    private val logger = Logger("MusicLibrary")


    init {
        val properties = Properties.getInstance()
        this.firstRun = !properties.getBooleanProperty("libraryScanningDone") || !this.dbFile.exists()

        this.albumCoverCache = File(dir, ".cover_cache")
        this.artistCache = File(dir, ".artist_cache")
        if (!this.artistCache.exists()) {
            this.artistCache.mkdirs()
        }
    }

    fun setup(): MusicLibrary {
        this.daoHelper.forEach {
            if (this.firstRun) {
                TableUtils.createTableIfNotExists(connectionSource, it.clazz)
            }

            it.makeDao(connectionSource)
        }

        if (!this.firstRun) {
            this.queryData()
        }

        return this
    }

    fun closeConnection() {
        this.connectionSource.close()
    }

    fun scanMusicDir(musicDir: File, recursive: Boolean) {
        this.existingFiles.addAll(Util.scanSubDir(musicDir, recursive))
    }

    fun checkForNewSongs(): List<File> {
        val files: MutableList<File> = ArrayList()
        for (f in this.existingFiles) {
            val s: Song? = this.songDao.findSongByFile(f)
            if (s == null) {
                files.add(f)
            }
        }

        return files
    }

    @Throws(SQLException::class)
    private fun queryData() {
        for (dao in this.daoHelper) {
            val queryResult = dao.queryAll()

            if (!this.firstRun && dao is SetupDao) {
                dao.setup(queryResult)
            }
        }
    }

    @Throws(SQLException::class)
    fun insertAndRequery() {
        for (dao in this.daoHelper) {
            dao.batchCreateExisting()
        }

        for (dao in this.daoHelper) {
            dao.items.clear()
        }

        this.queryData()
    }

    @Throws(SQLException::class)
    fun setupNMFirstRun(songs: List<Song>) {
        //all other daos have their data added in the "songfactory", but the actual songs are added here
        this.songDao.addToList(songs)

        //TODO: maybe optimize this? do we actually have two instances of every item by doing this or are they cached????
        val albums: List<Album> = LinkedList(this.albumDao.items)

        logger.print(Logger.Type.INFO, "Calling insertAndRequery")
        this.insertAndRequery()

        logger.print(Logger.Type.INFO, "Setting up artistsongs")
        this.artistSongDao.batchCreateAndSetup(songs)

        logger.print(Logger.Type.INFO, "Setting up artistalbums")
        this.artistAlbumDao.batchCreateAndSetup(albums, songs)
    }

    @Throws(SQLException::class)
    fun deletePlaylist(playlist: Playlist) {
        this.playlistDao.items.remove(playlist)
        val deleteBuilder: DeleteBuilder<PlaylistSong, Int> = this.playlistSongDao.deleteBuilder()
        deleteBuilder.where().eq(PlaylistSong.PLAYLIST_ID_COLUMN_NAME, playlist.id)
        deleteBuilder.delete()

        this.playlistDao.delete(playlist)
    }

    companion object : SingletonHolder<MusicLibrary, File>(::MusicLibrary)
}